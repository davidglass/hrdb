﻿CREATE TABLE [pdf].[WGSKeyActivities] (
    [Recid]       INT            NOT NULL,
    [WGSRecid]    INT            NOT NULL,
    [SequenceID]  INT            NULL,
    [Percentage]  DECIMAL (18)   NULL,
    [Description] VARCHAR (6000) NULL
);

