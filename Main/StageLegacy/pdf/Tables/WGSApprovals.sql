﻿CREATE TABLE [pdf].[WGSApprovals] (
    [Recid]                 INT          NOT NULL,
    [WGSRecid]              INT          NOT NULL,
    [ApproverName]          VARCHAR (60) NULL,
    [JobTitle]              VARCHAR (50) NULL,
    [ApproverLanId]         CHAR (12)    NULL,
    [ApproverSeqNum]        CHAR (2)     NULL,
    [ApproveDisapprove]     CHAR (1)     NULL,
    [ApproveDisapproveDate] DATETIME     NULL,
    [ReviewedForLanId]      CHAR (12)    NULL
);

