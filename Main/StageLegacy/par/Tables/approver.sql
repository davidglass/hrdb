﻿CREATE TABLE [par].[approver] (
    [approverid]     INT           NOT NULL,
    [parID]          INT           NOT NULL,
    [seqNum]         INT           NOT NULL,
    [personID]       INT           NOT NULL,
    [approvername]   VARCHAR (25)  NULL,
    [dateApproved]   DATETIME      NULL,
    [approvedyn]     CHAR (1)      NULL,
    [comments]       VARCHAR (250) NULL,
    [approvedbyid]   INT           NULL,
    [approvedbyname] VARCHAR (25)  NULL
);

