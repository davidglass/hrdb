﻿CREATE TABLE [par].[ParTypes] (
    [ParTypeId] INT          NOT NULL,
    [ParType]   VARCHAR (50) NULL,
    [Sequence]  CHAR (2)     NULL,
    CONSTRAINT [PK_ParTypes] PRIMARY KEY CLUSTERED ([ParTypeId] ASC)
);

