﻿CREATE TABLE [flex].[FlexWorkWeek](
	[ID] [int] NOT NULL,
	[FlexID] [char](10) NOT NULL,
	[PositionNum] [varchar](4) NULL,
	[SubPosNum] [int] NULL,
	[BeginDay] [varchar](9) NULL,
	[BeginHour] [varchar](2) NULL,
	[BeginMinute] [varchar](2) NULL,
	[BeginAMPM] [varchar](8) NULL,
	[EndDay] [varchar](9) NULL,
	[EndHour] [varchar](2) NULL,
	[EndMinute] [varchar](2) NULL,
	[EndAMPM] [varchar](8) NULL,
	[Week1] [varchar](60) NULL,
	[Week2] [varchar](60) NULL,
	[WorkSchedule] [varchar](200) NULL,
	[WorkDays] [varchar](60) NULL,
	[Shift] [varchar](15) NULL,
	[HRUpdateBy] [varchar](12) NULL,
	[HRUpdateDate] [datetime] NULL,
	[WorkWeek] [varchar](200) NULL,
	[HRMSWorkScheduleCode] [varchar](4) NULL,
	[UpdatedToHRAdmin] [char](1) NULL
) ON [PRIMARY]

