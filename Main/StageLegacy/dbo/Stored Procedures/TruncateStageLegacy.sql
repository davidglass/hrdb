﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TruncateStageLegacy]

AS
BEGIN
TRUNCATE TABLE [par].[Appointment];
TRUNCATE TABLE [par].[Approver];
TRUNCATE TABLE [par].[ParData];
TRUNCATE TABLE [par].[ParTypes];
TRUNCATE TABLE [organization].[Class_Codes]; 
TRUNCATE TABLE [organization].[DesignatedWorkWeek]; 
TRUNCATE TABLE [organization].[Employee];
TRUNCATE TABLE [organization].[Employee_History]; 
TRUNCATE TABLE [organization].[Positions];
TRUNCATE TABLE [organization].[CostCenter];
TRUNCATE TABLE [organization].[WACounty];
TRUNCATE TABLE [dishrms].[WorkSchedule];
TRUNCATE TABLE [flex].[Flex];
TRUNCATE TABLE [flex].[FlexWorkWeek];
END