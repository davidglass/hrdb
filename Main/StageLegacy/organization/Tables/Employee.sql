﻿CREATE TABLE [organization].[Employee] (
    [PersonId]            INT           NOT NULL,
    [DOPPersonId]         CHAR (9)      NULL,
    [Lanid]               VARCHAR (50)  NULL,
    [FullName]            VARCHAR (30)  NULL,
    [LastName]            VARCHAR (30)  NULL,
    [FirstName]           VARCHAR (30)  NULL,
    [MiddleInitial]       VARCHAR (15)  NULL,
    [SSN]                 VARCHAR (15)  NULL,
    [AnniversaryDate]     CHAR (15)     NULL,
    [PositionNum]         VARCHAR (4)   NULL,
    [subposnum]           VARCHAR (1)   NULL,
    [ClassCode]           VARCHAR (5)   NULL,
    [PayRange]            VARCHAR (2)   NULL,
    [Step]                VARCHAR (5)   NULL,
    [Salary]              NUMERIC (18)  NULL,
    [IncrementDate]       VARCHAR (25)  NULL,
    [PayRollName]         VARCHAR (50)  NULL,
    [IntrainingIndicator] CHAR (1)      NULL,
    [Comments]            VARCHAR (250) NULL,
    [Updated_by]          VARCHAR (20)  NULL,
    [Updated_date]        DATETIME      NULL,
    [Updated_ipaddr]      VARCHAR (20)  NULL,
    [DISHireDate]         DATETIME      NULL,
    [PayrollGroup]        CHAR (1)      NULL,
    [HRGroup]             CHAR (1)      NULL,
    [AdminGroup]          CHAR (1)      NULL,
    [HelpDesk]            CHAR (1)      NULL,
    [ErgoTraining]        DATETIME      NULL,
    CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([PersonId] ASC)
);



