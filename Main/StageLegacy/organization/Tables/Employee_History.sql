﻿CREATE TABLE [organization].[Employee_History] (
    [ID]                  INT           NOT NULL,
    [PersonId]            INT           NOT NULL,
    [DOPPersonId]         CHAR (9)      NULL,
    [Lanid]               CHAR (12)     NULL,
    [FullName]            VARCHAR (30)  NULL,
    [LastName]            VARCHAR (30)  NULL,
    [FirstName]           VARCHAR (30)  NULL,
    [MiddleInitial]       VARCHAR (15)  NULL,
    [SSN]                 VARCHAR (15)  CONSTRAINT [DF_Employee_History_SSN] DEFAULT ((999999999)) NULL,
    [AnniversaryDate]     CHAR (15)     NULL,
    [PositionNum]         VARCHAR (4)   NULL,
    [subposnum]           VARCHAR (1)   NULL,
    [ClassCode]           VARCHAR (5)   NULL,
    [PayRange]            VARCHAR (2)   NULL,
    [Step]                VARCHAR (5)   NULL,
    [Salary]              NUMERIC (18)  NULL,
    [IncrementDate]       VARCHAR (25)  NULL,
    [PayRollName]         VARCHAR (50)  NULL,
    [IntrainingIndicator] CHAR (1)      NULL,
    [ErgoTraining]        DATETIME      NULL,
    [Comments]            VARCHAR (250) NULL,
    [Updated_by]          VARCHAR (20)  NULL,
    [Updated_date]        DATETIME      NULL,
    [Updated_ipaddr]      VARCHAR (20)  NULL,
    [DISHireDate]         DATETIME      NULL,
    [PositionClassCode]   CHAR (5)      NULL,
    [In_training]         CHAR (1)      NULL,
    [CostCenter]          CHAR (4)      NULL,
    [WorkDays]            VARCHAR (60)  NULL,
    [Shift]               VARCHAR (15)  NULL,
    [WorkPhone]           VARCHAR (15)  NULL,
    [MailStop]            VARCHAR (50)  NULL,
    [AttendUnit]          VARCHAR (2)   NULL,
    [Supervisor]          INT           NULL,
    [WorkPeriod]          VARCHAR (15)  NULL,
    [AppointingIndicator] CHAR (1)      NULL,
    [ManagerIndicator]    CHAR (1)      NULL,
    [SupervisorIndicator] CHAR (1)      NULL,
    [UnitId]              INT           NULL,
    [LastUpdatedBy]       CHAR (20)     NULL,
    [LastUpdatedDate]     DATETIME      NULL,
    [Selective]           CHAR (1)      NULL,
    [CBA]                 CHAR (1)      NULL,
    [Unit]                VARCHAR (50)  NULL,
    [Section]             VARCHAR (50)  NULL,
    [Division]            VARCHAR (50)  NULL,
    [Tableupdated]        CHAR (3)      NULL,
    [Jobtitle]            VARCHAR (45)  NULL,
    [SupervisorName]      VARCHAR (30)  NULL,
    [ParID]               INT           NULL,
    [intermittent]        CHAR (1)      NULL,
    [project]             CHAR (1)      NULL,
    [seasonal]            CHAR (1)      NULL,
    [jvac]                CHAR (3)      NULL,
    [pointvalue]          CHAR (4)      NULL,
    [cautionzone]         CHAR (1)      NULL,
    [HistoryUpdateDate]   DATETIME      NULL,
    [section4]            CHAR (1)      NULL,
    [workerscomp]         VARCHAR (4)   NULL,
    [nonpermanent]        CHAR (1)      NULL,
    CONSTRAINT [PK_Employee_History] PRIMARY KEY CLUSTERED ([ID] ASC)
);








GO
CREATE NONCLUSTERED INDEX [IX_SupervisorPersonId]
    ON [organization].[Employee_History]([Supervisor] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PositionNum]
    ON [organization].[Employee_History]([PositionNum] ASC);

