﻿CREATE TABLE [organization].[WACounty]
(
	[CountyID] CHAR(2) NOT NULL PRIMARY KEY, 
    [CountyName] VARCHAR(25) NULL
)
