﻿CREATE TABLE [organization].[Class_Codes] (
    [ClassCode] VARCHAR (5)  NOT NULL,
    [JobTitle]  VARCHAR (45) NULL,
    [active]    CHAR (1)     NULL,
    CONSTRAINT [PK_Class_Codes] PRIMARY KEY CLUSTERED ([ClassCode] ASC)
);



