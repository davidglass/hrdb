﻿CREATE TABLE [organization].[DesignatedWorkWeek] (
    [PositionNum]        VARCHAR (4)   NOT NULL,
    [DesignatedWorkWeek] VARCHAR (200) NULL,
    [BeginDay]           VARCHAR (9)   NULL,
    [BeginHour]          VARCHAR (2)   NULL,
    [BeginMinute]        VARCHAR (2)   NULL,
    [BeginAMPM]          VARCHAR (8)   NULL,
    [EndDay]             VARCHAR (9)   NULL,
    [EndHour]            VARCHAR (2)   NULL,
    [EndMinute]          VARCHAR (2)   NULL,
    [EndAMPM]            VARCHAR (8)   NULL,
    CONSTRAINT [PK_DesignatedWorkWeek] PRIMARY KEY CLUSTERED ([PositionNum] ASC)
);

