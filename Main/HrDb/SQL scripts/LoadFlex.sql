
insert into EmpFlexSched
select f.flexid, f.empid,
case when week1 = '' then workschedule else week1 end as week1,
case when week2 = '' then null else week2 end as week2
from legacy.flexworkweek ww
join legacy.flex f
on ww.flexid = f.flexid
and f.flexid = (
	select max(flexid) from legacy.flex
	where empid = f.empid
	and status = 'Completed'
)

update a
set WorkHoursWk1 = efs.Week1,
WorkHoursWk2 = efs.Week2
from Appointment a
join EmpFlexSched efs
on a.EmployeeId = efs.EmpId
and (a.EndDate is null or a.EndDate > getdate());

