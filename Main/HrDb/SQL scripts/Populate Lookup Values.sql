﻿insert into approle (id, name)
values (1, 'Admin'),
(2, 'HR'),
(3, 'Supervisor'),
(4, 'Non-Supervisor'),
(5, 'OfficeSupport');

insert into approvalapp
(name, isactive)
values('PAR',1), ('PDF',1), ('TimeSheet',1)


insert  into approvalcode
(id, name, isactive)
values(7, 'AppointingAuthority', 1), (8, 'FirstApproval', 1), (9, 'AppointingWithReview', 1)


insert into approvalstatuscode
(name, isactive)
values('Active', 1), ('Canceled', 1), ('Approved', 1), ('Rejected', 1), ('Created', 1)

insert into approvaltaskcode
(name, isactive)
values('MoveFwdRegardless', 1), ('Standard', 1), ('SignOff', 1)

insert into EmployeeGroupLkUp
(EmployeeGroupCode,	[Description])
values ('0', 'Permanent'), ('6', 'NonPerm On Call'), ('9', 'Appointed'), ('B', 'Civil Service Exempt'), ('C', 'Project'), ('H', 'NonPerm Ltd')

insert into EmployeeSubGroupLkUp
(EmployeeSubGroupCode, [Description])
values ('00', 'Hourly (H) OT Exempt'), ('01', 'Monthly(M) OT Exempt'), ('02', 'Daily OT Exempt'), ('03', 'H-OT Elig>Daily Sche'), 
('04', 'M-OT Elig>Daily Sche'), ('05', 'H-OT Elig>40hrs/wk'), ('06', 'M-OT Elig>40hrs/wk'), ('09', 'H-OT Elig Law En>160'), 
('10', 'M-OT Elig Law En>160'), ('11', 'H-OT Elig Law En>171'), ('12', 'M-OT Elig Law En>171'), ('13', 'H-OT Elig>8hrs/wkday'),
('14', 'M-OT Elig>8hrs/wkday'), ('15', 'H-OT Elig Nrs>80h/2w'), ('16', 'M-OT Elig Nrs>80h/2w'), ('17', 'H-OT Elig>Shift'), ('18', 'M-OT Elig>Shift'),
('19', 'H-OT Elig>Sched WSF'), ('20', 'M-OT Elig>Sched WSF')


insert into PayScaleGroupCode
(Code, [Description])
values 
('01', NULL), ('31', NULL), ('33', NULL), ('35', NULL), ('43', NULL), ('46', NULL), ('50', NULL), ('54', NULL), ('55', NULL), ('56', NULL),
('57', NULL), ('58', NULL), ('60', NULL), ('62', NULL), ('66', NULL), ('70', NULL), ('BAND 1', NULL), ('BAND 2', NULL), ('BAND 3', NULL),
('BAND 4', NULL), ('BAND 5', NULL)

insert into PayScaleLevelCode
(Code, [Description])
values
('01', NULL), ('02', NULL), ('03', NULL), ('04', NULL), ('05', NULL), ('A', NULL), ('B', NULL), ('C', NULL), ('D', NULL), ('E', NULL), 
('F', NULL), ('G', NULL), ('H', NULL), ('I', NULL), ('J', NULL), ('K', NULL), ('L', NULL), ('M', NULL)

insert into PayScaleReason
(Code, [Description])
values
('01', 'Periodic Increment'), ('02', 'Rng Inc Due to Director Action'), ('03', 'Rdctn in slry w/in rng Discipl'), ('04', 'Return from Red in Salary'),  ('05', 'Salary Adjmnt by Leg Action'), ('06', 'W Rate (WMS Only)'), ('07', 'Y Rate (Non WMS Only)'), ('08', 'WMS Raise - Int Slry Rel Prob'), 
('09', 'WMS Raise - Grwth & Developmen'), ('10', 'WMS Raise - Rec&Retention Prob'), ('11', 'Employee Put on Disability Pay'),
('12', 'Employee Taken off Dis Pay'), ('13', 'Employee Put on Special Pay'), ('14', 'Employee Taken Off Special Pay'), ('15', 'Range Adjust-Multiple Level'), ('16', 'Salary Adjust-Higher Education'), ('17', 'Salary Adjust-Inversion/Comp'), ('18', 'Sal. Adjust-Recruit/Retention'), ('19', 'Initial Appointment'), ('20', 'Appointment Change'),      
('21', 'Salary adjmnt by Appting Auth'), ('22', 'Pay Adjust Align, Dev Train'), ('23', 'WMS Extraordinary Resp Comp'), ('24', 'Removal  WMS Extraord Resp Cmp'), ('25', 'Non-Employee'), ('26', 'Longevity')

insert into PayScaleTypeArea
(Id, PsType, PsArea, [Description])
values 
(1, '01', '01', 'WFSE - Standard Progression'), (2, '00', '01', 'NonRep - Standard Progression'), (3, '00', '02', 'NonRep - WMS'), (4,	'00', '40',	'NonRep - Non-Payscale'),
(5,	'00', '41',	'NonRep - Exempt')

insert into PersonnelSubArea
(Code, [Description])
values
('0001', 'Non Represented'), ('0002', 'WMS'), ('0003', 'Exempt'), ('00GW', 'St Inf/ConfCall'), ('00JZ', 'Agencywide')

insert into WorkContractType
(WorkContractTypeCode, WorkContractTypeDesc)
values
(' ', 'Permanent'), ('00', 'In Training'), ('02', 'Probation'), ('03', 'Trial Service'), ('04', 'In Training/Probation'), ('05', 'In Training/Trial Service'), ('09', 'Acting'), ('10', 'Transitional'), ('11', 'Transitional/In Training'), ('12', 'Transitional/Non-Permanent'), ('14', 'NonPerm On call'), ('15', 'NonPerm Limited'), ('16', 'Exempt'), ('20', 'Project'), ('21', 'Project/Trial Service'), ('22', 'Project/Probation'), ('23', 'WMS Review')

insert into WorkersCompCode
(Code, [Description])
values
('0000', 'None assigned'), ('4902', 'Clerical/Admin'), ('5300', 'Admin Field Staff'), ('5307', 'Warehouse'), ('6901', 'Volunteers')

INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (1, N'Adams Bldg. - 1st floor', N'Adams Bldg.')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (2, N'Adams Bldg. - 2nd floor', N'Adams Bldg.')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (3, N'Jefferson Bldg. - 1st floor', N'Jefferson Bldg.')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (4, N'Jefferson Bldg. - 2nd floor', N'Jefferson Bldg.')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (5, N'512 Bldg. - 1st floor', N'512 Bldg.')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (6, N'512 Bldg. - 2nd floor', N'512 Bldg.')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (7, N'Forum Bldg. - 1st floor', N'Forum Bldg.')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (8, N'Forum Bldg. - 2nd floor', N'Forum Bldg.')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (9, N'512 Bldg. - 3rd floor', N'512 Bldg.')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (10, N'Chandler Court', N'Chandler Court')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (11, N'Supply Warehouse', N'Supply Warehouse')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (12, N'OB2', N'OB2')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (13, N'Lacey IT', N'Lacey IT')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (14, N'Lacey Node Site', N'Lacey Node Site')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (15, N'Seattle Node Site', N'Seattle Node Site')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (16, N'Vancouver Node Site', N'Vancouver Node Site')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (17, N'Yakima Node Site', N'Yakima Node Site')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (18, N'Spokane Node Site', N'Spokane Node Site')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (20, N'Republic Bldg.', N'Republic Bldg.')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (21, N'1500 Office Building North 1st Fl', N'1500 Office Building North 1st Fl')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (22, N'1500 Office Building North 2nd Fl', N'1500 Office Building North 2nd Fl')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (23, N'1500 Office Building North 3rd Fl', N'1500 Office Building North 3rd Fl')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (24, N'1500 Office Building North 4th Fl', N'1500 Office Building North 4th Fl')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (25, N'1500 Office Building North 5th Fl', N'1500 Office Building North 5th Fl')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (26, N'1500 Office Building North 6th Fl', N'1500 Office Building North 6th Fl')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (27, N'1500 Office Building South 1st Fl', N'1500 Office Building South 1st Fl')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (28, N'1500 Office Building South 2nd Fl', N'1500 Office Building South 2nd Fl')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (29, N'1500 Office Building South 3rd Fl', N'1500 Office Building South 3rd Fl')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (30, N'State Data Center Floor 1', N'State Data Center Floor 1')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (31, N'State Data Center Floor 2', N'State Data Center Floor 2')
GO
INSERT [legacy].[Location] ([ID], [Location], [Building]) VALUES (34, N'State Data Center Basement', N'State Data Center Basement')
GO

INSERT INTO [dbo].[ParStatus]
           ([Id]
           ,[Description])
     VALUES
           (1, 'Created'), (2, 'Pending Approval'), (3, 'Approved'), (4, 'Processed' ), (5, 'Pushed'), (6, 'Reconciled'), (7, 'Rejected'), (8, 'Deleted')


INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0110', N'House of Representatives (REP)', N'House of Representatives (REP)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0120', N'Senate (SEN)', N'Senate (SEN)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0130', N'Joint Transportation Committee (JTC)', N'Joint Transportation Committee (JTC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0140', N'Joint Legislative Audit and Review Committee (JLARC)', N'Joint Legislative Audit and Review Committee (JLARC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0200', N'Legislative Evaluation and Accountability Program Committee (LEAP)', N'Legislative Evaluation and Accountability Program Committee (LEAP)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0350', N'Office of the State Actuary (OSA)', N'State Actuary (OSA)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0370', N'Office of Legislative Support Services (LSS)', N'Legislative Support Services (LSS)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0380', N'Joint Legislative Systems Committee (JLS)', N'Joint Legislative Systems Committee (JLS)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0400', N'Statute Law Committee (SLC)', N'Statute Law Committee (SLC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0450', N'Supreme Court (SUP)', N'Supreme Court (SUP)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0460', N'State Law Library (LAW)', N'Law Library (LAW)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0480', N'Court of Appeals (COA)', N'Court of Appeals (COA)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0500', N'Commission on Judicial Conduct (CJC)', N'Commission on Judicial Conduct (CJC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0550', N'Administrative Office of the Courts (AOC)', N'Administrative Office of the Courts (AOC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0560', N'Office of Public Defense (OPD)', N'Public Defense (OPD)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0570', N'Office of Civil Legal Aid (OCLA)', N'Civil Legal Aid (OCLA)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0750', N'Office of the Governor (GOV)', N'Governor (GOV)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0800', N'Office of the Lieutenant Governor (LTG)', N'Lieutenant Governor (LTG)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0820', N'Public Disclosure Commission (PDC)', N'Public Disclosure Commission (PDC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0850', N'Office of the Secretary of State (SEC)', N'Secretary of State (SEC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0860', N'Governor''s Office of Indian Affairs (INA)', N'Governor''s Office of Indian Affairs (INA)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0870', N'Washington State Commission on Asian Pacific American Affairs (APA)', N'Commission on Asian Pacific American Affairs (APA)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0900', N'Office of the State Treasurer (OST)', N'State Treasurer (OST)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0910', N'Redistricting Commission (RDC)', N'Redistricting Commission (RDC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0950', N'Office of the State Auditor (SAO)', N'State Auditor (SAO)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'0990', N'Washington Citizens'' Commission on Salaries for Elected Officials (COS)', N'Citizens'' Commission on Salaries for Elected Officials (COS)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1000', N'Office of the Attorney General (ATG)', N'Attorney General (ATG)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1010', N'Caseload Forecast Council (CFC)', N'Caseload Forecast Council (CFC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1020', N'Department of Financial Institutions (DFI)', N'Financial Institutions (DFI)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1030', N'Department of Commerce (COM)', N'Commerce (COM)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1040', N'Economic and Revenue Forecast Council (ERFC)', N'Economic and Revenue Forecast Council (ERFC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1050', N'Office of Financial Management (OFM)', N'Financial Management (OFM)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1060', N'Washington Economic Development Finance Authority (EDA)', N'Economic Development Finance Authority (EDA)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1070', N'State Health Care Authority (HCA)', N'Health Care Authority (HCA)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1100', N'Office of Administrative Hearings (OAH)', N'Administrative Hearings (OAH)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1160', N'State Lottery Commission (LOT)', N'Lottery Commission (LOT)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1170', N'Washington State Gambling Commission (GMB)', N'Gambling Commission (GMB)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1180', N'Washington State Commission on Hispanic Affairs (CHA)', N'Commission on Hispanic Affairs (CHA)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1190', N'Washington State Commission on African-American Affairs (CAA)', N'Commission on African-American Affairs (CAA)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1200', N'Human Rights Commission (HUM)', N'Human Rights Commission (HUM)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1240', N'Department of Retirement Systems (DRS)', N'Retirement Systems (DRS)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1260', N'State Investment Board (SIB)', N'Investment Board (SIB)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1350', N'Innovate Washington (IWA)', N'Innovate Washington (IWA)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1400', N'Department of Revenue (DOR)', N'Revenue (DOR)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1420', N'Board of Tax Appeals (BTA)', N'Board of Tax Appeals (BTA)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1470', N'Office of Minority and Women''s Business Enterprises (OMWBE)', N'Minority and Women''s Business Enterprises (OMWBE)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1480', N'Washington State Housing Finance Commission (HFC)', N'Housing Finance Commission (HFC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1600', N'Office of the Insurance Commissioner (INS)', N'Insurance Commissioner (INS)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1630', N'Consolidated Technology Services (CTS)', N'Consolidated Technology Services (CTS)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1650', N'State Board of Accountancy (ACB)', N'Board of Accountancy (ACB)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1670', N'Forensic Investigation Council (FIC)', N'Forensic Investigation Council (FIC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1790', N'Department of Enterprise Services (DES)', N'Enterprise Services (DES)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1850', N'Washington Horse Racing Commission (HRC)', N'Horse Racing Commission (HRC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1900', N'Board of Industrial Insurance Appeals (IND)', N'Board of Industrial Insurance Appeals (IND)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'1950', N'Liquor Control Board (LCB)', N'Liquor Control Board (LCB)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'2050', N'Board of Pilotage Commissioners (BPC)', N'Board of Pilotage Commissioners (BPC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'2150', N'Utilities and Transportation Commission (UTC)', N'Utilities and Transportation Commission (UTC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'2200', N'Board for Volunteer Firefighters and Reserve Officers (BVFFRO)', N'Board for Volunteer Firefighters and Reserve Officers (BVFFRO)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'2250', N'Washington State Patrol (WSP)', N'State Patrol (WSP)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'2270', N'Washington State Criminal Justice Training Commission (CJT)', N'Criminal Justice Training Commission (CJT)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'2280', N'Washington Traffic Safety Commission (STS)', N'Traffic Safety Commission (STS)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'2350', N'Department of Labor and Industries (L&I)', N'Labor and Industries (L&I)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'2400', N'Department of Licensing (DOL)', N'Licensing (DOL)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'2450', N'Military Department (MIL)', N'Military Department (MIL)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'2750', N'Public Employment Relations Commission (PERC)', N'Public Employment Relations Commission (PERC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3000', N'Department of Social and Health Services (DSHS)', N'Social and Health Services (DSHS)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3030', N'Department of Health (DOH)', N'Health (DOH)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3040', N'Tobacco Settlement Authority (TOB)', N'Tobacco Settlement Authority (TOB)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3050', N'Department of Veterans'' Affairs (DVA)', N'Veterans'' Affairs (DVA)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3100', N'Department of Corrections (DOC)', N'Corrections (DOC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3150', N'Department of Services for the Blind (DSB)', N'Services for the Blind (DSB)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3400', N'Student Achievement Council (SAC)', N'Student Achievement Council (SAC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3410', N'Law Enforcement Officers’ and Fire Fighters’ Plan 2 Retirement Board (LEOFF)', N'Law Enforcement Officers’ and Fire Fighters’ Plan 2 Retirement Board (LEOFF)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3460', N'Washington Higher Education Facilities Authority (WHEFA)', N'Higher Education Facilities Authority (WHEFA)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3500', N'Superintendent of Public Instruction (SPI)', N'Superintendent of Public Instruction (SPI)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3510', N'State School for the Blind (SFB)', N'School for the Blind (SFB)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3520', N'State Board for Community and Technical Colleges (SBCTC)', N'Board for Community and Technical Colleges (SBCTC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3530', N'Washington State Center for Childhood Deafness and Hearing Loss (CDHL)', N'Center for Childhood Deafness and Hearing Loss (CDHL)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3540', N'Workforce Training and Education Coordinating Board (WFTECB)', N'Workforce Training and Education Coordinating Board (WFTECB)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3550', N'Department of Archaeology and Historic Preservation (DAHP)', N'Archaeology and Historic Preservation (DAHP)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3560', N'Life Sciences Discovery Fund Authority (LSDFA)', N'Life Sciences Discovery Fund Authority (LSDFA)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3570', N'Department of Early Learning (DEL)', N'Early Learning (DEL)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3590', N'Washington Charter School Commission (WCSC)', N'Charter School Commission (WCSC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3600', N'University of Washington (UW)', N'University of Washington (UW)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3650', N'Washington State University (WSU)', N'Washington State University (WSU)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3700', N'Eastern Washington University (EWU)', N'Eastern Washington University (EWU)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3750', N'Central Washington University (CWU)', N'Central Washington University (CWU)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3760', N'The Evergreen State College (TESC)', N'The Evergreen State College (TESC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3800', N'Western Washington University (WWU)', N'Western Washington University (WWU)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3870', N'Washington State Arts Commission (ART)', N'Arts Commission (ART)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3900', N'Washington State Historical Society (WHS)', N'Historical Society (WHS)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'3950', N'Eastern Washington State Historical Society (EWH)', N'Eastern Washington State Historical Society (EWH)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4050', N'Department of Transportation (DOT)', N'Transportation (DOT)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4060', N'County Road Administration Board (CRAB)', N'County Road Administration Board (CRAB)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4070', N'Transportation Improvement Board (TIB)', N'Transportation Improvement Board (TIB)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4100', N'Transportation Commission (TRC)', N'Transportation Commission (TRC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4110', N'Freight Mobility Strategic Investment Board (FMSIB)', N'Freight Mobility Strategic Investment Board (FMSIB)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4120', N'Washington Materials Management and Financing Authority (WMMFA)', N'Materials Management and Financing Authority (WMMFA)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4600', N'Columbia River Gorge Commission (CRG)', N'Columbia River Gorge Commission (CRG)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4610', N'Department of Ecology (ECY)', N'Ecology (ECY)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4620', N'Washington Pollution Liability Insurance Program (PLI)', N'Pollution Liability Insurance Program (PLI)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4650', N'State Parks and Recreation Commission (PARKS)', N'Parks and Recreation Commission (PARKS)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4670', N'Recreation and Conservation Funding Board (RCFB)', N'Recreation and Conservation Funding Board (RCFB)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4680', N'Environmental and Land Use Hearings Office (ELUHO)', N'Environmental and Land Use Hearings Office (ELUHO)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4710', N'State Conservation Commission (SCC)', N'Conservation Commission (SCC)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4770', N'Department of Fish and Wildlife (DFW)', N'Fish and Wildlife (DFW)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4780', N'Puget Sound Partnership (PSP)', N'Puget Sound Partnership (PSP)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4900', N'Department of Natural Resources (DNR)', N'Natural Resources (DNR)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'4950', N'Department of Agriculture (AGR)', N'Agriculture (AGR)')
GO
INSERT [hrms].[AgencyCode] ([OfmAgencyCode], [AgencyTitle], [AgencyTitleShort]) VALUES (N'5400', N'Employment Security Department (ES)', N'Employment Security Department (ES)')
GO