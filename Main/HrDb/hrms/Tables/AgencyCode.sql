﻿CREATE TABLE [hrms].[AgencyCode] (
    [OfmAgencyCode]    CHAR (4)       NOT NULL,
    [AgencyTitle]      NVARCHAR (255) NULL,
    [AgencyTitleShort] NVARCHAR (255) NULL
);

