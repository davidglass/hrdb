﻿CREATE TABLE [hrms].[EmergencyContact] (
    [PersonId]        INT       NULL,
    [EffectiveDate]   DATE      NULL,
    [EndDate]         DATE      NULL,
    [Member]          CHAR (4)  NULL,
    [LastName]        CHAR (40) NULL,
    [FirstName]       CHAR (40) NULL,
    [TelephoneNumber] CHAR (14) NULL
);

