﻿CREATE TABLE [hrms].[OrgStructure] (
    [OrgUnitDescription] NVARCHAR (255) NULL,
    [OrgUnitId]          INT            NULL,
    [Level]              INT            NULL,
    [ParentOrgUnitId]    INT            NULL
);

