﻿CREATE TABLE [hrms].[Position] (
    [Id]                   INT          NOT NULL,
    [AgencyCode]           CHAR (4)     NULL,
    [PositionTitleLeg]     CHAR (12)    NULL,
    [PositionTitle]        VARCHAR (40) NULL,
    [EffectiveDate]        DATE         NULL,
    [EndDate]              DATE         NULL,
    [JobId]                INT          NULL,
    [OrgUnitId]            INT          NULL,
    [SupervisorPositionId] INT          NULL,
    [EmployeeGroupCode]    CHAR (1)     NULL,
    [EmployeeSubGroupCode] CHAR (2)     NULL,
    [JobTitle]             VARCHAR (40) NULL,
    [JobTitleLegacy]       VARCHAR (12) NULL,
    [PayGradeType]         CHAR (2)     NULL,
    [PayGradeArea]         CHAR (2)     NULL,
    [PayGrade]             CHAR (8)     NULL,
    [PayGradeLevelFrom]    CHAR (2)     NULL,
    [PayGradeLevelTo]      CHAR (2)     NULL,
    [MinSalary]            NCHAR (14)   NULL,
    [MaxSalary]            NCHAR (14)   NULL,
    [PersonnelSubArea]     CHAR (4)     NULL,
    CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED ([Id] ASC)
);







