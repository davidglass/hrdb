﻿CREATE TABLE [hrms].[Addresses] (
    [PersonId]      INT       NOT NULL,
    [EffectiveDate] DATE      NULL,
    [EndDate]       DATE      NULL,
    [AddressType]   CHAR (4)  NULL,
    [StreetLine1]   CHAR (60) NULL,
    [StreetLine2]   CHAR (40) NULL,
    [City]          CHAR (40) NULL,
    [State]         CHAR (3)  NULL,
    [ZipCode]       CHAR (10) NULL,
    [County]        CHAR (3)  NULL,
    [Country]       CHAR (3)  NULL,
    [HomePhone]     CHAR (20) NULL,
    [PhoneType]     CHAR (4)  NULL,
    [PhoneNumber]   CHAR (20) NULL
);

