﻿CREATE TABLE [hrms].[Job] (
    [Id]             INT          NOT NULL,
    [JobTitle]       VARCHAR (40) NOT NULL,
    [JobTitleLegacy] VARCHAR (12) NOT NULL,
    [IsActive]       BIT          CONSTRAINT [DF_Job_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_hrms.Job] PRIMARY KEY CLUSTERED ([Id] ASC)
);



