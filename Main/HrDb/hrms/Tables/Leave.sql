﻿CREATE TABLE [hrms].[Leave] (
    [PersonId]         INT         NOT NULL,
    [EffectiveDate]    DATETIME    NULL,
    [LeaveType]        NUMERIC (2) NOT NULL,
    [BeginDate]        DATETIME    NULL,
    [EndDate]          DATETIME    NULL,
    [BeginningBalance] NCHAR (16)  NULL,
    [LeaveEarned]      NCHAR (16)  NULL,
    [LeaveTaken]       NCHAR (16)  NULL,
    [LeavePaid]        NCHAR (16)  NULL,
    [LeaveAdj]         NCHAR (16)  NULL,
    [LeaveDonated]     NCHAR (16)  NULL,
    [LeaveReturned]    NCHAR (16)  NULL,
    [LeaveReceived]    NCHAR (16)  NULL,
    [EndBalance]       NCHAR (16)  NULL,
    CONSTRAINT [PK_Leave] PRIMARY KEY CLUSTERED ([PersonId] ASC, [LeaveType] ASC)
);

