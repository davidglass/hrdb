﻿CREATE TABLE [hrms].[WorkSchedule] (
    [HRMSCode]    VARCHAR (8)  NOT NULL,
    [Description] VARCHAR (50) NULL,
    CONSTRAINT [PK_WorkSchedule] PRIMARY KEY CLUSTERED ([HRMSCode] ASC)
);

