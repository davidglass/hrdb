﻿CREATE TABLE [legacy].[PositionAppLinkId] (
    [PositionId] INT NOT NULL,
    [KeyCompId]  INT NULL,
    [PdfId]      INT NULL,
    [WmsId]      INT NULL,
    CONSTRAINT [PK_PositionAppLinkId] PRIMARY KEY CLUSTERED ([PositionId] ASC)
);

