﻿CREATE TABLE [legacy].[CostCenter] (
    [CostCenter] INT          NOT NULL,
    [DescShort]  VARCHAR (20) NULL,
    [DescLong]   VARCHAR (40) NULL,
    [Status]     CHAR (1)     NULL,
    [PersonId]   INT          NULL,
    [Fiscalyear] INT          NOT NULL,
    CONSTRAINT [PK_CostCenter_1] PRIMARY KEY CLUSTERED ([CostCenter] ASC, [Fiscalyear] ASC)
);



