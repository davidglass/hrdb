﻿CREATE TABLE [legacy].[Location] (
    [ID]       INT          NOT NULL,
    [Location] VARCHAR (40) NULL,
    [Building] VARCHAR (40) NULL,
    [IsActive] BIT          CONSTRAINT [DF_Location_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED ([ID] ASC)
);



