﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-11-03
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[GetDelegatedPosIds] 
(	
	-- Add the parameters for the function here
	@LanId varchar(50),
	@AppId int
)
RETURNS TABLE 
AS
RETURN 
(
	select distinct DelegatedPositionId
	from ApprovalDelegate d
	join AppUser u
	on d.DelegatedToPersonId = u.EmployeeId
	and d.StartDate < getdate()
	and (d.EndDate is null or d.EndDate > getdate())
	and d.Active = 1
	and u.LanID = @LanId
	join ApprovalDelegateApp a
	on d.Id = a.DelegateId
	and a.AppId = @AppId
)