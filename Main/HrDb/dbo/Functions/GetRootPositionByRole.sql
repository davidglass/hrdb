﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-01-17
-- Description:	returns requested root if Admin/HR role, else Position id of user
-- =============================================
CREATE FUNCTION [dbo].[GetRootPositionByRole] 
(
	@LanId varchar(50),
	@RequestedRoot int
)
RETURNS int
AS
BEGIN
	DECLARE @RootPosId int
		select @RootPosId =
			case when (
				aur.RoleId in (1,2) -- Admin, HR, controller-level role
				or u.RoleId in (1,2) -- global role
			)
			then @RequestedRoot
			else a.PositionId
			end
			from appuser u
			left join appointment a
			on u.employeeid = a.EmployeeId
				and (a.enddate is null or a.enddate > getdate())
			left join AppUserRole aur
			on aur.UserId = u.Id
				and aur.AppId = 3 -- AppController.PositionHierarchy
			where lanid = @lanid;
	RETURN @RootPosId
END