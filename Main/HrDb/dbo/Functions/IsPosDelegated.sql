﻿

-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-22
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[IsPosDelegated]
(
	@AppId int,
	@PositionId int,
	@LanId varchar(50),
	@AsOf datetime
)
RETURNS bit
AS
BEGIN

--declare @AppId int = 2;
--declare @PositionId int = 5548;
--declare @LanId varchar(50) = 'DIS\DavidG';
--declare @AsOf datetime = getdate(); -- pass in wf.CreatedDate...

DECLARE @IsDelegated bit
select @IsDelegated =
	case when exists (select 1 from ApprovalDelegateApp da
	join ApprovalDelegate d
	on da.DelegateId = d.Id
	join AppUser u
	on d.DelegatedToPersonId = u.EmployeeId
	where AppId = @AppId
	and u.LanId = @LanId
	and d.DelegatedPositionId = @PositionId
	and d.StartDate < @AsOf
	and (d.EndDate is null or dateadd(d, 1, d.EndDate) > @AsOf)
	and d.Active=1)
	then cast(1 as bit)
	else cast(0 as bit) end;
return @IsDelegated;
END