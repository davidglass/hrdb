﻿

-- =============================================
-- Author:		Dave Glass
-- Create date: 2011-12-22
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[IsSupervisor] 
(
	@PosId int
)
RETURNS bit
AS
BEGIN
	DECLARE @Result bit

--declare @posid int = 5078;
	select @Result = cast(case when exists (
		select 1 from Position p join Appointment a
		on p.Id = a.PositionId -- exclude non-current supervised positions
		and p.SupervisorPositionId = @PosId
		and a.EndDate is null
		)
	then 1 else 0 end as bit)
	RETURN @Result
END