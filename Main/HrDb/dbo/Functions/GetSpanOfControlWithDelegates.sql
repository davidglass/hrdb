﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2014-01-17
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[GetSpanOfControlWithDelegates] 
(	
	-- TODO: modify this to take positionid instead of lanid and omit self?
	@LanId varchar(50),
	@AppId int
)
RETURNS TABLE 
AS
RETURN 
(
	select ps2.Id
	--, ps2.Ancestry
	from
	AppUser u
	join Employee e
	on u.EmployeeId = e.Id
	join Appointment a
	on a.EmployeeId = e.Id
	and (a.EndDate is null or a.EndDate > getdate())
	join PositionStructure ps
--	on ps.Id = a.PositionId
	on ps.Id in (
		select * from dbo.GetDelegatedPosIds(@LanId, @AppId)
		union select a.PositionId
	)
	join PositionStructure ps2
	on left(ps2.Ancestry, datalength(ps.Ancestry)) = ps.Ancestry
	--and ps2.Id <> ps.Id
	where u.LanId = @LanId

	union

	select PositionId
	from Par
	where ActionCode = 'P0' -- New Position
	and PosSupervisorPosId in ( --6230
		select ps2.Id from PositionStructure ps
		join PositionStructure ps2
		on ps.ancestry = left(ps2.ancestry, datalength(ps.ancestry))
--		on left(ps.ancestry, datalength(ps2.ancestry)) = ps.ancestry
		where ps.Id = (
			select a.PositionId
				from AppUser u join Appointment a
				on u.EmployeeId = a.EmployeeId
				and (a.EndDate is null or a.EndDate > getdate())
				where u.LanId = @LanId
		)
	)
)