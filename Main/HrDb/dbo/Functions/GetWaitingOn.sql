﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-10-16
-- Description:	GetWaitingOn
-- =============================================
CREATE FUNCTION [dbo].[GetWaitingOn] 
(
	@ApprovalFlowId int
)
RETURNS varchar(50)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result varchar(50)
	-- Add the T-SQL statements to compute the return value here
	-- 
	SELECT @Result = case when e.PreferredName is null or e.PreferredName = '' then e.FirstName else e.PreferredName end
		+ ' ' + e.LastName
		from
		ApprovalWorkflow wf
		join ApprovalTask t
		on wf.Id = t.ApprovalWorkflowId
		join ApprovalStatusCode sc
		on t.StatusCodeId = sc.Id
		join Appointment a
		on ApproverPositionId = a.PositionId
		and (a.EndDate is null or a.EndDate > getdate())
		and a.IsPrimary = 1
		join Employee e
		on a.EmployeeId = e.Id
		where
		wf.Id = @ApprovalFlowId
		and
		-- theoretically wf status should never be 1 unless it has active tasks:
		wf.StatusCodeId = 1
		and t.StatusCodeId = 1;

	if @Result is null and exists (
		select 1 from
		ApprovalTask t
		join approvaldelegate d
		on d.DelegatedPositionId = t.ApproverPositionId
		and d.Active = 1
		and d.StartDate < getdate()
		and (d.EndDate is null or d.EndDate > getdate())
		where t.ApprovalWorkflowId = @ApprovalFlowId
		and t.StatusCodeId = 1 -- Active
	)
	begin
		set @Result = 'DELEGATE'
	end

	if @Result is null
	begin
		select @Result =
		case when ApproveGroupId = 1 then
			case when TaskCodeId = 2 then 'HR Review'
			else 'HR Signoff' end
		end
		from ApprovalTask
		where StatusCodeId = 1 -- Active
		and ApprovalWorkflowId = @ApprovalFlowId
		and ApproveGroupId = 1 -- HR
	end
	-- Return the result of the function
	RETURN @Result

END