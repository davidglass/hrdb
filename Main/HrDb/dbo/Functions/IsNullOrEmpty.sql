﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-11-13
-- Description:	
-- =============================================
CREATE FUNCTION IsNullOrEmpty 
(
	@p1 varchar(max),
	@p2 varchar(max)
)
RETURNS varchar(max)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result varchar(max)

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = case when @p1 is null or @p1 = '' then @p2 else @p1 end
	-- Return the result of the function
	RETURN @Result

END