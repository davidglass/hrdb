﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2012-12-03
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[GetSubUnitCount] 
(
	-- Add the parameters for the function here
	@unitId int
)
RETURNS int
AS
BEGIN
	DECLARE @count int
	select @count = count(*)
		from OrgStructure os -- orgunit
		where ParentId = @unitId
	RETURN @count
END