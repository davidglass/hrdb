﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2012-12-03
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[GetOrgUnitPositionCount] 
(
	-- Add the parameters for the function here
	@unitId int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @poscount int

	-- Add the T-SQL statements to compute the return value here
	SELECT @poscount = @unitId

	select @poscount = count(*)
		from Position p
		join OrgStructure os -- orgunit
		on p.OrgUnitId = os.Id
		join OrgStructure ros -- root orgunit
		on ros.Ancestry = left(os.Ancestry, datalength(ros.Ancestry))
		--and rou.Id = sn.Id) as PositionCount,
		and ros.Id = @unitId
		where IsAbolished = 0
	-- Return the result of the function
	RETURN @poscount

END