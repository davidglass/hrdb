﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-22
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[IsPosInSpanOfControl] 
(
	@PositionId int,
	@LanId varchar(50)
)
RETURNS bit
AS
BEGIN
	DECLARE @IsInSpan bit
	select @IsInSpan =
	case when exists (
		select 1 from GetSpanOfControl(@LanId) p
		where p.Id = @PositionId
	)
	then cast(1 as bit)
	else cast(0 as bit)
	end
	return @IsInSpan
END