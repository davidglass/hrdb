﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-01-21
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[CanReadAll] 
(
	@LanId varchar(50),
	@ControllerName varchar(50)
)
RETURNS bit
AS
BEGIN
	DECLARE @Result bit

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = 

	case when exists (
		select cast(1 as bit)
		from
		AppController a
		join AppRolePermissions p
		on a.ControllerName = @ControllerName
		and a.Id = p.AppId
		join AppUserRole r
		on p.RoleId = r.RoleId
		and r.AppId = p.AppId
		join AppUser u
		on u.LanId = @LanId
		and r.UserId = u.Id
		and p.CanRead = 1
		-- allow HR and Admin roles to read all (else filtered to span of control)
		join AppRole ar
		on r.RoleId = ar.Id
		and ar.Name in ('HR', 'Admin')
	)
	or (select RoleId from AppUser where LanId = @LanId) in (1,2)
	then 1
	else cast(0 as bit)
	end

	RETURN @Result
END