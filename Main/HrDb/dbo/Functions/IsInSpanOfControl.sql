﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-01-28
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[IsInSpanOfControl] 
(
	@EmployeeId int,
	@LanId varchar(50)
)
RETURNS bit
AS
BEGIN
--declare @EmployeeId int = 5980;
--declare @LanId varchar(50) = 'DIS\CurtisS';
	DECLARE @IsInSpan bit
	select @IsInSpan =
	case when exists (
		select 1 from GetSpanOfControl(@LanId) p
		join appointment a
		on a.PositionId = p.Id
		and a.EmployeeId = @EmployeeId
		and (a.EndDate is null or a.EndDate > getdate())
	)
	then cast(1 as bit)
	else cast(0 as bit)
	end
	return @IsInSpan
END