﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-02-26
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[GetParDisplayOuName] 
(
	@OuId int
)
RETURNS varchar(50)
AS
BEGIN
	DECLARE @Result varchar(50);
--	select @Result = (select top 1 OrgUnitName
	select @Result = (select top 1 ocou.Abbreviation
--	aos.Id as OrgUnitId, aos.Ancestry, OrgUnitName, ou.ShortName
	from OrgStructure os
	join OrgStructure aos
	on os.Id = @OuId
	and left(os.ancestry, datalength(aos.ancestry)) = aos.ancestry
	join OrgUnit ou
	--on aos.Id = ou.Id
	on os.Id = ou.Id
	join OrgChartOrgUnit ocou
	on aos.Id = ocou.OrgUnitId
	where datalength(aos.ancestry) < 4 -- exclude OUs deeper than 1 below CTS Operations...
	order by datalength(aos.ancestry) desc);
	return @Result;
END