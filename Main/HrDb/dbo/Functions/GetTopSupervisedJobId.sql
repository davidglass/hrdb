﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-08-26
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[GetTopSupervisedJobId] 
(
	@SupervisorPosId int
)
RETURNS int
AS
BEGIN
	DECLARE @Result int

--	declare @SupervisorPosId int = 5548; -- ATS Mgr
	select @Result = (select top 1 jc.Id
	from Position p
	join Position sp
	on p.SupervisorPositionId = sp.Id
	and sp.Id = @SupervisorPosId
	and p.PayGradeArea <> '40' -- exclude Exec Asst, with max salary of $400k
	join JobClass jc
	on p.JobId = jc.Id
	order by jc.MaxAnnualSalary desc)

	--select @Result = (select top 1 p.JobId -- top 1 is OK here.
	--from Position p
	--join Position sp
	--on p.SupervisorPositionId = sp.Id
	--join StageHRMS.dbo.Position hrp
	--on p.HrmsPositionId = hrp.Id
	--where p.SupervisorPositionId = @SupervisorPosId
	--and hrp.MinSalary = (
	--	select max(MinSalary)
	--	from StageHRMS.dbo.Position hrp2
	--	where hrp2.SupervisorPositionId = sp.HrmsPositionId
	--))
	RETURN @Result
END