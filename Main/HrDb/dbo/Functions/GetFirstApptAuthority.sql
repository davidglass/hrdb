﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-21
-- Description:	
-- =============================================
CREATE FUNCTION GetFirstApptAuthority 
(
	-- Add the parameters for the function here
	@PosId int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @AaPosId int
--	declare @PosId int = 5219;
	set @AaPosId = (select top 1 ps2.Id
	from
	PositionStructure ps
	join PositionStructure ps2
	on ps2.Ancestry = left(ps.Ancestry, datalength(ps2.Ancestry))
	and datalength(ps2.Ancestry) < datalength(ps.Ancestry)
	join Position p
	on ps2.Id = p.Id
	where ps.Id = @PosId
	and p.IsAppointingAuthority = 1
	order by datalength(ps2.Ancestry) desc)

	RETURN @AaPosId

END