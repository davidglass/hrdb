﻿CREATE TABLE [dbo].[EmployeeDateSpec] (
    [EmployeeId] INT      NOT NULL,
    [ds01]       DATETIME NULL,
    [ds02]       DATETIME NULL,
    [ds03]       DATETIME NULL,
    [ds04]       DATETIME NULL,
    [ds05]       DATETIME NULL,
    [ds07]       DATETIME NULL,
    [ds09]       DATETIME NULL,
    [ds18]       DATETIME NULL,
    [ds26]       DATETIME NULL,
    [ds27]       DATETIME NULL,
    CONSTRAINT [PK_EmployeeDateSpec] PRIMARY KEY CLUSTERED ([EmployeeId] ASC)
);

