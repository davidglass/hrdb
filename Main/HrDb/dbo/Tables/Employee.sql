﻿CREATE TABLE [dbo].[Employee] (
    [Id]             INT          CONSTRAINT [DF_Employee_Id] DEFAULT (NEXT VALUE FOR [dbo].[Employee_seq]) NOT NULL,
    [lanid]          VARCHAR (50) NULL,
    [HrmsPersonId]   INT          NULL,
    [FirstName]      VARCHAR (40) CONSTRAINT [DF_Employee_FirstName] DEFAULT ('New') NOT NULL,
    [LastName]       VARCHAR (40) CONSTRAINT [DF_Employee_LastName] DEFAULT ('New') NOT NULL,
    [LastUpdated]    DATETIME     CONSTRAINT [DF_Employee_LastUpdated] DEFAULT (getdate()) NOT NULL,
    [MiddleName]     VARCHAR (40) NULL,
    [Suffix]         VARCHAR (15) NULL,
    [PreferredName]  VARCHAR (40) NULL,
    [DOB]            DATETIME     NULL,
    [EmailAddress]   VARCHAR (50) NULL,
    [Gender]         CHAR (1)     NULL,
    [MaritalStatus]  CHAR (1)     NULL,
    [Created]        DATETIME     CONSTRAINT [DF_Employee_Created] DEFAULT (getdate()) NOT NULL,
    [LegacyPersonId] INT          NULL,
    [HireDate]       DATETIME     NULL,
    CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([Id] ASC)
);

















