﻿CREATE TABLE [dbo].[AppUser_ExpandedOrgNode] (
    [AppUserId] INT NOT NULL,
    [OrgUnitId] INT NOT NULL,
    CONSTRAINT [PK_AppUser_ExpandedOrgNode] PRIMARY KEY CLUSTERED ([AppUserId] ASC, [OrgUnitId] ASC),
    CONSTRAINT [FK_AppUser_ExpandedOrgNode_AppUser] FOREIGN KEY ([AppUserId]) REFERENCES [dbo].[AppUser] ([Id]),
    CONSTRAINT [FK_AppUser_ExpandedOrgNode_OrgStructure] FOREIGN KEY ([OrgUnitId]) REFERENCES [dbo].[OrgStructure] ([Id])
);

