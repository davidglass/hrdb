﻿CREATE TABLE [dbo].[AppController] (
    [Id]             INT          NOT NULL,
    [ControllerName] VARCHAR (50) NOT NULL,
    [IsApi]          BIT          CONSTRAINT [DF_AppController_IsApi] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AppController] PRIMARY KEY CLUSTERED ([Id] ASC)
);



