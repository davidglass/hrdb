﻿CREATE TABLE [dbo].[ApprovalDelegate] (
    [Id]                  INT      IDENTITY (1, 1) NOT NULL,
    [DelegatedToPersonId] INT      NOT NULL,
    [DelegatedPositionId] INT      NOT NULL,
    [StartDate]           DATE     NOT NULL,
    [EndDate]             DATE     NULL,
    [Indefinite]          BIT      NOT NULL,
    [Active]              BIT      CONSTRAINT [DF_ApprovalDelegate_Active] DEFAULT ((1)) NOT NULL,
    [CreatorPersonId]     INT      NULL,
    [DeletePersonId]      INT      NULL,
    [CreationDate]        DATETIME NULL,
    [DeleteDate]          DATETIME NULL,
    CONSTRAINT [PK_Delegate] PRIMARY KEY CLUSTERED ([Id] ASC)
);







