﻿CREATE TABLE [dbo].[PayScaleTypeArea] (
    [Id]          INT          NOT NULL,
    [PsType]      CHAR (2)     NOT NULL,
    [PsArea]      CHAR (2)     NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_PayScaleTypeArea] PRIMARY KEY CLUSTERED ([Id] ASC)
);

