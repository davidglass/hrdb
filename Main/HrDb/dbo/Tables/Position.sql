﻿CREATE TABLE [dbo].[Position] (
    [Id]                        INT            NOT NULL,
    [OrgUnitId]                 INT            NULL,
    [CostCenter]                INT            NULL,
    [JobId]                     INT            NULL,
    [JobTitle]                  VARCHAR (255)  NULL,
    [LegacyClassCode]           VARCHAR (5)    NULL,
    [LegacyPositionNum]         CHAR (4)       NULL,
    [LegacyUnitId]              INT            NULL,
    [LegacyLastUpdated]         DATETIME       NULL,
    [WorkingTitle]              VARCHAR (255)  NULL,
    [HrmsPositionId]            INT            NULL,
    [SupervisorPositionId]      INT            NULL,
    [IsAbolished]               BIT            CONSTRAINT [DF_Position_IsAbolished] DEFAULT ((0)) NOT NULL,
    [CBA]                       BIT            CONSTRAINT [DF_Position_CBA] DEFAULT ((0)) NOT NULL,
    [ValidFromDate]             DATE           NULL,
    [ValidToDate]               DATE           NULL,
    [x_created]                 DATETIME       CONSTRAINT [DF_Position_x_created] DEFAULT (getdate()) NOT NULL,
    [IsInTraining]              BIT            CONSTRAINT [DF_Position_IsInTraining] DEFAULT ((0)) NOT NULL,
    [GeneralDescription]        VARCHAR (1500) NULL,
    [PersonnelSubArea]          CHAR (4)       NULL,
    [IsBackgroundCheckRequired] BIT            CONSTRAINT [DF_Position_IsBackgroundCheckRequired] DEFAULT ((0)) NOT NULL,
    [WorkersCompCode]           CHAR (4)       CONSTRAINT [DF_Position_WorkersCompCode] DEFAULT ('0000') NOT NULL,
    [IsSupervisor]              BIT            CONSTRAINT [DF_Position_IsSupervisor] DEFAULT ((0)) NOT NULL,
    [IsManager]                 BIT            CONSTRAINT [DF_Position_IsManager] DEFAULT ((0)) NOT NULL,
    [IsAppointingAuthority]     BIT            CONSTRAINT [DF_Position_IsAppointingAuthority] DEFAULT ((0)) NOT NULL,
    [EeGroup]                   CHAR (1)       NULL,
    [EeSubGroup]                CHAR (2)       NULL,
    [IsSection4]                BIT            CONSTRAINT [DF_Position_IsSection4] DEFAULT ((0)) NOT NULL,
    [PayGradeType]              CHAR (2)       NULL,
    [PayGradeArea]              CHAR (2)       NULL,
    [PayGrade]                  VARCHAR (8)    NULL,
    [IsTandem]                  BIT            CONSTRAINT [DF_Position_IsTandem] DEFAULT ((0)) NOT NULL,
    [LocationId]                INT            NULL,
    [DefaultWorkScheduleCode]   VARCHAR (8)    CONSTRAINT [DF_Position_DefaultWorkScheduleCode] DEFAULT ('FULL') NOT NULL,
    [PayGradeTypeArea]          INT            NULL,
    [ShiftDesignation]          VARCHAR (1500) NULL,
    [Jvac]                      VARCHAR (3)    NULL,
    [PointValue]                INT            NULL,
    [IsRetireEligible]          BIT            CONSTRAINT [DF_Position_IsRetireEligible] DEFAULT ((1)) NULL,
    [SupervisorEmpId]           INT            NULL,
    [ShiftCode]                 CHAR (1)       CONSTRAINT [DF_Position_ShiftCode] DEFAULT ('D') NOT NULL,
    [IsShiftDiffEligible]       BIT            CONSTRAINT [DF_Position_IsShiftDiffEligible] DEFAULT ((0)) NOT NULL,
    [IsStandbyPayEligible]      BIT            CONSTRAINT [DF_Position_IsStandbyPayEligible] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Position_JobClass] FOREIGN KEY ([JobId]) REFERENCES [dbo].[JobClass] ([Id]),
    CONSTRAINT [FK_Position_OrgUnit] FOREIGN KEY ([OrgUnitId]) REFERENCES [dbo].[OrgUnit] ([Id]),
    CONSTRAINT [FK_Position_Position] FOREIGN KEY ([SupervisorPositionId]) REFERENCES [dbo].[Position] ([Id])
);


























GO
CREATE NONCLUSTERED INDEX [IX_CostCenter]
    ON [dbo].[Position]([CostCenter] ASC);

