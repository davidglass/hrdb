﻿CREATE TABLE [dbo].[EmpFlexSched] (
    [FlexId]           INT          NOT NULL,
    [EmpId]            INT          NOT NULL,
    [Week1]            VARCHAR (60) NULL,
    [Week2]            VARCHAR (60) NULL,
    [WorkScheduleCode] VARCHAR (4)  NULL,
    CONSTRAINT [PK_EmpFlexSched] PRIMARY KEY CLUSTERED ([FlexId] ASC, [EmpId] ASC)
);



