﻿CREATE TABLE [dbo].[WorkersCompCode] (
    [Code]        CHAR (4)     NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_WorkersCompCode] PRIMARY KEY CLUSTERED ([Code] ASC)
);

