﻿CREATE TABLE [dbo].[AttachmentApptRequest] (
    [AttachmentId]  INT NOT NULL,
    [ApptRequestId] INT NOT NULL,
    CONSTRAINT [PK_AttachmentAppointment] PRIMARY KEY CLUSTERED ([AttachmentId] ASC, [ApptRequestId] ASC),
    CONSTRAINT [FK_AttachmentAppointment_Appointment] FOREIGN KEY ([ApptRequestId]) REFERENCES [dbo].[ApptActionRequest] ([Id]),
    CONSTRAINT [FK_AttachmentAppointment_Attachment] FOREIGN KEY ([AttachmentId]) REFERENCES [dbo].[Attachment] ([Id])
);

