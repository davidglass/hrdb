﻿CREATE TABLE [dbo].[ApprovalTask] (
    [Id]                    INT      IDENTITY (1, 1) NOT NULL,
    [ApprovalWorkflowId]    INT      NOT NULL,
    [ApproverPositionId]    INT      NULL,
    [ApproveGroupId]        INT      NULL,
    [StatusCodeId]          INT      NOT NULL,
    [TaskCodeId]            INT      NOT NULL,
    [FinalApproverPersonId] INT      NULL,
    [WorkflowOrder]         INT      NULL,
    [CompletedDate]         DATETIME NULL,
    [Comment]               TEXT     NULL,
    CONSTRAINT [PK_ApprovalTask] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ApprovalTask_ApprovalWorkflow] FOREIGN KEY ([ApprovalWorkflowId]) REFERENCES [dbo].[ApprovalWorkflow] ([Id]),
    CONSTRAINT [FK_ApprovalTask_StatusCode] FOREIGN KEY ([StatusCodeId]) REFERENCES [dbo].[ApprovalStatusCode] ([Id]),
    CONSTRAINT [FK_ApprovalTask_TaskCode] FOREIGN KEY ([TaskCodeId]) REFERENCES [dbo].[ApprovalTaskCode] ([Id])
);

