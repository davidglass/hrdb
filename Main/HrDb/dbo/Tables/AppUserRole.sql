﻿CREATE TABLE [dbo].[AppUserRole] (
    [UserId] INT NOT NULL,
    [AppId]  INT NOT NULL,
    [RoleId] INT NOT NULL,
    CONSTRAINT [PK_AppUserRole] PRIMARY KEY CLUSTERED ([UserId] ASC, [AppId] ASC, [RoleId] ASC)
);

