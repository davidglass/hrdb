﻿CREATE TABLE [dbo].[ActionCode] (
    [Code]            CHAR (2)     NOT NULL,
    [Description]     VARCHAR (50) NOT NULL,
    [HrmsDescription] VARCHAR (50) NULL,
    CONSTRAINT [PK_ActionCode] PRIMARY KEY CLUSTERED ([Code] ASC)
);



