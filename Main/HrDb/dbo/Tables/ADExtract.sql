﻿CREATE TABLE [dbo].[ADExtract] (
    [Id]                         INT           IDENTITY (1, 1) NOT NULL,
    [cn]                         VARCHAR (255) NOT NULL,
    [givenname]                  VARCHAR (255) NULL,
    [mail]                       VARCHAR (255) NULL,
    [physicaldeliveryofficename] VARCHAR (255) NULL,
    [sn]                         VARCHAR (255) NULL,
    [telephonenumber]            VARCHAR (255) NULL,
    [objguidstring]              CHAR (32)     NOT NULL,
    [objectsidstring]            CHAR (83)     NULL,
    [samaccountname]             VARCHAR (255) NULL,
    [company]                    VARCHAR (255) NULL,
    [department]                 VARCHAR (255) NULL,
    [title]                      VARCHAR (255) NULL,
    [adspath]                    VARCHAR (255) NULL,
    [distinguishedname]          VARCHAR (255) NULL,
    [lastlogontimestamp]         VARCHAR (255) NULL,
    [manager]                    VARCHAR (255) NULL,
    [postofficebox]              VARCHAR (255) NULL,
    [userprincipalname]          VARCHAR (255) NULL,
    [whencreated]                VARCHAR (255) NULL,
    [whenchanged]                VARCHAR (255) NULL,
    [employeeid]                 VARCHAR (255) NULL,
    CONSTRAINT [PK_ADExtract] PRIMARY KEY CLUSTERED ([Id] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'for storing HRMS Personnel ID when available', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ADExtract', @level2type = N'COLUMN', @level2name = N'employeeid';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'surname, LDAP equivalent to Last Name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ADExtract', @level2type = N'COLUMN', @level2name = N'sn';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'cube / office #', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ADExtract', @level2type = N'COLUMN', @level2name = N'physicaldeliveryofficename';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'primary email address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ADExtract', @level2type = N'COLUMN', @level2name = N'mail';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LDAP equivalent to First Name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ADExtract', @level2type = N'COLUMN', @level2name = N'givenname';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'common name - formatted as Last, First (AGY) ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ADExtract', @level2type = N'COLUMN', @level2name = N'cn';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Extract of interesting LDAP attributes from Global Catalog (entire forest).  Updated nightly', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ADExtract';

