﻿CREATE TABLE [dbo].[CostCenter] (
    [Id]           INT          NOT NULL,
    [CharCode]     CHAR (4)     NOT NULL,
    [DescShort]    VARCHAR (20) NULL,
    [DescLong]     VARCHAR (40) NULL,
    [IsActive]     BIT          NOT NULL,
    [ManagerEmpId] INT          NULL,
    CONSTRAINT [PK_CostCenter] PRIMARY KEY CLUSTERED ([Id] ASC)
);

