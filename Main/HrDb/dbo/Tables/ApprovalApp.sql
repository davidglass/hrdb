﻿CREATE TABLE [dbo].[ApprovalApp] (
    [Id]       INT          IDENTITY (1, 1) NOT NULL,
    [Name]     VARCHAR (50) NOT NULL,
    [IsActive] BIT          NOT NULL,
    CONSTRAINT [PK_App] PRIMARY KEY CLUSTERED ([Id] ASC)
);

