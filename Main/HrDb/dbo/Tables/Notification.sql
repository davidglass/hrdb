﻿CREATE TABLE [dbo].[Notification] (
    [Id]                INT           IDENTITY (1, 1) NOT NULL,
    [MailTo]            VARCHAR (255) NOT NULL,
    [Subject]           VARCHAR (255) NOT NULL,
    [Body]              VARCHAR (MAX) NOT NULL,
    [CreateDate]        DATETIME      CONSTRAINT [DF_Notification_CreateDate] DEFAULT (getdate()) NOT NULL,
    [RelatedEntityType] VARCHAR (50)  NULL,
    [RelatedEntityId]   INT           NULL,
    [NotificationType]  VARCHAR (50)  NULL,
    CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED ([Id] ASC)
);

