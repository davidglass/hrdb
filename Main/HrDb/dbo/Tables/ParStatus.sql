﻿CREATE TABLE [dbo].[ParStatus] (
    [Id]          INT          NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ParStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);



