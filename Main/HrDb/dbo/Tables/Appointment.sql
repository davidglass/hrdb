﻿CREATE TABLE [dbo].[Appointment] (
    [Id]                        INT            IDENTITY (1, 1) NOT NULL,
    [PositionId]                INT            NOT NULL,
    [EmployeeId]                INT            NOT NULL,
    [StartDate]                 DATE           NOT NULL,
    [EndDate]                   DATE           NULL,
    [IsPrimary]                 BIT            CONSTRAINT [DF_Appointment_IsPrimary] DEFAULT ((1)) NOT NULL,
    [FillLevelJobId]            INT            NULL,
    [ContractTypeCode]          CHAR (2)       NULL,
    [WorkScheduleCode]          CHAR (4)       NULL,
    [EmploymentPercent]         DECIMAL (5, 2) NULL,
    [ContractEndDate]           DATETIME       NULL,
    [TimeMgtStatus]             CHAR (1)       CONSTRAINT [DF_Appointment_TimeMgtStatus] DEFAULT ('9') NOT NULL,
    [PayScaleTypeArea]          INT            NULL,
    [PayScaleReason]            CHAR (2)       NULL,
    [PayScaleEffectiveDate]     DATETIME       NULL,
    [PayScaleGroup]             VARCHAR (6)    NULL,
    [PayScaleLevel]             VARCHAR (2)    NULL,
    [PayNextIncrease]           DATETIME       NULL,
    [PayCapUtilityLevel]        DECIMAL (5, 2) NULL,
    [PayAnnualSalary]           DECIMAL (8, 2) NULL,
    [PayHourlyRate]             DECIMAL (5, 2) NULL,
    [ShiftDesignation]          VARCHAR (200)  NULL,
    [LastUpdatedDate]           DATETIME       CONSTRAINT [DF_Appointment_LastUpdatedDate] DEFAULT (getdate()) NOT NULL,
    [IsOnLeaveOfAbsence]        BIT            DEFAULT ((0)) NOT NULL,
    [WorkHoursWk1]              VARCHAR (255)  NULL,
    [WorkHoursWk2]              VARCHAR (255)  NULL,
    [PayShiftDifferential]      BIT            CONSTRAINT [DF_Appointment_HasShiftDifferential] DEFAULT ((0)) NOT NULL,
    [StepMEligibleDate]         DATETIME       NULL,
    [StandbyPay]                BIT            CONSTRAINT [DF_Appointment_StandbyPay] DEFAULT ((0)) NOT NULL,
    [FillOtEligibilitySubgroup] CHAR (2)       CONSTRAINT [DF_Appointment_FillOtEligibilitySubgroup] DEFAULT ('01') NOT NULL,
    CONSTRAINT [PK_Appointment] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Appointment_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([Id]),
    CONSTRAINT [FK_Appointment_EmployeeSubGroupLkUp] FOREIGN KEY ([FillOtEligibilitySubgroup]) REFERENCES [dbo].[EmployeeSubGroupLkUp] ([EmployeeSubGroupCode]),
    CONSTRAINT [FK_Appointment_JobClass] FOREIGN KEY ([FillLevelJobId]) REFERENCES [dbo].[JobClass] ([Id]),
    CONSTRAINT [FK_Appointment_Position] FOREIGN KEY ([PositionId]) REFERENCES [dbo].[Position] ([Id])
);























