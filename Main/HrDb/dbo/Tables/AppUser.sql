﻿CREATE TABLE [dbo].[AppUser] (
    [Id]                  INT          CONSTRAINT [DF_AppUser_Id] DEFAULT (NEXT VALUE FOR [AppUser_seq]) NOT NULL,
    [LanId]               VARCHAR (50) NULL,
    [EmployeeId]          INT          NULL,
    [RoleId]              INT          NULL,
    [ObjectGuidHexString] CHAR (32)    NULL,
    CONSTRAINT [PK_AppUser] PRIMARY KEY CLUSTERED ([Id] ASC)
);










GO
CREATE UNIQUE NONCLUSTERED INDEX [AK_AppUser]
    ON [dbo].[AppUser]([LanId] ASC);

