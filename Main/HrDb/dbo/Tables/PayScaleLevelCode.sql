﻿CREATE TABLE [dbo].[PayScaleLevelCode] (
    [Code]        VARCHAR (2)  NOT NULL,
    [Description] VARCHAR (50) NULL,
    CONSTRAINT [PK_PayScaleLevelCode] PRIMARY KEY CLUSTERED ([Code] ASC)
);

