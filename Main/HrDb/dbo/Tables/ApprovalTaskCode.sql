﻿CREATE TABLE [dbo].[ApprovalTaskCode] (
    [Id]       INT          IDENTITY (1, 1) NOT NULL,
    [Name]     VARCHAR (20) NOT NULL,
    [IsActive] BIT          NOT NULL,
    CONSTRAINT [PK_TaskType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

