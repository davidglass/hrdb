﻿CREATE TABLE [dbo].[ApptActionRequest] (
    [Id]                INT            IDENTITY (1, 1) NOT NULL,
    [PositionId]        INT            NOT NULL,
    [EmployeeId]        INT            NULL,
    [ActionReasonId]    INT            NULL,
    [ActionCode]        CHAR (2)       NOT NULL,
    [EffectiveDate]     DATETIME       CONSTRAINT [DF_ApptActionRequest_EffectiveDate] DEFAULT (getdate()) NOT NULL,
    [RequestedByUserId] INT            CONSTRAINT [DF_ApptActionRequest_RequestedByUserId] DEFAULT ((1)) NOT NULL,
    [CreateDate]        DATETIME       CONSTRAINT [DF_ApptActionRequest_CreateDate] DEFAULT (getdate()) NOT NULL,
    [ApptId]            INT            NULL,
    [EndDate]           DATETIME       NULL,
    [FillLevel]         INT            NULL,
    [ContractTypeCode]  CHAR (2)       NULL,
    [WorkScheduleCode]  CHAR (4)       CONSTRAINT [DF_ApptActionRequest_WorkScheduleCode] DEFAULT ('FULL') NULL,
    [TimeMgtStatus]     CHAR (1)       NULL,
    [LeaveAccrual_ATID] CHAR (2)       CONSTRAINT [DF_ApptActionRequest_LeaveAccrual_ATID] DEFAULT ('00') NULL,
    [EmploymentPercent] DECIMAL (5, 2) CONSTRAINT [DF_ApptActionRequest_EmploymentPercent] DEFAULT ((100.00)) NOT NULL,
    [IsPartTime]        BIT            CONSTRAINT [DF_ApptActionRequest_IsPartTime] DEFAULT ((0)) NOT NULL,
    [Notes]             VARCHAR (MAX)  NULL,
    [ContractMonths]    INT            CONSTRAINT [DF_ApptActionRequest_ContractMonths] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ApptActionRequest] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ApptActionRequest_ApptActionReason] FOREIGN KEY ([ActionReasonId]) REFERENCES [dbo].[ApptActionReason] ([Id]),
    CONSTRAINT [FK_ApptActionRequest_AppUser] FOREIGN KEY ([RequestedByUserId]) REFERENCES [dbo].[AppUser] ([Id])
);







