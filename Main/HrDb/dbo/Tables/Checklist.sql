﻿CREATE TABLE [dbo].[Checklist] (
    [ParId]           INT          NOT NULL,
    [Cubicle]         VARCHAR (20) NULL,
    [Comments]        TEXT         NULL,
    [UserIdType]      INT          NULL,
    [SimilarTo]       INT          NULL,
    [ComputerEquip]   BIT          NULL,
    [StateDevice]     BIT          NULL,
    [VPNToken]        BIT          NULL,
    [ActiveSync]      BIT          NULL,
    [Broadband]       BIT          NULL,
    [Telecom]         BIT          NULL,
    [Scan]            BIT          NULL,
    [BuildingBadge]   BIT          NULL,
    [SDCBadge]        BIT          NULL,
    [Keys]            BIT          NULL,
    [CostCenter]      BIT          NULL,
    [NamePlate]       BIT          NULL,
    [BusinessCards]   BIT          NULL,
    [UpdateDt]        DATETIME     NULL,
    [UpdateBy]        INT          NULL,
    [CkEmpId]         INT          NULL,
    [UpdateADAccount] BIT          NULL,
    [CubicleTo]       VARCHAR (20) NULL,
    CONSTRAINT [PK_Checklist] PRIMARY KEY CLUSTERED ([ParId] ASC)
);




GO
-- =============================================
-- Author:		PM
-- Create date: 09/12/2014
-- Description:	Inserts a record into Email
-- =============================================
CREATE TRIGGER [dbo].[AfterInsertChecklist]
   ON  [dbo].[Checklist]
   AFTER INSERT
AS 
BEGIN

declare @ParId varchar(5)
select @ParId = cast(parid as varchar) from inserted


-- Check what database server trigger is on, do not send emails to production DLs when in development.

declare @toAddress varchar(50)
set @toAddress = 'pauline.mckusick@cts.wa.gov'

if @@SERVERNAME = 'SSVDBVMCTSP1'
begin
set @toAddress = 'CTSDLAdminTeam@cts.wa.gov'
end 

			
		INSERT INTO Email
           (fromAddress, fromDisplayName, toAddress, toDisplayName, body, subject, isBodyHtml, source, sourceId)
     VALUES
           ('no-reply@cts.wa.gov', 'CTS Checklist System', @toAddress, 'Checklist Administrators', 

		   '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" ' + '"http://www.w3.org/TR/html4/loose.dtd">' + '
<html><head><meta name="viewport" content="width=device-width" /></head><body><p>A new Checklist has been created and is ready for your action.</p>
        <p><a href="' + 'http://apps.cts.wa.gov/hr/checklist/' + @Parid  + '">View Checklist</a></p><p>Thank you.</p></body></html>'
           
		   ,'Checklist has been created #' + @Parid , 1, 'Checklist', @Parid)

END