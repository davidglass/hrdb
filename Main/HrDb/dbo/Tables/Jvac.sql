﻿CREATE TABLE [dbo].[Jvac] (
    [Code]   VARCHAR (10) NOT NULL,
    [Points] INT          NOT NULL,
    [Band]   INT          NULL,
    CONSTRAINT [PK_Jvac] PRIMARY KEY CLUSTERED ([Code] ASC)
);

