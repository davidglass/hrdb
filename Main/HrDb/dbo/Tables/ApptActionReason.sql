﻿CREATE TABLE [dbo].[ApptActionReason] (
    [Id]               INT           NOT NULL,
    [Reason]           VARCHAR (255) NULL,
    [NewHire_U0]       CHAR (2)      NULL,
    [ApptChange_U3]    CHAR (2)      NULL,
    [Separation_U5]    CHAR (2)      NULL,
    [Rehire_U6]        CHAR (2)      NULL,
    [LOA_actv_U8]      CHAR (2)      NULL,
    [LOA_inactv_U9]    CHAR (2)      NULL,
    [LOA_return_UA]    CHAR (2)      NULL,
    [QuickHire_UI]     CHAR (2)      NULL,
    [StatusChange_UJ]  CHAR (2)      NULL,
    [QuickRehire_UK]   CHAR (2)      NULL,
    [ConcurrentEmp_UM] CHAR (2)      NULL,
    [IsActive]         BIT           CONSTRAINT [DF_ApptAction_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ApptAction] PRIMARY KEY CLUSTERED ([Id] ASC)
);

