﻿CREATE TABLE [dbo].[ApproveGroupMembers] (
    [ApproveGroupId] INT NOT NULL,
    [PositionId]     INT NOT NULL,
    CONSTRAINT [PK_ApproveGroupMembers] PRIMARY KEY CLUSTERED ([ApproveGroupId] ASC, [PositionId] ASC),
    CONSTRAINT [FK_ApproveGroupMembers_ApproveGroup] FOREIGN KEY ([ApproveGroupId]) REFERENCES [dbo].[ApproveGroup] ([Id]),
    CONSTRAINT [FK_ApproveGroupMembers_Position] FOREIGN KEY ([PositionId]) REFERENCES [dbo].[Position] ([Id])
);

