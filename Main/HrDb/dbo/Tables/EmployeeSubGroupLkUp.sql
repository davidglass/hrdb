﻿CREATE TABLE [dbo].[EmployeeSubGroupLkUp]
(
	[EmployeeSubGroupCode] CHAR(2) NOT NULL PRIMARY KEY, 
    [Description] VARCHAR(25) NULL
)
