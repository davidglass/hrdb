﻿CREATE TABLE [dbo].[ApproveGroup] (
    [Id]       INT  IDENTITY (1, 1) NOT NULL,
    [Name]     TEXT NOT NULL,
    [IsActive] BIT  NOT NULL,
    CONSTRAINT [PK_Approval.ApproveGroup] PRIMARY KEY CLUSTERED ([Id] ASC)
);

