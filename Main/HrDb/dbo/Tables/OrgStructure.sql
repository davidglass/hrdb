﻿CREATE TABLE [dbo].[OrgStructure] (
    [Id]       INT            NOT NULL,
    [ParentId] INT            NULL,
    [Ancestry] VARBINARY (50) NOT NULL,
    CONSTRAINT [PK_OrgStructure] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_OrgStructure_OrgUnit] FOREIGN KEY ([Id]) REFERENCES [dbo].[OrgUnit] ([Id]),
    CONSTRAINT [AK_Ancestry] UNIQUE NONCLUSTERED ([Ancestry] ASC)
);












GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[OrgStructure_InsteadOfUpdate]
   ON  [dbo].[OrgStructure] 
   instead of update
AS 
BEGIN
	SET NOCOUNT ON;
	declare @id int
	declare @PidOld int
	declare @PidNew int
	declare @AncOld varbinary(50)
	declare @AncNew varbinary(50)
	declare @ParAncNew varbinary(50)
	declare @ChildCount int
	declare @shiftedAnc varbinary(50);
	
	begin try
		begin tran
			select @id = deleted.Id,
				@PidOld = deleted.ParentId,
				@AncOld = OrgStructure.Ancestry
			from deleted, OrgStructure
			where deleted.Id = OrgStructure.Id

			-- TODO: optional node history table...insert entire branch before deleting.

			declare @nodecc int;
			select @nodecc = count(*) from OrgStructure
				where ParentId = @id

			select @PidNew = ParentId,
					@AncNew = Ancestry
			from inserted;

			-- Transfer node to new parent:
			-- *note*, @PidNew could potentially be either null *or* = @PidOld here:
			-- 100118: not sure what purpose PidOld null check serves anymore...breaks with multiple roots.
			--if (@PidOld is not null and @PidOld != @PidNew) begin
			
			if ((@PidOld is null and @PidNew is not null) or @PidOld != @PidNew) begin

				select @ParAncNew = OrgStructure.Ancestry from inserted, OrgStructure
				where inserted.ParentId = OrgStructure.Id
				-- break if attempt is made to move node to become a child of itself:
				if left(@ParAncNew, datalength(@AncOld)) = @AncOld
				begin
					raiserror('node can not be made a child of itself.',11,1) with seterror
					return
				end

				-- ChildCount for inserting node as final sibling under new parent:
				-- count immediate, pre-existing children of new parent node:
				select @ChildCount = count(*) from OrgStructure
				where datalength(ancestry) = datalength(@ParAncNew)+1
				--where datalength(Ancestry) = datalength(@ParAncNew)+2
				and left(Ancestry,datalength(@ParAncNew)) = @ParAncNew;

				-- if @AncNew is provided it supersedes final sibling append behavior,
				-- but if replacing existing node, the successive siblings and their children
				-- must be incremented first.  ChildCount may be unnecessary for this option.

				-- TODO: verify that @AncNew is eligible
				-- to be inserted as a child of the new parent.
				-- left(@AncNew, datalength(@ParAncNew) = @ParAncNew
				-- and final byte of @AncNew <= ChildCount
				-- (this is not critical, but could prevent misuse of this trigger)

				-- both changed (#3):
				if @AncNew is not null and @AncNew != @AncOld begin
					if left(@AncNew, datalength(@AncOld)) = @AncOld
					begin
						raiserror('node can not be made a child of itself.',11,1) with seterror
						return
					end
					-- slide down successive siblings and their children (to make room):
					-- TODO: account for sliding down former parent here.
					-- that prevents matching former successive siblings to close ancestry gap.
					-- use some logic to set @exParentSlid.
					update OrgStructure
					set Ancestry = 
					/* single-byte version */
					cast(
						cast(
							left(ancestry,datalength(@AncNew)-1)
						as varbinary(50)) +
						cast(
							cast(
								cast(right(left(ancestry,datalength(@AncNew)),1)
								as binary(1))
							as tinyint)+1
						as binary(1)) +
						cast(
							right(ancestry, datalength(ancestry)-datalength(@AncNew))
						as varbinary(50))
					as varbinary(50))
					
					/* end single-byte version */
					/* double-byte version
					cast(
						cast(
							left(Ancestry,datalength(@AncNew)-2)
						as varbinary(50)) +
						cast(
							cast(
								cast(right(left(Ancestry,datalength(@AncNew)),2)
								as binary(2))
							as smallint)+1
						as binary(2)) +
						cast(
							right(Ancestry, datalength(Ancestry)-datalength(@AncNew))
						as varbinary(50))
					as varbinary(50))
					
					-- end double-byte version */
					where
						left(Ancestry, datalength(@ParAncNew)) = @ParAncNew
						and	Ancestry >= @AncNew

					-- compensate for slide (to update children after move):
					select @shiftedAnc = Ancestry from OrgStructure where id = @id;

					-- now update subject node:
					update OrgStructure
					set ParentId = @PidNew,
					-- Name = @nodename,
					Ancestry = @AncNew
					where Id = @id;
					
					-- and children:
					update OrgStructure
					set Ancestry =
						cast(@AncNew
						+ cast(right(Ancestry, datalength(Ancestry)-datalength(@AncOld)) as varbinary(50))
						as varbinary(50))
					where left(Ancestry, datalength(@AncOld)) = @shiftedAnc;
				end

				-- only ParentID changed (#1)
				else begin
					update OrgStructure
					set ParentId = @PidNew,
					--, Name = @nodename,
					Ancestry = CAST(@ParAncNew + CAST((@ChildCount+1) as binary(1)) as varbinary(50))
					--Ancestry = CAST(@ParAncNew + CAST((@ChildCount+1) as binary(2)) as varbinary(50))
					where Id = @id;

					update OrgStructure
					set Ancestry =
						cast(@ParAncNew
						+ cast((@ChildCount+1) as binary(1))
						--+ cast((@ChildCount+1) as binary(2))
						+ cast(right(Ancestry, datalength(Ancestry)-datalength(@AncOld)) as varbinary(50))
						as varbinary(50))
					where left(Ancestry, datalength(@AncOld)) = @AncOld
				end

				-- now close any gap in ancestry of former siblings:
				-- first update siblings:
				
				if @shiftedAnc is not null begin
					set @AncOld = @shiftedAnc;
				end
				update OrgStructure
				set Ancestry = 
				cast(
					cast(
						left(@AncOld,datalength(@AncOld)-1)
						--left(@AncOld,datalength(@AncOld)-2)
					as varbinary(50)) +
					cast(
						cast(
							cast(right(ancestry,1)
							as binary(1))
						as tinyint)-1
					as binary(1))
					--		cast(right(Ancestry,2)
					--		as binary(2))
					--	as smallint)-1
					--as binary(2))
				as varbinary(50))
				where left(ancestry,datalength(@AncOld)-1) = left(@AncOld,datalength(@AncOld)-1)
				--where left(Ancestry,datalength(@AncOld)-2) = left(@AncOld,datalength(@AncOld)-2)
				and datalength(Ancestry) = datalength(@AncOld)
				and left(Ancestry,datalength(@AncOld)) > @AncOld	
				-- then update siblings' children as necessary:
				update OrgStructure
				set Ancestry = 
				cast(
					cast(
						left(@AncOld,datalength(@AncOld)-1)
						--left(@AncOld,datalength(@AncOld)-2)
					as varbinary(50)) +
					cast(
						cast(
							cast(right(left(ancestry,datalength(@AncOld)),1)
							as binary(1))
						as tinyint)-1
					  as binary(1)) +
						--	cast(right(left(Ancestry,datalength(@AncOld)),2)
						--	as binary(2))
						--as smallint)-1
					 -- as binary(2)) +
					cast(
						right(Ancestry, datalength(Ancestry)-datalength(@AncOld))
					as varbinary(50))
				as varbinary(50))
				where left(ancestry,datalength(@AncOld)-1) = left(@AncOld,datalength(@AncOld)-1)
				--where left(Ancestry,datalength(@AncOld)-2) = left(@AncOld,datalength(@AncOld)-2)
				and datalength(Ancestry) > datalength(@AncOld)
				and left(Ancestry,datalength(@AncOld)) > @AncOld
			end

			--	Change sort order beneath existing parent (#2):
			else if (@PidOld is not null and @AncNew is not null and @AncNew != @AncOld) begin
				-- *note, need to either suspend AK constraint during this process
				-- or temporarily hold node being moved.  ALTER INDEX is SQL05-specific...
				-- decided storing the node in 0xFF temporarily is better than
				-- rebuilding index after move...
				-- since SQL00 doesn't allow disabling / enabling indexes (just dropping / rebuilding).
				-- also there are performance and security considerations. 
				-- TODO: try temp table instead?
				-- first hold updated node in 0xFF (can't use 0x00 because it is considered equal to parent node)
				-- this means we are limited to 254 sibling nodes per parent since node zero equals the parent.
				-- remember updates called within an update trigger do not fire recursively...

				--*note*, following two steps could be combined easily...
				-- temporarily move node to end of sibling list:
				update OrgStructure
				set Ancestry =
				cast(
					cast(
						left(@AncNew, datalength(@AncNew)-1)
					as varbinary(50)) + 0xFF
					--	left(@AncNew, datalength(@AncNew)-2)
					--as varbinary(50)) + 0xFFFF
				as varbinary(50))
				where Id = @id

				-- also temporarily update node's children (to prevent integrity violation):
				update OrgStructure
				set Ancestry =
				cast(
					cast(
						left(@AncNew, datalength(@AncNew)-1)
					as varbinary(50)) + 0xFF +
					--	left(@AncNew, datalength(@AncNew)-2)
					--as varbinary(50)) + 0xFFFF +
					cast(
						right(Ancestry,datalength(Ancestry)-datalength(@AncNew))
					as varbinary(50))
				as varbinary(50))
				where left(Ancestry,datalength(@AncOld)) = @AncOld
				and datalength(Ancestry) > datalength(@AncOld)

				-- update siblings and their children:
				-- first close any gap created by moving out the subject node:
				update OrgStructure
				set Ancestry = 
				cast(
					cast(
						left(ancestry,datalength(@AncNew)-1)
						--left(Ancestry,datalength(@AncNew)-2)
					as varbinary(50)) +
					cast(
						cast(
							cast(right(left(ancestry,datalength(@AncNew)),1)
							as binary(1))
						as tinyint)-1
					as binary(1)) +
					--		cast(right(left(Ancestry,datalength(@AncNew)),2)
					--		as binary(2))
					--	as smallint)-1
					--as binary(2)) +
					cast(
						right(Ancestry, datalength(Ancestry)-datalength(@AncNew))
					as varbinary(50))
				as varbinary(50))
				where datalength(Ancestry) >= datalength(@AncNew)
					and left(Ancestry, datalength(@AncNew)) <= @AncNew
					and left(Ancestry, datalength(@AncOld)) > @AncOld
					and cast(right(left(ancestry, datalength(@AncNew)),1)
					as binary(1)) != 0xFF
					--and cast(right(left(Ancestry, datalength(@AncNew)),2)
					--as binary(2)) != 0xFFFF
		-- done closing gap

				update OrgStructure
				set Ancestry = 
				cast(
					cast(
						left(ancestry,datalength(@AncNew)-1)
						--left(Ancestry,datalength(@AncNew)-2)
					as varbinary(50)) +
					cast(
						cast(
							cast(right(left(ancestry,datalength(@AncNew)),1)
							as binary(1))
						as tinyint)+1
					as binary(1)) +
					--		cast(right(left(Ancestry,datalength(@AncNew)),2)
					--		as binary(2))
					--	as smallint)+1
					--as binary(2)) +
					cast(
						right(Ancestry, datalength(Ancestry)-datalength(@AncNew))
					as varbinary(50))
				as varbinary(50))
				where datalength(Ancestry) >= datalength(@AncNew)
					and left(Ancestry, datalength(@AncNew)) >= @AncNew
					and left(Ancestry, datalength(@AncNew)) < @AncOld
					and cast(right(left(ancestry, datalength(@AncNew)),1)
					as binary(1)) != 0xFF
					--and cast(right(left(Ancestry, datalength(@AncNew)),2)
					--as binary(2)) != 0xFFFF

				-- now update the node with real new ancestry:
				update OrgStructure
				set Ancestry = @AncNew
				where Id = @id

				-- now update children of node that was just moved:
				-- (remember pid doesn't change).
				update OrgStructure
				set Ancestry = cast(
					@AncNew +
					cast (right(Ancestry, datalength(Ancestry) - datalength(@AncOld)) as varbinary(50))
				as varbinary(50))
				where left(Ancestry,datalength(@AncNew)) =
				cast(left(@AncNew,datalength(@AncNew)-1) as varbinary(50))+0xFF
				--cast(left(@AncNew,datalength(@AncNew)-2) as varbinary(50))+0xFFFF
				and datalength(Ancestry) > datalength(@AncNew)
			end
			-- NEW CASE: moving an existing root node:
			--else if (@PidOld is null and @PidNew is not null) begin
				--raiserror('moving root nodes is not currently supported.',11,1) with seterror
			--end
			-- *NOTE*, this final condition should never occur now...
--			else begin
--				-- neither pid nor ancestry have changed, so update only remaining fields
--				update Node16
----				set Name = @nodename, Expanded = @Expanded, Uri = @uri
--				set Name = @nodename, Uri = @uri
--				where Id = @id;
--				-- if node has collapsed, collapse all subnodes:
--				-- (removed this since recursive CTE for GetTier gave massive perf boost):
--				--if @ExpandedOrig = 1 and @Expanded = 0 begin
--				--	update n2 set Expanded = 0
--				--	from Node16 n
--				--	join Node16 n2
--				--	on n.Id = @id
--				--	and n.ancestry = left(n2.ancestry, datalength(n.ancestry))
--				--	and n2.Expanded = 1;
--				--end
--			end
			commit tran
		return;
	end try
	begin catch
		rollback;
		declare @errmsg nvarchar(255);
		select @errmsg = ERROR_MESSAGE();
		raiserror('node update failed: %s',11,1,@errmsg) with seterror;
	end catch
END
GO
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-10
-- Description:	calculates Ancestry durin
-- =============================================
CREATE TRIGGER [dbo].[OrgStructure_InsteadOfInsert] 
   ON  [dbo].[OrgStructure] 
   instead of insert
AS 
BEGIN
	SET NOCOUNT ON;

	begin try
		begin tran
		declare @pid int;
		declare @id int;
		declare @Ancestry varbinary(50);
		declare @ChildCount smallint;

		select
			@id = Id,
			@pid = ParentId,
			@Ancestry = Ancestry
		from inserted;

		begin
			-- TODO: this should handle non-existent ParentIDs...
			if @pid is null begin
				insert into OrgStructure (Id, Ancestry)
				values (@id, @Ancestry);
			end
			else begin
				select @Ancestry = Ancestry from OrgStructure where Id = @pid
				-- count immediate children:
				select @ChildCount = count(*) from OrgStructure
				where ParentId = @pid

				insert into OrgStructure (Id, ParentId, Ancestry)
				values (@id, @pid,
					CAST(@Ancestry + CAST((@ChildCount+1) as binary(1)) as varbinary(50))
				)
			end
		end
		commit tran
		return;
	end try
	begin catch
		rollback;
		declare @errmsg nvarchar(255);
		select @errmsg = ERROR_MESSAGE();
		raiserror('node insert failed: %s',11,1,@errmsg) with seterror;
	end catch
END