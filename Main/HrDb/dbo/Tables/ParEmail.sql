﻿CREATE TABLE [dbo].[ParEmail] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [ParId]         INT            NOT NULL,
    [HtmlBody]      VARCHAR (MAX)  NOT NULL,
    [TimeSent]      DATETIME       NOT NULL,
    [SentTo]        VARCHAR (1000) NOT NULL,
    [SentBcc]       VARCHAR (100)  NOT NULL,
    [SentFrom]      VARCHAR (50)   NOT NULL,
    [AttachmentIds] VARCHAR (255)  NULL,
    CONSTRAINT [PK_ParEmailLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);



