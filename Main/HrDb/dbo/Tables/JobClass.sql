﻿CREATE TABLE [dbo].[JobClass] (
    [Id]              INT            IDENTITY (60000000, 1) NOT NULL,
    [HrmsJobId]       INT            NULL,
    [LegacyCode]      VARCHAR (12)   NOT NULL,
    [JobTitle]        VARCHAR (40)   NOT NULL,
    [IsActive]        BIT            CONSTRAINT [DF_JobClass_IsActive] DEFAULT ((1)) NOT NULL,
    [MaxAnnualSalary] DECIMAL (8, 2) NULL,
    CONSTRAINT [PK_JobClass] PRIMARY KEY CLUSTERED ([Id] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_LegacyClassCode]
    ON [dbo].[JobClass]([LegacyCode] ASC);

