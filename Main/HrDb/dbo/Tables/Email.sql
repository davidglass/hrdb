﻿CREATE TABLE [dbo].[Email] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [fromAddress]     VARCHAR (100) NULL,
    [fromDisplayName] VARCHAR (100) NULL,
    [toAddress]       VARCHAR (100) NULL,
    [toDisplayName]   VARCHAR (100) NULL,
    [body]            VARCHAR (MAX) NULL,
    [subject]         VARCHAR (MAX) NULL,
    [isBodyHtml]      BIT           NULL,
    [sentDateTime]    DATETIME      NULL,
    [exception]       VARCHAR (MAX) NULL,
    [source]          VARCHAR (50)  NULL,
    [sourceId]        INT           NULL
);





