﻿CREATE TABLE [dbo].[Attachment] (
    [Id]          INT             IDENTITY (1, 1) NOT NULL,
    [FileName]    VARCHAR (MAX)   NOT NULL,
    [Content]     VARBINARY (MAX) NOT NULL,
    [SizeInBytes] INT             NOT NULL,
    [Description] TEXT            NULL,
    [MimeType]    VARCHAR (100)   NOT NULL,
    CONSTRAINT [PK_Attachment] PRIMARY KEY CLUSTERED ([Id] ASC)
);

