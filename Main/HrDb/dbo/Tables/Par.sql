﻿CREATE TABLE [dbo].[Par] (
    [Id]                         INT            IDENTITY (1, 1) NOT NULL,
    [PositionId]                 INT            NULL,
    [EmployeeId]                 INT            NULL,
    [ActionReasonId]             INT            NULL,
    [ActionCode]                 CHAR (2)       NULL,
    [EffectiveDate]              DATETIME       NULL,
    [RequestedByUserId]          INT            NULL,
    [CreateDate]                 DATETIME       CONSTRAINT [DF_Par_CreateDate] DEFAULT (getdate()) NOT NULL,
    [AppointmentId]              INT            NULL,
    [ContractTypeCode]           CHAR (2)       NULL,
    [ContractEndDate]            DATETIME       NULL,
    [WorkScheduleCode]           CHAR (4)       NULL,
    [PercentFullTime]            DECIMAL (5, 2) NULL,
    [FillLevel]                  INT            NULL,
    [Notes]                      VARCHAR (MAX)  CONSTRAINT [DF_Par_Notes] DEFAULT ('') NOT NULL,
    [EmpFirstName]               VARCHAR (40)   NULL,
    [EmpLastName]                VARCHAR (40)   NULL,
    [EmpMiddleName]              VARCHAR (40)   NULL,
    [EmpPreferredName]           VARCHAR (40)   NULL,
    [EmpDOB]                     DATETIME       NULL,
    [EmpGender]                  CHAR (1)       NULL,
    [EmpSSN]                     CHAR (9)       NULL,
    [EmpHrmsId]                  INT            NULL,
    [PosHrmsId]                  INT            NULL,
    [PosOrgUnitId]               INT            NULL,
    [PosCostCenter]              INT            NULL,
    [PosJobId]                   INT            NULL,
    [PosWorkingTitle]            VARCHAR (40)   NULL,
    [PosSupervisorPosId]         INT            NULL,
    [PosValidFrom]               DATETIME       NULL,
    [PosValidTo]                 DATETIME       NULL,
    [PosIsInTraining]            BIT            NULL,
    [PosGeneralDescription]      VARCHAR (1500) NULL,
    [PosPersonnelSubArea]        CHAR (4)       NULL,
    [PosReqsBackgroundCheck]     BIT            NULL,
    [PosWorkersCompCode]         CHAR (4)       NULL,
    [PosIsSupervisor]            BIT            NULL,
    [PosIsManager]               BIT            NULL,
    [PosIsAppointingAuthority]   BIT            NULL,
    [EmpAddrStreetLine1]         VARCHAR (60)   NULL,
    [EmpAddrStreetLine2]         VARCHAR (40)   NULL,
    [EmpAddrCity]                VARCHAR (40)   NULL,
    [EmpAddrState]               CHAR (2)       NULL,
    [EmpAddrZipCode]             VARCHAR (10)   NULL,
    [EmpAddrCountyCode]          CHAR (2)       NULL,
    [EmpAddrHomePhone]           VARCHAR (20)   NULL,
    [EmpDS01_Anniversary]        DATETIME       NULL,
    [EmpDS02_Appointment]        DATETIME       NULL,
    [EmpDS03_CtsHire]            DATETIME       NULL,
    [EmpDS04_PriorPid]           DATETIME       NULL,
    [EmpDS05_Seniority]          DATETIME       NULL,
    [EmpDS07_UnbrokenService]    DATETIME       NULL,
    [EmpDS09_VacLeaveFrozen]     DATETIME       NULL,
    [EmpDS18_PersonalHolidayElg] DATETIME       NULL,
    [EmpDS26_PersonalLeaveElg]   DATETIME       NULL,
    [EmpDS27_TsrLeaveElg]        DATETIME       NULL,
    [EmpRaceCodes]               VARCHAR (50)   NULL,
    [EmpHispanic]                BIT            CONSTRAINT [DF_Par_EmpHispanic] DEFAULT ((0)) NOT NULL,
    [EmpMilitaryStatus]          VARCHAR (255)  NULL,
    [EmpMilitaryBranch]          VARCHAR (255)  NULL,
    [EmpDisability]              BIT            CONSTRAINT [DF_Par_EmpDisability] DEFAULT ((0)) NOT NULL,
    [EmpSuffix]                  VARCHAR (50)   NULL,
    [ApptTimeMgtStatus]          CHAR (1)       NULL,
    [ApptPayScaleTypeArea]       INT            NULL,
    [ApptPayScaleReason]         CHAR (2)       NULL,
    [ApptPayScaleEffectiveDate]  DATETIME       NULL,
    [ApptPayScaleGroup]          VARCHAR (6)    NULL,
    [ApptPayScaleLevel]          VARCHAR (2)    NULL,
    [ApptPayNextIncrease]        DATETIME       NULL,
    [ApptPayCapUtilityLevel]     DECIMAL (5, 2) NULL,
    [ApptPayAnnualSalary]        DECIMAL (8, 2) NULL,
    [ApptPayHourlyRate]          DECIMAL (5, 2) NULL,
    [PosPayGradeTypeArea]        INT            NULL,
    [PosPayGrade]                VARCHAR (8)    NULL,
    [PosEeGroup]                 CHAR (1)       NULL,
    [PosEeSubgroup]              CHAR (2)       NULL,
    [PosIsSection4]              BIT            NULL,
    [PosIsTandem]                BIT            NULL,
    [PosLocationId]              INT            NULL,
    [PosWorkScheduleCode]        VARCHAR (8)    NULL,
    [PosShiftDesignation]        VARCHAR (1500) NULL,
    [PosJvac]                    VARCHAR (3)    NULL,
    [PosPointValue]              INT            NULL,
    [PosIsRetireEligible]        BIT            CONSTRAINT [DF_Par_PosIsRetireEligible] DEFAULT ((1)) NULL,
    [StatusId]                   INT            CONSTRAINT [DF_Par_StatusId] DEFAULT ((1)) NOT NULL,
    [ApprovalWorkflowId]         INT            NULL,
    [ApptShiftDesignation]       VARCHAR (200)  NULL,
    [LastUpdatedDate]            DATETIME       CONSTRAINT [DF_Par_LastUpdatedDate] DEFAULT (getdate()) NOT NULL,
    [LastUpdatedByUser]          INT            NULL,
    [ProcessedDate]              DATETIME       NULL,
    [TargetAgencyCode]           CHAR (4)       CONSTRAINT [DF_Par_TargetAgencyCode] DEFAULT ('1630') NOT NULL,
    [IsTransferOut]              BIT            CONSTRAINT [DF_Par_IsTransferOut] DEFAULT ((0)) NOT NULL,
    [ApptShiftDifferential]      BIT            CONSTRAINT [DF_Par_ApptShiftDifferential] DEFAULT ((0)) NOT NULL,
    [ApptWorkHoursWk1]           VARCHAR (60)   NULL,
    [ApptWorkHoursWk2]           VARCHAR (60)   NULL,
    [PosShiftCode]               CHAR (1)       CONSTRAINT [DF_Par_PosShiftCode] DEFAULT ('D') NOT NULL,
    [ApptStepMEligibleDate]      DATETIME       NULL,
    [ApptStandbyPay]             BIT            CONSTRAINT [DF_Par_ApptStandbyPay] DEFAULT ((0)) NOT NULL,
    [DeactivateClassCode]        BIT            CONSTRAINT [DF_Par_DeactivateClassCode] DEFAULT ((0)) NOT NULL,
    [PosShiftDiffEligible]       BIT            CONSTRAINT [DF_Par_PosShiftDiffEligible] DEFAULT ((0)) NOT NULL,
    [PosStandbyEligible]         BIT            CONSTRAINT [DF_Par_PosStandbyEligible] DEFAULT ((0)) NOT NULL,
    [IsTransferIn]               BIT            CONSTRAINT [DF_Par_IsTransferIn] DEFAULT ((0)) NOT NULL,
    [FromAgencyCode]             CHAR (4)       NULL,
    [ApptFillOtEligCode]         CHAR (2)       CONSTRAINT [DF_Par_ApptFillOtEligCode] DEFAULT ('01') NULL,
    [IsPayChangeOnly]            BIT            CONSTRAINT [DF_Par_IsPayChangeOnly] DEFAULT ((0)) NOT NULL,
    [EmpEmail]                   VARCHAR (50)   NULL,
    [IsNameChangeOnly]           BIT            CONSTRAINT [DF_Par_IsNameChangeOnly] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Par] PRIMARY KEY CLUSTERED ([Id] ASC)
);





































