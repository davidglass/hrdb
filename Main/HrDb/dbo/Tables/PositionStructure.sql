﻿CREATE TABLE [dbo].[PositionStructure] (
    [Id]                   INT            NOT NULL,
    [SupervisorPositionId] INT            NULL,
    [Ancestry]             VARBINARY (50) NOT NULL,
    CONSTRAINT [PK_PositionStructure] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PositionStructure_Position] FOREIGN KEY ([Id]) REFERENCES [dbo].[Position] ([Id]),
    CONSTRAINT [FK_PositionStructure_PositionStructure] FOREIGN KEY ([SupervisorPositionId]) REFERENCES [dbo].[PositionStructure] ([Id]),
    CONSTRAINT [AK_PosAncestry] UNIQUE NONCLUSTERED ([Ancestry] ASC)
);




GO

-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-10
-- Description:	calculates Ancestry durin
-- =============================================
CREATE TRIGGER [dbo].[PositionStructure_InsteadOfInsert] 
   ON  [dbo].[PositionStructure] 
   instead of insert
AS 
BEGIN
	SET NOCOUNT ON;

	begin try
		begin tran
		declare @svid int;
		declare @id int;
		declare @Ancestry varbinary(50);
		declare @ChildCount smallint;

		select
			@id = Id,
			@svid = SupervisorPositionId,
			@Ancestry = Ancestry
		from inserted;

		begin
			-- TODO: this should handle non-existent ParentIDs...
			if @svid is null begin
				insert into PositionStructure (Id, Ancestry)
				values (@id, @Ancestry);
			end
			else begin
				select @Ancestry = Ancestry from PositionStructure where Id = @svid
				-- count immediate children:
				select @ChildCount = count(*) from PositionStructure
				where SupervisorPositionId = @svid

				insert into PositionStructure (Id, SupervisorPositionId, Ancestry)
				values (@id, @svid,
					CAST(@Ancestry + CAST((@ChildCount+1) as binary(1)) as varbinary(50))
				)
			end
		end
		commit tran
		return;
	end try
	begin catch
		rollback;
		declare @errmsg nvarchar(255);
		select @errmsg = ERROR_MESSAGE();
		raiserror('node insert failed: %s',11,1,@errmsg) with seterror;
	end catch
END
GO


