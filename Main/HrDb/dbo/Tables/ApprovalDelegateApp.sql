﻿CREATE TABLE [dbo].[ApprovalDelegateApp] (
    [DelegateId] INT NOT NULL,
    [AppId]      INT NOT NULL,
    CONSTRAINT [PK_DelegateApp] PRIMARY KEY CLUSTERED ([DelegateId] ASC, [AppId] ASC),
    CONSTRAINT [FK_DelegateApp_App] FOREIGN KEY ([AppId]) REFERENCES [dbo].[ApprovalApp] ([Id]),
    CONSTRAINT [FK_DelegateApp_Delegate] FOREIGN KEY ([DelegateId]) REFERENCES [dbo].[ApprovalDelegate] ([Id])
);

