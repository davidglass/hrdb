﻿CREATE TABLE [dbo].[OrgChartOrgUnit] (
    [OrgUnitId]    INT          NOT NULL,
    [OrgUnitName]  VARCHAR (50) NOT NULL,
    [Abbreviation] VARCHAR (50) NULL,
    CONSTRAINT [PK_OrgChartOrgUnits] PRIMARY KEY CLUSTERED ([OrgUnitId] ASC),
    CONSTRAINT [FK_OrgChartOrgUnit_OrgUnit] FOREIGN KEY ([OrgUnitId]) REFERENCES [dbo].[OrgUnit] ([Id])
);



