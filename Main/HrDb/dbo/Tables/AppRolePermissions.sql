﻿CREATE TABLE [dbo].[AppRolePermissions] (
    [AppId]     INT NOT NULL,
    [RoleId]    INT NOT NULL,
    [CanCreate] BIT CONSTRAINT [DF_AppRolePermissions_CanCreate] DEFAULT ((0)) NOT NULL,
    [CanRead]   BIT CONSTRAINT [DF_AppRolePermissions_CanRead] DEFAULT ((0)) NOT NULL,
    [CanUpdate] BIT CONSTRAINT [DF_AppRolePermissions_CanUpdate] DEFAULT ((0)) NOT NULL,
    [CanDelete] BIT CONSTRAINT [DF_AppRolePermissions_CanDelete] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AppRolePermissions] PRIMARY KEY CLUSTERED ([AppId] ASC, [RoleId] ASC)
);

