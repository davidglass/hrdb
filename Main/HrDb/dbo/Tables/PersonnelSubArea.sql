﻿CREATE TABLE [dbo].[PersonnelSubArea] (
    [Code]        CHAR (4)     NOT NULL,
    [Description] VARCHAR (50) NULL,
    CONSTRAINT [PK_PersonnelSubArea] PRIMARY KEY CLUSTERED ([Code] ASC)
);

