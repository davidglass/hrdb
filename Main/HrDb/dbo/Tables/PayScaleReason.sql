﻿CREATE TABLE [dbo].[PayScaleReason] (
    [Code]        CHAR (2)      NOT NULL,
    [Description] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_PayScaleReason] PRIMARY KEY CLUSTERED ([Code] ASC)
);

