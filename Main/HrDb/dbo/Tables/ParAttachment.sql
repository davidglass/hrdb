﻿CREATE TABLE [dbo].[ParAttachment] (
    [Id]          INT             IDENTITY (1, 1) NOT NULL,
    [FileName]    VARCHAR (MAX)   NOT NULL,
    [Content]     VARBINARY (MAX) NOT NULL,
    [SizeInBytes] INT             NOT NULL,
    [Description] TEXT            NULL,
    [MimeType]    VARCHAR (100)   NOT NULL,
    [ParId]       INT             NOT NULL,
    CONSTRAINT [PK_ParAttachment] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ParAttachment_Par] FOREIGN KEY ([ParId]) REFERENCES [dbo].[Par] ([Id])
);

