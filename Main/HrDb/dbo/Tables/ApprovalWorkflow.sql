﻿CREATE TABLE [dbo].[ApprovalWorkflow] (
    [Id]             INT      IDENTITY (1, 1) NOT NULL,
    [StatusCodeId]   INT      NOT NULL,
    [ApprovalCodeId] INT      NOT NULL,
    [AppId]          INT      NOT NULL,
    [PositionId]     INT      NULL,
    [PersonId]       INT      NULL,
    [CreatedDate]    DATETIME NOT NULL,
    CONSTRAINT [PK_ApprovalWorkflow] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ApprovalWorkflow_App] FOREIGN KEY ([AppId]) REFERENCES [dbo].[ApprovalApp] ([Id]),
    CONSTRAINT [FK_ApprovalWorkflow_ApprovalCode] FOREIGN KEY ([ApprovalCodeId]) REFERENCES [dbo].[ApprovalCode] ([Id]),
    CONSTRAINT [FK_ApprovalWorkflow_StatusCode] FOREIGN KEY ([StatusCodeId]) REFERENCES [dbo].[ApprovalStatusCode] ([Id])
);

