﻿CREATE TABLE [dbo].[LanImpersonation] (
    [LanId]             VARCHAR (50) NOT NULL,
    [ImpersonatedLanId] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_LanImpersonation] PRIMARY KEY CLUSTERED ([LanId] ASC)
);

