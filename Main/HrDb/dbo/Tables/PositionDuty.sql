﻿CREATE TABLE [dbo].[PositionDuty] (
    [Id]                    INT           CONSTRAINT [DF_PositionDuty_Id] DEFAULT (NEXT VALUE FOR [dbo].[PositionDuty_seq]) NOT NULL,
    [PositionDescriptionId] INT           NOT NULL,
    [Duty]                  VARCHAR (255) NULL,
    [TasksHtml]             VARCHAR (MAX) NULL,
    [TimePercent]           INT           NULL,
    CONSTRAINT [PK_PositionDuty] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PositionDuty_PositionDescription] FOREIGN KEY ([PositionDescriptionId]) REFERENCES [dbo].[PositionDescription] ([Id]) ON DELETE CASCADE
);



