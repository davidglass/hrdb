﻿CREATE TABLE [dbo].[PayScaleGroupCode] (
    [Code]        VARCHAR (6)   NOT NULL,
    [Description] VARCHAR (255) NULL,
    CONSTRAINT [PK_PayScaleGroupCode] PRIMARY KEY CLUSTERED ([Code] ASC)
);

