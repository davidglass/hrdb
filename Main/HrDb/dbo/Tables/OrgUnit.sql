﻿CREATE TABLE [dbo].[OrgUnit] (
    [Id]              INT          NOT NULL,
    [HrmsOrgUnitId]   INT          NULL,
    [Description]     VARCHAR (40) NULL,
    [ShortName]       VARCHAR (12) NULL,
    [Abbreviation]    VARCHAR (5)  NULL,
    [SupervisorPosId] INT          NULL,
    CONSTRAINT [PK_OrgUnit] PRIMARY KEY CLUSTERED ([Id] ASC)
);



