﻿CREATE TABLE [dbo].[WorkContractType] (
    [WorkContractTypeCode] CHAR (2)      NOT NULL,
    [WorkContractTypeDesc] VARCHAR (100) NULL,
    CONSTRAINT [PK_WorkContractType] PRIMARY KEY CLUSTERED ([WorkContractTypeCode] ASC)
);

