﻿CREATE TABLE [dbo].[ApprovalCode] (
    [Id]       INT          NOT NULL,
    [Name]     VARCHAR (20) NOT NULL,
    [IsActive] BIT          NOT NULL,
    CONSTRAINT [PK_ApprovalType] PRIMARY KEY CLUSTERED ([Id] ASC)
);



