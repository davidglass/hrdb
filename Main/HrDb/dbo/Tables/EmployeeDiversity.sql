﻿CREATE TABLE [dbo].[EmployeeDiversity] (
    [EmployeeId]     INT          NOT NULL,
    [RaceCodes]      VARCHAR (50) NULL,
    [MilStatusCodes] VARCHAR (50) NULL,
    [MilBranchCodes] VARCHAR (50) NULL,
    [Hispanic]       BIT          NULL,
    [Disability]     BIT          NULL,
    CONSTRAINT [PK_EmployeeDiversity] PRIMARY KEY CLUSTERED ([EmployeeId] ASC)
);

