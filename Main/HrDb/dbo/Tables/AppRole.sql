﻿CREATE TABLE [dbo].[AppRole] (
    [Id]   INT           NOT NULL,
    [Name] VARCHAR (255) NULL,
    CONSTRAINT [PK_AppRole] PRIMARY KEY CLUSTERED ([Id] ASC)
);

