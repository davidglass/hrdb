﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-06-19
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetParActionTypes] 
AS
BEGIN
  select Code as Value,
  Description as [Text]
  from ActionCode
  union select 'U3-F' as Value,
  'Fill, Transfer In' as [Text]
  union select 'U3-P' as Value,
  'Pay Change' as [Text]
  order by Description
END