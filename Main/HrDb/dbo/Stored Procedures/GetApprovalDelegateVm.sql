﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 9/3/13
-- Description:	Get approval delegate vm
-- =============================================
CREATE PROCEDURE [dbo].[GetApprovalDelegateVm]
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		--test to see if result.
	if Exists(
		Select [ApprovalDelegate].Id
		FROM [ApprovalDelegate]
		left outer join Appointment on [ApprovalDelegate].DelegatedPositionId = Appointment.PositionId
		where [ApprovalDelegate].[Id] = @Id
		and (Appointment.EndDate is null) or (Appointment.EndDate > getdate())
	)
	Begin
		SELECT top 1 --top 1 included for dual fills
			Pos.WorkingTitle [DelegatedPositionTitle],
			DelEmp.Id DelegatedToId,
			DelEmp.LastName DelegatedToLastName,
			DelEmp.PreferredName DelegatedToPreferredName,
			DelEmp.EmailAddress DelegatedToEmail,
			PosEmp.Id [DelegatedPositionPersonId],
			PosEmp.LastName [DelegatedPositionPersonLastName],
			PosEmp.PreferredName [DelegatedPositionPersonPreferredName],
			PosEmp.EmailAddress DelegatedPositionEmail,
			ApprovalDelegate.CreationDate,
			ApprovalDelegate.DeleteDate,
			CreateEmp.LastName CreateEmpLastName,
			CreateEmp.PreferredName CreateEmpPreferredName,
			DeleteEmp.LastName DeleteEmpLastName,
			DeleteEmp.PreferredName DeleteEmpPreferredName
		FROM [ApprovalDelegate]
		left outer join Position Pos on [ApprovalDelegate].DelegatedPositionId = Pos.Id
		left outer join Appointment on [ApprovalDelegate].DelegatedPositionId = Appointment.PositionId
		left outer join Employee PosEmp on Appointment.EmployeeId = PosEmp.Id
		left outer join Employee DelEmp on [ApprovalDelegate].DelegatedToPersonId = DelEmp.Id
		left outer join Employee CreateEmp on ApprovalDelegate.CreatorPersonId = CreateEmp.Id
		left outer join Employee DeleteEmp on ApprovalDelegate.DeletePersonId = DeleteEmp.Id
		where [ApprovalDelegate].[Id] = @Id
		and (Appointment.EndDate is null) or (Appointment.EndDate > getdate())
	End
	else
	Begin

		SELECT top 1 --top 1 included for dual fills
			Pos.WorkingTitle [DelegatedPositionTitle],	
			DelEmp.Id DelegatedToId,
			DelEmp.LastName DelegatedToLastName,
			DelEmp.PreferredName DelegatedToPreferredName,
			DelEmp.EmailAddress DelegatedToEmail,
			null [DelegatedPositionPersonId],
			'Vacant' [DelegatedPositionPersonLastName],
			'' [DelegatedPositionPersonPreferredName],
			'' DelegatedPositionEmail,
			ApprovalDelegate.CreationDate,
			ApprovalDelegate.DeleteDate,
			CreateEmp.LastName CreateEmpLastName,
			CreateEmp.PreferredName CreateEmpPreferredName,
			DeleteEmp.LastName DeleteEmpLastName,
			DeleteEmp.PreferredName DeleteEmpPreferredName
		FROM [ApprovalDelegate]
		left outer join Position Pos on [ApprovalDelegate].DelegatedPositionId = Pos.Id
		left outer join Employee DelEmp on [ApprovalDelegate].DelegatedToPersonId = DelEmp.Id
		left outer join Employee CreateEmp on ApprovalDelegate.CreatorPersonId = CreateEmp.Id
		left outer join Employee DeleteEmp on ApprovalDelegate.DeletePersonId = DeleteEmp.Id
		where [ApprovalDelegate].[Id] = @Id

	End



End