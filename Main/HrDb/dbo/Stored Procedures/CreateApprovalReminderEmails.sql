﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 8/4/14
-- Description:	Finds active approval tasks that have not been been completed in x days 
-- of last notification and sends reminder email.
-- =============================================
create PROCEDURE [dbo].[CreateApprovalReminderEmails] 
	-- Add the parameters for the stored procedure here

	@daysToWaitBeforeReminder int
	
AS
BEGIN


Insert into Email (fromAddress,
				fromDisplayName,
				toAddress,
				toDisplayName,
				body,
				[subject],
				isBodyHtml,
				[source],
				sourceId)

--Built on assumption that each new email has a higher Id value
select
	email.fromAddress,
	email.fromDisplayName,
	email.toAddress,
	email.toDisplayName,
	email.body,
	'Reminder: ' + email.[subject] [newSubject],
	email.isBodyHtml,
	email.[source],
	email.sourceId from (
		--select only highest Id email sent for given task
		select email.sourceId, max(Id) emailId from email 
			where email.source = 'ApprovalTask'
			Group by email.sourceId) lastMessage
		
		--only active tasks need reminders
		join ApprovalTask on lastMessage.sourceId = ApprovalTask.Id and ApprovalTask.StatusCodeId = 1
		
		--get email data
		join Email on lastMessage.emailId = email.Id
	
	--check to see if sent date is older than days. Ignore null to prevent spamming on system error.
	where email.sentDateTime < getdate() - @daysToWaitBeforeReminder
			   
			   
END