﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 6/4/13
-- Description:	Get attachment info to display
-- =============================================
CREATE PROCEDURE [dbo].[GetAttachmentInfo]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT  
		[Id],
		[FileName],
		[SizeInBytes],
		[Description],
		[MimeType]
	FROM [Attachment]
END