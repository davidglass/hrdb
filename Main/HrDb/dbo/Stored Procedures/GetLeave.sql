﻿
-- =============================================
-- Author:		P.McKusick
-- Create date: 09/05/2013
-- Description:	Get HRMS Leave records for Employee Detail
-- =============================================
CREATE PROCEDURE [dbo].[GetLeave] @EmpId int
AS
BEGIN

SELECT
	e.Id
      ,EffectiveDate
      ,LeaveType
	  ,case when LeaveType = 30 then 'Sick' when LeaveType = 31 then 'Annual' when LeaveType = 40 then 'Excess' when LeaveType = 42 then 'Personal Holiday'
	  when LeaveType = 38 then 'Military' when LeaveType = 41 then 'Shared Leave' when LeaveType = 48  then 'Personal Leave Day' when LeaveType = 49 then 'TSR'  else 'Unknown' end AS [LeaveTypeDesc]
      ,BeginDate
      ,EndDate
      ,cast(substring( BeginningBalance,1,10) + '.' + substring( BeginningBalance,11,5) as decimal(7,2)) as BeginningBalance
      ,cast(substring( LeaveEarned,1,10) + '.' + substring( LeaveEarned,11,5) as decimal(7,2)) as LeaveEarned
      ,cast(substring( LeaveTaken,1,10) + '.' + substring( LeaveTaken,11,5) as decimal(7,2)) as LeaveTaken
      ,cast(substring( LeavePaid,1,10) + '.' + substring( LeavePaid,11,5) as decimal(7,2)) as LeavePaid
      ,cast(substring( LeaveAdj,1,10) + '.' + substring( LeaveAdj,11,5) as decimal(7,2)) as LeaveAdj
      ,cast(substring( LeaveDonated,1,10) + '.' + substring( LeaveDonated,11,5) as decimal(7,2)) as LeaveDonated
      ,cast(substring( LeaveReturned,1,10) + '.' + substring( LeaveReturned,11,5) as decimal(7,2)) as LeaveReturned
      ,cast(substring( LeaveReceived,1,10) + '.' + substring( LeaveReceived,11,5) as decimal(7,2)) as LeaveReceived
      ,cast(substring( EndBalance,1,10) + '.' + substring( EndBalance,11,5) as decimal(7,2)) as EndBalance
-- no production joins to stage, please:
--FROM [stageHRMS].[dbo].[Leave] AS l
FROM hrms.[Leave] AS l
LEFT JOIN [dbo].[Employee] AS e ON e.HrmsPersonId = l.PersonId
WHERE e.Id  = @EmpId
END