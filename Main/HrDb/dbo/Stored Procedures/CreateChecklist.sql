﻿-- =============================================
-- Author:		Pauline McKusick
-- Create date: 04/10/2014
-- Description:	create a new checklist
-- =============================================
CREATE PROCEDURE [dbo].[CreateChecklist] 
	-- Add the parameters for the stored procedure here

	@ParId int
	
AS
BEGIN

declare @EmpId int
set @EmpId = 
(select
case when p.EmployeeId is null then EmpA.EmployeeId else p.EmployeeId end as EmpId from par p
left join Appointment EmpA on EmpA.PositionId = p.PositionId and ((EmpA.enddate is null or EmpA.enddate > getdate()) and EmpA.StartDate <= getdate())
where p.id = @ParId)

		INSERT INTO Checklist
			   (Parid, CkEmpId)
		VALUES
			   (@Parid, @EmpId)


				INSERT INTO Email
           (fromAddress, fromDisplayName, toAddress, toDisplayName, body, subject, isBodyHtml)
     VALUES
           ('no-reply@cts.wa.gov', 'CTS Checklist System', 'CTSDLAdminTeam@cts.wa.gov', 'Checklist Administrators', 

		   '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" ' + '"http://www.w3.org/TR/html4/loose.dtd">' + '
<html><head><meta name="viewport" content="width=device-width" /></head><body><p>A new Checklist has been created and is ready for your action.</p>
        <p><a href="' + 'http://apps.cts.wa.gov/hr/checklist/' + cast(@Parid as varchar)  + '">View Checklist</a></p><p>Thank you.</p></body></html>'
           
		   ,'Checklist has been created #' + cast(@Parid as varchar) , 1)
				   
END