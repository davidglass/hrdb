﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-09-23
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetOuPositions] 
	@Id int
AS
BEGIN
	select * from Position
	where OrgUnitId = @Id
	order by case when IsSupervisor = 1 then 1 else 2 end,
	WorkingTitle;
END