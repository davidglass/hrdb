﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-06-14
-- Description:
--TODO:
-- [] add query params,
-- [] create, use PosIsInSpanOfControl (ala EmpIsInSpanOfControl)
-- =============================================
CREATE PROCEDURE [dbo].[GetPositionList]
@ClassCode varchar(12) = null,
@CostCenter int = null,
@IsVacant bit = null,
@LanId varchar(50)
AS
BEGIN

--declare @ClassCode varchar(12) = '479N';
--declare @ClassCode varchar(12) = null;
--declare @LanId varchar(50) = 'DIS\MarkP';
----declare @IsVacant bit = cast(1 as bit);
--declare @IsVacant bit = cast(1 as bit);
--declare @CostCenter int = 3410

	select
	p.Id,
	--p.HrmsPositionId,
	isnull(ou.shortname, '[PENDING]') as OuName,
	legacyclasscode as ClassCode,
	CostCenter,
	jc.JobTitle,
	WorkingTitle,
	isnull(e.LastName,
		isnull(
		(
		select e2.LastName from Appointment a2
		join Employee e2
		on a2.PositionId = p.Id
		and e2.Id = a2.EmployeeId
		and EndDate = (select max(EndDate) from Appointment where PositionId = p.Id)
		), 'VACANT')
	) as LastName,
	isnull(isnull(e.PreferredName, e.FirstName), 'VACANT') as FirstName,
	SupervisorPositionId,
	isnull(SupervisorEmpId, 0), -- testing whether null breaks model
	cast(case when a.id is null then 1 else 0 end as bit) as IsVacant
	from position p
	left join OrgUnit ou
	on p.OrgUnitId = ou.Id
	left join appointment a
	on p.id = a.PositionId
	and (a.EndDate is null or a.EndDate > getdate())
	left join JobClass jc
	on p.JobId = jc.Id
	left join Employee e
	on a.EmployeeId = e.Id
	where isabolished = 0
	and
		(@ClassCode is null or
		LegacyClassCode = @ClassCode)
	and (@CostCenter is null or
		CostCenter = @CostCenter)
	and (@IsVacant is null
	or
	(@IsVacant = cast(1 as bit) and a.Id is null)
	or
	(@IsVacant = cast(0 as bit) and a.Id is not null)
	)
	order by shortname

END