﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-11-12
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetPdIncumbentName] 
	-- Add the parameters for the stored procedure here
	@PdId int
AS
BEGIN
select distinct
case when (select count(1)
	from PositionDescription pd
	join Appointment a
	on pd.Id = @PdId
	and pd.PositionId = a.PositionId
	and (a.EndDate is null or a.EndDate > getdate())
	) > 1 then 'MULTIPLE'
else
	case when a.Id is null then '' else
		e.lastname + ', ' + 
		case when (e.preferredname is null or e.preferredname = '') then e.firstname
			else e.preferredname
		end
	end
end
as IncumbentName
from PositionDescription pd
left join Appointment a
on pd.PositionId = a.PositionId
and (a.EndDate is null or a.EndDate > getdate())
left join Employee e
on a.EmployeeId = e.Id
where pd.Id = @PdId
END