﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 9/13/13
-- Description:	create new delegate app link for all active apps
-- =============================================
create PROCEDURE [dbo].[CreateApprovalDelegateAppLinkAllActive] 
	-- Add the parameters for the stored procedure here


	@DelegateId int
	
AS
BEGIN


		INSERT ApprovalDelegateApp
			   (DelegateId,
			   AppId)
		Select @DelegateId,
			   Apps.Id
			   from ApprovalApp Apps
			   where Apps.IsActive = 1
			   

			   
END