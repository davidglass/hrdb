﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-08-19
-- Description:	TODO: expand this to allow different params for filtering...
-- yes, this is similar to GetParList, could probably be combined.
-- =============================================
CREATE PROCEDURE [dbo].[GetOpenPars] 
	@PositionId int = null,
	@EmployeeId int = null
AS
BEGIN
	select
	par.Id,
	ac.Description as ActionDescription,
--	isnull(ar.Reason, 'n/a') as ActionType,
	ar.Reason as ActionType,
	EffectiveDate, EmpLastName
	from par
	join ActionCode ac
	on par.ActionCode = ac.Code
	left join ApptActionReason ar
	on par.ActionReasonId = ar.Id
	where
	(
	( PositionId = @PositionId and @EmployeeId is null)
	or 
	( EmployeeId = @EmployeeId and @PositionId is null)
	)
	and StatusId in (1,2,3) -- Created, Pending Approval, Approved
	order by EffectiveDate;
END