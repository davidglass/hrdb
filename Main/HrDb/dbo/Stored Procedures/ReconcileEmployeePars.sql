﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-07
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ReconcileEmployeePars] 
AS
BEGIN
begin tran
	-- set newly-generated HRMS personnel #s for new hires:
	update e
	set HrmsPersonId = hrp.Id
	from employee e
	join appointment a
	on e.id = a.EmployeeId
	and (a.EndDate is null or a.EndDate > getdate())
	join par p on p.EmployeeId = e.Id
	and p.ActionCode = 'U0'
	join StageHRMS.dbo.Person hrp
	on p.EmpSSN = hrp.SSN
	where e.HrmsPersonId = 0;

	-- now set new personnel #, redact SSN from Par:
	update p
	set EmpHrmsId = hrp.Id,
	EmpSSN = null,
	StatusId = 6 -- reconciled
	from
	par p join StageHRMS.dbo.Person hrp
	on p.EmpSSN = hrp.SSN
	where p.ActionCode = 'U0'
--	and p.StatusId in (4,5) -- Processed, Pushed
	and p.StatusId = 5 -- Pushed

	-- non-transfer-out Separations:
	update par
	set statusid = 6 -- reconciled
	from par
	join stagehrms.dbo.EmployeeAction xn
	on par.EmpHrmsId = xn.PersonId
	where par.actioncode = 'U5'
	and xn.ActionCode = 'U5'
--	and StatusId in (4,5) -- Processed, Pushed
	and StatusId = 5 -- Pushed

	-- Transfer Outs:
	update par set statusid = 6 -- Reconciled
	from par
	left join StageHRMS.dbo.Person p
	on par.EmpHrmsId = p.Id
	where actionreasonid = 81
	and IsTransferOut = 1
	and p.Id is null
--	and StatusId in (4,5) -- Processed, Pushed
	and StatusId = 5 -- Pushed

	-- Transfer Ins:
	update par
	set StatusId = 6 -- Reconciled
	from par
	join stagehrms.dbo.person p
	on par.EmpHrmsId = p.Id
	where statusid <> 6
	and ActionReasonId = 81
	and IsTransferIn = 1
--	and StatusId in (4,5) -- Processed, Pushed
	and StatusId = 5 -- Pushed

	-- Appointment Changes
	update par
	set statusid = 6 -- Reconciled
	from par
	join stagehrms.dbo.OrganizationalAssignment oa
	on par.PosHrmsId = oa.PositionId
	and par.FillLevel = oa.JobId
	and par.ContractTypeCode = oa.WorkContractTypeCode
	and oa.OrgKey = par.PosCostCenter
--	and StatusId in (4,5) -- Processed, Pushed
	and StatusId = 5 -- Pushed
	--and par.EffectiveDate = oa.EffectiveDate

commit;
END