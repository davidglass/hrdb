﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-10
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.GetOuBreadcrumbs 
	-- Add the parameters for the stored procedure here
	@Id int
AS
BEGIN
	--declare @Id int = 31008300;
	select os.id, ou.shortname
	from orgstructure os
	join OrgStructure n
	on n.Id = @Id
	and os.Id <> n.Id -- omit target OU
	and left(n.ancestry, datalength(os.ancestry)) = os.ancestry
	join orgunit ou
	on os.id = ou.id
	order by os.ancestry
END