﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-04-22
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[AddOrgStructureSubnodes] 
	@SuperNodeId int
AS
BEGIN
	declare @OrgUnitId int;
	--declare @SuperNodeId int = 31008202 -- "DIRECTOR" OrgUnit (aka CTS)
	insert into OrgStructure (Id, ParentId, Ancestry)

	--declare @SuperNodeId int = 31008265;
	--declare @SuperNodeId int = 31008202;
	(
	select ou.OrgUnitId, @SuperNodeId
	, oun.Ancestry + cast (row_number() over (
			order by ou.OrgUnitDescription
		) as binary(1)
	)
	from hrms.OrgStructure ou
	join OrgStructure oun
	on oun.Id = @SuperNodeId
	and ou.ParentOrgUnitId = oun.Id
	);
	--and ou.ParentOrgUnitId = @SuperNodeId
	--on ou.ParentOrgUnitId = @SuperNodeId

	-- now iterate over just-inserted subnodes:
	declare n_curs cursor local fast_forward read_only for
		select oun.Id
		from OrgStructure oun
		where ParentId = @SuperNodeId
		
	open n_curs;
	fetch next from n_curs into @OrgUnitId
	while @@fetch_status = 0 begin
		exec AddOrgStructureSubnodes @OrgUnitId;
		fetch next from n_curs into @OrgUnitId;
	end
	close n_curs;
	deallocate n_curs;
END