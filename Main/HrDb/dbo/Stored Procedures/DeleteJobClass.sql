﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-02-05
-- Description:	doing this as proc instead of using EF
--		to avoid having to pre-select record before deleting it.
-- =============================================
CREATE PROCEDURE DeleteJobClass
	@Id int
AS
BEGIN
	delete from JobClass where Id = @Id;
END