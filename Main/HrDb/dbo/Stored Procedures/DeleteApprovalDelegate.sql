﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 2013-08-20
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DeleteApprovalDelegate] 
	@Id int,
	@DeletePersonId int = null,
	@DeleteDate datetime = null
AS
BEGIN
	update ApprovalDelegate set Active = 0, DeletePersonId = @DeletePersonId, DeleteDate = @DeleteDate where Id = @Id
	--delete from ApprovalDelegateApp where DelegateId = @Id;
	--delete from ApprovalDelegate where Id = @Id;
END