﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-04-22
-- Description:	IN PROGRESS...note, former appointments are included (see Mizell and Cox, e.g.) in historical assignments...
-- TODO (current done, not history): omit former appointments, load Current Positions and Appts separately from historical ones.
-- TODO: set expiration of non-current appointments
-- TODO: nullable UserId param.  If null, lookup from LanId.  For easy faking/testing
-- =============================================
CREATE PROCEDURE [dbo].[GetOrgStructure]
	--@UserId int = 1, 
	@LanId varchar(50), 
	@rootId int = 31008202 -- entire agency default root
AS
BEGIN
--declare @UserId int = 1;
--declare @rootId int = 31008251; -- CSD, testing double-filled AD
--declare @rootId int = 31008255; -- ATS, for including parent OUs
--declare @rootId int = 31008202; -- ATS, for including parent OUs
--declare @LanId varchar(50) = 'DIS\DavidG';
declare @UserId int;
-- TODO: make GetUserId a scalar function instead of proc?
-- NO, functions can't insert...which is why it's a proc.
exec @UserId = GetUserId @UserName = @LanId;

with nodes (Id, Ancestry, Name, PositionCount, SubUnitCount, Depth, NodeType, Expanded, HrmsJobId, ClassCode, IsSupervisor, Incumbent, IncumbentFirst, IncumbentId, IsVacant)
as (
	select os.Id,
	os.Ancestry,
	ou.description as Name,
	dbo.GetOrgUnitPositionCount(ou.Id) as PositionCount,
	dbo.GetSubUnitCount(ou.Id) as SubUnitCount,
	datalength(os.ancestry) as Depth,
	'OrgUnit' as NodeType,
	--cast(1 as bit) as Expanded, -- TODO: real per-user expansion
	case when exists (
		select 1 from AppUser_ExpandedOrgNode ns
		where ns.AppUserId = @UserId
		and ns.OrgUnitId = ou.Id
	) then cast(1 as bit) else cast(0 as bit) end as Expanded,

	cast(0 as int) as HrmsJobId,
	'' as ClassCode,
	cast(0 as bit) as IsSupervisor,
	'' as Incumbent,
	'' as IncumbentFirst,
	null as IncumbentId,
	cast(null as bit) as IsVacant
	from
	orgstructure os
	join orgunit ou
	on os.id = ou.id
	and ou.id = @rootId
	-- union all here for recursive expansion
	-- union all
	-- select * from orgstructure os join nodes on os.ParentId = nodes.Id
	union all

	select os.Id,
	os.Ancestry,
	ou.description as Name,
	dbo.GetOrgUnitPositionCount(ou.Id) as PositionCount,
	dbo.GetSubUnitCount(ou.Id) as SubUnitCount,
	datalength(os.ancestry) as Depth,
	'OrgUnit' as NodeType,
	--cast(1 as bit) as Expanded, -- TODO: real per-user expansion
	case when exists (
		select 1 from AppUser_ExpandedOrgNode ns
		where ns.AppUserId = @UserId
		and ns.OrgUnitId = ou.Id
	) then cast(1 as bit) else cast(0 as bit) end as Expanded,
	0 as HrmsJobId,
	'' as ClassCode,
	cast(0 as bit) as IsSupervisor,
	'' as Incumbent,
	'' as IncumbentFirst,
	null as IncumbentId,
	cast(null as bit) as IsVacant
	from
	orgstructure os
	join nodes on os.ParentId = nodes.Id
	join orgunit ou
	on os.id = ou.id
	and exists (
		select 1 from AppUser_ExpandedOrgNode ns
		where ns.OrgUnitId = nodes.Id
		and ns.AppUserId = @UserId
	)
)
select * from nodes
-- trying to union in parent OU:
--union
--	select os.Id,
--	os.Ancestry,
--	ou.description as Name,
--	dbo.GetOrgUnitPositionCount(ou.Id) as PositionCount,
--	dbo.GetSubUnitCount(ou.Id) as SubUnitCount,
--	datalength(os.ancestry) as Depth,
--	'OrgUnit' as NodeType,
--	cast(1 as bit) as Expanded,
--	--case when exists (
--	--	select 1 from AppUser_ExpandedOrgNode ns
--	--	where ns.AppUserId = @UserId
--	--	and ns.OrgUnitId = ou.Id
--	--) then cast(1 as bit) else cast(0 as bit) end as Expanded,
--	0 as HrmsJobId,
--	'' as ClassCode,
--	cast(0 as bit) as IsSupervisor,
--	'' as Incumbent,
--	'' as IncumbentFirst,
--	null as IncumbentId,
--	cast(null as bit) as IsVacant
--	from
--	orgstructure os
----	join nodes on os.ParentId = nodes.Id
--	join orgunit ou
--	on os.id = ou.id
--	where os.Id = (select ParentId from OrgStructure where Id = @rootId)

union
	select p.Id,
	Ancestry,
	-- choose shorter of WorkingTitle or JobTitle:
	case when p.WorkingTitle is not null
		and len(p.WorkingTitle) < len(j.JobTitle)
		then p.WorkingTitle else j.JobTitle end as Name,
--	j.JobTitle as Name,
	0 as PositionCount,
	0 as SubUnitCount,
	datalength(Ancestry) + 1 as Depth,
	'Position' as NodeType,
	cast(0 as bit) as Expanded,
	p.JobId as HrmsJobId,
	--j.JobTitleLegacy as ClassCode,
	j.LegacyCode as ClassCode,
	dbo.IsSupervisor(p.Id) as IsSupervisor,
	--case when (select count(*) from Appointment where PositionId = p.Id
	--	and EndDate is null
	--) > 1 then 'MULTI'

	--select fe.LastName
	--from Employee fe
	--where fe.Id = (
	--	select EmployeeId
	--	from Appointment
	--	where PositionId = 
	--)

--	else e.LastName + ', ' + e.FirstName end as Incumbent,
	--else
	isnull(e.LastName, 
		(select
		case when fe.Id = (
			select EmployeeId
			from Appointment
			where PositionId = p.Id
			and EndDate is null and StartDate > getdate()
		) then fe.LastName + ': ' + (select cast(
			cast(datepart(m, StartDate) as varchar(255)) 
			+ '/' + cast(datepart(d, StartDate) as varchar(255)) 
			as varchar(255)
		)
			from Appointment where PositionId = p.Id
			and EndDate is null and StartDate > getdate())
		else fe.LastName end as LastName
		from Employee fe
		where fe.Id = (
			select EmployeeId
			from Appointment
			where PositionId = p.Id
			and (
				(EndDate = (
					select max(EndDate)
					from Appointment
					where PositionId = p.Id
				) and not exists (select 1 from appointment
				where positionid = p.Id and enddate is null) 
				)
				or ( -- future starts
					EndDate is null and StartDate > getdate()
				)
			)
		))
	)
	--end
	as Incumbent,
	--e.LastName as Incumbent,
	e.FirstName as IncumbentFirst,
	e.Id as IncumbentId,
	case when e.Id is null then cast (1 as bit) else cast(0 as bit) end as IsVacant
	from Position p
	--left join hrms.Job j
	left join JobClass j
	on p.JobId = j.Id
	join nodes n2
	on p.OrgUnitId = n2.Id
	and n2.Expanded = 1
	and p.IsAbolished = 0
	left join Appointment a on p.Id = a.PositionId
	and a.Id = (
		select Id from Appointment a2
		where a2.PositionId = a.PositionId
		and a2.StartDate = a.StartDate
		-- excluding future starts:
		and a2.StartDate < getdate()
		and (a2.EndDate is null or a2.EndDate > getdate())
	)
	left join Employee e on a.EmployeeId = e.Id
	--where a.Id is null
	----or (
	--	select 1 from Appointment
	--	where EndDate is null
	--	and PositionId = a.PositionId
	--)
order by Ancestry, Depth, IsVacant, IsSupervisor desc, Name, Incumbent, IncumbentFirst
END