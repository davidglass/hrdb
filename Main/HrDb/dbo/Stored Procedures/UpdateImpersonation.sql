﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-02-24
-- Description:	
-- =============================================
CREATE PROCEDURE UpdateImpersonation 
	-- Add the parameters for the stored procedure here
	@LanId varchar(50), 
	@ImpersonatedUserId int = null
AS
BEGIN
	--declare @LanId varchar(50) = 'dis\davidg';
	--declare @ImpersonatedUserId int = 158; -- KeriM
	--	declare @ImpersonatedUserId int = null;
	if @ImpersonatedUserId is null begin
		delete from LanImpersonation
		where LanId = @LanId;
	end
	else begin -- upsert:
		if not exists
			(select 1 from lanimpersonation where LanId = @LanId) begin
				insert into LanImpersonation (LanId, ImpersonatedLanId) (
					select @LanId, u.LanId
					from AppUser u
					where u.Id = @ImpersonatedUserId
				)
			end
		else begin
			update li
			set li.ImpersonatedLanId = u.LanId
			from LanImpersonation li
			join AppUser u
			on
			li.LanId = @LanId
			and
			u.Id = @ImpersonatedUserId;
		end
	END
END