﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-08
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[LoadAppUsersNew] 
AS
BEGIN
	insert into appuser (LanId, EmployeeId, RoleId, ObjectGuidHexString)
	(select 'DIS\' +left(userprincipalname, patindex('%@%', userprincipalname)-1) as LanId,
	e.Id as EmployeeId,
	case when p.IsSupervisor = 1 then 3 else 4 end as RoleId,
	ad.objguidstring as ObjectGuidHexString
	from Employee e
	join Appointment a
	on e.id = a.EmployeeId
	and (a.enddate is null or a.enddate > getdate())
	join Position p
	on a.PositionId = p.Id
	left join appuser u
	on e.id = u.employeeid
	left join ADExtract ad
	on e.LastName = ad.sn
	and ad.company = 'CTS'
	left join appuser u2
	on u2.LanId = 'DIS\' +left(userprincipalname, patindex('%@%', userprincipalname)-1)
	where
	substring(userprincipalname, patindex('%@%', userprincipalname) + 1, 6) = 'dis.wa' -- dis.wa.gov + dis.wa.lcl
	and company = 'CTS'
	and u.Id is null
	and u2.Id is null)
END