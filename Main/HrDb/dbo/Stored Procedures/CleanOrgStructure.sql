﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CleanOrgStructure]
AS
BEGIN
delete from hrms.orgstructure
  where level <> (select min(level)
       from hrms.orgstructure os where os.orgunitid = hrms.orgstructure.orgunitid)
END