﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 11/6/13
-- Description:	Get Employee of given lan id
-- =============================================
CREATE PROCEDURE [dbo].[GetEmployeeIdOfLanId]
	
	@lanId varchar(50)
AS
BEGIN

	--select  *
	
	-- from Employee 
	-- where lanid = @lanId 
	-- order by created desc
	select EmployeeId as Id from AppUser
	where LanId = @lanId
	
END