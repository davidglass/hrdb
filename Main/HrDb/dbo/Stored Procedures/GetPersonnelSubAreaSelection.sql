﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-06-19
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetPersonnelSubAreaSelection] 
AS
BEGIN
  select Code as Value,
  Description + ' (' + Code + ')' as [Text]
  from PersonnelSubArea
  order by Description
END