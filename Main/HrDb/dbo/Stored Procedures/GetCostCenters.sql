﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-02-06
-- Description:	
-- =============================================
CREATE PROCEDURE GetCostCenters 
AS
BEGIN
	select cc.Id,
	cc.IsActive,
	cc.DescShort,
	cc.DescLong,
	cc.ManagerEmpId,
	case when a2.Id is null then '[VACANT]'
	else m.LastName + ', ' + isnull(m.PreferredName, m.FirstName)
	end
	as ManagerName,
	count(case when p.Id is not null then 1 end) as Positions,
	count(case when p.Id is not null and a.Id is null then 1 end) as Vacancies
	from
	CostCenter cc
	left join position p
	on p.CostCenter = cc.Id
	and p.IsAbolished = 0
	left join appointment a
	on p.Id = a.PositionId
	and (a.EndDate > getdate() or a.EndDate is null)
	left join Employee m on cc.ManagerEmpId = m.Id
	left join Appointment a2 on a2.EmployeeId = m.Id
	and (a2.EndDate is null or a2.EndDate > getdate())
	group by cc.Id, cc.IsActive, cc.DescShort, cc.DescLong, cc.ManagerEmpId, m.LastName, m.FirstName, m.PreferredName, a2.Id
	order by
	cc.IsActive desc,
	Positions desc,
	DescShort
END