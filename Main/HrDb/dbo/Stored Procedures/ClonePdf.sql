﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-05-30
-- Description:	clone PositionDescription to new deep copy,
-- including related PositionDuties.
-- =============================================
CREATE PROCEDURE [dbo].[ClonePdf] 
	-- Add the parameters for the stored procedure here
	@PdfId int, 
	@LanId varchar(50)
AS
BEGIN
	--declare @PdfId int = 1038;
	--declare @LanId varchar(50) = 'DIS\DavidG';

	SET NOCOUNT ON;
	begin try
		declare @UserId int;
		declare @Id int;
		begin tran
			exec @UserId = GetUserId @UserName = @LanId
			-- get ID in advance for new PositionDescription record:
			select @Id = NEXT VALUE FOR [dbo].[PositionDescription_seq];
			-- INSERT Pdf clone FIRST, THEN Duties
			insert into PositionDescription (
				Id,
				PositionId,
				ProposedJobId,
				PositionObjective,
				WorkingTitle,
				LastUpdatedByUserId,
				LastUpdatedAt,
				LegacyStatus
			)
			(select
				@Id,
				PositionId,
				ProposedJobId,
				PositionObjective,
				WorkingTitle,
				@UserId as LastUpdatedByUserId,
				getdate() as LastUpdatedAt,
				'cloned from Description #' + cast(@PdfId as varchar) as LegacyStatus
			from PositionDescription
			where Id = @PdfId
			);

			-- now clone duties:
			insert into PositionDuty (Id, PositionDescriptionId, Duty, TasksHtml, TimePercent)
			(	select
				NEXT VALUE FOR [dbo].[PositionDuty_seq],
				@Id,
				pd.Duty,
				pd.TasksHtml,
				pd.TimePercent
				from PositionDuty pd
				where pd.PositionDescriptionId = @PdfId
			);
		commit tran
		select pd.Id,
			ProposedJobId,
			jc.LegacyCode as ProposedClassCode,
			jc.JobTitle as ProposedClassTitle,
			LastUpdatedByuserId,
			WorkingTitle,
			LegacyStatus
			from PositionDescription pd
			join JobClass jc on pd.ProposedJobId = jc.Id
			where pd.Id = @Id;
	end try
	begin catch
		rollback;
		declare @errmsg nvarchar(255);
		select @errmsg = ERROR_MESSAGE();
		raiserror('processing error: %s',11,1,@errmsg) with seterror;
		return;
	end catch
END