﻿
-- =============================================
-- Author:		Mark Paul
-- Create date: 6/4/13
-- Description:	Get specific attachment to download
-- =============================================
CREATE PROCEDURE [dbo].[GetAttachment]
	-- Add the parameters for the stored procedure here
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT  
		[Id],
		[FileName],
		[SizeInBytes],
		[Description],
		[MimeType],
		[Content] 
	FROM [Attachment]
	where [Id] = @Id
END