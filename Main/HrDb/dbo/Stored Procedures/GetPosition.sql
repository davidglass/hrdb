﻿
-- =============================================
-- Author:		Mark Paul
-- Create date: 8/22/13
-- Description:	Get specific Position
-- =============================================
create PROCEDURE [dbo].[GetPosition]
	-- Add the parameters for the stored procedure here
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT  
		*
	FROM Position
	where [Id] = @Id
END