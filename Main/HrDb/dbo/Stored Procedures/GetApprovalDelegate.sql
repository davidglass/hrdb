﻿
-- =============================================
-- Author:		Mark Paul
-- Create date: 8/20/13
-- Description:	Get specific approval delegate
-- =============================================
create PROCEDURE [dbo].[GetApprovalDelegate]
	-- Add the parameters for the stored procedure here
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT  
		*
	FROM ApprovalDelegate
	where [Id] = @Id
END