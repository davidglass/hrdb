﻿
-- =============================================
-- Author:		Mark Paul
-- Create date: 8/21/13
-- Description:	Get all tasks associated with workflow
-- =============================================
create PROCEDURE [dbo].[GetApprovalWorkflowTasks]
	-- Add the parameters for the stored procedure here
	@WorkflowId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT  
		Id,
		ApproverPositionId,
		ApproveGroupId,
		StatusCodeId [StatusCode],
		TaskCodeId [TaskCode],
		FinalApproverPersonId,
		WorkflowOrder,
		CompletedDate,
		Comment
	FROM ApprovalTask
	where ApprovalWorkflowId = @WorkflowId
END