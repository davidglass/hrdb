﻿CREATE PROCEDURE [dbo].[GetWorkScheduleRules]
AS
BEGIN
	select hrmscode as [Value],
	description + ' [' + hrmscode + ']' as [Text]
--	from StageHRMS.dbo.dishrms_WorkSchedule
	from hrms.WorkSchedule
END