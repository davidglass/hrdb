﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-06-03
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[LoadEmployeeDateSpec] 
AS
BEGIN
insert into EmployeeDateSpec
select
	e.Id as EmployeeId,
--	ds01.personnelnumber,
	ds01.eventdate as ds01,
	ds02.eventdate as ds02,
	ds03.eventdate as ds03,
	ds04.eventdate as ds04,
	ds05.eventdate as ds05,
	ds07.eventdate as ds07,
	ds09.eventdate as ds09,
	ds18.eventdate as ds18,
	ds26.eventdate as ds26,
	ds27.eventdate as ds27

from Employee e
join StageHRMS.dbo.PersonEvent ds01
on ds01.PersonId = e.HrmsPersonId
and ds01.EventTypeCode = '01'
join Appointment a
on e.Id = a.EmployeeId
and a.EndDate is null

left join StageHRMS.dbo.PersonEvent ds02
on ds01.PersonId = ds02.PersonId
and ds02.EventTypeCode = '02'

left join StageHRMS.dbo.PersonEvent ds03
on ds01.PersonId = ds03.PersonId
and ds03.EventTypeCode = '03'

left join StageHRMS.dbo.PersonEvent ds04
on ds01.PersonId = ds04.PersonId
and ds04.EventTypeCode = '04'

left join StageHRMS.dbo.PersonEvent ds05
on ds01.PersonId = ds05.PersonId
and ds05.EventTypeCode = '05'

left join StageHRMS.dbo.PersonEvent ds07
on ds01.PersonId = ds07.PersonId
and ds07.EventTypeCode = '07'

left join StageHRMS.dbo.PersonEvent ds09
on ds01.PersonId = ds09.PersonId
and ds09.EventTypeCode = '09'

left join StageHRMS.dbo.PersonEvent ds18
on ds01.PersonId = ds18.PersonId
and ds18.EventTypeCode = '18'

left join StageHRMS.dbo.PersonEvent ds26
on ds01.PersonId = ds26.PersonId
and ds26.EventTypeCode = '26'

left join StageHRMS.dbo.PersonEvent ds27
on ds01.PersonId = ds27.PersonId
and ds27.EventTypeCode = '27'

order by employeeid

END