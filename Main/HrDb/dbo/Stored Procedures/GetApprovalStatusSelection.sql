﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-09-05
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetApprovalStatusSelection] 
AS
BEGIN
  select cast (Id as varchar(9)) as Value,
  Name as [Text]
  from ApprovalStatusCode
  order by Name
END