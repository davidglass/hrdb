﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-05-05
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetPositionDescriptions] 
	-- Add the parameters for the stored procedure here
	@PositionId int
AS
BEGIN
--declare @PositionId int = 5078;
	select pd.Id,
	ProposedJobId,
	pd.WorkingTitle,
	j.LegacyCode as ProposedClassCode,
	j.JobTitle as ProposedClassTitle,
	pd.LastUpdatedAt,
	pd.EffectiveDate as ApprovalDate, -- TODO: make sure this is importing right from approvals...
	pd.LegacyStatus
	from PositionDescription pd
	left join JobClass j -- DG changed to left join for when no JC is set.
	on pd.ProposedJobId = j.Id
	where PositionId = @PositionId
	order by pd.id desc;
END