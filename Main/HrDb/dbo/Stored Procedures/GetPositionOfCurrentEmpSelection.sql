﻿
-- =============================================
-- Author:		Mark Paul
-- Create date: 2013-11-21
-- Description:	option list for current employees with position as value
-- =============================================
CREATE PROCEDURE [dbo].[GetPositionOfCurrentEmpSelection] 
AS
BEGIN
	select cast(p.id as varchar) as Value,
	isnull(e.LastName,
		'VACANT' +
			case when exists (select 1 from appointment where positionid = p.id)
			then ', ' +
			(select e.LastName
			from Appointment a
			join Employee e
			on a.EmployeeId = e.Id
			and a.PositionId = p.Id
			and a.EndDate = (select max(EndDate) from Appointment where PositionId = p.Id)
			)
			else ', Pos Id: ' + cast(p.id as varchar) end
	)
	+ case when a.Id is null then '' else ', ' + isnull(e.PreferredName, e.FirstName) end
	as [Text]
	from position p
	left join Appointment a	on p.Id = a.PositionId
	and (a.EndDate is null or a.EndDate > getdate())
	left join Employee e on e.Id = a.EmployeeId
	where IsAbolished = 0
	order by 
	isNull(e.LastName, 'zz'), --add to put vacant at buttom of list
	[text]
END

	--select cast(a.PositionId as varchar(10)) as Value,
	--e.LastName + ', ' + isnull(e.PreferredName, e.FirstName) as [Text]
	--from Employee e
	--join Appointment a
	--on e.Id = a.EmployeeId
	--and a.EndDate is null
	--order by e.LastName, e.FirstName