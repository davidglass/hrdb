﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 8/21/13
-- Description:	save data on approvalTask
-- =============================================
create PROCEDURE [dbo].[SaveApprovalTask] 
	-- Add the parameters for the stored procedure here

	@Id int,
	@StatusCodeId int,
	@TaskCodeId int,
	@PositionId int = null,
	@ApproveGroupId int = null,
	@Comment text,
	@WorkflowOrder int,
	@FinalApproverPersonId int = null,
	@CompletedDate datetime = null
	
AS
BEGIN


		UPDATE ApprovalTask
		set
			StatusCodeId = @StatusCodeId,
			TaskCodeId = @TaskCodeId,
			ApproverPositionId = @PositionId,
			ApproveGroupId = @ApproveGroupId,
			Comment = @Comment,
			WorkflowOrder = @WorkflowOrder,
			FinalApproverPersonId = @FinalApproverPersonId,
			CompletedDate = @CompletedDate
		where Id = @Id

			   
END