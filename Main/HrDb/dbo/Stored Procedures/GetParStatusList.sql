﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-08-27
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[GetParStatusList] 
AS
BEGIN
	select cast(Id as varchar(10)) as Value, Description as [Text]
	from ParStatus
	order by Description
END