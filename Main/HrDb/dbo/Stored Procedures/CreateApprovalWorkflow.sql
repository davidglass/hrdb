﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 8/14/13
-- Description:	add a new Location (room) and return its Id
-- =============================================
CREATE PROCEDURE [dbo].[CreateApprovalWorkflow] 
	-- Add the parameters for the stored procedure here


	@StatusCodeId int,
	@ApprovalCodeId int,
	@AppId int,
	@PositionId int = null,
	@PersonId int = null,
	@CreatedDate datetime
	
AS
BEGIN


		INSERT INTO ApprovalWorkflow
			   (StatusCodeId,
			   ApprovalCodeId,
			   AppId,
			   PositionId,
			   PersonId,
			   CreatedDate)
		VALUES
			   (@StatusCodeId,
			   @ApprovalCodeId,
			   @AppId,
			   @PositionId,
			   @PersonId,
			   @CreatedDate)
			   
		select top 1 Id from ApprovalWorkflow order by Id desc

			   
END