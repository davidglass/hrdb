﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-07
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[UpdateApptPay] 
AS
BEGIN
	update a
	set PayScaleTypeArea = pst.Id,
	PayScaleReason = pay.PayScaleReasonForChangeCode,
	PayScaleEffectiveDate = pay.EffectiveDate,
	PayScaleGroup = pay.PayScaleGroup,
	PayScaleLevel = pay.PayScaleLevel,
	PayAnnualSalary = pay.AnnualSalary
	from stagehrms.dbo.BasicPay pay
	join Employee e
	on pay.PersonId = e.HrmsPersonId
	join Appointment a
	on e.Id = a.EmployeeId
	and (a.EndDate is null or a.EndDate > getdate())
	join PayScaleTypeArea pst
	on pay.PayScaleType = pst.PsType
	and pay.PayScaleArea = pst.PsArea
	where pay.EffectiveDate > a.PayScaleEffectiveDate
END