﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-06-14
-- Description:	TODO: make this filterable?
-- =============================================
CREATE PROCEDURE [dbo].[GetParList]
@ActionCode varchar(4) = '',
@OrgUnitId int = 0,
@WorkflowStatusId int = 0,
@ParStatusId int = 0,
@FromDate datetime = null,
@ToDate datetime = null,
@Query varchar(255) = '',
@EmployeeId int = 0,
@LanId varchar(50)
AS
BEGIN
--set statistics time on;
--declare @ActionCode varchar(4) = 'U3-F';
--declare @OrgUnitId int = 0;
--declare @WorkflowStatusId int = 0;
--declare @ParStatusId int = 0;
--declare @FromDate datetime = '1900-01-01';
--declare @ToDate datetime = getdate();
--declare @Query varchar(255);
--declare @EmployeeId int = 0;
--declare @LanId varchar(50) = 'dis\davidg';

  -- TODO: union Appointment Action Requests with other kinds of Action Requests for PAR list...
  -- [DONE]: make ActionReasonId not null, FK
  --update ApptActionRequest
  --set ActionReasonId = 61 -- Probationary
--  select * from par where id = 199;

	select r.Id,
	r.CreateDate,
	wfs.Name as WorkFlowStatusDesc,
	wf.StatusCodeId as WorkflowStatusId,
		--r.ActionReasonId,
--		ac.Description + ': ' + ar.Reason as ActionType,
		r.ActionCode,
		case when r.IsTransferOut = 1 then 'Separation'
		when r.IsTransferIn = 1 then 'Fill, Transfer In'
		when r.IsPayChangeOnly = 1 then 'Pay Change'
		when r.IsNameChangeOnly = 1 then 'Name Change'
		else ac.Description end as ActionDescription,
		ar.Reason as ActionType,
		r.EmployeeId,
		isnull(EmpLastName, '') +
			case when EmpLastName is null then '' else ', ' end + 
			case when EmpFirstName is null then '' else left(EmpFirstName, 1) + '.' end
			as EmpLastFirst,
		EmpFirstName, EmpLastName,
		r.PositionId,
		r.PosOrgUnitId,
		--ocou.OrgUnitName as OrgUnitShortName,
		dbo.GetParDisplayOuName(r.PosOrgUnitId) as OrgUnitShortName,
--		ou.ShortName,
--		ou.ShortName as OrgUnitShortName,
--		'TODO' as Originator,
		au.LanId as Originator,
		--'TODO' as WorkflowStatus,
		ps.Description as ParStatusDesc,
		EffectiveDate,

		case when wf.StatusCodeId = 1 then isnull(dbo.GetWaitingOn(r.ApprovalWorkflowId), 'ACTIVE')
		else wfs.Name end as ApprovalStatusDesc
		--WaitingOn

--	from ApptActionRequest r
	from Par r
	join ActionCode ac
--	on r.ActionCode = ac.Code
	on r.ActionCode = ac.Code
	join ParStatus ps on StatusId = ps.Id
	--join Employee e
	--on r.EmployeeId = e.Id
	-- converted Position, OrgUnit to left join to enable new Positions:
	left join Position p
	on PositionId = p.Id

	-- TODO: return all positions for HR/admin roles...
	-- join dbo.GetSpanOfControl(@LanId) span
	-- on span.Id = p.Id

	left join OrgChartOrgUnit ocou
	on r.PosOrgUnitId = ocou.OrgUnitId
--	on p.OrgUnitId = ocou.OrgUnitId
	left join OrgUnit ou
--	on p.OrgUnitId = ou.Id
	on r.PosOrgUnitId = ou.Id
	join AppUser au
	on RequestedByUserId = au.Id
	left join ApptActionReason ar
	on r.ActionReasonId = ar.Id
	left join ApprovalWorkflow wf
	on r.ApprovalWorkflowId = wf.Id
	left join ApprovalStatusCode wfs
	on wf.StatusCodeId = wfs.Id

	-- filter on ActionCode if provided:
	where (@ActionCode = ''
		or (@ActionCode = r.ActionCode and @ActionCode <> 'U5' and r.IsTransferOut = 0 and r.IsTransferIn = 0 and r.IsPayChangeOnly = 0)
		or (@ActionCode = 'U5' and (r.ActionCode = 'U5'	or r.IsTransferOut = 1))
		or (@ActionCode = 'U3-F' and (r.ActionCode = 'U3' and r.IsTransferIn = 1))
		or (@ActionCode = 'U3-P' and (r.ActionCode = 'U3' and r.IsPayChangeOnly = 1))
	)
	and (@EmployeeId = 0 or @EmployeeId = r.EmployeeId)
	and (@OrgUnitId = 0 or @OrgUnitId = r.PosOrgUnitId)
--	and (@ParStatusId = 0 or @ParStatusId = r.StatusId)
-- including all statuses now...just block 4+ from re-processing.
	--and r.StatusId in (1,2,3,6) -- exclude Processed, Reconciled; they can't be reversed.
	and (@WorkflowStatusId = 0 or @WorkflowStatusId = wf.StatusCodeId)
	and (@FromDate is null or r.CreateDate >= @FromDate)
	and (@ToDate is null or r.CreateDate < dateadd(d, 1, @ToDate))
-- TODO: handle @q (query).  check LastName, FirstName, ParId, PositionId.
	and (@Query is null
		or @Query = ''
		or EmpLastName like @Query + '%' -- wildcard at end only
		or EmpPreferredName like @Query + '%'
		or try_convert(int, @Query) = r.Id -- ParId match
		or try_convert(int, @Query) = r.PositionId
	)
	and (
		dbo.CanReadAll(@LanId, 'Par') = 1
		or -- else only span of control.
		r.PositionId in (
			select Id from dbo.GetSpanOfControl(@LanId)
		)
	)
	order by case when EffectiveDate is null then 2 else 1 end, EffectiveDate
END