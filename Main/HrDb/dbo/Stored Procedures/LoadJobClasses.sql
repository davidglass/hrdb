﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-01-31
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[LoadJobClasses] 
AS
BEGIN
--truncate table JobClass; can't truncate FK
delete from jobclass;
SET IDENTITY_INSERT JobClass ON
insert into JobClass (Id, HrmsJobId, LegacyCode, JobTitle, IsActive, MaxAnnualSalary)
(
	select distinct JobId, JobId, JobTitleLegacy, JobTitle, cast(1 as bit),
	case when personnelsubarea in ('0002','0003') -- wms/exempt
	then cast(maxsalary as numeric)/100
	else cast(maxsalary as numeric)*24/100
	end as MaxAnnualSalary
	from StageHRMS.dbo.Position p
	where p.id <> 71027332 -- exclude DeTray's hourly position
)
SET IDENTITY_INSERT JobClass OFF
exec LoadUnusedJobClasses;
END