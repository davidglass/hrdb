﻿

-- =============================================
-- Author:		D. Glass
-- Create date: 2014-11-17
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[CreatePdFromPar] 
	@PositionId int = null, -- TODO: create approval chain from @LanId if no PositionId...
	@LanId varchar(50),
	-- next 6 items added versus CreatePdWithApproval...
	@LinkedParId int,
	@ProposedJobId int,
	@WorkingTitle varchar(50) = null,
	@PositionTypeCXW char(1) = 'C', -- not null
	@wxProposedJvacCode varchar(10) = null,
	--@xProposedJvacEvalPts int = null,
	@IsSupervisor bit = 0 -- not null
AS
BEGIN
--declare @PositionId int = 5548;
--declare @LanId varchar(50) = 'DIS\DavidG';
	declare @Id int;
	select @Id = NEXT VALUE FOR [dbo].[PositionDescription_seq];
	begin try
		begin tran
			declare @userId int;
			--declare @WfId int; -- workflow Id;

			-- GetUserId is a proc instead of a function so it can insert missing users:
			exec @userId = GetUserId @UserName = @LanId;
			--exec @WfId = CreatePdApprovalWorkflow @PositionId = @PositionId;

			insert into PositionDescription (
				Id,
				PositionId,
				xCurrentJobId,
				ProposedJobId,
				WorkingTitle,
				LastUpdatedByUserId,
				LegacyStatus,
				TopJobClassSupervised,
				TotalFTESupervised,
				DirectFteSupervised,
				PositionTypeCXW,
				IsSupervisor,
--				xProposedJvacEvalPts,
				wxCurrentJvacCode,
				wxProposedJvacCode,
				xCurrentBand,
				xProposedBand,
				LinkedParId
			)
			select
			@Id,
			p.Id as PositionId,
			jc.Id as xCurrentJobId, -- default to current job class
			@ProposedJobId as ProposedJobId,
			dbo.IsNullOrEmpty(@WorkingTitle, p.WorkingTitle),
			@userId,
			-- TODO: create Status column, independent of LegacyStatus (used only for import)
			-- or possibly modify ParStatus lookup to be shared among records (e.g. "RecordStatus")?
			'New' as LegacyStatus,
			dbo.GetTopSupervisedJobId(p.Id) as TopJobClassSupervised,
			(select cast(count(*) as numeric(5,2))
			from PositionStructure ps
			join PositionStructure subps
			on left(subps.Ancestry, datalength(ps.Ancestry)) = ps.Ancestry
			and datalength(subps.Ancestry) > datalength(ps.Ancestry)
			and ps.Id = p.Id) as TotalFteSupervised,
			-- this only counts direct reports, should count all (separate field?)
			(select cast(count(*) as numeric(4,2)) from position where SupervisorPositionId = p.Id)
				as DirectFteSupervised,
			@PositionTypeCXW
			--case when p.PersonnelSubArea = '0003' then 'X' -- eXempt
			--when p.PersonnelSubArea = '0002' then 'W' -- WMS
			--else 'C' end -- Classified
			as PositionTypeCXW,
			isnull(@IsSupervisor, p.IsSupervisor),
			--isnull(@xProposedJvacEvalPts, p.PointValue),
			p.Jvac as wxCurrentJvacCode,
			isnull(@wxProposedJvacCode, p.Jvac),
			p.PayGrade as xCurrentBand,
			p.PayGrade as xProposedBand,
			@LinkedParId
			from Position p
			join JobClass jc
			on p.JobId = jc.Id
			where p.Id = @PositionId;

				insert into PositionDuty (PositionDescriptionId, Duty, TasksHtml, TimePercent)
	values (@Id, 'Default Duty', '<ul><li>Sample Task 1</li><li>Sample Task 2</li></ul>', 0);

		commit tran;
	end try
	begin catch
		rollback;
		declare @errmsg nvarchar(255);
		select @errmsg = ERROR_MESSAGE();
		raiserror('data error: %s',11,1,@errmsg) with seterror;
		return;
	end catch;

	-- this populates PositionDescriptionRow entity (summary in list view), so not returning full record.
	select pd.Id,
	ProposedJobId,
	pd.WorkingTitle,
	j.LegacyCode as ProposedClassCode,
	j.JobTitle as ProposedClassTitle,
	pd.EffectiveDate as ApprovalDate, -- TODO: make sure this is importing right from approvals...
	pd.LastUpdatedAt,
	pd.LegacyStatus
	from PositionDescription pd
	join JobClass j
	on pd.ProposedJobId = j.Id
	where pd.Id = @Id;
END