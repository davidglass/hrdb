﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-07
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetUnreconciledPars] 
AS
BEGIN
	select * from par
	where statusid in (4,5)
	--and effectivedate < dateadd(MONTH, -1, getdate())
	order by effectivedate;
END