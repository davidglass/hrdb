﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-08-26
-- Description:	Search
-- =============================================
CREATE PROCEDURE [dbo].[Search] 
	-- Add the parameters for the stored procedure here
	@Entity varchar(50) = null, 
	@Query varchar(50) = null,
	@IncludeAbolished bit = 0,
	@IncludeHistory bit = 0
AS
BEGIN
--declare @Query varchar(50) = '5078'
--declare @Query varchar(50) = '6084' -- "VACANT Paul"

--declare @Query varchar(50) = '5156';
----declare @Query varchar(50) = 'Tim';
--declare @IncludeHistory bit = 1;
--declare @IncludeAbolished bit = 0;

	select
		p.Id as PositionId,
		p.IsAbolished,
		e.Id,
		isnull(e.PreferredName, e.FirstName) as FirstName,
		e.LastName,
		a.StartDate,
		a.EndDate,
		p.OrgUnitId,
		isnull(p.WorkingTitle, p.JobTitle) as Title,
--		ou.ShortName as OuShortName
		ou.Description as OuDesc
	from Position p
	left join Appointment a	on a.PositionId = p.Id
	left join Employee e on a.EmployeeId = e.Id
	and
		((a.EndDate is null or a.EndDate > getdate())
		or
		@IncludeHistory = 1
		)
	left join OrgUnit ou
	on p.OrgUnitId = ou.Id
	where
	(
		@IncludeAbolished = 1
		or
		p.IsAbolished = 0
	)
	and (
		PreferredName like '%' + @Query + '%'
		or FirstName like '%' + @Query + '%'
		or LastName like '%' + @Query + '%'
		or ((PositionId = try_convert(int, @Query)
		and	(( a.EndDate is null or a.EndDate > getdate())
			or
			@IncludeHistory = 1
		)))
		or (
			PositionId = try_convert(int, @Query)
			and not exists( -- this is for selecting vacant positions:
				select 1 from appointment a
				where a.PositionId = try_convert(int, @Query)
				and EndDate is null or EndDate > getdate()
			)
		)
	)

END