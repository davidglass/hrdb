﻿
-- =============================================
-- Author:		Mark Paul
-- Create date: 4/16/13
-- Description:	add a new Attachment and return its Id
-- =============================================
CREATE PROCEDURE [dbo].[AddAttachment] 
	-- Add the parameters for the stored procedure here

	@FileName varchar(max),
	@Description text,
	@Content varbinary(max),
	@SizeInBytes int,
	@MimeType varchar(100),
	@AppointmentId int -- this should be ParId
--	@ParId int
	
AS
BEGIN


		INSERT INTO Attachment
			   ([FileName],
			   [Description],
			   Content,
			   SizeInBytes,
			   MimeType)
		VALUES
			   (@FileName,
			   @Description,
			   @Content,
			   @SizeInBytes,
			   @MimeType)

		INSERT INTO AttachmentApptRequest
		(AttachmentId, 
		ApptRequestId)
		Values
		((select top 1 Id from Attachment order by Id desc),
		@AppointmentId)

			   
		select top 1 Id from Attachment order by Id desc

			   
END