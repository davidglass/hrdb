﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-02-21
-- Description:	
-- =============================================
CREATE PROCEDURE GetImpersonatedUserId
	@LanId varchar(50)
AS
BEGIN
	select u.Id as ImpersonatingUserId
	from LanImpersonation i
	join AppUser u
	on i.LanId = @LanId
	and u.LanId = i.ImpersonatedLanId;
END