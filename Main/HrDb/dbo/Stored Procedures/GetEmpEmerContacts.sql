﻿-- =============================================
-- Author:		P.McKusick
-- Create date: 08/26/2013
-- Description:	Get Employee Emergency Contact Info
-- =============================================
CREATE PROCEDURE [dbo].[GetEmpEmerContacts] @EmpId int
AS
BEGIN


select 
e.Id
,case when ec.Member = 7 then 'Emergency contact' when ec.Member is null then 'Unlnown' else ec.Member end [Member]
,case when ec.LastName is null then 'None' else ec.LastName end [LastName]
,case when ec.FirstName is null then 'None' else ec.FirstName end [FirstName] 
,case when ec.TelephoneNumber is null then 'None' else ec.TelephoneNumber end [TelephoneNumber]

from [dbo].[Employee] AS e
--LEFT JOIN [StageHRMS].dbo.[EmergencyContact] AS ec ON ec.PersonId = e.HrmsPersonId
-- no production joins to stage dbs, please.
LEFT JOIN hrms.[EmergencyContact] AS ec ON ec.PersonId = e.HrmsPersonId
where e.Id = @EmpId
END