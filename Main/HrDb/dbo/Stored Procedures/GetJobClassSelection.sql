﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-06-10
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetJobClassSelection] 
AS
BEGIN

--select distinct cast(id as varchar) as Value,
--rtrim(jobtitlelegacy) + ' - ' + jobtitle as [Text]
--from hrms.Job
--order by [Text]

select cast(id as varchar) as [Value],
LegacyCode + ' - ' + JobTitle as [Text]
from JobClass
where IsActive = 1
order by [Text]

END