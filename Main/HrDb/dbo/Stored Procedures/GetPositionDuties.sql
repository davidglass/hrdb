﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-05-05
-- Description:	
-- =============================================
CREATE PROCEDURE GetPositionDuties 
	@PositionDescriptionId int
AS
BEGIN
	select Id, PositionDescriptionId,
	Duty, TasksHtml, TimePercent
	from PositionDuty where PositionDescriptionId = @PositionDescriptionId
END