﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 10/10/13
-- Description:	Get list of all employees curently delegated to task
-- =============================================
CREATE PROCEDURE [dbo].[GetEmployeesDelegatedToTask]
	@TaskId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT  
		Employee.Id [Id],
		Employee.PreferredName [PreferredName],
		Employee.LastName [LastName],
		Employee.EmailAddress [Email]
	FROM ApprovalTask
	left outer join ApprovalWorkflow on ApprovalWorkflow.Id = ApprovalTask.ApprovalWorkflowId
	left outer join ApprovalDelegate on ApprovalDelegate.DelegatedPositionId = ApprovalTask.ApproverPositionId 
	left outer join Employee on ApprovalDelegate.DelegatedToPersonId = Employee.Id
	left outer join ApprovalDelegateApp on ApprovalDelegateApp.DelegateId = ApprovalDelegate.Id
	where ApprovalTask.Id = @TaskId
	and ApprovalDelegateApp.AppId = ApprovalWorkflow.AppId
	and (ApprovalDelegate.Indefinite = 1 or ApprovalDelegate.EndDate >= DATEADD(day, -1, getdate())) --subtract day so that end date of current date is still active
	and ApprovalDelegate.StartDate <= GETDATE()
	and ApprovalDelegate.Active = 1
	
	
	 

END