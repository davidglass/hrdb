﻿
-- =============================================
-- Author:		Mark Paul
-- Create date: 6/4/13
-- Description:	Get specific attachment to download
-- 2013-07-03 DG: copied to GetParAttachment.
-- =============================================
CREATE PROCEDURE [dbo].[GetParAttachment]
	@Id int
AS
BEGIN
    SELECT  
		[Id],
		[FileName],
		[SizeInBytes],
		[Description],
		[MimeType],
		[Content] 
	FROM [ParAttachment]
	where [Id] = @Id
END