﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-02
-- Updated 6/3/13 by Mark Paul
-- Description:	sample query for feeding search grid
-- (simple search on @q query string match in first or last name)
-- =============================================
CREATE PROCEDURE [dbo].[GetPositionAppointments]
	@q varchar(255) = null
AS
BEGIN
	select
	e.Id as EmpId, isnull(LastName, '[VACANT]') as LastName, FirstName,
	p.Id as PosId,
	p.OrgUnitId,
	isnull(unit.[Description], '') as OrgUnitDesc,
	isnull(j.JobTitle, '') as JobTitle,
	a.StartDate,
	a.EndDate,
	cast((case when a.Id is null then 1 else 0 end) as bit) as IsVacant
	from Position p
	left join OrgUnit unit
	on p.OrgUnitId = unit.Id
	left join Appointment a
	on p.Id = a.PositionId
	left join Employee e
	on a.EmployeeId = e.Id
	--left join hrms.Job j
	left join JobClass j
	on p.JobId = j.Id
	where @q is null
	or e.LastName like '%' + @q + '%'
	or e.FirstName like '%' + @q + '%'
	or p.Id = try_parse(@q as int)
	order by case when a.Id is null then 2 else 1 end, LastName
END