﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-12-06
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SetContractEndDates] 
AS
BEGIN
	update a
	set ContractEndDate = 
	case when ContractTypeCode in ('02', '03', '04', '05', '10', '11', '12', '21', '22')
	then dateadd(MONTH, 6, eds.ds02)
	when ContractTypeCode = '23' then -- WMS Review
	dateadd(Y, 1, eds.ds02) end
	from Appointment a
	join EmployeeDateSpec eds
	on a.EmployeeId = eds.EmployeeId
END