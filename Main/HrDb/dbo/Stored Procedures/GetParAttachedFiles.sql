﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-06-26
-- Description:	list of attached files with binary content
-- =============================================
CREATE PROCEDURE [dbo].[GetParAttachedFiles] 
	@ParId int
AS
BEGIN
	select
	pa.Id as AttachmentId,
	--a.Id as AttachmentId,
	[FileName],
	Content
--	from AttachmentApptRequest pa -- ParAttachment
	from ParAttachment pa -- ParAttachment
	where pa.ParId = @ParId;
	--join Attachment a
	--on pa.AttachmentId = a.Id
	--and pa.ApptRequestId = @ParId;
END