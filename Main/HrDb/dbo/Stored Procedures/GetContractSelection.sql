﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-06-19
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetContractSelection] 
AS
BEGIN
  select WorkContractTypeCode as Value,
  WorkContractTypeDesc as [Text]
  from WorkContractType
  order by WorkContractTypeDesc
END