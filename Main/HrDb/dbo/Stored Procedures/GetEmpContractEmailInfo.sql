﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetEmpContractEmailInfo]
	@EmpId int
AS
BEGIN
	select
	e.Id as EmpId,
	e.PreferredName + ' ' + e.LastName  as EmpName,
	se.PreferredName + ' ' + se.LastName as SupervisorName,
	p.Id as PositionId,
	p.WorkingTitle,
	ct.WorkContractTypeDesc,
	eds.ds02 as ApptBegan,
	a.ContractEndDate
	from Appointment a
	join WorkContractType ct
	on a.ContractTypeCode = ct.WorkContractTypeCode
	join Position p
	on a.PositionId = p.Id
	and a.EmployeeId = @empid
	and (a.EndDate is null or a.EndDate > getdate())
	join Employee e on e.Id = @empid
	join EmployeeDateSpec eds on eds.EmployeeId = @empid
	join Appointment spa
	on p.SupervisorPositionId = spa.PositionId
	and (spa.EndDate is null or spa.EndDate > getdate())
	join Employee se
	on spa.EmployeeId = se.Id
END