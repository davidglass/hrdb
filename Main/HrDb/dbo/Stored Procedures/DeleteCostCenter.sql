﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2014-02-07
-- Description:	doing this as proc instead of using EF
--		to avoid having to pre-select record before deleting it.
-- =============================================
CREATE PROCEDURE [dbo].[DeleteCostCenter]
	@Id int
AS
BEGIN
	delete from CostCenter where Id = @Id;
END