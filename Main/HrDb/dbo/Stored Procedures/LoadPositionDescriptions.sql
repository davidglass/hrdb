﻿CREATE PROCEDURE [dbo].[LoadPositionDescriptions] 
AS
BEGIN

	IF EXISTS (SELECT * FROM sys.foreign_keys 
		WHERE object_id = OBJECT_ID(N'FK_PositionDuty_PositionDescription')
		AND parent_object_id = OBJECT_ID(N'dbo.PositionDuty'))
	BEGIN
		-- can't truncate table without DROPPING FK. (disabling doesn't work)
		alter table PositionDuty drop FK_PositionDuty_PositionDescription;
	END;

-- TODO: FK from duty to description:
truncate table positionduty;
truncate table positiondescription;

-- TODO: set TopJobClassSupervised, TotalFteSupervised separately...

insert into positiondescription
(Id, PositionId, ProposedJobId,
PositionObjective, EffectiveDate,
WorkingTitle,
LegacyStatus,
LastReviewDate,
--LastUpdatedByUserId,
LastUpdatedAt,
IsLead,
TotalFteSupervised,
TopJobClassSupervised,
IsSupervisor,
AssignsWork,
Instructs,
EvalsPerformance,
ChecksWork,
PlansWork,
Disciplines,
Hires,
Terminates,
SupervisorLeadAddInfo,
SupervisionAddInfo,
WorkingConditionsOther,
QualificationsRequired,
QualificationsPreferred,
SpecialRequirements,
InTrainingPlan,
PositionTypeCXW
)
select
pd.recid as Id,
pd.ObjectAbbreviation as PositionId,
--p.JobId as ProposedJobId,
-- this is creating false matches (e.g. Cowan):
isnull(j.Id, p.JobId) as ProposedJobId,
--j.Id as ProposedJobId, -- set null if no match with current JobClasses...
--ProposedClassTitleCode,
pd.PositionObjective,
apv.ApproveDisapproveDate as EffectiveDate,
pd.WorkingTitle,
pd.Status as LegacyStatus,
pd.DateLastReviewed as LastReviewDate,
--0 as LastUpdatedByUserId,
--pd.Lastupdatedby,
pd.LastUpdatedDate as LastUpdatedAt,
case when pdsv.LeadPosition = 'Y' then 1 else 0 end as IsLead,
isnull((select sum(cast(sub.hoursperweek as float))
	from StageLegacy.pdf.WGSSubordinate sub
	where sub.WGSRecid = pd.RecId) / 40, 0)
	as TotalFteSupervised,
(
select top 1 jobid from StageHRMS.dbo.Position p
where p.PositionTitleLeg in (
		select positionnum from stagelegacy.pdf.WGSSubordinate sub
		where sub.wgsrecid = pd.recid
	)
	and p.MaxSalary = (
		select max(maxsalary) from StageHRMS.dbo.Position p
		where p.PositionTitleLeg in (
			select positionnum from stagelegacy.pdf.WGSSubordinate sub
			where sub.wgsrecid = pd.RecId
		)
	)
) as TopJobClassSupervised,
case when pdsv.SupervisoryPosition = 'Y' then 1 else 0 end as IsLead,
case when pdsv.AssignsWork = 'Y' then 1 else 0 end as AssignsWork,
case when pdsv.Instructs = 'Y' then 1 else 0 end as Instructs,
case when pdsv.Evaluates = 'Y' then 1 else 0 end as EvalsPerformance,
case when pdsv.Corrects = 'Y' then 1 else 0 end as ChecksWork, -- TODO: verify this translation
0 as PlansWork, -- no mapping
case when pdsv.Disciplines = 'Y' then 1 else 0 end as Disciplines,
case when pdsv.Terminates = 'Y' then 1 else 0 end as Terminates,
case when pdsv.Hires = 'Y' then 1 else 0 end as Hires,
pdsv.AdditionalInfo as SupervisorLeadAddInfo,
pdsv.HowIsWorkAssigned + ' INTERACTS WITH ' + pdsv.PositionInteraction
as SupervisionAddInfo,
pd.WorkingConditions as WorkingConditionsOther,
pdg.RequiredEducation as QualificationsRequired,
pdg.DesiredEducation as QualificationsPreferred,
pd.SpecialRequirements as SpecialRequirements,
it.intrainStatement as InTrainingPlan,
'C' as PositionTypeCXW -- [C]lassified, e[X]empt, [W]MS

from StageLegacy.pdf.WGSPositionDesc pd
join StageLegacy.pdf.WGSSupervisory pdsv
on pd.RecId = pdsv.WGSRecID
join StageLegacy.pdf.WGSGeneral pdg
on pd.RecId = pdg.WGSRecid

join Position p
on pd.ObjectAbbreviation = p.LegacyPositionNum
-- DG: switched to left join to pull all PDs (approved or not):
left join StageLegacy.pdf.wgsapprovals apv
on apv.ApproverSeqNum = (select max(ApproverSeqNum)
	from StageLegacy.pdf.wgsapprovals apv2
	where apv2.WGSRecid = pd.recid)
and apv.ApproveDisapproveDate = (
	select max(ApproveDisapproveDate)
	from StageLegacy.pdf.wgsapprovals apv2
	where apv2.WGSRecid = pd.recid
	and apv2.ApproverSeqNum = apv.ApproverSeqNum
	)
and apv.wgsrecid = pd.recid
left join StageLegacy.par.intraining as it
--on it.RecId = pd.IntrainingID
on pd.parid = it.parid
and pd.parid <> 0
left join JobClass j
on j.LegacyCode = pd.ProposedClassTitleCode
where p.IsAbolished = 0
--and pd.Status = 'Completed'
order by Id;

update positiondescription
set directftesupervised = totalftesupervised;

insert into positionduty (
	Id,
	PositionDescriptionId,
	Duty,
	TasksHtml,
	TimePercent
)
select
duty.recid as Id,
pdf.Id,
--rtrim(left(description, 255)),
rtrim(left(duty.description, 60)), -- include only first 60 characters as "Duty"
duty.description,
cast(duty.percentage as int)
from StageLegacy.pdf.WGSKeyActivities duty
join PositionDescription pdf
on duty.WGSRecid = pdf.Id
--join StageLegacy.pdf.WGSPositionDesc pdf
--on duty.WGSRecid = pdf.RecId
--join Position p on pdf.ObjectAbbreviation = p.LegacyPositionNum
--and p.IsAbolished = 0 -- exclude abolished duties to enable FK
where percentage <> 0

-- now insert WMS positions:
-- TODO: insert single WMS duty from cqms.wmsPosDesc.PrincipalDuty after this.
insert into positiondescription
(Id,
LegacyId,
PositionId,
ProposedJobId,
PositionObjective,
EffectiveDate,
WorkingTitle,
LegacyStatus,
LastReviewDate,
LastUpdatedAt,
IsSupervisor,
QualificationsRequired,
wxDecisionResponsibility,
wxFinancialBudget,
wxFinancialImpact,
--wAccountabilityControlInfluence,
PositionTypeCXW
)

select NEXT VALUE FOR [dbo].[PositionDescription_seq]
over (order by LegacyId) as Id,
--select
wms.LegacyId,
wms.PositionId,
jc.Id as ProposedJobId,
wms.PositionObjective,
wms.EffectiveDate,
wms.WorkingTitle,
wms.LegacyStatus,
wms.LastReviewDate,
wms.LastUpdatedAt,
p.IsSupervisor,
wms.QualificationsRequired,
wms.DecisionResponsibility,
wms.FinancialBudget,
wms.FinancialImpact,
--wms.ScopeOfControl, -- REDUNDANT since budget and supervision are already known. gen output from those if needed.
'W' as PositionTypeCXW

--p.WorkingTitle,
--p.LegacyClassCode
from StageLegacy.cqwms.wmsPosDesc wms
join Position p
on wms.PositionId = p.Id
left join JobClass jc -- DG changed to left join to include PDs with no JC set.
on wms.LegacyClassCode = jc.LegacyCode

-- insert Duty field for WMS:
insert into positionduty (
	Id,
	PositionDescriptionId,
	Duty,
	TasksHtml,
	TimePercent
)
(	select
	NEXT VALUE FOR [dbo].[PositionDuty_seq] as Id,
	pd.Id as PositionDescriptionId,
	left(wms.PrincipalDuty, 60) as Duty,
	wms.PrincipalDuty as TasksHtml,
	95 as TimePercent
	from PositionDescription pd
	join StageLegacy.cqwms.wmsPosDesc wms
	on pd.LegacyId = wms.LegacyId
);

--where IsAbolished = 0
--and wms.LegacyClassCode = p.LegacyClassCode

-- re-create FK:
alter table PositionDuty
add constraint FK_PositionDuty_PositionDescription
foreign key (PositionDescriptionId) references PositionDescription (Id)
ON DELETE CASCADE;

END