﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-10
-- Description:	history of positions held by specified employee
-- =============================================
CREATE PROCEDURE [dbo].[GetEmployeeAppts] 
	@EmpId int
AS
BEGIN
--declare @EmpId int = 5856; -- 5980
select * from appointment
where employeeid = @EmpId
order by case when enddate is null then 1 else 2 end, startdate desc

END