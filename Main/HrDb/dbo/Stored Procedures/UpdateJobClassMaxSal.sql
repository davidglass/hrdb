﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-08-28
-- Description:	update JobClass from StageHRMS...
-- =============================================
CREATE PROCEDURE [dbo].[UpdateJobClassMaxSal] 
AS
BEGIN
update JobClass
set MaxAnnualSalary = (
  select top 1
  case when personnelsubarea in ('0002','0003') -- wms/exempt
	then cast(maxsalary as numeric)/100
	else cast(maxsalary as numeric)*24/100
	end as MaxAnnual
  from StageHRMS.dbo.Position p
  where p.jobid = JobClass.Id
  and p.id <> 71027332 -- exclude DeTray's hourly position
)
END