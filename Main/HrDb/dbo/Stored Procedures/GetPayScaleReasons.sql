﻿
CREATE PROCEDURE [dbo].[GetPayScaleReasons]
AS
BEGIN
	select Code as [Value],
	description as [Text]
	from PayScaleReason
	order by description
END