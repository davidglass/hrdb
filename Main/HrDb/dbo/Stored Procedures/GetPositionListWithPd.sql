﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-08-08
-- Description:	list Classified only (for now), with link to current PD if exists...
-- 2014-08-14: imported WMS PDs, added all positions to this list
-- =============================================
CREATE PROCEDURE [dbo].[GetPositionListWithPd] 
AS
BEGIN
	select p.Id as PositionId,
	case when personnelsubarea = '0002' then 'WMS' -- WMS
       when personnelsubarea = '0003' then 'Exempt' -- eXempt
       else 'Classified' end as PositionType, --CXW,  -- Classified
	pd.Id as PdId,
	pd.EffectiveDate,
	--convert(varchar, EffectiveDate, 101) EffectiveDateString, -- zero-pads...
	cast(datepart(m, pd.EffectiveDate) as varchar)
	+ '/' + cast(datepart(d, pd.EffectiveDate) as varchar)
	+ '/' + cast(datepart(yyyy, pd.EffectiveDate) as varchar)
	as EffectiveDateString, -- seems easier than post-processing Json DateTime serialization.
	--pd.WorkingTitle,
	p.WorkingTitle,
	-- TODO: include multi-fill, previous incumbent (VACANT Jones) logic here:
	isnull(e.LastName, '') as IncumbentLast,
	isnull(e.PreferredName, isnull(e.FirstName, '')) as IncumbentFirst,
	p.JobId,
	pd.ProposedJobId,
	-- TODO: remove following line, show mismatch in UI only...
	case when p.JobId = pd.ProposedJobId then '' else 'MISMATCH' end as JobMatch,
	--p.JobId as PosJobId,
	jc.LegacyCode as JobClassLeg,
	--pd.ProposedJobId,
	jcp.LegacyCode as ProposedJobClassLeg
	from
	Position p
	join JobClass jc
	on p.JobId = jc.Id
	left join
	PositionDescription pd
	on pd.Id = (
		select max(Id) from
		PositionDescription pd2
		where pd2.PositionId = p.Id
	)
	left join JobClass jcp
	on pd.ProposedJobId = jcp.Id
	left join Appointment a
	on a.PositionId = p.Id
	and (a.EndDate is null or a.EndDate > getdate())
	and exists (
		select 1 from appointment a2
		where a2.employeeid = a.employeeid
		and (a2.enddate is null or a2.enddate > getdate())
		and a2.positionid = p.Id
	)
	left join Employee e
	on a.EmployeeId = e.Id
	and (a.EndDate is null or a.EndDate > getdate())
	where p.IsAbolished = 0
--	and p.PayGradeArea = '01'
	--and (
	--	p.JobId <> pd.ProposedJobId
	--	or pd.Id is null
	--)
	order by p.Id
END

--select * from appointment
--where positionid = 874
--select * from employee where id in (4225, 6866)