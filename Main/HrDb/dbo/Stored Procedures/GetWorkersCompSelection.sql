﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-08-12
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetWorkersCompSelection] 
AS
BEGIN
  select Code as Value,
  Code + ': ' + Description as [Text]
  from WorkersCompCode
  order by Value
END