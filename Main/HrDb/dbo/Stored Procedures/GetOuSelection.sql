﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-06
-- Description:	get tree-sorted list of OUs for populating select list
-- =============================================
CREATE PROCEDURE [dbo].[GetOuSelection] 
AS
BEGIN
	select cast(ou.Id as varchar) as Value,
	replicate(': ', datalength(Ancestry)) + ou.Description  + ' [' + cast(ou.Id as varchar) + ']' as [Text]
	from OrgStructure os
	join OrgUnit ou
	on os.Id = ou.Id
	order by Ancestry;
END