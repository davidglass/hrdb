﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-30
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetIncumbentAppts]
	-- Add the parameters for the stored procedure here
	@PosId int
AS
BEGIN
--declare @PosId int = 5353;
	select a.Id,
	a.FillLevelJobId as FillLevel,
	--rtrim(j.JobTitleLegacy) + ' - ' + j.JobTitle as FillLevelDesc,
	j.LegacyCode + ' - ' + j.JobTitle as FillLevelDesc,
	a.EmployeeId as EmpId,
	a.ContractTypeCode,
	wc.WorkContractTypeDesc,
	a.WorkScheduleCode,
	ws.Description as WorkScheduleDesc,
	a.EmploymentPercent,
	e.HrmsPersonId as PersonnelNumber,
	e.FirstName, e.MiddleName, e.LastName, e.Suffix,
	e.PreferredName, e.DOB, e.Gender, e.MaritalStatus,
	StartDate,
	a.EndDate,
	eds.ds01 as AnniversaryDate,
	eds.ds02 as AppointmentDate,
	eds.ds03 as CtsHireDate,
	eds.ds04 as PriorPidDate,
	eds.ds05 as SeniorityDate,
	eds.ds07 as UnbrokenServiceDate,
	eds.ds09 as VacLeaveFrozenDate,
	eds.ds18 as PersonalHolidayElgDate,
	eds.ds26 as PersonalLeaveElgDate,
	eds.ds27 as TsrLeaveElgDate,
	StreetLine1,
	City,
	State,
	ZipCode,
	HomePhone,
	isnull(County, '00') as CountyId
	from Appointment a join Employee e
	on a.EmployeeId = e.Id
	and a.PositionId = @PosId
	-- TODO: make j, ws, and wc INNER JOINs (currently left join to facilitate newly double-filled position.
	left join hrms.WorkSchedule ws
	on ws.HRMSCode = a.WorkScheduleCode
	--left join hrms.Job j
	left join JobClass j
	on FillLevelJobId = j.Id
	left join WorkContractType wc
	on a.ContractTypeCode = wc.WorkContractTypeCode
	left join hrms.Addresses addr
	on e.HrmsPersonId = addr.PersonId
	left join EmployeeDateSpec eds
	on e.Id = eds.EmployeeId
	-- ADDED TO FIX future-ending appts:
	where (a.EndDate is null or a.EndDate > getdate())
	and (addr.AddressType is null or addr.AddressType = '1')

END