﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-08-18
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetEmpSummary] 
	@id int,
	@LanId varchar(50)
AS
BEGIN
	-- TODO:
	-- if @id = 0, look up EmployeeId from @LanId,
	-- else ensure @LanId employee has read rights for Employee @id.
	--declare @id int = 6039; -- Curtis
	--declare @LanId varchar(50) = 'DIS\DavidG';
	if @id = 0 begin
		select @id = EmployeeId from AppUser
		where LanId = @LanId
	end
	select
		@id as Id,
		isnull(e.PreferredName, e.FirstName) as FirstName,
		e.LastName,
		a.PositionId,
		p.WorkingTitle,
		jc.LegacyCode as JobClass,
		ou.Id as OrgUnitId,
		ou.Description as OrgUnitName
	from Employee e
	left join Appointment a
	on e.Id = a.EmployeeId
	and (a.enddate is null or a.enddate > getdate())
	left join Position p
	on a.PositionId = p.Id
	left join JobClass jc
	on p.JobId = jc.Id
	left join OrgUnit ou
	on p.OrgUnitId = ou.Id
	where e.Id = @id
END