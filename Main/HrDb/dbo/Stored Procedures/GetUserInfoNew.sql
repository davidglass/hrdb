﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-01-29
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetUserInfoNew] 
	@LanId varchar(50)
	
AS
BEGIN

--declare @LanId varchar(50) = 'DIS\paulinem';
	select
	u.Id as UserId,
	@LanId as RealLanId,
	isnull(li.ImpersonatedLanId, @LanId) as LanId,
	r.Name as RoleName,
	dbo.IsNullOrEmpty(e.PreferredName, isnull(e.FirstName, 'Non-Employee')) as FirstName,
	ad.mail [EmailAddress],
	a.PositionId

	from AppUser u

	join AppRole r
	on u.RoleId = r.Id

	left join Employee e
	on u.EmployeeId = e.Id

	left join LanImpersonation li
	on li.LanId = @LanId

	left join Appointment a
	on a.EmployeeId = e.Id
	and (a.EndDate is null or a.EndDate > getdate()) 

	left join ADExtract ad on 'DIS\' + ad.samaccountname = @LanId
	and ad.company = 'CTS'

	where u.LanId = isnull(li.ImpersonatedLanId, @LanId)
END