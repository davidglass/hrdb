﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-01-27
-- Description:	populates AppUsers from Employee,
--   assumes employee.lanid is set.  appuser.lanid is AK (required, unique)
-- =============================================
CREATE PROCEDURE [dbo].[LoadAppUsers] 
AS
BEGIN

	--truncate table Appuser;
	delete from Appuser;
	insert into AppUser (LanId, EmployeeId, RoleId) 
	(
	select 'DIS\' + lanid as LanId,
	e.Id as EmployeeId,
	4
	from Employee e
	join Appointment a
	on e.Id = a.EmployeeId
	and (a.EndDate is null or a.EndDate > getdate())
	)
	--insert into appuser(EmployeeId, LanId)
	--(
	--select e.id, 'dis\' + e.lanid
	--from employee e
	--join appointment a
	--on e.id = a.employeeid
	--and (a.enddate is null or a.enddate > getdate())
	--left join appuser u
	--on e.id = u.employeeid
	--where u.employeeid is null
	--and e.lanid is not null -- TODO: manually add lanid for new employees, re-run this
	--)
	update u 
	set RoleId = case
	when LanId in ('DIS\DavidG', 'DIS\PaulineM', 'DIS\MarkP', 'DIS\VairamV')
	then 1
	when LanId in ('DIS\LaurenF', 'DIS\CherR', 'DIS\BenM') 
	then 2
	when p.IsSupervisor = 1
	then 3
	else 4
	end
	from AppUser u
	join Appointment a
	on u.EmployeeId = a.EmployeeId
	and (a.EndDate is null or a.EndDate > getdate())
	join Position p
	on a.PositionId = p.Id

END