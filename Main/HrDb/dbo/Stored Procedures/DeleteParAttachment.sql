﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-06-27
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DeleteParAttachment] 
	@Id int
AS
BEGIN

	-- TODO: create ParAttachment table including ParId...this is 1-M, not M-M
	--delete from AttachmentApptRequest where AttachmentId = @ParId;
	--delete from Attachment where Id = @ParId;
	delete from ParAttachment where Id = @Id;
END