﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-23
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetNewHireApptTypes] 
AS
BEGIN
select id, Reason, newhire_u0
from ApptActionReason
where IsActive = 1
and newhire_u0 is not null
END