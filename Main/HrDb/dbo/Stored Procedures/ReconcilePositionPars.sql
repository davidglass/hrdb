﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-06
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ReconcilePositionPars]
AS
BEGIN

	begin tran;
	-- reconcile abolished, new position PARs:
	update p set hrmspositionid = hrp.id,
	ValidFromDate = hrp.EffectiveDate
	from position p
	join stagehrms.dbo.position hrp
	on p.id = cast(hrp.positiontitleleg as int)
	where p.HrmsPositionId is null;

	update par set StatusId = 6 -- reconciled
	where
	actioncode = 'P2' and not exists (
		select 1 from stagehrms.dbo.position hrp
		where cast(hrp.PositionTitleLeg as int) = par.positionid
	)
	or
	actioncode = 'P0' and exists (
		select 1 from stagehrms.dbo.position hrp
		where cast(hrp.PositionTitleLeg as int) = par.positionid
	)
	--and StatusId in (4,5); -- only reconcile Processed or Pushed PARs
	and StatusId = 5; -- only reconcile Pushed PARs

	update p
	set StatusId = 6 -- Reconciled
	from Par p
	join StageHRMS.dbo.Position hrp
	on p.PosHrmsId = hrp.Id
	and p.PosJobId = hrp.JobId
	and p.PosOrgUnitId = hrp.OrgUnitId
	join Position sp
	on p.PosSupervisorPosId = sp.Id
	and sp.HrmsPositionId = hrp.SupervisorPositionId
	where p.ActionCode = 'P1'
	--and StatusId in (4,5); -- only reconcile Processed or Pushed PARs
	and StatusId = 5; -- only reconcile Pushed PARs

	commit;
END