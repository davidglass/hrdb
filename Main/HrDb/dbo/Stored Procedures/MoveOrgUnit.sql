﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-10
-- Description:	relies on instead of update trigger for heavy lifting.
-- TODO: test for valid SibIndex before attempting update
-- =============================================
CREATE PROCEDURE [dbo].[MoveOrgUnit]
	@Id int, 
	@ParentId int,
	@SibIndex int 
AS
BEGIN
-- TODO: make SibIndex nullable in model or it will convert nulls to 0
	if @SibIndex is null begin
		update OrgStructure
		set ParentId = @ParentId
		where Id = @Id
	end
	else begin
		update OrgStructure
		set ParentId = @ParentId,
		Ancestry = (
			select
				cast(
					Ancestry +
					cast(@SibIndex + 1 as binary(1)) -- 8-bit version
				as varbinary(50))
			from OrgStructure where Id = @ParentId
		)
		where Id=@Id
	end
END