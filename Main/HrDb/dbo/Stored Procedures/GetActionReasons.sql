﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-28
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetActionReasons] 
	-- Add the parameters for the stored procedure here
	@ActionCode char(2)
AS
BEGIN
	--declare @actioncode char(2) = 'u5';
	declare @stmt nvarchar(4000);
--	select @stmt = 'select Id, Reason
	select @stmt = 'select cast(Id as varchar(9)) as Value,
	Reason as [Text]
	from ApptActionReason
	where
	'
	+ case
	when @actioncode = 'u0' then 'NewHire_U0'
	when @actioncode = 'u3' then 'ApptChange_U3'
	when @actioncode = 'u5' then 'Separation_U5'
	when @actioncode = 'u6' then 'Rehire_U6'
	when @actioncode = 'u8' then 'LOA_actv_U8'
	when @actioncode = 'u9' then 'LOA_inactv_U9'
	when @actioncode = 'ua' then 'LOA_return_UA'
	when @actioncode = 'uj' then 'StatusChange_UJ'
	end
	+ ' is not null and IsActive = 1
	order by Reason
	'
	exec sp_executesql @statement = @stmt;
END