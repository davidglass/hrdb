﻿
-- =============================================
-- Author:		Mark Paul
-- Create date: 8/21/13
-- Description:	Get specific approval workflow of task
-- =============================================
create PROCEDURE [dbo].[GetApprovalWorkflowOfTask]
	-- Add the parameters for the stored procedure here
	@TaskId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT  
		ApprovalWorkflow.[Id],
		ApprovalWorkflow.StatusCodeId [StatusCode],
		ApprovalCodeId [ApprovalCode],
		AppId [App],
		PositionId,
		PersonId,
		CreatedDate
	FROM ApprovalWorkflow
	left outer join ApprovalTask on ApprovalTask.ApprovalWorkflowId = ApprovalWorkflow.Id
	where ApprovalTask.Id = @TaskId
END