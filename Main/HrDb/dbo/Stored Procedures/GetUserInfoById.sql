﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-12-11
-- Description:	
-- =============================================
CREATE PROCEDURE GetUserInfoById 
	-- Add the parameters for the stored procedure here
	@Id int
AS
BEGIN
	declare @LanId varchar(50);
	select @LanId = LanId from AppUser where Id = @Id;
	exec GetUserInfoNew @LanId = @LanId;
END