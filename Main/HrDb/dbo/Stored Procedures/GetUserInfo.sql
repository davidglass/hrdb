﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-11-08
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetUserInfo] 
	-- Add the parameters for the stored procedure here
	@LanId varchar(50)
AS
BEGIN
	-- TODO: rewrite this to match ADExtract, retrun more complete user profile (e.g. EmpId, etc.)
	select top 1 isnull(preferredname, '(unrecognized)') from Employee
	where
	@LanId = 'DIS\' + lanid;
END