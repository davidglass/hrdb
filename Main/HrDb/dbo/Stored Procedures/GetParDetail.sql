﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-07-02
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetParDetail] 
	@Id int,
	@LanId varchar(50),
	@EmpId int = null -- if @EmpId is supplied, look up existing Employee info
AS
BEGIN
	-- TODO: use common Request sequence for multiple ActionRequest types (e.g. Appt, Position, etc.)

	-- NOTE: PercentFullTime = EmploymentPercent,
--	--
--declare @Id int = 1107;
--declare @LanId varchar(50) = 'DIS\DavidG';
--declare @EmpId int = null;
--declare @EmpId int = 5980;

	-- only show detail for Pars with PositionId in span of control unless HR/Admin role:
	--and (

	-- TEST ACCESS before running query:
	declare @CanRead int; -- sic
	select @CanRead = case when 
		dbo.CanReadAll(@LanId, 'Par') = 1
		or exists -- else only span of control.
		( select 1 from Par p
		join dbo.GetSpanOfControl(@LanId) span
		on p.PositionId = span.Id
		and p.Id = @Id )
	then 1
	else 0
	end
		
	--delegate addition
	if @CanRead = 0 begin
		select @CanRead = case when exists 
			(select 1 from Par p
				join (select ps2.Id
					from AppUser u
						join ApprovalDelegate delegate on u.EmployeeId = delegate.DelegatedToPersonId
						join ApprovalDelegateApp on ApprovalDelegateApp.DelegateId = delegate.Id
						join ApprovalApp on ApprovalApp.Id = ApprovalDelegateApp.AppId
						join PositionStructure ps on delegate.DelegatedPositionId = ps.Id
						join PositionStructure ps2 on left(ps2.Ancestry, datalength(ps.Ancestry)) = ps.Ancestry
							where u.LanId = @LanId
							and ApprovalApp.Name = 'PAR'							
							and ((delegate.Indefinite = 1) or (delegate.EndDate >= DATEADD(day, -1, getdate())))) span
					on p.PositionId = span.Id and p.Id = @Id  )
		then 1
		else 0
		end
	end

	if @CanRead = 0 begin
		raiserror('Access Denied.',11,1) with seterror
		return
	end

	select
	--a.Id as ApptId, -- *NOTE* will be null when filling pos with current emp.
	-- p.*,
	p.Id, p.PositionId,
	p.DeactivateClassCode,
	-- NOTE, this doesn't verify that 
--	case when e.Id is null then p.EmployeeId else e.Id end as EmployeeId,
	case when @EmpId is null then p.EmployeeId else e.Id end as EmployeeId,
	p.ActionReasonId, isnull(p.ActionCode, 'UNSPECIFIED') as ActionCode,
	CASE when p.ActionCode = 'U0' then aar.NewHire_U0
		when p.ActionCode = 'U3' then aar.ApptChange_U3
		when p.ActionCode = 'U5' then aar.Separation_U5
		when p.ActionCode = 'U6' then aar.Rehire_U6
		when p.ActionCode = 'U8' then aar.LOA_actv_U8
		when p.ActionCode = 'UJ' then aar.StatusChange_UJ
		else 'UNSPECIFIED'
	END as ActionReasonCode,
	p.EffectiveDate,
--	case when p.ActionCode = 'U5' then a.ContractTypeCode 
	case when p.ActionCode in ('P0', 'P1', 'P2') then '' -- default inserts double-blank (Permanent code)
	else
	p.ContractTypeCode
	end as ContractTypeCode,
	-- CHANGE TEST
--	select * from appointment where employeeid = 3987

--select * from employee
--where HrmsPersonId = 501474

	cast(case when
		(p.ContractTypeCode != a.ContractTypeCode
			or
			p.ActionCode in ('U0', 'U6')
		)
		then 1 else 0
		end as bit)
		as ContractTypeCodeChanged,
	p.TargetAgencyCode,
	p.FromAgencyCode,
	agt.AgencyTitleShort as AgencyDescription,
	p.IsTransferIn,
	p.IsTransferOut,
	p.StatusId,
	p.ContractEndDate,
	-- hack for comparing potential nulls:
	-- NOTE, excluding xChanged for New, Rehire actions:
	--cast(case when p.ActionCode not in ('U0', 'U6')
	--	and isnull(p.ContractEndDate, 0) != isnull(a.ContractEndDate, 0)
	--	then 1 else 0 end as bit)
	--	as ContractEndDateChanged,
	-- (REMOVED New Hire Exclusion, count all new as changed):
	cast(case
		when
			isnull(p.ContractEndDate, 0) != isnull(a.ContractEndDate, 0)
			or (p.ActionCode in ('U0', 'U6')
				and p.ContractEndDate is not null)
		then 1 else 0 end as bit)
		as ContractEndDateChanged,
	p.WorkScheduleCode,
	cast(
		case when p.WorkScheduleCode != a.WorkScheduleCode
		or p.ActionCode in ('U0', 'U6')
		then 1 else 0 end as bit)
		as WorkScheduleCodeChanged,
	p.PercentFullTime,
	cast(
		case when p.PercentFullTime != a.EmploymentPercent
		or p.ActionCode in ('U0', 'U6')
		then 1 else 0 end as bit)
		as PercentFullTimeChanged,
	p.ApptTimeMgtStatus,
	cast(
		case when p.ApptTimeMgtStatus != a.TimeMgtStatus
		or p.ActionCode in ('U0', 'U6')
		then 1 else 0 end as bit)
		as ApptTimeMgtStatusChanged,
	p.ApptPayScaleEffectiveDate,
	cast(
		case when p.ApptPayScaleEffectiveDate != a.PayScaleEffectiveDate
		or p.ActionCode in ('U0', 'U6')
		then 1 else 0 end as bit)
		as ApptPayScaleEffectiveDateChanged,
	p.ApptPayScaleReason,
	cast(
		case when p.ApptPayScaleReason != a.PayScaleReason
		or p.ActionCode in ('U0', 'U6')
		then 1 else 0 end as bit)
		as ApptPayScaleReasonChanged,
	psr.Description as ApptPayScaleReasonDesc,
	p.ApptPayScaleTypeArea,
	cast(case when p.ApptPayScaleTypeArea != a.PayScaleTypeArea
		or p.ActionCode in ('U0', 'U6')
		then 1 else 0 end as bit)
		as ApptPayScaleTypeAreaChanged,
	pst.Description + ' (' + pst.PsType + '-' + pst.PsArea + ')' as ApptPayScaleTypeAreaDesc,
	p.ApptPayScaleGroup,
	cast(case when p.ApptPayScaleGroup != a.PayScaleGroup
			or p.ActionCode in ('U0', 'U6')
		then 1 else 0 end as bit)
		as ApptPayScaleGroupChanged,
	p.ApptPayScaleLevel,
	cast(case when p.ApptPayScaleLevel != a.PayScaleLevel
			or p.ActionCode in ('U0', 'U6')
		then 1 else 0 end as bit)
		as ApptPayScaleLevelChanged,
	p.ApptPayAnnualSalary,
	cast(case when p.ApptPayAnnualSalary != a.PayAnnualSalary
			or p.ActionCode in ('U0', 'U6')
		then 1 else 0 end as bit)
		as ApptPayAnnualSalaryChanged,
	p.ApptPayHourlyRate,
	cast(case when
		isnull(p.ApptPayHourlyRate, 0) != isnull(a.PayHourlyRate,0)
			or (p.ActionCode in ('U0', 'U6') and p.ApptPayHourlyRate is not null)
		then 1 else 0 end as bit)
		as ApptPayHourlyRateChanged,
	p.ApptFillOtEligCode,
	cast(case when
		isnull(p.ApptFillOtEligCode, 'na') != isnull(a.FillOtEligibilitySubgroup, 'na')
			or (p.ActionCode in ('U0', 'U6') and p.ApptFillOtEligCode is not null)
		then 1 else 0 end as bit)
		as ApptFillOtEligCodeChanged,
	p.ApptPayNextIncrease,
	p.ApptStepMEligibleDate,
	cast(case when
			-- hack for comparing potential null dates:
			isnull(p.ApptPayNextIncrease, 0) != isnull(a.PayNextIncrease, 0)
			or (p.ActionCode in ('U0', 'U6') and p.ApptPayNextIncrease is not null)
		then 1 else 0 end as bit)
		as ApptPayNextIncreaseChanged,
	p.ApptPayCapUtilityLevel,
	p.ApptShiftDifferential,
	-- *note*, New Hires will never show appt properties changed since no appt exists.
	cast(case when p.ApptShiftDifferential != a.PayShiftDifferential
			or p.ActionCode in ('U0', 'U6')
	then 1 else 0 end as bit)
		as ApptShiftDifferentialChanged,
	p.ApptStandbyPay,
--	p.ApptShiftDesignation,
	p.FillLevel,
	cast(
		case when p.FillLevel <> a.FillLevelJobId
		or p.ActionCode in ('U0', 'U6')
		then 1 else 0 end as bit) as FillLevelChanged,
	p.Notes,

	-- if @EmpId is passed, values from employee should supersede existing Par values.
	case when @EmpId is null then p.EmpFirstName
	else e.FirstName end as EmpFirstName,
	cast(case when
	isnull(p.EmpFirstName, 'none') != isnull(e.FirstName,'none') then 1 else 0 end as bit)
	as EmpFirstNameChanged,
	case when @EmpId is null then p.EmpLastName
	else e.LastName end as EmpLastName,
	cast(case when
	isnull(p.EmpLastName, 'none') != isnull(e.LastName,'none') then 1 else 0 end as bit)
	as EmpLastNameChanged,
	--cast(
	--case when p.ActionCode not in ('U0', 'U6')
		--and isnull(p.ContractEndDate, 0) != isnull(a.ContractEndDate, 0)
		--then 1 else 0 end as bit)
		--as ContractEndDateChanged,
	case when @EmpId is null then p.EmpMiddleName
	else e.MiddleName end as EmpMiddleName,
	cast(case when
	isnull(p.EmpMiddleName, 'none') != isnull(e.MiddleName,'none') then 1 else 0 end as bit)
	as EmpMiddleNameChanged,
	case when @EmpId is null then isnull(p.EmpPreferredName, p.EmpFirstName)
	else isnull(e.PreferredName, e.FirstName) end as EmpPreferredName,
	cast(case when
	isnull(p.EmpPreferredName, 'none') != isnull(e.PreferredName,'none') then 1 else 0 end as bit)
	as EmpPreferredNameChanged,
	case when @EmpId is null then p.EmpSuffix
	else e.Suffix end as EmpSuffix,
	cast(case when
	isnull(p.EmpSuffix, 'none') != isnull(e.Suffix,'none') then 1 else 0 end as bit)
	as EmpSuffixChanged,
	p.EmpEmail,
	case when @EmpId is null and p.ActionCode = 'U0' then p.EmpDOB
--	else e.DOB end as EmpDOB,
	else null end as EmpDOB,
	case when @EmpId is null then p.EmpGender
	else e.Gender end as EmpGender,
	case when @EmpId is null  then p.EmpSSN
	else '---------' end as EmpSSN, 
	-- TODO: add "and p.EmployeeId is null" clause to remaining fields of interest (e.g. first, last)...
--	case when @EmpId is null and p.EmployeeId is null then p.EmpHrmsId
	case when 
		(@EmpId is null and p.EmployeeId = 0)
		or p.ActionCode = 'U6' -- Rehire (with Personnel #)
		or p.IsTransferIn = 1
	then p.EmpHrmsId
	else e.HrmsPersonId end as EmpHrmsId,

--	case when @EmpId is null and (p.EmployeeId is null or p.EmployeeId = 0) then p.EmpFirstName
--	else e.FirstName end as EmpFirstName,
--	case when @EmpId is null and (p.EmployeeId is null or p.EmployeeId = 0) then p.EmpLastName
--	else e.LastName end as EmpLastName,
--	case when @EmpId is null and (p.EmployeeId is null or p.EmployeeId = 0) then p.EmpMiddleName
--	else e.MiddleName end as EmpMiddleName,
--	case when @EmpId is null and (p.EmployeeId is null or p.EmployeeId = 0) then p.EmpPreferredName
--	else e.PreferredName end as EmpPreferredName,
--	case when @EmpId is null and (p.EmployeeId is null or p.EmployeeId = 0) then p.EmpSuffix
--	else e.Suffix end as EmpSuffix,
--	case when @EmpId is null then p.EmpDOB
--	else e.DOB end as EmpDOB,
--	case when @EmpId is null then p.EmpGender
--	else e.Gender end as EmpGender,
--	case when @EmpId is null then p.EmpSSN
--	else '---------' end as EmpSSN, 
--	-- TODO: add "and p.EmployeeId is null" clause to remaining fields of interest (e.g. first, last)...
----	case when @EmpId is null and p.EmployeeId is null then p.EmpHrmsId
--	case when @EmpId is null and p.EmployeeId = 0 then p.EmpHrmsId
--	else e.HrmsPersonId end as EmpHrmsId,


	-- TODO: hide address for non-new-hire actions (emp can change in self-service)
	p.EmpAddrStreetLine1,
	p.EmpAddrStreetLine2,
	p.EmpAddrCity,
	p.EmpAddrCountyCode,
	cty.CountyName as EmpAddrCountyName,
	p.EmpAddrZipCode, p.EmpAddrState,
	p.EmpAddrHomePhone,
	case when @EmpId is null then p.EmpDS01_Anniversary
	else eds.ds01 end as EmpDS01_Anniversary,
	eds.ds01,
	cast(case when
	isnull(p.EmpDS01_Anniversary, 0) != isnull(eds.ds01, 0) then 1 else 0 end as bit)
	as EmpDS01_AnniversaryChanged,
	case when @EmpId is null then p.EmpDS02_Appointment
	else eds.ds02 end as EmpDS02_Appointment,
	cast(case when
	isnull(p.EmpDS02_Appointment, 0) != isnull(eds.ds02, 0) then 1 else 0 end as bit)
	as EmpDS02_AppointmentChanged,
	case when @EmpId is null then p.EmpDS03_CtsHire
	else eds.ds03 end as EmpDS03_CtsHire,
	cast(case when
	isnull(p.EmpDS03_CtsHire, 0) != isnull(eds.ds03, 0) then 1 else 0 end as bit)
	as EmpDS03_CtsHireChanged,
	case when @EmpId is null then p.EmpDS04_PriorPid
	else eds.ds04 end as EmpDS04_PriorPid,
	cast(case when
	isnull(p.EmpDS04_PriorPid, 0) != isnull(eds.ds04, 0) then 1 else 0 end as bit)
	as EmpDS04_PriorPidChanged,
	case when @EmpId is null then p.EmpDS05_Seniority
	else eds.ds05 end as EmpDS05_Seniority,
	cast(case when
	isnull(p.EmpDS05_Seniority, 0) != isnull(eds.ds05, 0) then 1 else 0 end as bit)
	as EmpDS05_SeniorityChanged,
	case when @EmpId is null then p.EmpDS07_UnbrokenService
	else eds.ds07 end as EmpDS07_UnbrokenService,
	cast(case when
	isnull(p.EmpDS07_UnbrokenService, 0) != isnull(eds.ds07, 0) then 1 else 0 end as bit)
	as EmpDS07_UnbrokenServiceChanged,
	case when @EmpId is null then p.EmpDS09_VacLeaveFrozen
	else eds.ds09 end as EmpDS09_VacLeaveFrozen,
	cast(case when
	isnull(p.EmpDS09_VacLeaveFrozen, 0) != isnull(eds.ds09, 0) then 1 else 0 end as bit)
	as EmpDS09_VacLeaveFrozenChanged,
	case when @EmpId is null then p.EmpDS18_PersonalHolidayElg
	else eds.ds18 end as EmpDS18_PersonalHolidayElg,
	cast(case when
	isnull(p.EmpDS18_PersonalHolidayElg, 0) != isnull(eds.ds18, 0) then 1 else 0 end as bit)
	as EmpDS18_PersonalHolidayElgChanged,
	case when @EmpId is null then p.EmpDS26_PersonalLeaveElg
	else eds.ds26 end as EmpDS26_PersonalLeaveElg,
	cast(case when
	isnull(p.EmpDS26_PersonalLeaveElg, 0) != isnull(eds.ds26, 0) then 1 else 0 end as bit)
	as EmpDS26_PersonalLeaveElgChanged,
	case when @EmpId is null then p.EmpDS27_TsrLeaveElg
	else eds.ds27 end as EmpDS27_TsrLeaveElg,
	-- TODO: hide this EEO/AA data for non-new-hires.
	EmpRaceCodes,
	EmpHispanic,
	EmpMilitaryStatus,
	EmpMilitaryBranch,
	EmpDisability,
	ws.Description as WorkScheduleDescription,
	-- this JobTitle info needs to come from PAR, not Position
	--fj.JobTitleLegacy + ' - ' + fj.JobTitle as FillTitle,
	fj.LegacyCode + ' - ' + fj.JobTitle as FillTitle,
	--fj.JobTitleLegacy as FillClass,
	fj.LegacyCode as FillClass,
	ac.Description as ActionDescription,
	ar.Reason as ActionType,
	j.JobTitle as PositionJobTitle,
	--j.JobTitleLegacy as PositionClassCode,
	j.LegacyCode as PositionClassCode,

	-- TODO: verify that this when is the best way to do this...
	-- this populates the orgunitid to match existing position the first time the Par is edited.
	--case when pos.Id is null or pos.OrgUnitId is null or PosOrgUnitId <> pos.OrgUnitId
	--then PosOrgUnitId
	--else pos.OrgUnitId end as PosOrgUnitId,
	--ou.Id as PosOrgUnitId,
	p.PosOrgUnitId,
	cast(case when
	isnull(p.PosOrgUnitId, 0) != isnull(pos.OrgUnitId, 0) then 1 else 0 end as bit)
	as PosOrgUnitIdChanged,

	ou.ShortName as OrgUnitShortName,
	ou.HrmsOrgUnitId as PosHrmsOrgUnitId,
	wc.WorkContractTypeDesc as ContractType,
--	pos.HrmsPositionId as PosHrmsId,
	p.PosHrmsId,
	--pos.JobId as PosJobId,
	p.PosJobId,
	cast(case when
		isnull(p.PosJobId, 0) != isnull(pos.JobId, 0) then 1 else 0 end as bit)
		as PosJobIdChanged,
	p.PosWorkingTitle,
	cast(case when
		isnull(p.PosWorkingTitle, 'none') != isnull(pos.WorkingTitle, 0) then 1 else 0 end as bit)
		as PosWorkingTitleChanged,
--	pos.WorkingTitle as PosWorkingTitle, -- supersedes Par field
	--case when pos.Id is null or pos.WorkingTitle is null or PosWorkingTitle <> pos.WorkingTitle
	--then PosWorkingTitle
	---- populate form from existing description if it hasn't been changed in Par:
	--else pos.WorkingTitle end as PosWorkingTitle,

--	pos.CostCenter as PosCostCenter, -- supersedes Par...
	p.PosCostCenter,
	cast(case when
		isnull(p.PosCostCenter, 0) != isnull(pos.CostCenter, 0) then 1 else 0 end as bit)
		as PosCostCenterChanged,
	p.PosWorkersCompCode,
	cast(case when
		isnull(p.PosWorkersCompCode, 'none') != isnull(pos.WorkersCompCode, 'none') then 1 else 0 end as bit)
		as PosWorkersCompCodeChanged,
--	pos.WorkersCompCode,

-- TODO: join to lookup (it now exists)...
	case
	when p.PosWorkersCompCode = '0000' then 'None Assigned'
	when p.PosWorkersCompCode = '4902' then 'Clerical / Admin (4902)'
	when p.PosWorkersCompCode = '5300' then 'Admin Field Staff (5300)'
	when p.PosWorkersCompCode = '5307' then 'Warehouse (5307)'
	when p.PosWorkersCompCode = '6901' then 'Volunteers (6901)'
	end
	--case
	--when pos.WorkersCompCode = '0000' then 'None Assigned'
	--when pos.WorkersCompCode = '4902' then 'Clerical / Admin (4902)'
	--when pos.WorkersCompCode = '5300' then 'Admin Field Staff (5300)'
	--when pos.WorkersCompCode = '5307' then 'Warehouse (5307)'
	--when pos.WorkersCompCode = '6901' then 'Volunteers (6901)'
	--end
	as WorkersCompDescription,
--	pos.SupervisorPositionId as PosCtsSupervisorId,
--	p.PosSupervisorPosId as PosCtsSupervisorId,
	p.PosSupervisorPosId,
	cast(case when
	isnull(p.PosSupervisorPosId, 0) != isnull(pos.SupervisorPositionId, 0) then 1 else 0 end as bit)
	as PosSupervisorPosIdChanged,
	sup.HrmsPositionId as PosHrmsSupervisorId,
	--pos.PersonnelSubArea as PosPersonnelSubArea, -- TODO: use Par value
	--pos.EeGroup as PosEeGroup,
	--pos.EeSubGroup as PosEeSubGroup,
	p.PosPersonnelSubArea as PosPersonnelSubArea, -- TODO: use Par value
	cast(case when
	isnull(p.PosPersonnelSubArea, 'none') != isnull(pos.PersonnelSubArea, 'none') then 1 else 0 end as bit)
	as PosPersonnelSubAreaChanged,
	p.PosEeGroup,
	cast(case when
	isnull(p.PosEeGroup, 'n') != isnull(pos.EeGroup, 'n') then 1 else 0 end as bit)
	as PosEeGroupChanged,
	p.PosEeSubgroup,
	cast(case when
	isnull(p.PosEeSubgroup, 'n') != isnull(pos.EeSubGroup, 'n') then 1 else 0 end as bit)
	as PosEeSubgroupChanged,
	p.PosIsInTraining,
	cast(case when
	isnull(p.PosIsInTraining, 0) != isnull(pos.IsInTraining, 0) then 1 else 0 end as bit)
	as PosIsInTrainingChanged,
	p.PosIsAppointingAuthority,
	cast(case when
	isnull(p.PosIsAppointingAuthority, 0) != isnull(pos.IsAppointingAuthority, 0) then 1 else 0 end as bit)
	as PosIsAppointingAuthorityChanged,
--	p.PosIsAppointingAuthority as PosIsApptAuth,
	p.PosIsManager,
	cast(case when
	isnull(p.PosIsManager, 0) != isnull(pos.IsManager, 0) then 1 else 0 end as bit)
	as PosIsManagerChanged,
	p.PosIsSupervisor,
	cast(case when
	isnull(p.PosIsSupervisor, 0) != isnull(pos.IsSupervisor, 0) then 1 else 0 end as bit)
	as PosIsSupervisorChanged,
	p.PosJvac,
	cast(case when
	isnull(p.PosJvac, '') != isnull(pos.Jvac, '') then 1 else 0 end as bit)
	as PosJvacChanged,
	p.PosPointValue,
	cast(case when
	isnull(p.PosPointValue, 0) != isnull(pos.PointValue, 0) then 1 else 0 end as bit)
	as PosPointValueChanged,
	p.PosIsSection4,
	cast(case when
	isnull(p.PosIsSection4, 0) != isnull(pos.IsSection4, 0) then 1 else 0 end as bit)
	as PosIsSection4Changed,
	p.PosReqsBackgroundCheck,
	cast(case when
	isnull(p.PosReqsBackgroundCheck, 0) != isnull(pos.IsBackgroundCheckRequired, 0) then 1 else 0 end as bit)
	as PosReqsBackgroundCheckChanged,

	--pos.IsAppointingAuthority as PosIsApptAuth,
	--pos.IsManager as PosIsManager,
	--pos.IsSupervisor as PosIsSupervisor,
	--pos.IsSection4 as PosIsSection4,
	--pos.IsBackgroundCheckRequired as PosIsBackgroundCheckRequired,
	--pos.PayGradeType as PosPayGradeType,
	--case when pos.PayGradeType = '01' then 'CBA' else 'Non-represented' end
	--	as PosPayGradeTypeDesc,
	--pos.PayGradeArea as PosPayGradeArea,
	--case when pos.PayGradeArea = '01' then 'Standard Progression'
	--	when pos.PayGradeArea = '02' then 'WMS'
	--	when pos.PayGradeArea = '40' then 'Non-Payscale'
	--	when pos.PayGradeArea = '41' then 'Exempt'
	--end as PosPayGradeAreaDesc,
	p.PosPayGradeTypeArea,
	cast(case when
	isnull(p.PosPayGradeTypeArea, 0) != isnull(pos.PayGradeTypeArea, 0) then 1 else 0 end as bit)
	as PosPayGradeTypeAreaChanged,
	
	pg.Description + ' (' + pg.PsType + '-' + pg.PsArea + ')'
		as PosPayGradeTypeAreaDesc,

-- TODO: trim incoming paygrades...
--update position set paygrade = rtrim(paygrade);
--update par set pospaygrade = rtrim(pospaygrade);
--update payscalegroupcode set code = rtrim(code);
--	pos.PayGrade as PosPayGrade,
	p.PosPayGrade,
	cast(case when
	isnull(p.PosPayGrade, 'none') != isnull(pos.PayGrade, 'none') then 1 else 0 end as bit)
	as PosPayGradeChanged,
	p.PosIsTandem,
	cast(case when
	isnull(p.PosIsTandem, 0) != isnull(pos.IsTandem, 0) then 1 else 0 end as bit)
	as PosIsTandemChanged,
	p.PosLocationId,
	cast(case when
	isnull(p.PosLocationId, 0) != isnull(pos.LocationId, 0) then 1 else 0 end as bit)
	as PosLocationIdChanged,
	loc.Location as PosLocation,
	p.PosWorkScheduleCode,
	p.ApptWorkHoursWk1,
	p.ApptWorkHoursWk2,
	cast(case when
	isnull(p.PosWorkScheduleCode, 'none') != isnull(pos.DefaultWorkScheduleCode, 'none') then 1 else 0 end as bit)
	as PosWorkScheduleCodeChanged,
--	pos.DefaultWorkScheduleCode as PosWorkScheduleCode,
	pws.Description as PosWorkScheduleDescription,
	-- pos.Id null == new Position (TODO: use sequence so never happens in future?)
	-- use Par PosGeneralDescription if new Position or new/changed description:
	--case when pos.Id is null or pos.GeneralDescription is null or PosGeneralDescription <> pos.GeneralDescription
	--then PosGeneralDescription
	---- populate form from existing description if it hasn't been changed in Par:
	--else pos.GeneralDescription end as PosGeneralDescription
	p.PosGeneralDescription,
	cast(case when
	isnull(p.PosGeneralDescription, '') != isnull(pos.GeneralDescription, '') then 1 else 0 end as bit)
	as PosGeneralDescriptionChanged,
--	p.PosShiftDesignation,
	p.PosShiftCode,
	cast(case when p.PosShiftCode != pos.ShiftCode then 1 else 0 end as bit)
	as PosShiftCodeChanged,
	p.PosShiftDiffEligible,
	p.PosStandbyEligible,
	--isnull(p.PosShiftDesignation, '') != isnull(pos.ShiftDesignation, '') then 1 else 0 end as bit)
	--as PosShiftDesignationChanged,

	p.ApprovalWorkflowId,
	p.IsNameChangeOnly,
	p.IsPayChangeOnly,
	wf.StatusCodeId as ApprovalStatusId,
	cast(case when ck.ParId is null then 0 else 1 end as bit) as HasCheckList,
	-- TODO: make Validate nullable in ModelBase...
	--cast(0 as bit) as Validate -- for non-nullable Validate property of ModelBase...
	pe.TimeSent as ParEmailSentAt,
	pd.Id as PdId
	from Par p
	-- DG changed these to left joins to enable creation of new Positions:
	--join Position pos
	--on pos.Id = p.PositionId
	--join hrms.WorkSchedule pws
	--on pos.DefaultWorkScheduleCode = pws.HRMSCode
	-- HERE, should pos be used for anything but checking for changes?
	left join ApprovalWorkflow wf
	on p.ApprovalWorkflowId = wf.Id
	left join Position pos
	on pos.Id = p.PositionId
	left join hrms.WorkSchedule pws
	-- DG: REMOVING inappropriate left joins to pos.
	-- only need Position for chacking changes.

	--on pos.DefaultWorkScheduleCode = pws.HRMSCode
	on p.PosWorkScheduleCode = pws.HRMSCode

	left join legacy.Location loc
	on p.PosLocationId = loc.ID
--	on pos.LocationId = loc.ID

	left join OrgUnit ou -- changed to left join since pos may now be null (new Positions)
	on p.PosOrgUnitId = ou.Id
--	on pos.OrgUnitId = ou.Id
	left join Appointment a
	on a.EmployeeId = p.EmployeeId
	-- TODO: test that removing Position from this join doesn't break anything???...
	--and a.PositionId = p.PositionId
--	and (a.EndDate is null or a.EndDate > getdate())
	and (a.StartDate < getdate() -- exclude future starts
		and (a.EndDate is null or a.EndDate > getdate())
	)
	left join Position sup
--	on pos.SupervisorPositionId = sup.Id
	on p.PosSupervisorPosId = sup.Id
	left join hrms.WorkSchedule ws
	on p.WorkScheduleCode = ws.HRMSCode
	--left join hrms.Job fj
	left join JobClass fj
	on p.FillLevel = fj.Id
	left join ActionCode ac on p.ActionCode = ac.Code
	left join ApptActionReason ar on p.ActionReasonId = ar.Id
	--left join hrms.Job j
	left join JobClass j

	-- WHY WAS THIS CHANGED???
	on p.PosJobId = j.Id
	--on pos.JobId = j.Id

	left join WorkContractType wc
	on p.ContractTypeCode = wc.WorkContractTypeCode
	and p.ActionCode not in ('P0', 'P1', 'P2') -- exclude position par match on default double-space
--	left join StageLegacy.CLAO.WACounty cty
	left join legacy.WACounty cty
	on cty.CountyID = EmpAddrCountyCode
	left join Employee e
	--on p.EmployeeId = e.Id
	on (
		e.Id = @EmpId
		or (@EmpId is null and e.Id = p.EmployeeId)
	)
	left join EmployeeDateSpec eds
	on eds.EmployeeId = isnull(@EmpId, p.EmployeeId)
	left join ApptActionReason aar
	on p.ActionReasonId = aar.Id
	left join PayScaleTypeArea pst
	on pst.Id = p.ApptPayScaleTypeArea
	left join PayScaleTypeArea pg
	on p.PosPayGradeTypeArea = pg.Id
	left join PayScaleReason psr
	on psr.Code = p.ApptPayScaleReason
	left join hrms.AgencyCode agt
	on p.TargetAgencyCode = agt.OfmAgencyCode
	left join Checklist ck on p.Id = ck.ParId
	left join ParEmail pe
	on p.Id = pe.ParId
	and pe.TimeSent = (
		select max(timesent) from paremail
		where parid = p.Id
	)
	left join PositionDescription pd
	on p.Id = pd.LinkedParId
	where p.Id = @Id
END