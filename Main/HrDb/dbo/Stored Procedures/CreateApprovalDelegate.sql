﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 8/20/13
-- Description:	create new delegate
-- =============================================
CREATE PROCEDURE [dbo].[CreateApprovalDelegate] 
	-- Add the parameters for the stored procedure here


	@DelegatedToPersonId int,
	@DelegatedPositionId int,
	@StartDate datetime,
	@EndDate datetime = null,
	@Indefinite bit = 1,
	@CreatorPersonId int = null,
	@CreatedDate datetime = null
	
AS
BEGIN


		INSERT INTO ApprovalDelegate
			   (DelegatedToPersonId,
			   DelegatedPositionId,
			   StartDate,
			   EndDate,
			   Indefinite,
			   Active,
			   CreatorPersonId,
				CreationDate)
		VALUES
			   (@DelegatedToPersonId,
			   @DelegatedPositionId,
			   @StartDate,
			   @EndDate,
			   @Indefinite,
			   1,
			   @CreatorPersonId,
				@CreatedDate)
			   
		select top 1 Id from ApprovalDelegate order by Id desc

			   
END