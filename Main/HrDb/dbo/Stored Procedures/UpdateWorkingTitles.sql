﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.UpdateWorkingTitles
AS
BEGIN
	update p
	set WorkingTitle = hp.PositionTitle
	from
	Position p
	join hrms.Position hp
	on p.HrmsPositionId = hp.Id
END