﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-08
-- Description:	Load Abolished Positions (assumes current positions already loaded)
-- *NOTE*, omitting SupervisorPositionId lookup for performance reasons and meaninglessness.
-- (could include SupervisorPersonId if wanted)
-- =============================================
CREATE PROCEDURE [dbo].[LoadAbolishedPositionsFast] 
AS
BEGIN
	insert into Position (Id, OrgUnitId, CostCenter, JobId,
		JobTitle, LegacyClassCode, LegacyPositionNum, LegacyUnitId,
		LegacyLastUpdated, WorkingTitle, HrmsPositionId, SupervisorPositionId,
		IsAbolished)
	(
		select cast(h.positionnum as int) as Id,
		null as OrgUnitId,
		h.CostCenter,
		null as JobId,
		h.JobTitle,
		h.PositionClassCode as LegacyClassCode,
		h.positionnum as LegacyPositionNum,
		h.UnitId as LegacyUnitId,
		--h.Supervisor as SupervisorPersonId,
		h.LastUpdatedDate as LegacyLastUpdated,
		null as WorkingTitle,
		null as HrmsPositionId,
		null as SupervisorPositionId, -- omitting for performance reasons
		cast(1 as bit) as IsAbolished
		from StageLegacy.organization.Employee_History h
		where
		h.positionnum is not null
		and h.positionnum <> ''
		and left(h.positionnum, 2) <> 'UG'
		and not exists (
			select 1 from hrdb.dbo.Position p
			where
			p.Id = cast(h.PositionNum as int)
		)
		and
		h.Id = (
			select max(id)
			from StageLegacy.organization.Employee_History h3
			where h3.PositionNum = h.PositionNum
		)
		and h.CostCenter is not null
--		order by cast(h.PositionNum as int)
	)
END