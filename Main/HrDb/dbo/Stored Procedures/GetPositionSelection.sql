﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-02-08
-- Description:
-- TODO: make this more like GetReportingStructure...
-- =============================================
CREATE PROCEDURE [dbo].[GetPositionSelection]
AS
BEGIN
	select
--	p.id as positionid, a.id as [Value],
	cast(p.id as varchar(10)) as [Value],
	replicate(': ', datalength(ps.ancestry)) +
--	isnull(p.WorkingTitle, jc.jobtitle) + ' - ' 
	+ case when a.id is null then '[VACANT]' else
	dbo.IsNullOrEmpty(e.preferredname, e.firstname) + ' ' + e.LastName
	end
	+ case when p.IsSupervisor = 1 then ' (' + ou.ShortName + ')' else '' end
	+ case when p.SupervisorEmpId > 0 then ', SE:' + dbo.IsNullOrEmpty(se.preferredname, se.firstname) + ' ' + se.lastname else '' end
	as Text
	from positionstructure ps
	join position p
	on ps.Id = p.Id
	and p.IsAbolished != 1
	join OrgUnit ou
	on p.OrgUnitId = ou.Id
	left join Employee se
	on p.SupervisorEmpId = se.Id
	left join appointment a
	on a.positionid = p.id
	and (a.enddate is null or a.enddate > getdate())
	left join employee e
	on a.employeeid = e.id
	left join jobclass jc
	on p.jobid = jc.Id
	or p.legacyclasscode = jc.LegacyCode
	order by ancestry;
END

--update OrgUnit set shortname = rtrim(shortname)