﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-08-18
-- Description:	Get Direct Report positions for a given EmpId
-- =============================================
CREATE PROCEDURE [dbo].[GetDirectReports] 
	@Id int,
	@LanId varchar(50) -- ignored for now...
AS
BEGIN
	--declare @Id int = 6039; -- Curtis
	--declare @Id int = 6404; -- Christy
	--declare @LanId varchar(50) = 'DIS\DavidG';
	--declare @Id int = 6402; -- Heidi
	select drp.Id as PositionId,
	jc.LegacyCode as LegacyClassCode,
	drp.WorkingTitle,
	dra.EmployeeId as EmployeeId,
	isnull(e.PreferredName, e.FirstName) as FirstName,
	e.LastName,
	pd.Id as PdId,
	pd.EffectiveDate as PdDate,
	pd.LastUpdatedAt,
	DATEDIFF(d, pd.LastUpdatedAt, getdate()) as DaysOld, -- not using EffectiveDate since may not exist...
	jc2.LegacyCode as PdJobClass
	from Appointment a
	join Position p
	on a.PositionId = p.Id
	join Position drp
	on drp.SupervisorPositionId = p.Id
	join JobClass jc
	on jc.Id = drp.JobId
	left join Appointment dra
	on drp.Id = dra.PositionId
	and (dra.EndDate is null or dra.EndDate > getdate())
	left join Employee e
	on dra.EmployeeId = e.Id
	left join PositionDescription pd
	on pd.Id = (select max(Id) from PositionDescription
		where PositionId = drp.Id) -- newest PD, not necessarily Approved/Completed
	left join JobClass jc2 on pd.ProposedJobId = jc2.Id
	where a.EmployeeId = @Id
	and (a.EndDate is null or a.EndDate > getdate())
	order by
	case when e.Id is null then 2 else 1 end,
	LegacyClassCode,
	drp.WorkingTitle,
	e.LastName
END