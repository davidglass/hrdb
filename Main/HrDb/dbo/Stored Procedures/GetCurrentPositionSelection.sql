﻿
-- =============================================
-- Author:		Mark Paul
-- Create date: 2013-11-21
-- Description:	option list for current employees with position as value
-- =============================================
CREATE PROCEDURE [dbo].[GetCurrentPositionSelection] 
AS
BEGIN
--set statistics time on;
	--select cast(p.Id as varchar(10)) as Value,
	--cast(p.Id as varchar(10)) + ': ' +
	--isnull(
	--	(select top 1 e.LastName +', '+ isnull(e.PreferredName, e.FirstName) from Appointment left join Appointment a on p.Id = a.PositionId
	--		left join Employee e on a.EmployeeId = e.Id where ((a.EndDate is null) or (a.EndDate > getdate()))),
	--	isnull(
	--		'Vacant: ' + (select top 1 e.LastName from Appointment left join Appointment a on p.Id = a.PositionId
	--			left join Employee e on a.EmployeeId = e.Id order by a.EndDate desc) --bad performance
	--		,'Vacant'
	--	)
	--)+ ': ' +
	--isnull(isnull(p.WorkingTitle, p.JobTitle),'') as [Text]
	--from Position p
	--where p.IsAbolished = 0

-- modified for performance: -dg
	select cast(p.id as varchar) as Value,
	cast(p.Id as varchar) + ': '
	+ isnull(p.WorkingTitle, isnull(j.JobTitle, p.LegacyClassCode + ' [PENDING]')) + ' - '
	+ isnull(e.LastName,
		'VACANT' +
			case when exists (select 1 from appointment where positionid = p.id)
			then ' (' +
			(select e.LastName
			from Appointment a
			join Employee e
			on a.EmployeeId = e.Id
			and a.PositionId = p.Id
			and a.EndDate = (select max(EndDate) from Appointment where PositionId = p.Id)
			) + ')'
			else '' end
	)
	+ case when a.Id is null then '' else ', ' + isnull(e.PreferredName, e.FirstName) end
	as [Text]
	from position p
	left join Appointment a	on p.Id = a.PositionId
	and (a.EndDate is null or a.EndDate > getdate())
	left join Employee e on e.Id = a.EmployeeId
	--left join hrms.Job j on p.JobId = j.Id
	left join JobClass j on p.JobId = j.Id
	where IsAbolished = 0
	order by p.Id
END