﻿

CREATE PROCEDURE [dbo].[GetPayScaleTypeArea]
AS
BEGIN
	select cast(Id as char(1)) as [Value], -- values must be char/string to use as SelectListItems 
	description as [Text]
	from PayScaleTypeArea
	order by description
END