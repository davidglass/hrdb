﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-06-24
-- Description:	get user id
-- =============================================
CREATE PROCEDURE [dbo].[GetUserId] 
	-- Add the parameters for the stored procedure here
	@UserName varchar(50)
AS
BEGIN
	DECLARE @UserId int

	select @UserId = Id from AppUser
	where LanId = @UserName

	if @UserId is null begin
	-- put next val as default for AppUser
	select @UserId = (NEXT VALUE FOR [AppUser_seq]);
		--select @UserId = convert(int, current_value) FROM sys.sequences
		--WHERE name = 'AppUser_seq';
		insert into AppUser (Id, LanId)
		values (@UserId, @username);
	end

	return @UserId;
END