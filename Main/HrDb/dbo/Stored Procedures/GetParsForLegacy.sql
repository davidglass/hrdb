﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-10-04
-- Description:	Gets new PAR list for pushing to legacy system
-- =============================================
CREATE PROCEDURE [dbo].[GetParsForLegacy]
AS
BEGIN
select
--a2.PositionId as A2Positionid,
--a.PositionId as APositionid,
	par.Id as NewParId,
-- these fields are for Par.Appointment:
	e.HrmsPersonId,
	e.lanid,
	case when actioncode = 'P0' then 'Vacant Vacant'
		else case when par.EmpPreferredName = '' then par.EmpFirstName else par.EmpPreferredName end + ' ' + par.EmpLastname end as FullName,
	case when actioncode = 'P0' then 'Vacant'
		else par.EmpLastname end as LastName,
	case when actioncode = 'P0' then 'Vacant'
		else case when par.EmpPreferredName = '' then par.EmpFirstName else par.EmpPreferredName end end as FirstName,
	par.EmpMiddleName as MiddleInitial,
	--case when actioncode = 'P0' then pj.JobTitleLegacy else aj.JobTitleLegacy end as EmpClassCode,
	case when actioncode = 'P0' then pj.LegacyCode else aj.LegacyCode end as EmpClassCode,
	case when actioncode = 'P0' then 'Vacant Vacant'
	else UPPER(par.EmpLastName) + ', ' + UPPER(par.EmpFirstname) end as PayrollName,

-- these fields are for Par.PARDATA:			
	case
	when actioncode in ('U0', 'U6')
	-- Should only count as fill for U3 when position has changed.
	-- (prior appointment for employee exists and has different position id)
	or (actioncode = 'U3'
		and a.PositionId is not null
		and a2.PositionId is not null
		and a2.PositionId <> a.PositionId
	)
	or IsTransferIn = 1
	then 2 -- fill existing position
	when actioncode = 'U5' or (actioncode = 'U3' and actionreasonid  = '81') or IsTransferOut = 1 then 1 -- separation
	when actioncode = 'P2' then 10 -- abolish
	when actioncode = 'P1' then 12 -- update position (new type, no change to employee, nor x-fer within agency or reallocate)
	when actioncode = 'P0' then 3 -- new position
	when actioncode = 'U3' and actionreasonid = 2 then 7 -- schedule change
	-- TODO: enforce reallocation, complete in-training allowed only for classified appointments?
	-- TODO: modify these in statements to search description???  Or make sure they never change in future.
	when (actioncode = 'U3' and actionreasonid in (64,65,66,67)  or (actioncode = 'U3' and actionreasonid in (17,60)) and par.FillLevel = par.PosJobId) then 6 -- reallocate
	-- TODO: if U3 action is not a fill or schedule change or training complete
	--select * from ApptActionReason where ApptChange_U3 is not null
	when actionreasonid in (23,24) then 4 -- can be U3 ApptChange *or* UJ StatusChange
	when (actioncode = 'P1' and ((par.PosSupervisorPosId <> p.SupervisorPositionId) or (par.PosCostCenter <> p.CostCenter) or (par.PosOrgUnitId <> p.OrgUnitId))) then 5 -- TODO: count as "Transfer within Agency" (partypeid 5) if Position CostCenter, SupervisorPos, or OrgUnit are modified???
	-- (or just use "update position" action) #12
	when actioncode = 'U3' and actionreasonid = 60 then 13 -- name change
	else null end as partypeid,

	case
	when actioncode in ('U0', 'U6')
	-- Should only count as fill for U3 when position has changed.
	-- (prior appointment for employee exists and has different position id)
	or (actioncode = 'U3'
		and a.PositionId is not null
		and a2.PositionId is not null
		and a2.PositionId <> a.PositionId
	)
	or IsTransferIn = 1
	then 'Fill Current/Existing Position' -- fill existing position
	when actioncode = 'U5' or (actioncode = 'U3' and actionreasonid  = '81') or IsTransferOut = 1 then 'Separation' -- separation
	when actioncode = 'P2' then 'Abolish Position' -- abolish
	when actioncode = 'P1' then 'Update Position' -- update position (new type, no change to employee, nor x-fer within agency or reallocate)
	when actioncode = 'P0' then 'Create New Position' -- new position
	when actioncode = 'U3' and actionreasonid = 2 then 'Schedule Change' -- schedule change
	-- TODO: enforce reallocation, complete in-training allowed only for classified appointments?
	-- TODO: modify these in statements to search description???  Or make sure they never change in future.
	when (actioncode = 'U3' and actionreasonid in (64,65,66,67) or (actioncode = 'U3' and actionreasonid in (17,60)) and par.FillLevel = par.PosJobId) then 'Reallocate Position (Classified only)' -- reallocate
	-- TODO: if U3 action is not a fill or schedule change or training complete
	--select * from ApptActionReason where ApptChange_U3 is not null
	when actionreasonid in (23,24) then 'Completion of In-Training (Classified only)' -- can be U3 ApptChange *or* UJ StatusChange
	when (actioncode = 'P1' and ((par.PosSupervisorPosId <> p.SupervisorPositionId) or (par.PosCostCenter <> p.CostCenter) or (par.PosOrgUnitId <> p.OrgUnitId))) then 'Transfer Position within Agency '-- TODO: count as "Transfer within Agency" (partypeid 5) if Position CostCenter, SupervisorPos, or OrgUnit are modified???
	-- (or just use "update position" action) #12
	when actioncode = 'U3' and actionreasonid = 60 then 'Name Change' -- name change
	else null end as partype,

	'Completed' as workflowstatus,
	initEmp.Id as personID_Originator,
	initEmp.PreferredName + ' ' + initEmp.LastName as OriginatorName,
	CreateDate as DateOriginated,
	'New PAR #' + cast(par.Id as varchar(10)) as HRcomments,

	isnull(p.legacypositionnum, par.PositionId) as positionNum,
	-- TODO: logic for managing IsPrimary...only 1 incumbent can be Primary.
	case when a.IsPrimary = 1 or a.Id is null then 0 else 1 end as subposnum,
	case when par.ContractTypeCode in ('00', '04', '05', '11') then 'Y' else 'N' end
		as employeeintraining,  --Employee  (intrainingindicator, and intraining fields in Appoinment and ParData)

	case when (par.ContractTypeCode in ('20', '21', '22') or par.ActionCode = '64') then 'Y' else 'N' end
		as project,  --Project Position

	case when (par.ContractTypeCode in ('14', '15') or par.ActionCode in ('53', '54', '55', '56', '57', '58')) then 'Y' else 'N' end
		as nonperm,  --NonPerm Position
	
	case when par.PosIsInTraining = 1 then 'Y' else 'N' end as positionintraining, --Position (In_training and positionintraining fields in Appoinment and ParData)
	
	case when par.ContractTypeCode in ('12', '14', '15') then 'Y' else 'N' end
		as nonpermanent,
	
	case when par.PosPayGradeTypeArea = 1 then 'Y' else 'N' end as CBA,
	cast(par.Notes as varchar(1000)) as parComment,
	cast(par.Notes as varchar(1000)) as Comments,
	case when actioncode in ('U0', 'U6') then EffectiveDate else e.HireDate end as DISHireDate,

	--aj.JobTitleLegacy as classCode,
	p.CostCenter,
	case when p.legacyunitid is null then 168 else p.legacyunitid end as unitID,
	'CTS' as abbreviation, -- =~ DIV abbr. in legacy, but this should work too.
	--pj.JobTitleLegacy as positionclasscode,
	pj.LegacyCode as positionclasscode,
	case when actioncode in ('U0', 'U6') or IsTransferIn = 1 then 'Y' else 'N' end as newhire,
	--case when initEmp.LegacyPersonId <> e.Id then initEmp.LegacyPersonId else  isnull(e.Id, 0) end  as fillpersonid,
	case when par.EmployeeId <> e.LegacyPersonId then e.LegacyPersonId else par.EmployeeId end as fillpersonid,
	case when par.EmpPreferredName = '' then par.EmpFirstName else par.EmpPreferredName end  + ' ' + par.EmpLastName as fillfullname,
	
	case when e2Super.LegacyPersonId is null then eapptwork.LegacyPersonId else e2Super.LegacyPersonId end as SupervisorId,
	
	par.EffectiveDate as effectivedate,
	par.PosJvac as JVAC,
	par.PosPointValue as PointValue,
	par.ApptPayAnnualSalary as Salary,
	par.ApptPayScaleGroup as PayRange,
	par.ApptPayScaleLevel as Step,
	par.EmpDS01_Anniversary as AnniversaryDate,
	case when par.PosReqsBackgroundCheck = 1 then 'Y' else 'N' end BackgroundCheck, 
	case when par.PosIsTandem = 1 then 'Y' else 'N' end Tandem, 
	case when par.PosIsSection4 = 1 then 'Y' else 'N' end Section4,
	case when par.PosIsSupervisor = 1 then 'Y' else 'N' end SupervisorIndicator,
	case when par.PosIsManager = 1 then 'Y' else 'N' end ManagerIndicator,
	case when par.PosIsAppointingAuthority = 1 then 'Y' else 'N' end AppointingIndicator,
	par.PosLocationId as Location,
	case when par.ApptWorkHoursWk1 is null and actioncode not in ('P0','P1','P2')  then ws.Description 
	when actioncode  in ('P0','P1','P2') then pws.Description  else ('Week 1: ' + par.ApptWorkHoursWk1 + ' Week 2: ' + par.ApptWorkHoursWk2) end as WorkSchedule,
	case when par.posshiftcode  = 'D' then 'Day'
	when par.posshiftcode  = 'S' then 'Swing'
	when par.posshiftcode  = 'G' then 'Grave' end as [Shift],
	case when par.PosEeSubgroup = '01' then 'Exempt'
	when (par.PosEeSubgroup <> '01' and par.PosOrgUnitId in (31008300, 31008301, 31008302, 31008325, 31008326, 31008332) and (par.PosIsSupervisor = 0 or par.PosIsManager = 0 or par.PosIsAppointingAuthority = 0))  then 'Eligible-Shift'
	else 'Eligible' end as workperiod,
	par.PosWorkersCompCode as WorkersComp,
	isnull(e.LegacyPersonId, par.EmployeeId) as Personid,
	ad.telephonenumber as workphone,
	par.ApptWorkHoursWk1 as workhours,
	case when par.ApptWorkHoursWk1 is null then pws.Description else par.ApptWorkHoursWk1 end as workdays,
	case when par.ApptStandbyPay = 1 then 'Y' else 'N' end as StandbyPay,
	case when par.ApptShiftDifferential = 1 then 'Y' else 'N' end as ShiftDifferential,
	case when userrole.roleid = 2 or userrole.roleid = 1 then 'Y' else 'N' end as hrgroup,
	case when userrole.roleid = 2 or userrole.roleid = 1 then 'Y' else 'N' end as payrollgroup,
	case when userrole.roleid = 1 then 'Y' else 'N' end as admingroup

	
from par
join parstatus ps
on par.StatusId = ps.id
left join appointment a
on (
	par.employeeid = a.employeeid
	and par.positionid = a.positionid
	and (a.enddate is null or a.enddate > getdate())
)
--left join hrms.Job aj on par.FillLevel = aj.Id
left join JobClass aj on par.FillLevel = aj.Id
--left join hrms.Job pj on par.PosJobId = pj.Id
left join JobClass pj on par.PosJobId = pj.Id
left join appointment a2
on (
	par.employeeid = a2.employeeid
	and a2.enddate = a.startdate
	--and a.startdate = par.EffectiveDate
	and a2.enddate = par.effectivedate
	and a2.startdate <> a2.enddate -- exclude accidental appointments
)
--on (par.employeeid = a.employeeid
--and par.positionid <> a.positionid)
left join position p
on par.positionid = p.id
left join employee e
-- TODO: check that this is a legacy employee ?  New emps may have different legacy ids.
on par.employeeid = e.id
left join position spos on p.SupervisorPositionId = spos.Id
and p.SupervisorPositionId = spos.Id
left join appointment spa on
spa.PositionId = spos.Id
and spa.IsPrimary = 1
and (spa.EndDate is null
	or (
		spa.EndDate = (select max(EndDate) from appointment where positionid = spos.Id)
		and not exists (
			select 1 from appointment where positionid = spos.Id
			and enddate is null
		)
	)
)
left join appuser au
on par.RequestedByUserId = au.Id
left join employee initEmp
--on substring(au.LanId, 5, len(au.LanId)-4) = initEmp.lanid
on au.EmployeeId = initEmp.id
left join hrms.WorkSchedule as ws on ws.HRMSCode = par.WorkScheduleCode
left join adextract as ad on ad.samaccountname = e.lanid
left join hrms.workschedule as pws on pws.HRMSCode = par.PosWorkScheduleCode
left join appuser as userrole on userrole.EmployeeId = e.legacypersonid

-- get legacy supervisor personid of position in par, do not use position supervisor id, use par position supervisor
left join appointment a2Super on a2Super.PositionId = par.PosSupervisorPosId 
--and a2Super.EndDate is null 
	and ((a2Super.enddate is null or a2Super.enddate > getdate()) and a2Super.StartDate <= getdate())
left join employee e2Super on e2Super.id = a2Super.employeeid 
	

-- if null supervisor get supervisor in the workflow
left join ApprovalWorkflow aw on aw.Id = par.ApprovalWorkflowId
left join ApprovalTask at on at.ApprovalWorkflowId = aw.id and at.WorkflowOrder = 1
left join appointment apptwork on apptwork.PositionId = at.ApproverPositionId and apptwork.enddate is null
left join employee eapptwork on eapptwork.Id = apptwork.EmployeeId

where actioncode not in ('U8', 'U9', 'UA') -- exclude LOA-related PARs
and ps.description = 'Processed' and IsPayChangeOnly <> 1
order by par.Id 

END