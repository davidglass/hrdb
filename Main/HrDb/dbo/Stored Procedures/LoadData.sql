﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-09-18
-- Description:	truncates / empties and loads / reloads
--		Appointment,
--		Employee,
--		EmployeeDateSpec,
--		Position,
--		OrgStructure,
--		OrgUnit
--	includes historical DIS employees, positions, and appointments.
-- =============================================
CREATE PROCEDURE [dbo].[LoadData]
AS
BEGIN
truncate table Appointment;
delete from Employee;
truncate table EmployeeDateSpec;
truncate table DesignatedWorkWeek;

delete from PositionStructure;
delete from Position; -- before OrgUnit to prevent FK issue
--delete from hrms.Job; -- this may be obsolete
delete from JobClass; -- this replaced hrms.Job
delete from AppUser_ExpandedOrgNode;
delete from OrgStructure;
delete from OrgChartOrgUnit;
delete from OrgUnit;

-- TODO: don't truncate hrms.job, sync to preserve obsolete job codes...
-- *NOTE*, added dbo.JobClass table for this purpose, so truncating hrms.job is OK.
print 'step 01: ' + CONVERT(varchar, SYSDATETIME(), 121);
--insert into hrms.Job (Id, JobTitle, JobTitleLegacy)
--(select distinct JobId, JobTitle, JobTitleLegacy from StageHRMS.dbo.Position);
exec LoadJobClasses;

-- clear, reload Employee:
-- OK to populate directly from Stage for now...
-- should only need to run this once since new Employees will originate in new system in future. 
print 'step 02: ' + CONVERT(varchar, SYSDATETIME(), 121);
insert into employee (
	Id,
	LegacyPersonId,
	Lanid,
	HrmsPersonId,
	FirstName,
	PreferredName,
	MiddleName,
	LastName,
	Suffix,
	DOB,
	LastUpdated,
	EmailAddress, -- HRMS Email?
	Gender
)

select e.PersonId as Id,
	e.PersonId as LegacyPersonId,
	Lanid,
	case when doppersonid = '' then null
	else cast(doppersonid as int) end as HrmsPersonId,
	-- this leaves FirstName null for former DIS employees that went to DES:
	isnull(p.FirstName, e.FirstName) as FirstName,
	e.FirstName as PreferredName,
	p.MiddleName,
	e.LastName,
	null as Suffix, -- need Suffix from Gap data (currently omitted) if using.
	p.BirthDate as DOB,
	updated_date as LastUpdated,
	null as EmailAddress, -- TODO: populated from AdExtract
	p.Gender
	from StageLegacy.organization.Employee e
	left join StageHRMS.dbo.Person p
	on try_convert(int, e.DOPPersonId) = p.Id
	where e.firstname <> 'vacant'
	and e.lastname <> 'vacant'
	and e.firstname <> ''
	and e.lastname <> '';
	exec LoadExEmployees;

-- update personid to legacy personid field in employee table
-- (moved to insert above)
	--update e
	--set LegacyPersonid = l.id
	--from
	--Employee e
	--join employee l
	--on l.id = e.Id
	--where e.LegacyPersonId is null

	update e
	set emailaddress = ad.mail
	from Employee e
	join ADExtract ad
	on ad.userprincipalname = e.lanid + '@dis.wa.lcl'

-- reload OrgUnit:
print 'step 03: ' + CONVERT(varchar, SYSDATETIME(), 121);
insert into OrgUnit (
	Id,
	HrmsOrgUnitId,
	Description,
	ShortName,
	Abbreviation,
	SupervisorPosId
)
select Id, Id, OrgUnitDescription, OrgCodeLegacy, null, null
from stageHRMS.dbo.OrgUnit;

print 'step 04: ' + CONVERT(varchar, SYSDATETIME(), 121);
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008202, N'CTS', N'CTS')
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008251, N'Computing Services', N'CSD')
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008253, N'Telecom Services', N'TSD')
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008254, N'Customer Relations', N'CRT')
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008255, N'ATS', N'ATS')
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008256, N'Capital Projects', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008257, N'Security Services', N'ESS')
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008258, N'State Data Center', N'SDC')
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008259, N'Server Ops Support', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008260, N'CSD Planning & Design', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008261, N'Messaging', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31013237, N'Mainframe Services', N'MFS')
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008263, N'Mainframe Computing', N'MFC')
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008264, N'Storage', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008269, N'Telephony', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008276, N'Network Operations', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008300, N'MF Cmd Day', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008301, N'MF Cmd Grave', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008302, N'MF Cmd Swing', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008304, N'SDC Critical Environment', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008306, N'Enterprise Projects', N'EPO')
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008325, N'NCC Day', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008326, N'NCC Swing', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008328, N'Telephony Support', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008329, N'Enterprise Data Network', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008332, N'Service Desk', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008334, N'CSD PMO', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008335, N'Extended Messaging', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008336, N'Email/Vault/Gateway', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008337, N'Server Support', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008339, N'SQL Support', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008340, N'MF Database / Security', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008341, N'MF Operating Systems', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008342, N'MF Online / Util', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008344, N'Storage Support', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008405, N'SDC Space Mgmt', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008406, N'SDC Physical Security', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31008408, N'SDC Arch & Eng', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31011250, N'TSD Planning & Design', NULL)
INSERT [dbo].[OrgChartOrgUnit] ([OrgUnitId], [OrgUnitName], [Abbreviation]) VALUES (31012376, N'MF Command Center', NULL);

-- OrgStructure has FK to OrgUnit, could break if OrgUnits disappear without updating it:
print 'step 05: ' + CONVERT(varchar, SYSDATETIME(), 121);
exec RebuildOrgStructure;

--clear, insert current Positions:
--delete from Position;
print 'step 06: ' + CONVERT(varchar, SYSDATETIME(), 121);
insert into Position (
	Id,
	OrgUnitId,
	CostCenter,
	JobId,
	LegacyClassCode,
	LegacyPositionNum,
	LegacyUnitId,
	LegacyLastUpdated,
	WorkingTitle,
	HrmsPositionId,
	SupervisorPositionId,
	IsAbolished,
	CBA,
	ValidFromDate,
	ValidToDate,
	IsInTraining,
	PersonnelSubArea,
	IsBackgroundCheckRequired,
	WorkersCompCode,
	IsSupervisor,
	IsManager,
	IsAppointingAuthority,
	EeGroup,
	EeSubGroup,
	IsSection4,
	PayGradeType,
	PayGradeArea,
	PayGrade,
	IsTandem,
	LocationId,
	DefaultWorkScheduleCode,
	PayGradeTypeArea,
	ShiftDesignation,
	Jvac,
	PointValue,
	IsRetireEligible
)
-- DISTINCT to exclude multifills from schedule lookup
select cast(p.positionnum as int) as Id,
--e.FullName as SupervisorName, (includes VACANT Robins and VACANT Dell)
hp.OrgUnitId,
CostCenter,
hp.JobId,
--null as JobId, -- ClassCode is legacy field; can correlate to HRMS JobId.
p.ClassCode as LegacyClassCode,
p.positionnum as LegacyPositionNum,
UnitId as LegacyUnitId,
--h.Supervisor as SupervisorPersonId,
LastUpdatedDate as LegacyLastUpdated,
rtrim(hp.PositionTitle) as WorkingTitle,
hp.Id as HrmsPositionId,
case when p.positionnum = '5202' then null -- omit legacy Director supervisor; causes infinite recursion.
else cast (e.PositionNum as int) end as SupervisorPositionId,
cast(0 as bit) as IsAbolished,
-- CBA value in legacy is unreliable; use PersonnelSubArea from HRMS instead:
case when hp.PersonnelSubArea in ('00JZ', '00GW') then 1 else 0 end as CBA,
--case when p.CBA = 'Y' then 1 else 0 end as CBA,
hp.EffectiveDate as ValidFromDate,
hp.EndDate as ValidToDate,
-- TODO: consider HRMS ContractType if p.In_training value is unreliable:
case when p.In_training = 'Y' then 1 else 0 end as IsInTraining,
hp.PersonnelSubArea,
case when p.backgroundcheck = 'Y' then 1 else 0 end as IsBackgroundCheckRequired,
p.workerscomp as WorkersCompCode,
case when p.SupervisorIndicator  = 'Y' then 1 else 0 end as IsSupervisor,
case when p.ManagerIndicator  = 'Y' then 1 else 0 end as IsManager,
case when p.AppointingIndicator  = 'Y' then 1 else 0 end as IsAppointingAuthority,
hp.EmployeeGroupCode as EeGroup,
hp.EmployeeSubGroupCode as EeSubGroup,
case when p.section4 = 'Y' then 1 else 0 end as IsSection4,
hp.PayGradeType,
hp.PayGradeArea,
rtrim(hp.PayGrade) as PayGrade,
case when p.tandem = 'Y' then 1 else 0 end as IsTandem,
cast (p.MailStop as int) as LocationId,
-- TODO: see if this should always be set to FULL instead of copying from current assignment?
isnull(ws.WorkSchedule, 'FULL') as DefaultWorkScheduleCode,
ta.Id as PayGradeTypeArea,
p.Shift as ShiftDesignation,
p.jvac,
try_convert(int, p.PointValue) as PointValue, -- exclude "N/A" point value.
1 as IsRetireEligible -- TODO: get this from Gap data.  All are currently eligible.
from StageLegacy.organization.positions p
join StageLegacy.organization.employee e
on p.Supervisor = e.PersonId
left join stageHRMS.dbo.Position hp
on p.positionnum = hp.PositionTitleLeg
left join stageHRMS.dbo.OrganizationalAssignment oa
on oa.PositionId = hp.Id
and oa.EndDate > '3000-01-01'
left join stageHRMS.dbo.EmployeeWorkSchedule ws
on ws.PersonId = oa.PersonId
left join PayScaleTypeArea ta
on ta.PsArea = hp.PayGradeArea
and ta.PsType = hp.PayGradeType
where agency is null
-- exclude multifilled positions from current schedule lookup (e.g. CSD DIR 5353):
and (
	oa.PersonId = (
		select min(PersonId)
		from stageHRMS.dbo.OrganizationalAssignment oa2
		where PositionId = hp.Id
		and oa2.EndDate > '3000-01-01'
	)
	or oa.PersonId is null
);
update position set WorkingTitle='HR MANAGER' where id = 5430; -- special-case Margaret's working title...

print 'step 07: ' + CONVERT(varchar, SYSDATETIME(), 121);
-- this is the time-consuming proc, not really needed?
--exec LoadAbolishedPositions;
exec LoadAbolishedPositionsFast;
print 'step 08: ' + CONVERT(varchar, SYSDATETIME(), 121);
exec LoadCurrentAppointments;

print 'step 08a: ' + CONVERT(varchar, SYSDATETIME(), 121);
exec RebuildPosStructure;

print 'step 09: ' + CONVERT(varchar, SYSDATETIME(), 121);
exec LoadEmployeeDateSpec; -- loads directly from stage...TODO: consider whether need intermediate location???

-- set Mike M to non-primary:
print 'step 10: ' + CONVERT(varchar, SYSDATETIME(), 121);
update appointment set isprimary = 0 where employeeid = 6722 and positionid = 5353;

print 'step 11: ' + CONVERT(varchar, SYSDATETIME(), 121);
exec LoadHistoricalAppts;

print 'step 12: ' + CONVERT(varchar, SYSDATETIME(), 121);
exec SetContractEndDates;

---Add the Designated Work Week values from stageLeagcy
print 'step 13: ' + CONVERT(varchar, SYSDATETIME(), 121);
insert into DesignatedWorkWeek
(		PositionId,
		DesignatedWorkWeek,
		BeginDay,
		BeginHour,
		BeginMinute,
		BeginAMPM,
		EndDay,
		EndHour,
		EndMinute,
		EndAMPM
)

select PositionNum,
      DesignatedWorkWeek,
      BeginDay,
      BeginHour,
      BeginMinute,
      BeginAMPM,
      EndDay,
      EndHour,
      EndMinute,
      EndAMPM
	  from stageLegacy.organization.DesignatedWorkWeek

END