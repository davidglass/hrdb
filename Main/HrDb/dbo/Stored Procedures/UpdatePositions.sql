﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-02-09
-- Description:	
-- TODO: add @LanId, permission check...
-- =============================================
CREATE PROCEDURE UpdatePositions 
	@PosIds varchar(max),
	@NewSupervisorEmpId int
AS
BEGIN
	update Position
	set SupervisorEmpId = @NewSupervisorEmpId
	where @PosIds like '%,' + cast(Id as varchar(10)) + ',%'
END