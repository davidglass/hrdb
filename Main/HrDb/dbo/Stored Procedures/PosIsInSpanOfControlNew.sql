﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2014-11-03
-- Description:	
-- =============================================
create PROCEDURE [dbo].[PosIsInSpanOfControlNew] 
	-- Add the parameters for the stored procedure here
	@PositionId int, 
	@LanId varchar(50),
	@AppId int
AS
BEGIN
	select
	case when exists (
		select 1 from AppUser u
		where u.LanId = @LanId
		and u.RoleId in (1,2) -- Admin, HR
	) then cast(1 as bit)
	else	
		case when (
			exists (
				select 1 from GetSpanOfControl(@LanId) p
				where p.Id = @PositionId
			)
			or (select dbo.IsPosDelegated(
				@AppId,
				@PositionId,
				@LanId,
				getdate() -- @AsOf
			)) = 1
		)
		then cast(1 as bit)
		else cast(0 as bit)	
		end
	end as IsSub
END