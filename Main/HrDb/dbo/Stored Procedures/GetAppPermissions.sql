﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-11-01
-- Description:	gets CRUD permissions based on Controller type.
-- (e.g., ParController).  See AppController table.
-- TODO: pass IsApi to allow separate permission sets for MvcController and ApiController with same names.
-- =============================================
CREATE PROCEDURE [dbo].[GetAppPermissions] 
	@LanId varchar(255), 
	@AppName varchar(255)
AS
BEGIN
--declare @LanId varchar(255) = 'DIS\WendyH';
--declare @AppName varchar(255) = 'PositionDescription';
	select
	-- controller-specific permissions can granularly enhance AppUser Role permissions:
	-- Admin: CRUD, HR: CRUD, Supervisor: CRU
	case when au.RoleId in (1,2,3) then cast(1 as bit) else isnull(arp.CanCreate, cast(0 as bit)) end as CanCreate,
	case when au.RoleId in (1,2,3,4,5) then cast(1 as bit) else isnull(arp.CanRead, cast(0 as bit)) end as CanRead,
	case when au.RoleId in (1,2,3) then cast(1 as bit) else isnull(arp.CanUpdate, cast(0 as bit)) end as CanUpdate,
	case when au.RoleId in (1,2) then cast(1 as bit) else isnull(arp.CanDelete, cast(0 as bit)) end as CanDelete
	from
	appuser au
	left join AppController app
	on app.ControllerName = @appname
	-- removed this section to simplify App-level permissions by AppUser.RoleId
	--left join
	--appuserrole aur
	--on au.Id = aur.UserId
	--and app.Id = aur.AppId
	-- **NOTE**, app.Id below is AppController.Id, *NOT* ApprovalApp.Id.
	left join AppRolePermissions arp
	on arp.AppId = app.Id
	--and arp.RoleId = aur.RoleId
	and arp.RoleId = au.RoleId
	where au.LanId = @LanId

END