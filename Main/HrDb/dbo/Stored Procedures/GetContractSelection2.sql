﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-06-19
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetContractSelection2] 
AS
BEGIN
  select case when WorkContractTypeCode = '  ' then '--'
	else WorkContractTypeCode end
  as Value,
  WorkContractTypeDesc as [Text]
  from WorkContractType
  order by WorkContractTypeDesc
END