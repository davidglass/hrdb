﻿
-- =============================================
-- Author:		Pauline
-- Create date: <Create Date,,>
-- Description:	Update Par staus to pushed after par process has run
-- =============================================
CREATE PROCEDURE [dbo].[UpdateParStatus]
AS
BEGIN
	update p
	set p.StatusId = 5
	from
	Par p
	join ParPushed pp
	on pp.ParId = p.Id
	where p.statusid =  4
END