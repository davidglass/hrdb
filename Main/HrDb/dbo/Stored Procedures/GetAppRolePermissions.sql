﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-01-24
-- Description:	
-- =============================================
CREATE PROCEDURE GetAppRolePermissions 
AS
BEGIN
  select
  ControllerName,
  c.Id as ControllerId,
  r.Name as RoleName,
  r.Id as RoleId,
  CanCreate, CanRead, CanUpdate, CanDelete
  from appcontroller c
  join approlepermissions p
  on c.Id = p.AppId
  join approle r
  on p.roleid = r.id
  where isapi = 0
  order by controllername, r.name
END