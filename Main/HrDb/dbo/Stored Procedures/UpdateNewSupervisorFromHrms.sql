﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-09-09
-- Description:	Update supervisor positions when they change in HRMS
-- (due to handling of supervisor vacancies in ESS Leave system).
-- =============================================
CREATE PROCEDURE UpdateNewSupervisorFromHrms
AS
BEGIN
	update p
	set supervisorpositionid = sp.Id
	from Position p
	join stagehrms.dbo.position hrp
	on p.HrmsPositionId = hrp.Id
	join Position sp
	on hrp.SupervisorPositionId = sp.HrmsPositionId
	join Position sp2
	on p.SupervisorPositionId = sp2.Id
	where hrp.SupervisorPositionId <> 0
	and hrp.SupervisorPositionId <> sp2.HrmsPositionId;

	exec RebuildPosStructure;
END