﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-08-06
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetPositionDetail] 
	@PosId int,
	@LanId varchar(50)
AS
BEGIN
--declare @PosId int = 5078;
--declare @LanId varchar(50) = 'DIS\DavidG'
--declare @PosId int = 6332;

		-- TEST ACCESS before running query:
	declare @CanRead int; -- sic
	select @CanRead = case when 
		dbo.CanReadAll(@LanId, 'Position') = 1
		or exists -- else only span of control.
		( select 1 from Position p
		join dbo.GetSpanOfControl(@LanId) span
		on p.Id = span.Id
		and p.Id = @PosId )
	then 1
	else 0
	end
	if @CanRead = 0 begin
		raiserror('Access Denied.',11,1) with seterror
		return
	end

	select
	p.Id,
	p.LegacyPositionNum,
	p.WorkingTitle,
	p.JobId,
	-- default to hrms JobTitle for current positions, use cc.JobTitle for legacy:

-- replaced with dbo.JobClass:
	--case when p.JobId is null then cc.JobTitle
	--else j.JobTitle end as JobTitle,
	j.JobTitle,
	p.CostCenter,
	p.HrmsPositionId,
	p.PersonnelSubArea,
--	isnull(p.LegacyClassCode, j.JobTitleLegacy) as LegacyClassCode,
	isnull(p.LegacyClassCode, j.LegacyCode) as LegacyClassCode,

	case when p.ShiftCode = 'D' then 'Day'
	when p.ShiftCode = 'S' then 'Swing'
	when p.ShiftCode = 'G' then 'Grave' end as ShiftCode,
	p.IsAppointingAuthority,
	p.IsManager,
	p.IsSupervisor,
	p.IsBackgroundCheckRequired,
	p.IsSection4,
	p.IsTandem,
	p.IsInTraining,
	p.Jvac,
	p.PointValue as JvacPoints,
	p.OrgUnitId,
	ws.Description + ' (' + p.DefaultWorkScheduleCode + ')' as WorkSchedDesc,
	ou.Description as OuDescription,
	eg.Description + ' (' + p.EeGroup + ')' as EeGroupDescription,
	esg.Description + ' (' + p.EeSubGroup + ')' as EeSubGroupDescription,
	wcc.Description + ' (' + p.WorkersCompCode + ')' as WorkersCompDesc,
--	psta.PayGradeTypeArea, p.PayGrade,
	--psta.PsType as PayGradeType,
	--psta.PsArea as PayGradeArea,
	psta.Description + ' (' + psta.PsType + '-' + psta.PsArea + ')' as PayGradeTypeAreaDesc,
	p.PayGrade,

	pal.KeyCompId,
	pal.PdfId,
	pal.WmsId
	from Position p
--	join OrgUnit ou
	-- newly-created legacy Positions have no OrgUnits, HrmsId, or JobIds (since they are not in HRMS yet).
	left join OrgUnit ou
	on p.OrgUnitId = ou.Id
	left join JobClass j
	on (p.JobId = j.Id
		or (
			p.JobId is null
			and
			p.LegacyClassCode = j.LegacyCode
		)
	)

	--left join hrms.Job j
	--on p.JobId = j.Id
	
	-- combined hrms.Job and legacy.Class_Codes into dbo.JobClass
	--left join legacy.Class_Codes cc
	--on p.LegacyClassCode = cc.ClassCode

	left join EmployeeGroupLkUp eg on p.EeGroup = eg.EmployeeGroupCode
	left join EmployeeSubGroupLkUp esg on p.EeSubGroup = esg.EmployeeSubGroupCode
	left join legacy.PositionAppLinkId pal on p.Id = pal.PositionId
	left join WorkersCompCode wcc
	on p.WorkersCompCode = wcc.Code
	left join PayScaleTypeArea psta on p.PayGradeTypeArea = psta.Id
	left join hrms.WorkSchedule ws on p.DefaultWorkScheduleCode = ws.HRMSCode
	where p.Id = @PosId;
END