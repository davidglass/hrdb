﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-12-17
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetExpiringContracts60DayNotice] 
AS
BEGIN

-- TODO: move this to nightly db refresh process:
--update e
--set emailaddress = ad.mail
--from Employee e
--join appointment a on e.id = a.EmployeeId
--and (a.enddate is null or a.enddate > getdate())
--left  join adextract ad
--  on e.lanid + '@dis.wa.lcl' = ad.userprincipalname

	select
	isnull(se.PreferredName, se.FirstName) + ' ' + se.LastName as SupervisorName,
	se.EmailAddress,
	a.Id as Id,
	e.Id as EmpId,
	isnull(e.PreferredName, e.FirstName) + ' ' + e.LastName as EmpName,
	p.Id as PosId,
	p.WorkingTitle as PosTitle,
	WorkContractTypeDesc as ContractType,
	cast (
		case when a.startdate > eds.ds02
		then a.startdate else eds.ds02
		end -- current appointment start date
	as date) as ApptStart,
	cast (a.ContractEndDate as date) as ContractEnd,
	datediff(d, getdate(), a.ContractEndDate) as DaysLeft
	from Appointment a
	join WorkContractType wct
	on a.ContractTypeCode = wct.WorkContractTypeCode
	left join Employee e on a.EmployeeId = e.Id
	left join EmployeeDateSpec eds on a.EmployeeId = eds.EmployeeId
	left join Position p on a.PositionId = p.Id

	-- TODO: FirstNonVacantSupervisor function, handle double-filled supervisors
	left join Appointment sa on p.SupervisorPositionId = sa.PositionId
	and (sa.EndDate is null or sa.EndDate > getdate())
	left join Employee se on sa.EmployeeId = se.Id

	where a.ContractTypeCode in ('02', '03', '04', '05', '10', '11', '12', '21', '22', '23')
	and (a.EndDate is null or a.EndDate > getdate())
	and eds.ds02 is not null
	and a.ContractEndDate < dateadd(d, 60, getdate())
	-- TODO: option for re-sending notification or ignoring notification logs in dev config
	and not exists (
		select 1 from notification
		where RelatedEntityType = 'Appointment'
		and RelatedEntityId = a.Id
		and NotificationType = 'ContractTermEnding'
	)
	order by
		case when a.startdate > eds.ds02
		then a.startdate else eds.ds02
		end -- current appointment start date
END