﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-28
-- Description:	get county list with codes for employee home address
-- =============================================
CREATE PROCEDURE [dbo].[GetCountySelectList] 
AS
BEGIN
	select CountyID as Value, CountyName as [Text]
	from legacy.WACounty
	order by case when CountyID='00' then 1 else 2 end, CountyName

END