﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-12-08
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetSelectionJvac] 
AS
BEGIN
	select code as Value,
	code + '-' + cast(points as varchar) + ' (BAND ' + cast(Band as varchar) + ')' as Text
	from Jvac
	order by code
END