﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-01-27
-- Description:	
-- =============================================
CREATE PROCEDURE UpdateUserRoles 
AS
BEGIN
	update appuser
	set RoleId = case
		when lanid in (
			'dis\davidg',
			'dis\paulinem',
			'dis\markp',
			'dis\vairamv'
			) then 1 -- Admin
		when lanid in (
			'dis\benm',
			'dis\cherr',
			'dis\kerim',
			'dis\margaretl'
			) then 2 -- HR
		when exists (
			select 1 from
			appointment a
			join position p
			on a.employeeid = appuser.employeeid
			and a.positionid = p.id
			and p.IsSupervisor = 1
			and (a.enddate is null or a.enddate > getdate())
			and p.OrgUnitId <> 31008266 -- exclude HR (superfluous with previous condition met)
		) then 3 -- supervisors
		else 4 -- non-supervisors
	end
END