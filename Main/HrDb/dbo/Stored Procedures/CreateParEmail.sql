﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 12/2/13
-- Description:	create new Par email
-- =============================================
create PROCEDURE [dbo].[CreateParEmail] 
	-- Add the parameters for the stored procedure here


	@ParId int,
	@HtmlBody varchar(max),
	@TimeSent datetime,
	@SentTo varchar(1000),
	@SentBcc varchar(50) = null,
	@SentFrom varchar(50),
	@AttachmentIds varchar(255) = null
	
AS
BEGIN


		INSERT INTO ParEmail
			   (ParId,
				HtmlBody,
				TimeSent,
				SentTo,
				SentBcc,
				SentFrom,
				AttachmentIds)
			   
		VALUES
			   (@ParId,
				@HtmlBody,
				@TimeSent,
				@SentTo,
				@SentBcc,
				@SentFrom,
				@AttachmentIds)
			   
			   
END