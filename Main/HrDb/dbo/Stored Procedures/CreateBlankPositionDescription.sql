﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-15
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[CreateBlankPositionDescription] 
	@LanId varchar(50),
	@LinkedParId int = null,
	@WorkingTitle varchar(50) = null,
	@ProposedJobId int = null,
	@PositionTypeCXW char(1) = 'C', -- not null, defaults to 'C'
	--@xProposedJvacEvalPts int = null,
	@wxProposedJvacCode varchar(10) = null,
	@IsSupervisor bit = 0 -- not null, default 0
AS
BEGIN
	declare @userId int;
	-- GetUserIdWis a proc instead of a function so it can insert missing users:
	exec @userId = GetUserId @UserName = @LanId;
	declare @Id int;
	select @Id = NEXT VALUE FOR [dbo].[PositionDescription_seq];

	insert into PositionDescription (
		Id,
		WorkingTitle,
		CreatedByUserId,
		LastUpdatedByUserId,
		LinkedParId,
		ProposedJobId,
		PositionTypeCXW,
		--xProposedJvacEvalPts,
		wxProposedJvacCode,
		IsSupervisor
	)
	select
	@Id,
	dbo.IsNullOrEmpty(@WorkingTitle, 'New Position Working Title'),
	@userId,
	@userId,
	@LinkedParId,
	@ProposedJobId,
	@PositionTypeCXW,
	--@xProposedJvacEvalPts,
	@wxProposedJvacCode,
	@IsSupervisor;

	insert into PositionDuty (PositionDescriptionId, Duty, TasksHtml, TimePercent)
	values (@Id, 'Default Duty', '<ul><li>Sample Task 1</li><li>Sample Task 2</li></ul>', 0);

	select * from PositionDescription
	where Id = @Id; -- retrieve just-inserted PD with any defaults
END