﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-06-19
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetEmployeeGroups] 
AS
BEGIN
  select EmployeeGroupCode as Value,
  Description as [Text]
  from EmployeeGroupLkUp
  order by Description
END