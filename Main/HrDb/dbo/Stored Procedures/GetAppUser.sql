﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 2/6/14
-- Description:	Get Employee of given lan id
-- =============================================
create PROCEDURE [dbo].[GetAppUser]
	
	@lanId varchar(50)
AS
BEGIN

	select  *
	
	 from AppUser 
	 where lanid = @lanId 	
END