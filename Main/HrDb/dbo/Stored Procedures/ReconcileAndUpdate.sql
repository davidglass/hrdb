﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-08
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ReconcileAndUpdate] 
AS
BEGIN
	exec ReconcilePositionPars;
	exec ReconcileEmployeePars;
	exec RefreshEmpDateSpecs;
	exec UpdateApptPay;
	exec UpdateApptContracts;
	exec LoadAppUsersNew;
	exec RefreshLeave;
	exec RefreshAddress;
	exec RefreshEmergencyContact;
END