﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-09
-- Description:	get list of Position Descriptions
-- TODO: filter to current user position SpanOfControl,
--   add selectable filters for other attributes (like current, approval status, etc.)
-- =============================================
CREATE PROCEDURE [dbo].[GetPdList]
@LanId varchar(50),
@IncludeOld bit = 0,
@DirectReportsOnly bit = 1, -- default to DrOnly...
@QueryText varchar(max) = '',
@UpdatedSince datetime = null
AS
BEGIN
--	declare @LanId varchar(50) = 'dis\davidg';
----	--declare @LanId varchar(50) = 'dis\christyr';
--	declare @IncludeOld bit = 0;
--	declare @DirectReportsOnly bit = 1; -- default to DrOnly...
--	declare @QueryText varchar(50) = '';
----	declare @QueryText varchar(50) = 'sneddon';
--	declare @UpdatedSince datetime = '2005-01-01';

	declare @userId int;
	-- GetUserId is a proc instead of a function so it can insert missing users:
	exec @userId = GetUserId @UserName = @LanId;

	select pd.Id,
	a.StartDate, a.EndDate,
	pd.PositionId,
	ProposedJobId,
	case when pd.WorkingTitle is null or pd.WorkingTitle = '' then
	'N/A (' + jc.JobTitle + ')'
	else pd.WorkingTitle end as WorkingTitle,
	-- add these new columns to model:
	cast(case when e.Id is null then 1 else 0 end as bit) as IsVacant,
	case when a.Id is null then
	(
		select e2.LastName from Appointment a2
		join Employee e2
		on a2.PositionId = p.Id
		and e2.Id = a2.EmployeeId
		and EndDate = (select max(EndDate) from Appointment where PositionId = p.Id)
	)
	else e.LastName end as IncumbentLastName,
	case when e.PreferredName is null or e.PreferredName = '' then e.FirstName else e.PreferredName
	end as IncumbentFirstName,
	--case when e.Id is null then 'VACANT' else
	--e.LastName + ', ' + case when e.PreferredName is null or e.PreferredName = '' then e.FirstName else e.PreferredName end
	--end as Incumbent,
	jc.LegacyCode as CurrentClassCode,
	isnull(j.LegacyCode, 'N/A') as ProposedClassCode,
	j.JobTitle as ProposedClassTitle,
	pd.LastUpdatedAt,
	pd.EffectiveDate as ApprovalDate, -- TODO: make sure this is importing right from approvals...
	pd.LegacyStatus,
	case when wf.StatusCodeId = 1 then isnull(dbo.GetWaitingOn(wf.Id), 'ACTIVE')
	else wfs.Name end as ApprovalStatusDesc
	from PositionDescription pd
	join Position p
	on pd.PositionId = p.Id
	and p.IsAbolished = 0
	left join JobClass jc
	on p.JobId = jc.Id
	left join Appointment a
	on p.Id = a.PositionId
	--and a.StartDate < pd.EffectiveDate
	and (
			(
--			a.StartDate < pd.EffectiveDate
			a.StartDate < pd.LastUpdatedAt
			and
			(a.EndDate is null
				--or a.EndDate > pd.EffectiveDate)
				or a.EndDate > pd.LastUpdatedAt)
		)
	)
	--and (a.EndDate is null or a.EndDate > getdate())
	left join Employee e
	on a.EmployeeId = e.Id
	left join JobClass j -- DG changed to left join for when no JC is set.
	on pd.ProposedJobId = j.Id
	-- TODO: trying to exclude obsolete PDs; may need to allow 3-month diff?
	--where (pd.LastUpdatedAt >= a.StartDate
	--or a.Id is null -- vacancies
	--)
	left join ApprovalWorkflow wf
	on pd.ApprovalWorkflowId = wf.Id
	left join ApprovalStatusCode wfs
	on wf.StatusCodeId = wfs.Id
	where (
		dbo.CanReadAll(@LanId, 'PD') = 1
		or -- else only span of control or direct reports...
		(@DirectReportsOnly = 0 and
		pd.PositionId in (
			--select Id from dbo.GetSpanOfControl(@LanId)
			select Id from dbo.GetSpanOfControlWithDelegates(@LanId, 2)
		))
		or
		(@DirectReportsOnly = 1 and
		pd.PositionId in (
			select sp.Id
			from AppUser u
			join Employee e
			on
			u.LanId = @LanId
			and u.EmployeeId = e.Id
			join appointment a
			on a.EmployeeId = e.Id
			and (a.enddate is null or a.enddate > getdate())
			join position sp
			on sp.SupervisorPositionId = a.PositionId
		))
	)
	and (@IncludeOld = 1
		or pd.Id = (select top 1 Id from PositionDescription where PositionId = p.Id order by Id desc)
	)
	and (@UpdatedSince is null
		or pd.LastUpdatedAt >= @UpdatedSince
	)
	and (@QueryText = ''
		or pd.WorkingTitle like '%' + @QueryText + '%'
		or e.LastName like '%' + @QueryText + '%'
		or e.FirstName like '%' + @QueryText + '%'
		or e.PreferredName like '%' + @QueryText + '%'
		or try_convert(int, @QueryText) = pd.PositionId
	)
	union
	select pd.id,
	null, null,
	null,
	pd.ProposedJobId,
	pd.WorkingTitle,
	-- add these new columns to model:
	cast(1 as bit) as IsVacant,
	null,
	null,
	--null, -- Incumbent (converted to First+Last)
	null,
	isnull(j.LegacyCode, 'N/A') as ProposedClassCode,
	j.JobTitle as ProposedClassTitle,
	pd.LastUpdatedAt,
	pd.EffectiveDate as ApprovalDate, -- TODO: make sure this is importing right from approvals...
	null,
	'N/A' as ApprovalStatusDesc

	--null, null, null, null, null, null, null,
	--null, null, null, null, null, null, null
	from PositionDescription pd
	left join JobClass j
	on pd.ProposedJobId = j.Id
	where PositionId is null
	and (pd.CreatedByUserId = @userId
		or pd.LastUpdatedByUserId = @userId
		or dbo.CanReadAll(@LanId, 'PD') = 1
	)
	and (@QueryText = ''
		or pd.WorkingTitle like '%' + @QueryText + '%'
	)
	order by pd.id desc;
END