﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-10-28
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetAgencySelection] 
AS
BEGIN
	select OfmAgencyCode as [Value],
	AgencyTitleShort as [Text]
	from hrms.AgencyCode
	order by AgencyTitleShort;
END