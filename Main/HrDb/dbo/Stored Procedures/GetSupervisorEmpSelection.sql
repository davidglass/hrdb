﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-02-09
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetSupervisorEmpSelection]
AS
BEGIN
	select cast(e.Id as varchar(10)) as [Value],
	dbo.IsNullOrEmpty(e.PreferredName, e.FirstName) + ' ' + e.LastName as [Text]
	from Employee e
	join Appointment a
	on e.Id = a.EmployeeId
	and (a.EndDate is null or a.EndDate >= getdate())
	join Position p
	on a.PositionId = p.Id
	and p.IsSupervisor = 1
	order by
	dbo.IsNullOrEmpty(e.PreferredName, e.FirstName) + ' ' + e.LastName
END