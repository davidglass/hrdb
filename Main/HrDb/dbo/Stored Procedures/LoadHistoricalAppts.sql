﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-08
-- Description:	Load Historical Appts
-- =============================================
CREATE PROCEDURE [dbo].[LoadHistoricalAppts] 
AS
BEGIN
	insert into Appointment (
		PositionId,
		EmployeeId,
		StartDate,
		EndDate,
		FillLevelJobId
	)
	(
	select
	cast(h.PositionNum as int) as PositionId,
	h.PersonId as EmployeeId,
	updated_date as StartDate,
	(select max(updated_date) from StageLegacy.organization.employee_history
		where PersonId = h.PersonId
		and PositionNum = h.PositionNum
		and FirstName <> 'vacant'
		and LastName <> 'vacant'
	) as EndDate,
	null as UnderfilledAsJobId
	from StageLegacy.organization.employee_history h
	where h.Id = (
		select min(Id) from StageLegacy.organization.employee_history h2
		where h2.PersonId = h.PersonId
		and h2.PositionNum = h.PositionNum
		and FirstName <> 'vacant'
		and LastName <> 'vacant'
	)
	and left(h.PositionNum,2) <> 'UG'
	and h.PositionNum <> '9999' -- exclude bogus Position
	and h.LastName <> 'VACANT'
	and h.FirstName <> 'VACANT'
	and h.LastName <> ''
	and h.PositionNum <> '' -- see history Id 86198
	and not exists (
		select 1 from StageLegacy.organization.employee
		where PersonId = h.PersonId
		and PositionNum = h.PositionNum
	)
	and updated_date <>
		(select max(updated_date) from StageLegacy.organization.employee_history
			where PersonId = h.PersonId
			and PositionNum = h.PositionNum
		) -- exclude transient "Appointments"
	and not exists (
		select 1 from appointment
		where EmployeeId = h.PersonId
		and PositionId = cast(h.PositionNum as int)
	)
	)
--	and not exists (
----		select 1 from Employee e where e.Id = h.PersonId
--		select 1 from Position where Id = cast(h.PositionNum as int)
--	)
END