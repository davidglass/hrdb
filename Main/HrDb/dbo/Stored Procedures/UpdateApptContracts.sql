﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-07
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[UpdateApptContracts] 
AS
BEGIN
	update a
	set ContractTypeCode = oa.WorkContractTypeCode,
	ContractEndDate = case when oa.WorkContractTypeCode = '  ' -- Permanent
	then null else
		ContractEndDate
	end
	from employee e
	join appointment a on e.id = a.EmployeeId
	and (a.EndDate is null or a.EndDate > getdate())
	join stagehrms.dbo.OrganizationalAssignment oa
	on e.HrmsPersonId = oa.PersonId
	where a.ContractTypeCode <> oa.WorkContractTypeCode
	and oa.EffectiveDate >= a.ContractEndDate
	and a.ContractEndDate < getdate()
END