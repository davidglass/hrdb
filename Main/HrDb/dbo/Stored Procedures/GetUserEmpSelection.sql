﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-02-21
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetUserEmpSelection] 
AS
BEGIN
	select
	cast(u.Id as varchar(10)) as [Value],
	LastName + ', ' + dbo.IsNullOrEmpty(PreferredName, FirstName) as [Text]
	from appuser u
	join Employee e
	on u.EmployeeId = e.Id
	order by e.LastName, e.FirstName;
END