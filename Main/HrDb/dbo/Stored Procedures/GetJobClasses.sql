﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-01-31
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetJobClasses] 
AS
BEGIN
  select j.Id, j.HrmsJobId,
  j.JobTitle,
  j.LegacyCode,
  --count(case when (a.id is not null and (a.enddate is null or a.enddate > getdate())) then 1 end) as FilledPositions,
--  count(case when a.id is null or a.enddate < getdate() then 1 end) as Vacancies,
	(select count(1) from position p
		left join Appointment a
		on p.Id = a.PositionId
		and (a.EndDate is null or a.EndDate > getdate())
		where (p.JobId = j.Id
		or (p.JobId is null
			and p.LegacyClassCode = j.LegacyCode))
		and p.IsAbolished = 0
		and a.Id is null
	) as Vacancies,
  (select count(1) from position p
  where p.JobId = j.Id
  and p.IsAbolished = 0) as TotalPositions,
  IsActive
  from JobClass j
  left join Appointment a
  on j.Id = a.FillLevelJobId
  left join Position p
  on a.PositionId = p.Id
  group by j.Id, j.HrmsJobId, j.JobTitle, j.LegacyCode, IsActive
--  order by IsActive desc, TotalPositions desc, FilledPositions desc, j.JobTitle
  order by IsActive desc, TotalPositions desc, Vacancies desc, j.JobTitle
END