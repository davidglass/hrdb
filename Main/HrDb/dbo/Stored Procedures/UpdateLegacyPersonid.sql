﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateLegacyPersonid]
AS
BEGIN
	update e
	set LegacyPersonid = l.LegacyPersonid
	from
	Employee e
	join LegacyPersonIdNewPersonid l
	on l.NewPersonid = e.Id
	where e.LegacyPersonId is null
END