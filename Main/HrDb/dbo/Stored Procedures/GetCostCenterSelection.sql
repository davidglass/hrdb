﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-08-12
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetCostCenterSelection] 
AS
BEGIN
--select LastName + ': ' + DescShort + ' (' + CostCenter + ')' as [Text],
--select LastName + ': ' + DescShort + ' (' + cast(CostCenter as varchar(4)) + ')' as [Text],
--cast(CostCenter as varchar(4)) as Value
select LastName + ': ' + DescShort + ' (' + cc.CharCode + ')' as [Text],
cc.CharCode as Value
--from legacy.CostCenter cc join Employee e
--from legacy.CostCenter cc join Employee e
from CostCenter cc join Employee e
--on cc.MgrLanId = e.lanid
--on cc.MgrLanId = e.lanid
--on cc.PersonId = e.Id
on cc.ManagerEmpId = e.Id
join Appointment a
on e.Id = a.EmployeeId
and (a.EndDate is null or a.EndDate > getdate())
--where status = 'A' -- TODO: convert this col to IsActive bool
--where cc.FiscalYear = datepart(yy, dateadd(M, -6, getdate()))
where cc.IsActive = 1
--order by lastname, DescShort
order by lastname, DescShort
END