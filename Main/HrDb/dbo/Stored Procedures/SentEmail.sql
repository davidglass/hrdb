﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 2014-03-17
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SentEmail] 
	@Id int,
	@Exception varchar(max) = ''
AS
BEGIN
	if @Exception = ''
	Begin
		update email set sentDateTime = GetDate(), exception = @Exception where id = @Id
	End
	Else
		update email set exception = @Exception where id = @Id
END
GO
-- this breaks build since user is not part of project:
--GRANT EXECUTE
--    ON OBJECT::[dbo].[SentEmail] TO [HrEmailService]
--    AS [dbo];

