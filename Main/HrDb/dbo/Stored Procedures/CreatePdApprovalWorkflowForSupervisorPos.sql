﻿

-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-20
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[CreatePdApprovalWorkflowForSupervisorPos]
	@SuperPosId int,
	@PdId int
AS
BEGIN
	--declare @PdId int = 2153;
	--declare @SuperPosId int = 5547;
	declare @WfId int; -- workflow Id;
	declare @anclen int;

	-- 1 shorter than @anclen in @PositionId proc variant:
	select @anclen = datalength(ps.Ancestry)
	from PositionStructure ps where id = @SuperPosId;

	INSERT INTO ApprovalWorkflow
		(StatusCodeId,
		ApprovalCodeId,
		AppId,
		PositionId,
		CreatedDate)
	VALUES
--		(1, -- 1: Active (make default)
		(5, -- 1: Created (make default)
		9, -- 9: AppointingWithReview, can look up from AppName = 'PDF'
		2, -- 2: PDF, look up from AppName = 'PDF'
		null,
--		@PositionId,
		getdate());

	select @WfId = SCOPE_IDENTITY();

	-- insert approval tasks
	insert into ApprovalTask (
		ApprovalWorkflowId,
		ApproverPositionId,
		ApproveGroupId,
		WorkflowOrder,
		StatusCodeId,
		TaskCodeId
	)

	select @WfId,
	sps.Id as ApproverPositionId,
	null as ApproveGroupId,
	1 + @anclen - datalength(sps.Ancestry) as WorkflowOrder,
	5 as StatusCode, -- 'Created'
	2 as TaskCode -- 'Standard
	from PositionStructure ps
	join PositionStructure sps
	--on ps.Id = @PositionId
	on ps.Id = @SuperPosId
	and sps.Ancestry = left(ps.Ancestry, datalength(sps.Ancestry))
	--and datalength(sps.Ancestry) < @anclen
	and datalength(sps.Ancestry) <= @anclen
	union select @WfId, null, 1, 1, 5, 2 -- HR review
	-- removing Emp ack from PD approval workflow...
	--union select @WfId, @PositionId, null, 1 + @anclen, 5, 1 -- Emp ack
	--union select @WfId, null, 1, 2 + @anclen, 5, 3; -- HR signoff
	union select @WfId, null, 1, 3 + @anclen, 5, 3; -- HR signoff

	update PositionDescription
	set ApprovalWorkflowId = @WfId
	where Id = @PdId;

	exec GetPdApprovalTasks @PdId = @PdId;
	--return @WfId;

END