﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-01-11
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[UpdateApptShiftDiff] 
AS
BEGIN
-- *note* employees in non- "shift eligible" (legacy Overtime Status category) positions can still get shiftdifferential pay
-- (e.g. shift supervisors ).
update a
set payshiftdifferential = 1
from appointment a
join position p
on a.positionid = p.id
where p.shiftcode <> 'D';

update position
set IsShiftDiffEligible = 1
where OrgUnitId in (
	31008300, 31008301, 31008302, -- MF Cmd Center day, grave, swing
	31008325, 31008326, -- NCC day, swing
	31008332 -- service desk
)

END