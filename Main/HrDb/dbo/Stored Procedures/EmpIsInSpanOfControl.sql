﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-01-28
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[EmpIsInSpanOfControl] 
	-- Add the parameters for the stored procedure here
	@EmployeeId int, 
	@LanId varchar(50)
AS
BEGIN
	select
	case when exists (
		select 1 from AppUser u
		where u.LanId = @LanId
		and u.RoleId in (1,2) -- Admin, HR
	) then cast(1 as bit)
	else
	dbo.IsInSpanOfControl(@EmployeeId, @LanId)
	end as IsSub
END