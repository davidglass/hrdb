﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-12-06
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetExpiringContracts] 
AS
BEGIN
	select
	a.Id as Id,
	e.Id as EmpId,
	e.LastName +', ' + e.PreferredName as EmpName,
	p.Id as PosId,
	p.WorkingTitle as PosTitle,
	WorkContractTypeDesc as ContractType,
	cast (
		case when startdate > eds.ds02
		then startdate else eds.ds02
		end -- current appointment start date
	as date) as ApptStart,
	cast (a.ContractEndDate as date) as ContractEnd,
	datediff(d, getdate(), a.ContractEndDate) as DaysLeft
	from Appointment a
	join WorkContractType wct
	on a.ContractTypeCode = wct.WorkContractTypeCode
	left join Employee e on a.EmployeeId = e.Id
	left join EmployeeDateSpec eds on a.EmployeeId = eds.EmployeeId
	left join Position p on a.PositionId = p.Id
	where ContractTypeCode in ('02', '03', '04', '05', '10', '11', '12', '21', '22', '23')
	and (a.EndDate is null or a.EndDate > getdate())
	and eds.ds02 is not null	
	order by
		case when startdate > eds.ds02
		then startdate else eds.ds02
		end -- current appointment start date
END