﻿
-- =============================================
-- Author:		P. McKusick
-- Create date: 2014-10-09
-- Description:	truncate and reload leave data 
-- =============================================
CREATE PROCEDURE [dbo].[RefreshAddress] 
AS
BEGIN
	begin tran
		if (select count(*) from StageHRMS.dbo.Address) > 0 begin
			truncate table hrms.Addresses;
		
		insert into hrms.Addresses
		
		select  PersonId
		  ,EffectiveDate
		  ,EndDate
		  ,AddressType
		  ,StreetLine1
		  ,StreetLine2
		  ,City
		  ,State
		  ,ZipCode
		  ,County
		  ,Country
		  ,HomePhone
		  ,PhoneType
		  ,PhoneNumber
		 from StageHRMS.dbo.Address
		
		end
	commit;
END