﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 11/20/13
-- Description:	Get list of all active and future delegates
-- =============================================
CREATE PROCEDURE [dbo].[GetApprovalDelegates]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT  
		*
	FROM ApprovalDelegate
	where Active = 1 and
	((Indefinite = 1) or (ApprovalDelegate.EndDate >= DATEADD(day, -1, getdate()))) --subtract day so that end date of current date is still active
	order by StartDate
	

END