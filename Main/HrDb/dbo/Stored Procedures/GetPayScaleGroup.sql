﻿


CREATE PROCEDURE [dbo].[GetPayScaleGroup]
AS
BEGIN
	select Code as [Value], -- values must be char/string to use as SelectListItems 
	Code as [Text] -- TODO: get Description for PayScaleGroups, if possible...
	from PayScaleGroupCode
END