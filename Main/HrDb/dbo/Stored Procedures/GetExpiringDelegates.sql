﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 2014-2-05
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetExpiringDelegates] 
AS
BEGIN
	--I know, ugly query, threw it together quickly, TODO: refactor
    SELECT  
		DelegatedPositionId,
		isnull(PosEmp.LastName,
			'VACANT' +
			case when exists (select 1 from appointment where positionid = DelegatedPositionId)
				then ', ' +
				(select e.LastName
				from Appointment a
				join Employee e
				on a.EmployeeId = e.Id
				and a.PositionId = DelegatedPositionId
				and a.EndDate = (select max(EndDate) from Appointment where PositionId = DelegatedPositionId)
				)
			else ', Pos Id: ' + cast(DelegatedPositionId as varchar) end
		)
		+ case when a.Id is null then '' else ', ' + isnull(PosEmp.PreferredName, PosEmp.FirstName) end
		as DelegatedName,
		PosEmp.Id DelegatedEmpId,
		DelegatedToPersonId,
		DelEmp.LastName + ', ' + isnull(DelEmp.PreferredName, DelEmp.FirstName) DelegatedToName,
		ApprovalDelegate.StartDate,
		ApprovalDelegate.EndDate,
		datediff(d, getdate(), ApprovalDelegate.EndDate) as DaysLeft
	FROM ApprovalDelegate
	left outer join Position Pos on [ApprovalDelegate].DelegatedPositionId = Pos.Id
	left join Appointment a on [ApprovalDelegate].DelegatedPositionId = a.PositionId
		and(a.EndDate is null or a.EndDate > DATEADD(day, -1, getdate()))
	left join Employee PosEmp on a.EmployeeId = PosEmp.Id
	left outer join Employee DelEmp on [ApprovalDelegate].DelegatedToPersonId = DelEmp.Id		
	where (Indefinite = 0) 
	and Active = 1
	and (ApprovalDelegate.EndDate >= DATEADD(day, -15, getdate())) 
	and (ApprovalDelegate.EndDate < DATEADD(day, 30, getdate())) --go two weeks back and month forward
	order by DaysLeft
END