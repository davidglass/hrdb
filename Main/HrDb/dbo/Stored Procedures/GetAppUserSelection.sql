﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-01-27
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetAppUserSelection]
AS
BEGIN
  select cast(u.id as varchar(10)) as Value,
  case when EmployeeId is not null then
	lastname + ', ' + isnull(preferredname, firstname)
  else u.lanid 
  end as [Text]
  from
  appuser u
  left join
  employee e
  on e.id = u.employeeid
  order by
  case when EmployeeId is not null then
	lastname + ', ' + isnull(preferredname, firstname)
  else u.lanid 
  end
END