﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 8/20/13
-- Description:	create new delegate app link
-- =============================================
create PROCEDURE [dbo].[CreateApprovalDelegateAppLink] 
	-- Add the parameters for the stored procedure here


	@DelegateId int,
	@AppId int
	
AS
BEGIN


		INSERT INTO ApprovalDelegateApp
			   (DelegateId,
			   AppId)
		VALUES
			   (@DelegateId,
			   @AppId)
			   

			   
END