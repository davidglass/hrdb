﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-06-26
-- Description:	list of attachments with id and filename...no binary content
-- =============================================
CREATE PROCEDURE [dbo].[GetParAttachments] 
	@ParId int
AS
BEGIN
	select Id, [FileName]
	from ParAttachment
	where ParId = @ParId;
END