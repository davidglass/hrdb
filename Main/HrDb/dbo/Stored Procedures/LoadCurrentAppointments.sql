﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-09-24
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[LoadCurrentAppointments] 
AS
BEGIN
--truncate table Appointment;
insert into Appointment (
	PositionId,
	EmployeeId,
	StartDate,
	FillLevelJobId,
	ContractTypeCode,
	WorkScheduleCode,
	EmploymentPercent,
	TimeMgtStatus,
	PayScaleTypeArea,
	PayScaleReason,
	PayScaleEffectiveDate,
	PayScaleGroup,
	PayScaleLevel,
	PayAnnualSalary
)
select
	cast(e.positionnum as int) as PositionId,
	e.personid EmployeeId,
	--doppersonid,
	(select min(updated_date) from StageLegacy.organization.employee_history h
		where PersonId = e.PersonId and PositionNum = e.PositionNum
		-- only get most recent appointment to given position, not oldest:
		and not exists (
			select 1 from StageLegacy.organization.Employee_History
			where PersonId = e.PersonId
			and PositionNum <> e.PositionNum
			and updated_date > h.updated_date
		)
	)
	as StartDate,
	j.Id as FillLevelJobId,
	oa.WorkContractTypeCode as ContractTypeCode,
	ews.WorkSchedule as WorkScheduleCode,
	ews.FullTimePercent as EmploymentPercent,
	-- default nulls to 9; all but 1 record are 9s:
	cast(isnull(ews.TimeManagementStatus, 9) as char(1)) as TimeMgtStatus,
	psta.Id as PayScaleTypeArea,
	bp.PayScaleReasonForChangeCode as PayScaleReason,
	bp.EffectiveDate as PayScaleEffectiveDate,
	-- these should be trimmed by SSIS now:
	rtrim(bp.PayScaleGroup),
	rtrim(bp.PayScaleLevel),
	bp.AnnualSalary
	from StageLegacy.organization.employee e
	join StageLegacy.organization.positions p on e.positionnum = p.positionnum
	and p.agency is null
--	left join hrms.Job j
	left join JobClass j
		on (j.LegacyCode = e.ClassCode
--		on (j.JobTitleLegacy = e.classcode
			-- NOTE: or clause is to correct for obsolete fill levels:
			or (
				e.classcode is null and
				j.LegacyCode = (
--				j.JobTitleLegacy = (
					select classcode
					from StageLegacy.organization.positions
					where positionnum = e.positionnum
				)
			)
		)
	left join StageHRMS.dbo.OrganizationalAssignment oa
	on oa.PersonId = e.doppersonid
	left join StageHRMS.dbo.EmployeeWorkSchedule ews
	on e.doppersonid = ews.PersonId
	left join StageHRMS.dbo.BasicPay bp
	on e.DOPPersonId = bp.PersonId
	left join PayScaleTypeArea psta
	on psta.PsType = bp.PayScaleType
	and psta.PsArea = bp.PayScaleArea
	where
	e.firstname <> 'VACANT'
	and e.lastname <> 'vacant'
	and e.lastname <> ''
	and e.firstname <> 'VACANT'
	and e.lastname <> 'vacant'
	and e.lastname <> ''
END