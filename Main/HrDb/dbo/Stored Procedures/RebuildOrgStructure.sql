﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-04-22
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[RebuildOrgStructure] 
AS
BEGIN
	declare @OrgUnitId int;
	IF EXISTS (SELECT * FROM sys.foreign_keys 
		WHERE object_id = OBJECT_ID(N'FK_OrgStructure_OrgStructure')
		AND parent_object_id = OBJECT_ID(N'dbo.OrgStructure'))
	BEGIN
		-- can't truncate table without DROPPING FK. (disabling doesn't work)
		alter table OrgStructure drop FK_OrgStructure_OrgStructure;
	END;

	--disable triggers:
	disable trigger all on OrgStructure;

--	truncate table OrgStructure;
-- still can't truncate due to FK ref:
	delete from OrgStructure;
	select @OrgUnitId = OrgUnitId from hrms.OrgStructure
	where [Level] = 1;
	insert into OrgStructure (Id, Ancestry) -- omitting ParentId (CTS OrgUnit ID)
	values(@OrgUnitId, 0x01);
	exec AddOrgStructureSubnodes @OrgUnitId;
	-- disabling FK permanently to enable inactive OUs.
	--alter table OrgStructure add constraint FK_OrgStructure_OrgStructure
	--	foreign key (ParentId) references OrgStructure (Id);

	enable trigger all on OrgStructure;
END