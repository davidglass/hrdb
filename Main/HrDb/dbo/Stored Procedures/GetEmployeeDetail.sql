﻿-- =============================================
-- Author:		P.McKusick
-- Create date: 2013-08-26
-- Description:	Used for the Employee Detail in HRAdmin 2.0
-- =============================================
CREATE PROCEDURE [dbo].[GetEmployeeDetail] 

	@EmpId int 

	
AS
BEGIN
-- declare @EmpId int = 5856; KeriM, example of getting most recent appointment.
-- please don't use TOP 1 unless absolutely necessary.  It just hides queries that should be fixed.
--	SELECT TOP 1
select
--	e.Id [EmployeeId] 
	  e.Id
      ,e.lanid
      ,e.HrmsPersonId
      ,rtrim(ltrim(e.FirstName)) [FirstName]
      ,rtrim(ltrim(e.LastName)) [LastName]
      ,e.LastUpdated
      ,rtrim(ltrim(e.MiddleName)) [MiddleName]
      ,e.Suffix
      ,rtrim(ltrim(e.PreferredName)) [PreferredName]
      --,e.SSN
      ,e.DOB
      ,e.EmailAddress
      ,e.Gender
	  ,CASE WHEN e.Gender = 1 THEN 'Male' WHEN e.Gender = 2 THEN 'Female' END [GenderDesc]
      ,e.MaritalStatus
	  ,CASE WHEN e.MaritalStatus = 0 THEN 'Single' WHEN e.MaritalStatus = 1 THEN 'Married' WHEN e.MaritalStatus = 2 THEN 'Widow' WHEN e.MaritalStatus = 3 THEN 'Divorced'				WHEN e.MaritalStatus = 6 THEN 'Unknown' ELSE 'Unknown' END [MaritalStatusDesc]

	  ,p.Id [PositionId]
      ,p.OrgUnitId
	  ,o.Description [OrgUnitDesc]
      ,p.CostCenter
      ,p.JobId
	  ,j.JobTitle [JobDesc]
      ,p.JobTitle
      ,p.LegacyClassCode
	  ,cc.JobTitle [LegacyClassCodeDesc]
      ,p.LegacyPositionNum
      ,p.LegacyUnitId
      ,p.LegacyLastUpdated
      ,p.WorkingTitle
      ,p.HrmsPositionId
      ,p.SupervisorPositionId
      ,p.IsAbolished
      ,p.CBA
      ,p.ValidFromDate
      ,p.ValidToDate
      ,p.x_created
      ,p.IsInTraining
      ,p.GeneralDescription
      ,p.PersonnelSubArea
	  ,ps.Description [PersonnelSubAreaDesc]
      ,p.IsBackgroundCheckRequired
      ,p.WorkersCompCode
	  ,CASE WHEN p.WorkersCompCode = '0000' THEN 'None Assigned' WHEN p.WorkersCompCode = '4902' THEN 'Clerical / Admin'	WHEN p.WorkersCompCode = '5300' 
		THEN 'Admin Field Staff' WHEN p.WorkersCompCode = '5307' THEN 'Warehouse' WHEN p.WorkersCompCode = '6901' THEN 'Volunteers' END
		 [WorkersCompDesc]
      ,p.IsSupervisor
      ,p.IsManager
      ,p.IsAppointingAuthority
      ,p.EeGroup
	  ,eg.Description [EeGroupDesc]
      ,p.EeSubGroup
	  ,esg.Description [EeSubGroupDesc]
      ,p.IsSection4
      ,p.PayGradeType
      ,p.PayGradeArea
	  ,psta.Description [PayGradeTypeAreaDesc]
      ,p.PayGrade
      ,p.IsTandem
      ,p.LocationId
	  ,l.Location [LocationDesc]
	  ,ad.physicaldeliveryofficename [OfficeLoc]
      ,p.DefaultWorkScheduleCode
	  ,wsP.Description [DefaultWorkScheduleDesc]
	  ,a.Id [AppointmentId]
	  ,a.StartDate
	  ,a.EndDate
	  ,a.IsPrimary
	  ,a.FillLevelJobId
	  ,rtrim(ltrim(job.JobTitle)) [FillLevelDesc]
	  --,rtrim(ltrim(job.JobTitleLegacy)) [FillLevelClassCode]
	  ,job.LegacyCode [FillLevelClassCode]
	  ,a.ContractTypeCode
	  ,wct.WorkContractTypeDesc
	  ,a.WorkScheduleCode,
	  a.WorkHoursWk1,
	  a.WorkHoursWk2
	  ,wsE.Description [EmpWorkScheduleDesc]
	  ,a.EmploymentPercent
	  ,a.ContractEndDate
	  ,a.PayScaleTypeArea
	  ,a.PayScaleReason
	  ,psr.Description [PayScaleReasonDesc]
	  ,a.PayScaleEffectiveDate
	  ,a.PayScaleGroup
	  ,a.PayScaleLevel
	  ,a.PayNextIncrease
	  ,a.StepMEligibleDate
	  ,a.PayCapUtilityLevel
	  ,a.PayAnnualSalary
	  ,a.PayHourlyRate
	  ,a.PayShiftDifferential
	  ,a.StandbyPay
	  ,a.TimeMgtStatus
	  ,CASE WHEN a.TimeMgtStatus  = 9 THEN 'Actual Time Eval' WHEN  a.TimeMgtStatus  = 1 THEN 'Planned Time Eval' END [TimeMgtStatusDesc]
	  ,eds.ds01 [AnniversaryDT]
	  ,eds.ds02 [AppointmentDT]
	  ,case when e.hiredate is null then eds.ds03 else e.hiredate end [OriginalHireDT]
	  ,eds.ds04 [PriorPidDT]
	  ,eds.ds05 [SeniorityDT]
	  ,eds.ds07 [UnbrokenServiceDT]
	  ,eds.ds09 [VacLvFrozenUpToDT]
	  ,eds.ds18 [PrsnlHolidayElgbltyDT]
	  ,eds.ds26 [PrsnlLVDayElgbltyDT]
	  ,dww.DesignatedWorkWeek
	  ,ad.telephonenumber [WorkPhone]
	  ,ad.mail [Email]
	  ,ed.Disability
	  ,ed.Hispanic
	  ,ed.MilBranchCodes
	  ,ed.MilStatusCodes
	  ,ed.RaceCodes
	  ,case when p.ShiftCode = 'D' then 'Day'
	  when p.ShiftCode = 'S' then 'Swing'
	  when p.ShiftCode = 'G' then 'Grave' end as ShiftCode
	  ,p.ShiftDesignation
	  ,a.StepMEligibleDate as StepM
	  ,p.jvac as Jvac
	  ,p.PointValue as PointValue
	  ,ar.Name as AppRole
	
	 
	  
--  FROM [HrDb].[dbo].[Employee] AS e
  FROM [Employee] AS e -- removed db name qualifier to be usable in db w/different name.
  LEFT JOIN [dbo].[Appointment] AS a
  ON a.EmployeeId = e.Id
  and (a.EndDate is null
	or (a.EndDate =	(select max(enddate) from Appointment where EmployeeId = e.Id)
		and not exists (
			select 1 from Appointment
			where EmployeeId = e.Id
			and EndDate is null
		)
	)
  )
  LEFT JOIN [dbo].[Position] AS p ON p.Id = a.PositionId
  LEFT JOIN [dbo].[OrgUnit] AS o ON o.HrmsOrgUnitId = p.OrgUnitId
  LEFT JOIN [dbo].[EmployeeGroupLkUp] AS eg ON eg.EmployeeGroupCode = p.EeGroup
  LEFT JOIN [dbo].[EmployeeSubGroupLkUp] AS esg ON esg.EmployeeSubGroupCode = p.EeSubGroup
  LEFT JOIN [dbo].[PayScaleTypeArea] AS psta ON psta.PsType = p.PayGradeType and psta.PsArea = p.PayGradeArea
  LEFT JOIN [legacy].[Location] AS l on l.ID = p.LocationId
  LEFT JOIN [hrms].[WorkSchedule] AS wsP ON wsP.HRMSCode = p.DefaultWorkScheduleCode
  LEFT JOIN [hrms].[WorkSchedule] AS wsE ON wsE.HRMSCode = a.WorkScheduleCode
  LEFT JOIN [dbo].[WorkContractType] AS wct ON wct.WorkContractTypeCode = a.ContractTypeCode
  LEFT JOIN [dbo].[EmployeeDateSpec] AS eds ON eds.EmployeeId = e.Id
  --LEFT JOIN [hrms].[Job] AS j ON j.Id = p.JobId
  LEFT JOIN JobClass AS j ON j.Id = p.JobId
  -- DG: removed join to Stage.  Stage dbs should not be used for production queries.
  -- also not sure why we need 3-4 different JobTitles ? WorkingTitle, pos.JobTitle, job.JobTitle, cc.JobTitle
  --left join StageLegacy.organization.Class_Codes cc on cc.ClassCode = p.LegacyClassCode
  --LEFT JOIN [legacy].[Class_Codes] AS cc ON cc.ClassCode = p.LegacyClassCode
  LEFT JOIN JobClass AS cc ON cc.LegacyCode = p.LegacyClassCode
  LEFT JOIN [dbo].[DesignatedWorkWeek] AS dww ON dww.PositionId = p.LegacyPositionNum
  LEFT JOIN ADExtract AS ad ON try_convert(int, ad.employeeid)  = e.HrmsPersonId
	and e.HrmsPersonId <> 0
	and ad.company = 'CTS'
--  LEFT JOIN [Organization].[dbo].[ADExtract] AS ad ON try_convert(int, ad.employeeid)  = e.HrmsPersonId 
  LEFT JOIN [dbo].[EmployeeDiversity] AS ed ON ed.EmployeeId = e.Id
  --LEFT JOIN [hrms].[Job] AS job ON job.Id = a.FillLevelJobId
  LEFT JOIN JobClass AS job ON job.Id = a.FillLevelJobId
  LEFT JOIN [dbo].[PayScaleReason] AS psr ON psr.Code = a.PayScaleReason
  LEFT JOIN [dbo].[PersonnelSubArea] AS ps ON ps.Code = p.PersonnelSubArea
  LEFT JOIN [dbo].[AppUser] au on au.employeeid = e.id
  LEFT JOIN [dbo].[AppRole] ar on ar.id = au.RoleId

	

	WHERE e.Id = @EmpId
	

	ORDER BY a.StartDate DESC;



END