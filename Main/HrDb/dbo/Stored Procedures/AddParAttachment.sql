﻿

-- =============================================
-- Author:		Mark Paul
-- Create date: 4/16/13
-- Description:	add a new Attachment and return its Id
-- (duplicated original AddAttachment proc to this -DG)
-- =============================================
CREATE PROCEDURE [dbo].[AddParAttachment] 
	@FileName varchar(max),
	@Description text,
	@Content varbinary(max),
	@SizeInBytes int,
	@MimeType varchar(100),
	@ParId int
	
AS
BEGIN
		INSERT INTO ParAttachment
			   ([FileName],
			   [Description],
			   Content,
			   SizeInBytes,
			   MimeType,
			   ParId)
		VALUES
			   (@FileName,
			   @Description,
			   @Content,
			   @SizeInBytes,
			   @MimeType,
			   @ParId)

		select Id, [FileName], ParId
		from ParAttachment
		where Id = scope_identity();
END