﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-06-19
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetLocationSelection] 
AS
BEGIN
  select cast(Id as varchar(10)) as Value,
  Location as [Text]
  from legacy.Location
  where IsActive = 1
  order by Location
END