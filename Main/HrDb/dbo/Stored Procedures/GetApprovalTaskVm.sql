﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 8/22/13
-- Description:	Get approval
-- =============================================
CREATE PROCEDURE [dbo].[GetApprovalTaskVm]
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--test to see if result.
if Exists(
    Select [ApprovalTask].Id
	FROM [ApprovalTask]
	left outer join Appointment on ApproverPositionId = Appointment.PositionId
	where [ApprovalTask].[Id] = @Id
	and ((Appointment.EndDate is null) or (Appointment.EndDate > getdate()))
)
Begin
    SELECT top 1 --top 1 included for dual fills
		Pos.WorkingTitle [ApproverPositionTitle],
		Pos.IsAppointingAuthority [ApproverPositionIsAppointingAuth],
		ApproveGroup.Name [ApproveGroupName],		
		FinEmp.LastName [FinalApproverPersonLastName],
		FinEmp.PreferredName [FinalApproverPersonPreferredName],
		PosEmp.Id [ApproverPositionEmployeeId],
		isnull(PosEmp.EmailAddress, '') [ApproverPositionPersonEmail],
		PosEmp.LastName [ApproverPositionPersonLastName],
		PosEmp.PreferredName [ApproverPositionPersonPreferredName]
	FROM [ApprovalTask]
	left outer join Position Pos on ApproverPositionId = Pos.Id
	left outer join Appointment on ApproverPositionId = Appointment.PositionId
	left outer join Employee PosEmp on Appointment.EmployeeId = PosEmp.Id
	left outer join Employee FinEmp on FinalApproverPersonId = FinEmp.Id
	left outer join ApproveGroup on ApproveGroup.Id = ApproveGroupId
	left outer join ApprovalTaskCode on ApprovalTask.[TaskCodeId] = ApprovalTaskCode.Id
	where [ApprovalTask].[Id] = @Id
	and ((Appointment.EndDate is null) or (Appointment.EndDate > getdate()))
End
else
Begin

	SELECT top 1 --top 1 included for dual fills		
		Pos.WorkingTitle [ApproverPositionTitle],
		Pos.IsAppointingAuthority [ApproverPositionIsAppointingAuth],
		ApproveGroup.Name [ApproveGroupName],		
		FinEmp.LastName [FinalApproverPersonLastName],
		FinEmp.PreferredName [FinalApproverPersonPreferredName],
		0 [ApproverPositionEmployeeId],
		'' [ApproverPositionPersonEmail],
		'Vacant' [ApproverPositionPersonLastName],
		'' [ApproverPositionPersonPreferredName]
	FROM [ApprovalTask]
	left outer join Position Pos on ApproverPositionId = Pos.Id
	left outer join Employee FinEmp on FinalApproverPersonId = FinEmp.Id
	left outer join ApproveGroup on ApproveGroup.Id = ApproveGroupId
	left outer join ApprovalTaskCode on ApprovalTask.[TaskCodeId] = ApprovalTaskCode.Id
	where [ApprovalTask].[Id] = @Id

End
END