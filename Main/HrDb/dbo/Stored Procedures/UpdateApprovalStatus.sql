﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-22
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[UpdateApprovalStatus] 
	@Id int, -- ApprovalTask Id 
	@ApprovalStatus varchar(20), -- string name
	@Comment varchar(max) = null,
	@UpdaterLanId varchar(50)
AS
BEGIN
--declare @Id int = 431;
--declare @ApprovalStatus varchar(20) = 'Approved';
--declare @Comment varchar(max) = null;
--declare @UpdaterLanId varchar(50) = 'DIS\DavidG';
--declare @UpdaterLanId varchar(50) = 'DIS\JeromeL';
--declare @UpdaterLanId varchar(50) = 'DIS\ChristyR';

	-- always allow HR / Admin update:
	if (select RoleId from AppUser where LanId = @UpdaterLanId) not in (1,2)
	-- otherwise require approver position to be in span of control or initiating sequence:
	and
	-- *** TODO: modify IsPosInSpanOfControl to include delegates:
		(select dbo.IsPosInSpanOfControl(t.ApproverPositionId, @UpdaterLanId)
			from ApprovalTask t
			where t.Id = @Id
		) <> 1
	-- TODO: test this part.
	and -- check for delegates:
		(select dbo.IsPosDelegated(2, t.ApproverPositionId, @UpdaterLanId, wf.CreatedDate)
			from ApprovalTask t
			join ApprovalWorkflow wf
			on t.ApprovalWorkflowId = wf.Id
			and t.Id = @Id
		) <> 1
	-- allow anyone to initiate workflow to HR initial screen...
	-- TODO: consider restricting to IsPosInSpanOfControl for PD.PositionId
	and not exists (
		select 1 from ApprovalTask t
		where t.Id = @Id
		and @ApprovalStatus = 'Active'
		and t.WorkflowOrder = 1		
	)
	begin
		raiserror('Unauthorized',11,1) with seterror;
		return;
	end

	update t
	set t.StatusCodeId = sc.Id,
		t.Comment = @Comment,
		t.CompletedDate = getdate(),
		t.FinalApproverPersonId = u.EmployeeId
	from ApprovalTask t, ApprovalStatusCode sc
	join AppUser u
	on u.LanId = @UpdaterLanId
	where t.Id = @Id
	and sc.Name = @ApprovalStatus

	-- simulate ApprovalTaskController.TaskApproveForAppointingWithReview:
	if @ApprovalStatus in ('Approved', 'Canceled') begin
		if @ApprovalStatus = 'Approved' begin
			-- if AA approval, set PDF EffectiveDate to task completed date:
			update pd
			set EffectiveDate = t.CompletedDate
			from PositionDescription pd
			join ApprovalTask t
			on pd.ApprovalWorkflowId = t.ApprovalWorkflowId
			and t.Id = @Id
			join Position p
			on t.ApproverPositionId = p.Id
			and p.IsAppointingAuthority = 1;

			-- update any preceding Active or Created Standard Task statuses to Canceled,
			-- plus successive tasks if AA.
			update sibs
			set StatusCodeId = 2 -- Canceled
			from ApprovalTask sibs
			join ApprovalTask t
			on t.Id = @Id
			and sibs.ApprovalWorkflowId = t.ApprovalWorkflowId
			join Position p on t.ApproverPositionId = p.Id
			left join Appointment a -- for canceling vacant approvals...
			on sibs.ApproverPositionId = a.PositionId
			and (a.EndDate is null or a.EndDate > getdate())
			where (sibs.WorkflowOrder < t.WorkflowOrder
				or (sibs.WorkflowOrder > t.WorkflowOrder
					and p.IsAppointingAuthority = 1)
			)
			and (sibs.TaskCodeId = 2 -- Standard
				or (sibs.TaskCodeId = 1 -- Emp Ack
					and a.Id is null) -- Vacant Emps
				)
			and sibs.StatusCodeId in (1,5); -- Active, Created
		end
		else begin -- Canceled:
			update ApprovalTask
			set StatusCodeId = 2
			where Id = @Id;			
		end
		-- set first subsequent non-vacant/delegated task to Active
		update sibs
		set StatusCodeId = 1
		from ApprovalTask sibs
		join ApprovalTask t
		on t.Id = @Id
		and sibs.ApprovalWorkflowId = t.ApprovalWorkflowId
		and sibs.WorkflowOrder = (
			select min(WorkflowOrder)
			from ApprovalTask st
			-- left join to allow HR tasks (no appt):
			left join Appointment a
			on st.ApproverPositionId = a.PositionId
			and (a.EndDate is null or a.EndDate > getdate())
			where st.ApprovalWorkflowId = t.ApprovalWorkflowId
			and st.WorkflowOrder > t.WorkflowOrder
			and st.StatusCodeId = 5 -- Created
			and ((st.TaskCodeId in (2,1) -- Standard, Emp Ack
				and a.Id is not null) -- skip vacants
				-- include delegated approvals:
				or exists (
					select 1 from approvaldelegate del
					where del.DelegatedPositionId = st.ApproverPositionId
					and del.StartDate < getdate()
					and (del.EndDate is null or del.EndDate > getdate())
					and del.Active = 1
				)
				or (a.Id is null and TaskCodeId = 3) -- HR SignOff
			)
		)
		where sibs.WorkflowOrder > t.WorkflowOrder
		and sibs.StatusCodeId = 5; -- Created

	end -- end if Approved
	if @ApprovalStatus = 'Rejected' begin
		update sib
		set StatusCodeId =
		case when sib.Id = @Id then 4
		when sib.Id <> @Id and sib.StatusCodeId in (1,5) then 2
		else sib.StatusCodeId -- unchanged
		end
		from ApprovalTask sib
		join ApprovalTask t
		on sib.ApprovalWorkflowId = t.ApprovalWorkflowId
		and t.Id = @Id
	end
	if @ApprovalStatus = 'Active' begin
		update ApprovalTask
		set StatusCodeId = 1
		where Id = @Id;
	end

	update wf
	set StatusCodeId =
	case when @ApprovalStatus = 'Active' then 1
	when @ApprovalStatus = 'Approved'
		and t.TaskCodeId = 3 -- HR SignOff
		then 3 -- Approved
	when @ApprovalStatus = 'Rejected'
		then 4 -- Rejected
	else
		wf.StatusCodeId -- no change
	end
	from ApprovalWorkflow wf
	join ApprovalTask t
	on wf.Id = t.ApprovalWorkflowId
	and t.Id = @Id

	select CompletedDate, e.LastName, isnull(e.PreferredName, e.FirstName) as FirstName
	from ApprovalTask t
	join Employee e on t.FinalApproverPersonId = e.Id
	and t.Id = @Id;
END