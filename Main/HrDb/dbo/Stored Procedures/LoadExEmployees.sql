﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-08
-- Description:	Load ex-employees after loading current CTS employees
-- =============================================
CREATE PROCEDURE [dbo].[LoadExEmployees] 
AS
BEGIN
insert into Employee (Id, lanid, HrmsPersonId, FirstName, LastName, LastUpdated)
(
select
	eh.personid as Id,
	eh.lanid,
	cast (eh.doppersonid as int) as HrmsPersonId,
	rtrim(eh.firstname), rtrim(eh.lastname),
	eh.updated_date as LastUpdated
	--eh.personid, eh.firstname, eh.lastname, eh.fullname
	from StageLegacy.organization.Employee_History eh
	left join StageLegacy.organization.Employee e on eh.PersonId = e.PersonId
	where eh.LastName <> 'VACANT'
	and eh.FirstName <> 'VACANT'
	and eh.fullname <> ''
	--and eh.Tableupdated = 'EMP'
	and eh.ID = (
		select max(ID)
		from StageLegacy.organization.Employee_History
		where eh.PersonId = PersonId
		--and Tableupdated = 'EMP'
		and FirstName <> 'vacant'
		and LastName <> 'vacant'
	)
	-- exclude current employees (already loaded)
--	and not exists (
--		select 1
----		from StageLegacy.organization.Employee
--		from Employee
--		where Id = eh.PersonId
--		--and eh.personid in (5386, 5452)
--	)
	--uncomment to exclude previously loaded employees
	and not exists (
		select 1 from Employee
		where Id = eh.PersonId
	)
)
END