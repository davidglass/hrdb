﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-02-01
-- Description:	
-- =============================================
CREATE PROCEDURE LoadUnusedJobClasses 
AS
BEGIN
  insert into JobClass (LegacyCode, JobTitle, IsActive)
  (select ClassCode, left(cc.jobtitle, 40), cast(1 as bit) -- target jobtitle (for HRMS) is 40 char max.
   from stagelegacy.organization.class_codes cc
   left join JobClass jc
   on cc.ClassCode = jc.LegacyCode
   where active='Y'
   and jc.Id is null)
END