﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-31
-- Description:	
-- =============================================
CREATE PROCEDURE UpdateUserGuidsFromAd 
AS
BEGIN
	update u set ObjectGuidHexString = ad.objguidstring
	from appuser u
	join appointment a
	on u.EmployeeId = a.EmployeeId
	and (a.EndDate is null or a.EndDate > getdate())
	left join adextract ad
	on u.LanId = 'DIS\' + ad.samaccountname
	and
	substring(ad.userprincipalname, patindex('%@%', ad.userprincipalname) + 1, 6) = 'dis.wa' -- dis.wa.gov + dis.wa.lcl
	and company = 'CTS'
	where u.ObjectGuidHexString is null
END