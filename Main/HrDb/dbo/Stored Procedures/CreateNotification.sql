﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-12-30
-- Description:	Notification (email) log
-- =============================================
CREATE PROCEDURE [dbo].[CreateNotification] 
	-- Add the parameters for the stored procedure here
	@MailTo varchar(255),
	@Subject varchar(255),
	@Body varchar(max),
	@RelatedEntityType varchar(50), -- yes, this could be a lookup table instead of a string.
	@RelatedEntityId int,
	@NotificationType varchar(255)
AS
BEGIN
	insert into [Notification] (MailTo,
		[Subject],
		Body,
		RelatedEntityType,
		RelatedEntityId,
		NotificationType)
	values (@MailTo,
		@Subject,
		@Body,
		@RelatedEntityType,
		@RelatedEntityId,
		@NotificationType
	);
END