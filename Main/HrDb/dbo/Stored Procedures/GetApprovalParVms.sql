﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 8/22/13
-- Description:	Get approval
-- =============================================
create PROCEDURE [dbo].[GetApprovalParVms]
	@ApproverPositionId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	select 
	Par.Id ParId,
	Par.EffectiveDate,
	[ActionCode].[Description] [ParActionCode],
	[ApptActionReason].Reason ParActionReason,
	Par.ApprovalWorkflowId,
	ApprovalTask.Id ApprovalTaskId,
	Par.EmployeeId ParEmployeeId,
	Par.EmpPreferredName ParEmployeePreferredName,
	Par.EmpLastName ParEmployeeLastName,
	Par.PositionId ParPositionId,
	Par.PosWorkingTitle ParPosWorkingTitle

	from Par
	left outer join ApprovalWorkflow on Par.ApprovalWorkflowId = ApprovalWorkflow.Id
	left outer join ApprovalTask on ApprovalWorkflow.Id = ApprovalTask.ApprovalWorkflowId
	left outer join [ActionCode] on Par.ActionCode = [ActionCode].Code
	left outer join [ApptActionReason] on Par.ActionReasonId = [ApptActionReason].Id
	where ApprovalTask.ApproverPositionId = @ApproverPositionId
	and ApprovalTask.StatusCodeId = 1 --active


END