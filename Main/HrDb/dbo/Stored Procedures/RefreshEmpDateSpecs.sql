﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-08
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[RefreshEmpDateSpecs] 
AS
BEGIN
	begin tran
		if (select count(*) from StageHRMS.dbo.PersonEvent) > 0 begin
			truncate table EmployeeDateSpec;
			exec LoadEmployeeDateSpec;
		end
	commit;
END