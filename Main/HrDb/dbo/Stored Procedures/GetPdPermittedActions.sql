﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-11-05
-- Description:	this is currently only called on Update / Delete attempts...
-- CRUD bits: C=1, R=2, U=4, D=8
-- =============================================
CREATE PROCEDURE [dbo].[GetPdPermittedActions] 
	@PdId int = 0, 
	@LanId varchar(50)
AS
BEGIN
	-- block supervisors from updating if self position *or*
	-- HR screen complete.
	-- -- (replaced this line: Approval initiated (WF status = Active)
	-- 1: Create, 2: Read, 4: Update, 8: Delete
	-- READ:
	--declare @PdId int = 2056;
	--declare @LanId varchar(50) = 'DIS\cherr';

	--declare @PdId int = 2100; 
	--declare @LanId varchar(50) = 'DIS\judyf';

	select 2 as Value -- Read always allowed if user gets this far...

	-- CREATE:
	-- TODO: block Supervisors from Creating new Duties (or cloning) if WF is Active
--	union select case when u.RoleId in (1,2,3) -- admin, HR, Supervisor
	union select case when u.RoleId in (1,2) -- admin, HR, Supervisor
		-- allow creator to append duties / clone Positionless PDs.
		-- *NOTE*, there is no approval workflow for positionless PDs.
		-- it would be redundant with Create Position PARs.
		or ((select positionid from positiondescription pd
			where pd.Id = @PdId) is null
			and @LanId = (select LanId
				from AppUser u
				join PositionDescription pd
				on u.Id = pd.CreatedByUserId
				and pd.Id = @PdId
			)
		)
		-- prevent op on own position:
		or (u.RoleId = 3 and u.EmployeeId not in (
				select EmployeeId from Appointment a
				join PositionDescription pd on a.PositionId = pd.PositionId
				and (a.EndDate is null or a.EndDate > getdate())
				and pd.Id = @PdId
			)
--		or (u.RoleId = 3
			-- replaced approval logic block:
			and exists (
				select 1 from ApprovalTask t
				join PositionDescription pd
				on pd.Id = @PdId
				and t.ApprovalWorkflowId = pd.ApprovalWorkflowId
				and t.WorkflowOrder = 1 -- HR screen task
				and t.StatusCodeId in (1,5) -- Created or Active HR Screen task status
			)
			--and 
			--(select StatusCodeId
			--from ApprovalWorkflow wf
			--join PositionDescription pd
			--on wf.Id = pd.ApprovalWorkflowId
			--and pd.Id = @PdId) = 5
		)
	then 1 end -- (TODO: check on PdId, only valid for Clone op...)
	from AppUser u where u.LanId = @LanId

	-- UPDATE:
	union select case when u.RoleId in (1,2)
		-- allow updates of positionless PDs by creator:
		or ((select positionid from positiondescription pd
			where pd.Id = @PdId) is null
			and @LanId = (select LanId
				from AppUser u
				join PositionDescription pd
				on u.Id = pd.CreatedByUserId
				and pd.Id = @PdId
			)
		)
		-- prevent op on own position:
		or (u.RoleId = 3 and u.EmployeeId not in (
				select EmployeeId from Appointment a
				join PositionDescription pd on a.PositionId = pd.PositionId
				and (a.EndDate is null or a.EndDate > getdate())
				and pd.Id = @PdId
			)
			and exists (
				select 1 from ApprovalTask t
				join PositionDescription pd
				on pd.Id = @PdId
				and t.ApprovalWorkflowId = pd.ApprovalWorkflowId
				and t.WorkflowOrder = 1 -- HR screen task
				and t.StatusCodeId in (1,5) -- Created or Active HR Screen task status
			)
			-- replaced this logic with check for HR screen task status not in (Created, Active)
			--(select StatusCodeId
			--from ApprovalWorkflow wf
			--join PositionDescription pd
			--on wf.Id = pd.ApprovalWorkflowId
			--and pd.Id = @PdId)
			--= 5 -- Created (sequence not yet initiated)
		) -- admin, HR, Supervisors when not updating Self...TODO: add in delegates.
	then 4 end
	from AppUser u where u.LanId = @LanId

	-- DELETE:
	union select case when u.RoleId in (1,2) -- only Admin/HR can delete PD for now...
		-- allow updates of positionless PDs by creator:
		or ((select positionid from positiondescription pd
			where pd.Id = @PdId) is null
			and @LanId = (select LanId
				from AppUser u
				join PositionDescription pd
				on u.Id = pd.CreatedByUserId
				and pd.Id = @PdId
			)
		)
		or
		-- prevent op on own position:
			(u.RoleId = 3 and u.EmployeeId not in (
					select EmployeeId from Appointment a
					join PositionDescription pd on a.PositionId = pd.PositionId
					and (a.EndDate is null or a.EndDate > getdate())
					and pd.Id = @PdId
				)
		-- replaced CRUD logic to allow CRUD before HR screen complete:
		and exists (
			select 1 from ApprovalTask t
			join PositionDescription pd
			on pd.Id = @PdId
			and t.ApprovalWorkflowId = pd.ApprovalWorkflowId
			and t.WorkflowOrder = 1 -- HR screen task
			and t.StatusCodeId in (1,5) -- Created or Active HR Screen task status
		)
		--and
		--	(select StatusCodeId
		--	from ApprovalWorkflow wf
		--	join PositionDescription pd
		--	on wf.Id = pd.ApprovalWorkflowId
		--	and pd.Id = @PdId
		--	) = 5 -- Created (sequence not yet initiated)
		)
	then 8 end
	from AppUser u where u.LanId = @LanId
END