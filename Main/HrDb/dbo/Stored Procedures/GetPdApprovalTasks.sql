﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-21
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetPdApprovalTasks] 
	@PdId int
AS
BEGIN
	--declare @PdId int = 2049;
	select t.Id as Id,
	p.WorkingTitle,
	t.ApproverPositionId,
	isnull(e.PreferredName, e.FirstName) as FirstName,
	e.LastName,
	t.Comment,
	case when ascode.Name = 'Canceled' then 'Skipped' else ascode.Name end as ApprovalStatus,
	t.CompletedDate,
	e2.LastName as ApproverLastName,
	isnull(e2.PreferredName, e2.FirstName) as ApproverFirstName
	--case when EmployeeId is null and ApproverPositionId is not null then cast(1 as bit)
	--else cast(0 as bit) end as IsVacant
	from ApprovalTask t
	join ApprovalWorkflow wf on t.ApprovalWorkflowId = wf.Id
	join ApprovalStatusCode ascode
	on t.StatusCodeId = ascode.Id
	--join ApprovalTaskCode tc on t.TaskCodeId = tc.Id
	join PositionDescription pd
	on pd.Id = @PdId
	and pd.ApprovalWorkflowId = t.ApprovalWorkflowId
	left join Position p
	on t.ApproverPositionId = p.Id
	left join Appointment a
	on (
		a.Id = (select Id from Appointment
		where PositionId = p.Id
		-- this should select the most senior supervisor in a multi-filled supervisor position:
		and StartDate = (
				select min(StartDate) from Appointment a
				where a.PositionId = p.Id
				and (a.EndDate is null or a.EndDate > getdate())
			)
		)
	)
	left join Employee e
	on a.EmployeeId = e.Id
	left join Employee e2
	on t.FinalApproverPersonId = e2.Id

-- TODO: function to concatenate multiple delegate names, e.g. "Delegate1 OR Delegate2 OR..."
	--left join ApprovalDelegate d
	--on d.DelegatedPositionId = t.ApproverPositionId
	--and d.StartDate < wf.CreatedDate
	--and (d.EndDate is null or dateadd(d, 1, d.EndDate) > wf.CreatedDate)

	-- get approver first/last if approved...
	-- omit vacant positions from displayed approval workflow...
	where (ApproverPositionId is null
	or exists (
		select 1 from ApprovalDelegate d
		where d.DelegatedPositionId = t.ApproverPositionId
		and d.StartDate < wf.CreatedDate
		and (d.EndDate is null or dateadd(d, 1, d.EndDate) > wf.CreatedDate)
		and d.Active=1
		and exists (select 1 from ApprovalDelegateApp
			where delegateid = d.Id
			and appid = 2 -- PD
		)
	)
	or (a.id is not null))
	and (p.IsAppointingAuthority is null
		or p.IsAppointingAuthority = 0
		or t.ApproverPositionId = dbo.GetFirstApptAuthority(isnull(pd.PositionId,pd.SupervisorPositionId))
	)
	order by WorkflowOrder
END