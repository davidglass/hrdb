﻿
-- =============================================
-- Author:		Mark Paul
-- Create date: 8/20/13
-- Description:	Get all apps associated with delegate
-- =============================================
create PROCEDURE [dbo].[GetApprovalDelegateApps]
	-- Add the parameters for the stored procedure here
	@DelegateId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT  
		ApprovalApp.Id,
		ApprovalApp.IsActive,
		ApprovalApp.Name
	FROM ApprovalDelegateApp
	left outer join ApprovalApp on ApprovalApp.Id = AppId
	where DelegateId = @DelegateId
END