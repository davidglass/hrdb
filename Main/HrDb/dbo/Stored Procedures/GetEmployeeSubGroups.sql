﻿

-- =============================================
-- Author:		D. Glass
-- Create date: 2013-06-19
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetEmployeeSubGroups] 
AS
BEGIN
  select EmployeeSubGroupCode as Value,
  Description as [Text]
  from EmployeeSubGroupLkUp
  order by Description
END