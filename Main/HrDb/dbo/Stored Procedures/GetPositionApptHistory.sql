﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-12-13
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetPositionApptHistory] 
	@PositionId int
AS
BEGIN
--declare @Positionid int = 5081;
--declare @Positionid int = 5430;
	select
	a.StartDate,
	a.EndDate,
	rtrim(dbo.IsNullOrEmpty(e.preferredname, e.firstname)) + ' ' + e.lastname as EmpName,
	a.EmployeeId as EmpId,
	cast(case when exists (
		select 1 from appointment
		where EmployeeId = a.EmployeeId
		and (EndDate is null or EndDate > getdate())
	) then 1 else 0 end as bit) as CurrentEmp
	from appointment a
	left join employee e
	on a.employeeid = e.id
	where a.PositionId = @PositionId
	order by StartDate desc;
END