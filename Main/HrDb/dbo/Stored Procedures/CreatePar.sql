﻿CREATE PROCEDURE [dbo].[CreatePar]
	@LanId varchar(50),
	@PosId int, -- TODO: make this nullable, get new PosId from sequence for New Pos actions.
	@ActionType char(2), -- Action type (U-code)
	@ActionReasonId int,
	@EmployeeId int = null,
	@ApprovalWorkflowId int = null,
	@OrgUnitId int = null,
	@PosSupervisorPosId int = null,
	@IsTransferIn bit = 0,
	@IsTransferOut bit = 0,
	@IsPayChangeOnly bit = 0,
	@IsNameChangeOnly bit = 0,
	@IsNoShow bit = 0, -- superfluous with ActionReasonId == 16 ???
	-- defaulting ActionReason to Probationary (61) for now since defined as not null:
	-- @ActionReasonId int = null -- = 61 -- TODO: reconsider pre-creation of request for attachments...
	-- NOTE, next 6 params added to support create Par from pdf...
	@PosWorkingTitle varchar(100) = null,
	@PosJobId int = null,
	@PersonnelSubArea varchar(4) = null,
	@PosIsSupervisor bit = null,
	@PosPointValue int = null,
	@PdfId int = null
AS
BEGIN
	-- TODO: wrap this in transaction
	--declare @empId int;
	--declare @apptId int;
	declare @userId int;
	declare @CostCenterId int;
	--declare @SupervisorHrmsPosId int;
	-- TODO: make GetUserId a scalar function instead of proc?
	exec @userId = GetUserId @UserName = @LanId;
	-- TODO: create sequences for emp and appt, use instead of identity...
--	select @empId = next value for dbo.Employee_seq;
	--insert into Employee default values;
	--select @empId = convert(int, current_value) FROM sys.sequences
	--WHERE name = 'Employee_seq'
	if (@IsPayChangeOnly = 1 or @IsNameChangeOnly = 1) begin
		select @ActionReasonId =
		case when PayScaleTypeArea = 3 then 85 -- WMS
		when PayScaleTypeArea = 5 then 17 -- Exempt
		else 60 end -- Permanent
		from Appointment a
		where a.EmployeeId = @EmployeeId
		and (a.EndDate is null or a.EndDate > getdate())
	end

-- now postponing Appt insert until approval process complete...
	--insert into Appointment (PositionId, EmployeeId, StartDate)
	--values (@PosId, @empId, getdate());
	--select @apptId = SCOPE_IDENTITY();

--declare @hrmsId int;
--declare @jobId int;

-- this may be null if no @PosId is supplied (e.g. employee separation PAR)
-- TODO: move these into insert select statement
--select @hrmsId = HrmsPositionId, @jobId = JobId
--from Position where id = @PosId;
if @ActionType in ('U0', 'U6') -- New Hire, Rehire (former state emp with Personnel Number)
or @IsTransferIn = 1

begin
	select @EmployeeId = NEXT VALUE FOR [dbo].[Employee_seq];
end

-- TODO: more selectivity here...handle P0, P1, P2.
if @ActionType = 'P0'
begin
	if @PosSupervisorPosId is not null begin
		select @OrgUnitId = sp.OrgUnitId,
		@CostCenterId = sp.CostCenter
		--@SupervisorHrmsPosId = sp.HrmsPositionId
		from Position sp
		where Id = @PosSupervisorPosId
	end
	select @PosId = NEXT VALUE FOR [dbo].[Position_seq];
	insert into Par (
		ActionCode,
		PositionId,
		PosOrgUnitId,
		PosCostCenter,
		PosSupervisorPosId,
		RequestedByUserId,
		ApprovalWorkflowId,
		-- next 5 for PDF:
		PosWorkingTitle,
		PosJobId,
		PosPersonnelSubArea,
		PosIsSupervisor,
		PosPointValue
	)
	values (
		@ActionType,
		@PosId,
		@OrgUnitId,
		@CostCenterId,
		@PosSupervisorPosId,
		@userId,
		@ApprovalWorkflowId,
		-- added next 5 for PDF-generated PARs:
		@PosWorkingTitle,
		@PosJobId,
		@PersonnelSubArea,
		@PosIsSupervisor,
		@PosPointValue
	);
end

else if @ActionType in ('P1', 'P2') -- Position Update, Abolish
begin
	-- prevent abolishing pos w/active appt
	if (@ActionType = 'P2') and exists (
		select 1 from appointment
		where positionid = @PosId
		and (EndDate is null or EndDate > getdate())
	)
	begin
		declare @errmsg nvarchar(255);
		--select @errmsg = ERROR_MESSAGE();
		select @errmsg = 'Only vacant positions can be abolished.';
		raiserror('processing error: %s',11,1,@errmsg) with seterror;
		return;
	end
	insert into Par (
		ActionCode,
		ActionReasonId,
		PositionId,
		PosHrmsId,
		PosJobId,
		PosWorkingTitle,
		PosCostCenter,
		PosWorkersCompCode,
		PosOrgUnitId,
		PosSupervisorPosId,
		PosPersonnelSubArea,
		PosEeGroup,
		PosEeSubgroup,
		PosPayGradeTypeArea,
		PosPayGrade,
		PosIsInTraining,
		PosGeneralDescription,
		PosShiftCode,
--		PosShiftDesignation,
		PosShiftDiffEligible,
		PosStandbyEligible,
		PosReqsBackgroundCheck,
		PosIsAppointingAuthority,
		PosIsManager,
		PosIsSupervisor,
		PosJvac,
		PosPointValue,
		PosIsSection4,
		PosIsTandem,
		PosLocationId,
		PosWorkScheduleCode,
		PosIsRetireEligible,
		ApprovalWorkflowId,
		RequestedByUserId,
		DeactivateClassCode
	)
	select
		@ActionType,
		@ActionReasonId,
		@PosId,
		p.HrmsPositionId,
		isnull(@PosJobId, p.JobId),
		-- use these if supplied:
		--@PosWorkingTitle,
		--@PosJobId,
		--@PersonnelSubArea,
		--@PosIsSupervisor,
		--@PosPointValue
		isnull(@PosWorkingTitle, p.WorkingTitle),
		p.CostCenter,
		p.WorkersCompCode,
--		p.OrgUnitId,
		isnull(p.OrgUnitId, @OrgUnitId), -- insert @OrgUnitId for new positions...
		-- TODO: lookup OrgUnit supervisor pos to set here for new positions:
		p.SupervisorPositionId,
		isnull(@PersonnelSubArea, p.PersonnelSubArea),
		p.EeGroup,
		p.EeSubGroup,
		p.PayGradeTypeArea,
		p.PayGrade,
		p.IsInTraining,
		p.GeneralDescription,
		p.ShiftCode,
--		p.ShiftDesignation,
		p.IsShiftDiffEligible,
		p.IsStandbyPayEligible,
		p.IsBackgroundCheckRequired,
		p.IsAppointingAuthority,
		p.IsManager,
		isnull(@PosIsSupervisor, p.IsSupervisor),
		p.Jvac,
		isnull(@PosPointValue, p.PointValue),
		p.IsSection4,
		p.IsTandem,
		p.LocationId,
		p.DefaultWorkScheduleCode,
		p.IsRetireEligible,
		@ApprovalWorkflowId,
--		@hrmsId,
		@userId,
		-- set DeactivateClassCode if this is the sole unabolished position of the class:
		cast (case when @ActionType = 'P2'
			and (
				select count(1) from Position p2
				where IsAbolished = 0
				and p2.JobId = p.JobId
			) = 1
			--and not exists (
			--	select 1 from Appointment a
			--	join Position p2
			--	on a.PositionId = p2.Id
			--	and (a.EndDate is null or a.EndDate > getdate())
			--	and p2.JobId = p.JobId
			--)
			then 1 else 0 end as bit) -- DeactivateClassCode

	from Position p
	where p.Id = @PosId;
end;

-- TODO: distinguish between "Fill" Appt Change and other kinds here?
-- otherwise defaults to fill with current incumbent values.
-- New Hires should auto-exclude since they have no Employee Id or Appointment.
-- *NOTE*: @EmployeeId is null on initial fill from existing.
-- so can't assume EmpDOB will not be null in UI.
--else if @ActionType = 'U3' begin -- Appt Change
else begin -- default for non-position action
	insert into Par (
	ActionCode,
	ActionReasonId,
	Notes,
	IsTransferIn,
	IsTransferOut,
	PositionId,
	PosHrmsId,
	PosJobId,
	PosWorkingTitle,
	PosCostCenter,
	PosWorkersCompCode,
	PosOrgUnitId,
	PosSupervisorPosId,
	PosPersonnelSubArea,
	PosEeGroup,
	PosEeSubgroup,
	PosPayGradeTypeArea,
	PosPayGrade,
	PosIsInTraining,
	PosGeneralDescription,
	--PosShiftDesignation,
	PosShiftCode,
	PosShiftDiffEligible,
	PosStandbyEligible,
	PosReqsBackgroundCheck,
	PosIsAppointingAuthority,
	PosIsManager,
	PosIsSupervisor,
	PosJvac,
	PosPointValue,
	PosIsSection4,
	PosIsTandem,
	PosLocationId,
	PosWorkScheduleCode,
	PosIsRetireEligible,

	EmployeeId,
	EmpFirstName,
	EmpLastName,
	EmpMiddleName,
	EmpPreferredName,
	EmpDOB,
	EmpHrmsId,

	-- Employee Date Specs:
	EmpDS01_Anniversary,
	EmpDS02_Appointment,
	EmpDS03_CtsHire,
	EmpDS04_PriorPid,
	EmpDS05_Seniority,
	EmpDS07_UnbrokenService,
	EmpDS09_VacLeaveFrozen,
	EmpDS18_PersonalHolidayElg,
	EmpDS26_PersonalLeaveElg,
	EmpDS27_TsrLeaveElg,

	-- Appointment:
	FillLevel,
	ContractTypeCode,
	ContractEndDate,
	WorkScheduleCode,
	ApptWorkHoursWk1,
	ApptWorkHoursWk2,
	ApptTimeMgtStatus,
	PercentFullTime,
	-- Basic Pay (requires Appointment):
	ApptPayScaleTypeArea,
	ApptPayScaleReason,
	ApptPayScaleEffectiveDate,
	ApptPayScaleGroup,
	ApptPayScaleLevel,
	ApptPayNextIncrease,
	ApptStepMEligibleDate,
	ApptPayCapUtilityLevel,
	ApptPayAnnualSalary,
	ApptPayHourlyRate,
	ApptFillOtEligCode,
	ApptShiftDifferential,
--	ApptShiftDesignation,
	RequestedByUserId,
	ApprovalWorkflowId,
	IsPayChangeOnly,
	IsNameChangeOnly
	)
	-- TODO: prevent problems (multiple records returning) from multifills here (should be fixed):

	--declare @ActionType char(2) ='U3';
	--declare @ActionReasonId int = 0;
	--declare @PosId int = 5353;
	--declare @EmployeeId int = 6722;
	----declare @PosId int = 548;
	----declare @EmployeeId int = 3917;
	--declare @userId int = 4444;
	--declare @ApprovalWorkflowId int = 9999;

	select
		@ActionType,
		@ActionReasonId,
		case when @ActionReasonId = 16 then
			'Revert Previous Hire (Did Not Start)'
			else '' end,
		@IsTransferIn,
		@IsTransferOut,
		@PosId,
		p.HrmsPositionId,
		p.JobId,
		p.WorkingTitle,
		p.CostCenter,
		p.WorkersCompCode,
		p.OrgUnitId,
		p.SupervisorPositionId,
		p.PersonnelSubArea,
		p.EeGroup,
		p.EeSubGroup,
		p.PayGradeTypeArea,
		p.PayGrade,
		p.IsInTraining,
		p.GeneralDescription,
		--p.ShiftDesignation,
		p.ShiftCode,
		p.IsShiftDiffEligible,
		p.IsStandbyPayEligible,
		p.IsBackgroundCheckRequired,
		p.IsAppointingAuthority,
		p.IsManager,
		p.IsSupervisor,
		p.Jvac,
		p.PointValue,
		p.IsSection4,
		p.IsTandem,
		p.LocationId,
		p.DefaultWorkScheduleCode,
		p.IsRetireEligible,

		@EmployeeId,
		e.FirstName, -- null if @EmployeeId is null (e.g. fill existing)
		e.LastName,
		e.MiddleName,
		e.PreferredName,
		e.DOB,
		e.HrmsPersonId,

		-- Employee Date Spec if existing employee:
		eds.ds01,
		eds.ds02,
		eds.ds03,
		eds.ds04,
		eds.ds05,
		eds.ds07,
		eds.ds09,
		eds.ds18,
		eds.ds26,
		eds.ds27,
		-- Appointment info:
		isnull(a.FillLevelJobId, p.JobId), -- use current fill level if appt exists, else default
		a.ContractTypeCode,
		a.ContractEndDate,
		isnull(a.WorkScheduleCode, p.DefaultWorkScheduleCode),
		a.WorkHoursWk1,
		a.WorkHoursWk2,
		isnull(a.TimeMgtStatus, 9), -- 1:Actual, 9:Planned
		isnull(a.EmploymentPercent, 100),
		-- new Appt fields:
		-- if action is U0 (New) or U6 or U3 with different Position than current Appt,
		-- default this to values from the new position, else pull from existing Appt.
		case when @ActionType in ('U0', 'U6')
			-- insert defaults if filling with existing emp:
			-- TODO: test this with multifills, may need mod:
			or (@ActionType = 'U3' and (a.Id is null or a.EmployeeId <> @EmployeeId))
		then
			p.PayGradeTypeArea
		else a.PayScaleTypeArea end
		as PayScaleTypeArea,
--		a.PayScaleTypeArea,
		case when @ActionType in ('U0', 'U6')
			-- insert defaults if filling with existing emp:
			-- TODO: test this with multifills, may need mod:
			or (@ActionType = 'U3' and (a.Id is null or a.EmployeeId <> @EmployeeId))
			then '19' -- Initial Appointment
		else a.PayScaleReason end
		as PayScaleReason,
--		a.PayScaleReason,
		a.PayScaleEffectiveDate,
		case when @ActionType in ('U0', 'U6')
			-- insert defaults if filling with existing emp:
			-- TODO: test this with multifills, may need mod:
			or (@ActionType = 'U3' and (a.Id is null or a.EmployeeId <> @EmployeeId))
		then
			p.PayGrade
		else a.PayScaleGroup
		end as PayScaleGroup,
--		a.PayScaleGroup,
		a.PayScaleLevel,
		a.PayNextIncrease,
		a.StepMEligibleDate,
		a.PayCapUtilityLevel,
		a.PayAnnualSalary,
		a.PayHourlyRate,
		p.EeSubGroup, -- default appointment OT eligibility to match position
		-- default ShiftDifferential when filling Graveyard, Swing shifts:
		case when p.ShiftCode in ('G', 'S') then 1 else 0 end as ApptPayShiftDifferential,
--		a.ShiftDesignation,
		--@jobId,
		--@hrmsId,
		--p.JobId,
		@userId,
		@ApprovalWorkflowId,
		@IsPayChangeOnly,
		@IsNameChangeOnly
	from Position p
	left join Appointment a
	on p.Id = a.PositionId
	and (
		a.EmployeeId = @EmployeeId
		or a.EmployeeId is null
	)
	and a.EndDate is null -- TODO: include future-expiring appointments
	left join Employee e
	on e.Id = @EmployeeId -- don't join through Appointment as it may not exist yet.
	--on a.EmployeeId = e.Id
	--and .Id = @EmployeeId
	left join EmployeeDateSpec eds
	on e.Id = eds.EmployeeId
	where p.Id = @PosId;
end;
--else begin
---- currently includes
---- U0 (New Hire),
----P0	New Position
----P2	Abolish Position
----U0	New Hire
----U5	Separation
----U6	Rehire
----U8	Begin LOA - Active
----U9	Begin LOA - Inactive
----UA	Return from LOA
----UJ	Change of Status
--insert into Par (
--	EmployeeId,
--	PositionId,
--	PosHrmsId,
--	ActionCode,
--	FillLevel,
--	PercentFullTime,
--	PosCostCenter,
--	PosJobId,
--	PosSupervisorPosId,
--	RequestedByUserId
--	)
--	select
--		@EmployeeId,
--		@PosId,
--		p.HrmsPositionId,
--		@ActionType,
--		p.JobId, -- this is job class for Fill Level
--		100,
--		p.CostCenter,
--		p.JobId, -- this is Position job class
--		p.SupervisorPositionId,
--		@userId
--	from Position p

--	where p.Id = @PosId;
--end;
	if @PdfId is not null and @PdfId <> 0 begin
		update PositionDescription
		set LinkedParId = SCOPE_IDENTITY()
		where Id = @PdfId;
	end
	select * from Par where Id = SCOPE_IDENTITY(); -- new Par with default values.
END