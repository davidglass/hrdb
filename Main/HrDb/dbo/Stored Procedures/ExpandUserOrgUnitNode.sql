﻿
-- =============================================
-- Author:		Dave Glass
-- Create date: 2011-12-21
-- Description:	inserts NodeState record, interpreted as node expand for user.
-- =============================================
CREATE PROCEDURE [dbo].[ExpandUserOrgUnitNode] 
	@NodeId int,
	--@AppUserId int,
	@LanId varchar(50), 
	@IncludeSubNodes bit
AS
BEGIN
	declare @AppUserId int;
	exec @AppUserId = GetUserId @UserName = @LanId;

	if @IncludeSubNodes = cast(0 as bit) begin
		if not exists (select * from AppUser_ExpandedOrgNode where AppUserId = @AppUserId
		and OrgUnitId = @NodeId) begin
			insert into AppUser_ExpandedOrgNode (AppUserId, OrgUnitId)
			values (@AppUserId, @NodeId)
		end;
	end
	else begin
		-- expand all non-leaf subnodes:
		insert into AppUser_ExpandedOrgNode (AppUserId, OrgUnitId)
		(
			select @AppUserId, sn.Id
			from OrgStructure pn
			join OrgStructure sn
			on pn.Id = @NodeId
			and left(sn.Ancestry, datalength(pn.Ancestry)) = pn.Ancestry
			-- no longer excluding leaf nodes since all OrgUnit nodes (should) have Position leaves
			--and exists ( select * from OrgUnitNodeCTS where ParentId = sn.Id ) -- exclude leaf nodes
			and not exists (
				select OrgUnitId from AppUser_ExpandedOrgNode
				where AppUserId = @AppUserId
				and OrgUnitId = sn.Id
			)
		)
	end
END