﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-10-31
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetApprovalEmailVmNew]
	@TaskId int
AS
BEGIN
	-- find first active task in same workflow as passed-in task, if one exists:
	declare @ActiveTaskId int;
	-- TODO: verify w/Mark that max of one task can be active at once:
	-- this section relies upon that constraint, will break otherwise.
	select @ActiveTaskId = (
		select at.Id
		from ApprovalTask t
		join ApprovalTask at
		on t.Id = @TaskId
		and at.ApprovalWorkflowId = t.ApprovalWorkflowId
		and at.StatusCodeId = 1
	);
	if @ActiveTaskId is not null begin
		set @TaskId = @ActiveTaskId;
	end
	select distinct -- distinct to consolidate double-fills as (MULTIPLE) FilledByName
	pd.Id as PdId,
	pd.PositionId,
	pd.WorkingTitle,
	case when ApproveGroupId = 1 then cast(1 as bit) else cast(0 as bit) end as IsHR,
	--ApproverPositionId,

	-- TODO: GetFilledByNames fn for multi-fills
	case when (select count(1)
		from appointment
		where positionid = pd.PositionId
		and (enddate is null or enddate > getdate())) > 1
	then 'MULTIPLE'
	else
		case when fe.PreferredName is null
		or fe.PreferredName = '' then fe.FirstName
		else fe.PreferredName end 
		+ ' ' + fe.LastName
	end
	as FilledByName,

		--case when fe.PreferredName is null
		--or fe.PreferredName = '' then fe.FirstName
		--else fe.PreferredName end as FilledByFirstName,
		--fe.LastName as FilledByLastName,

	atc.Name as TaskCodeName,
	app.Name as AppName,
	case when ce.PreferredName is null
	or ce.PreferredName = '' then ce.FirstName
	else ce.PreferredName end as CreatorFirstName,
	ce.LastName as CreatorLastName,
	a.EmployeeId,
	del.DelegatedToPersonId,
	case when ApproveGroupId = 1 then 'hro@cts.wa.gov'
	when DelegatedToPersonId is null then aad.mail
	else ad.mail end as ApproverEmail,

	case when DelegatedToPersonId is null then
		case when ae.PreferredName is null
		or ae.PreferredName = '' then ae.FirstName
		else ae.PreferredName end
	else
		case when de.PreferredName is null
		or de.PreferredName = '' then de.FirstName
		else de.PreferredName end
	end as ApproverFirstName,
	case when DelegatedToPersonId is null then
		ae.LastName
	else
		de.LastName
	end as ApproverLastName
	from ApprovalTask t
	join ApprovalWorkFlow wf
	on t.ApprovalWorkflowId = wf.Id
	and t.Id = @TaskId
	-- TODO: special email for WF final approval event?
	and wf.StatusCodeId in (5,1) -- no emails unless WF is Created or Active?
	join ApprovalApp app
	on wf.AppId = app.Id
	join ApprovalTaskCode atc
	on t.TaskCodeId = atc.Id
	left join
	ApprovalDelegate del
	on t.ApproverPositionId = del.DelegatedPositionId
	and del.StartDate < getdate()
	and (del.EndDate is null or del.EndDate > getdate())
	and del.Active = 1
	left join Appointment a
	on a.PositionId = t.ApproverPositionId
	and (a.EndDate is null or a.EndDate > getdate())
	left join AppUser du
	on du.EmployeeId = del.DelegatedToPersonId
	left join Employee de -- Delegate Employeee
	on del.DelegatedToPersonId = de.Id
	left join AdExtract ad
	on du.ObjectGuidHexString = ad.objguidstring
	left join PositionDescription pd
	on wf.Id = pd.ApprovalWorkflowId
	left join AppUser creator
	on creator.Id = pd.CreatedByUserId
	left join Employee ce -- Creator Employee
	on creator.EmployeeId = ce.Id
	left join AppUser au
	on a.EmployeeId = au.EmployeeId
	left join ADExtract aad
	on au.ObjectGuidHexString = aad.objguidstring
	left join Employee ae
	on au.EmployeeId = ae.Id
	left join Appointment filled
	on pd.PositionId = filled.PositionId
	and (filled.EndDate is null or filled.EndDate > getdate())
	left join Employee fe -- Filledby Employee
	on filled.EmployeeId = fe.Id
END