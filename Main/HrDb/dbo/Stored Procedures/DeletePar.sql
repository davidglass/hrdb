﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-07-02
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[DeletePar] 
	@ParId int
AS
BEGIN
	if exists (select 1 from ParAttachment where parid = @ParId) begin
		raiserror('Attachments must be removed before deleting PARs.', 11, 1) with seterror;
		return;
	end
	if (select statusid from par where Id = @ParId) > 3
	begin
		raiserror('This PAR has already been processed.  To revert its changes a new PAR is needed.', 11, 1) with seterror;
		return;
	end
	delete from Par
	where Id = @ParId;

	--added delete of the checklist if PAR is deleted PM
	delete from Checklist
	where ParId = @ParId;
END