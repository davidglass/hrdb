﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-28
-- Description:	option list for current employees
-- =============================================
CREATE PROCEDURE [dbo].[GetCurrentEmpSelection] 
AS
BEGIN
	select cast(e.Id as varchar(10)) as Value,
	e.LastName + ', ' + dbo.IsNullOrEmpty(e.PreferredName, e.FirstName) as [Text]
	from Employee e
	join Appointment a
	on e.Id = a.EmployeeId
	and (a.EndDate is null or a.EndDate > getdate())
	order by e.LastName, e.FirstName
END