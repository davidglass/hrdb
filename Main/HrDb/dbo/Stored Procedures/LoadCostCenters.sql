﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-02-07
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[LoadCostCenters] 
AS
BEGIN
	truncate table CostCenter;
	insert into CostCenter
		select CostCenter,
		cast(costcenter as char(4)),
		DescShort,
		DescLong,
		case when Status = 'A' then 1 else 0 end as IsActive,
		PersonId as ManagerEmpId
		FROM [HrDb].[legacy].[CostCenter]
		where fiscalyear = '2014'

END