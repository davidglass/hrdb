﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 8/20/13
-- Description:	add a new Location (room) and return its Id
-- =============================================
CREATE PROCEDURE [dbo].[CreateApprovalTask] 
	-- Add the parameters for the stored procedure here

	@ApprovalWorkflowId int,
	@StatusCodeId int,
	@TaskCodeId int,
	@PositionId int = null,
	@ApproveGroupId int = null,
	@Comment text = '',
	@WorkflowOrder int
	
AS
BEGIN


		INSERT INTO ApprovalTask
			   (ApprovalWorkflowId,
			   StatusCodeId,
			   TaskCodeId,
			   ApproverPositionId,
			   ApproveGroupId,
			   Comment,
			   WorkflowOrder)
		VALUES
			   (@ApprovalWorkflowId,
			   @StatusCodeId,
			   @TaskCodeId,
			   @PositionId,
			   @ApproveGroupId,
			   @Comment,
			   @WorkflowOrder)
			   
		select top 1 Id from ApprovalTask order by Id desc

			   
END