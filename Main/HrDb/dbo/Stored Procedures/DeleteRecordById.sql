﻿CREATE PROCEDURE [dbo].[DeleteRecordById] 
	-- Add the parameters for the stored procedure here
	@TableName varchar(50),
	@Id int = 0
AS
BEGIN
	declare @q nvarchar(max);
	set @q = N'delete from ' + @TableName
	+ ' where id = ' + cast(@Id as varchar(10));
	exec sp_executesql @stmt = @q;
END