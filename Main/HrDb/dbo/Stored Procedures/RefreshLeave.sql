﻿-- =============================================
-- Author:		P. McKusick
-- Create date: 2014-10-09
-- Description:	truncate and reload leave data 
-- =============================================
CREATE PROCEDURE [dbo].[RefreshLeave] 
AS
BEGIN
	begin tran
		if (select count(*) from StageHRMS.dbo.Leave) > 0 begin
			truncate table hrms.leave;
		
		insert into hrms.leave
		
		select PersonId
		,EffectiveDate
		,LeaveType
		,BeginDate
		,EndDate
		,BeginningBalance
		,LeaveEarned
		,LeaveTaken
		,LeavePaid
		,LeaveAdj
		 ,LeaveDonated
		,LeaveReturned
		,LeaveReceived
		,EndBalance
		 from StageHRMS.dbo.Leave
		
		end
	commit;
END