﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-04-22
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[RebuildPosStructure] 
AS
BEGIN
	declare @DirPosId int = 5202; -- DIRECTOR
	IF EXISTS (SELECT * FROM sys.foreign_keys 
		WHERE object_id = OBJECT_ID(N'FK_PositionStructure_PositionStructure')
		AND parent_object_id = OBJECT_ID(N'dbo.PositionStructure'))
	BEGIN
		-- can't truncate table without DROPPING FK. (disabling doesn't work)
		alter table PositionStructure drop FK_PositionStructure_PositionStructure;
	END;

	--disable triggers:
	disable trigger all on PositionStructure;

--	truncate table PositionStructure;
-- still can't truncate due to FK ref:
	delete from PositionStructure;
	insert into PositionStructure (Id, Ancestry) -- omitting ParentId (CTS OrgUnit ID)
	values(@DirPosId, 0x01);
	exec AddPositionStructureSubnodes @DirPosId;
	alter table PositionStructure add constraint FK_PositionStructure_PositionStructure
		foreign key (SupervisorPositionId) references PositionStructure (Id);
	enable trigger all on PositionStructure;
END