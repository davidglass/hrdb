﻿
-- =============================================
-- Author:		Mark Paul
-- Create date: 8/20/13
-- Description:	Get PositionId of Employee. Checks Appointment table
-- =============================================
CREATE PROCEDURE [dbo].[GetPositionIdOfEmployee]
	
	@employeeId int
AS
BEGIN

	select top 1 PositionId from appointment where employeeId = @employeeId 
	and ((EndDate is null) or (EndDate > getdate()))
	
END