﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 6/4/13
-- Description:	Get approval
-- =============================================
CREATE PROCEDURE [dbo].[GetApprovalTask]
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT top 1
		[ApprovalTask].[Id],
		[ApprovalWorkflowId],
		[ApproverPositionId],
		[ApproveGroupId],
		--ApprovalStatusCode.Name [StatusCode],
		--ApprovalTaskCode.Name [TaskCode],
		[ApprovalTask].StatusCodeId [StatusCode],
		[ApprovalTask].TaskCodeId [TaskCode],
		[FinalApproverPersonId],
		[WorkflowOrder],
		[CompletedDate],
		[Comment]
	FROM [ApprovalTask]
	--left outer join ApprovalStatusCode on ApprovalTask.StatusCodeId = ApprovalStatusCode.Id
	--left outer join ApprovalTaskCode on ApprovalTask.[TaskCodeId] = ApprovalTaskCode.Id
	where [ApprovalTask].[Id] = @Id

END