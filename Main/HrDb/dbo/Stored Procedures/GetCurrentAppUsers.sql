﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-02-13
-- Description:	
-- =============================================
CREATE PROCEDURE GetCurrentAppUsers 
AS
BEGIN
  select u.Id, u.EmployeeId, RoleId
  from AppUser u
  join Appointment a
  on u.EmployeeId = a.EmployeeId
  and (a.EndDate is null or a.EndDate > getdate())
END