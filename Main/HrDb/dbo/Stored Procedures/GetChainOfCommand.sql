﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-01
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetChainOfCommand] 
	-- Add the parameters for the stored procedure here
	@Id int -- PositionId
AS
BEGIN
	-- DG new version 2014-06-20:
	-- declare @Id int = 5312;
	select ps2.Id,
	-- believe Mark's code ignores all but PositionIds anyway...
	-- fake fields are only being returned to keep from breaking cast to ChainLink.
	'IGNORED' as JobTitle,
	cast(0 as bit) as IsVacant, -- vacant positions will be excluded anyway, 
	'IGNORED' as IncumbentLastName,
	0 as Level, -- ignored,
	0 as EmployeeId -- ignored
	from PositionStructure ps
	join PositionStructure ps2
	on ps2.Ancestry = left(ps.Ancestry, datalength(ps2.Ancestry))
	and ps.Id = @Id
	where exists (
		select 1 from Appointment a
		where a.PositionId = ps2.Id
		and (enddate is null or enddate > getdate())
	)
	or ps2.Id = @Id
	order by ps2.Ancestry

-- TODO: move this to SSIS (clears legacy director "fake supervisor")
--update position 
--set supervisorpositionid = null
--where LegacyClassCode = 'EX011'; -- director
--declare @Id int = 5078; -- ppid = 5548, DIR pid = 5202
--declare @Id int = 5542; -- Anne K, good example with vacant sub and vacant supervisor
--declare @Id int = 5548;
-- declare @Id int = 5353; -- CSD AD (double-fill test)
--declare @Id int = 5430; -- HR Mgr pos, testing vacant MSD AD

--with chain as (
--	select Id, SupervisorPositionId,
--		JobId, WorkingTitle, 1 as Level
--	from Position
--	where Id = @Id
	
--	union all

--	select sp.Id, sp.SupervisorPositionId,
--		sp.JobId, sp.WorkingTitle, p.Level + 1 as Level
--	from position sp
--	join chain p
--	on p.SupervisorPositionId = sp.Id
--)
--select chain.Id,
----	left(j.JobTitle, 40) as JobTitle,
--	rtrim(case when chain.WorkingTitle is not null
--		and len(chain.WorkingTitle) < len(j.JobTitle)
--		then chain.WorkingTitle else left(j.JobTitle, 30) end) as JobTitle,
---- TODO: include date test here to exclude historical appointments
--case when a.Id is null
--	or a.EndDate is not null
--then cast(1 as bit) else cast(0 as bit) end as IsVacant,

--	isnull(e.LastName, 
--		(select fe.LastName
--		from Employee fe
--		where fe.Id = (
--			select EmployeeId
--			from Appointment
--			where PositionId = chain.Id
--			and EndDate = (
--				select max(EndDate)
--				from Appointment
--				where PositionId = chain.Id
--			)
--		))
--	)
----isnull(e.LastName, 'VACANT')
--+ case when (select count(*) from appointment
--	where positionid = chain.Id and enddate is null
--	) > 1 then
--	' (PRIMARY)'
--	else ''
--	end 
--as IncumbentLastName, Level,
--a.EmployeeId
--from chain
----join hrms.Job j
--join JobClass j
--on chain.JobId = j.Id
--left join Appointment a
--on chain.Id = a.PositionId
--and a.IsPrimary = 1
--left join Employee e
--on a.EmployeeId = e.Id
--where (a.EndDate is null -- current appointments only
--	or (
--		a.EndDate = (
--			select max(EndDate) from appointment
--			where positionId = chain.Id
--		)
--		and not exists (
--			select 1 from appointment
--			where positionid = chain.id
--			and enddate is null
--		)
--	)
--)
---- add subordinates:
----union
----select p.Id,
------	left(j.JobTitle, 40) as JobTitle,
----	rtrim(case when p.WorkingTitle is not null
----		and len(p.WorkingTitle) < len(j.JobTitle)
----		then p.WorkingTitle else left(j.JobTitle, 30) end) as JobTitle,
------	rtrim(p.WorkingTitle) as JobTitle,
------ TODO: include date test here to exclude historical appointments
----case when a.Id is null then cast(1 as bit) else cast(0 as bit) end as IsVacant,
----	isnull(e.LastName, 
----		(select fe.LastName
----		from Employee fe
----		where fe.Id = (
----			select EmployeeId
----			from Appointment
----			where PositionId = p.Id
----			and EndDate = (
----				select max(EndDate)
----				from Appointment
----				where PositionId = p.Id
----			)
----		))
----	) as IncumbentLastName,
------e.LastName as IncumbentLastName,
----0 as Level,
----a.EmployeeId
----from Position p
----join hrms.Job j
----on p.JobId = j.Id
----left join Appointment a
----on p.Id = a.PositionId
----and a.IsPrimary = 1
----and a.EndDate is null
----left join Employee e
----on a.EmployeeId = e.Id
----where p.SupervisorPositionId = @Id
--order by Level desc, IsVacant, JobTitle, IncumbentLastName



END