﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 8/21/13
-- Description:	save data on approvalWorkflow
-- =============================================
create PROCEDURE [dbo].[SaveApprovalWorkflow] 
	-- Add the parameters for the stored procedure here

	@Id int,
	@StatusCodeId int,
	@ApprovalCodeId int,
	@PositionId int = null,
	@PersonId int = null,
	@AppId int
	
AS
BEGIN


		UPDATE ApprovalWorkflow
		set
			StatusCodeId = @StatusCodeId,
			ApprovalCodeId = @ApprovalCodeId,
			PositionId = @PositionId,
			PersonId = @PersonId,
			AppId = @AppId
		where Id = @Id

			   
END