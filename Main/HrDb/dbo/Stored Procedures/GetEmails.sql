﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 2014-03-17
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetEmails] 
AS
BEGIN
	
	update email set exception = 'Taken by service' where sentDateTime is null and exception is null	
	select * from Email where sentDateTime is null and exception = 'Taken by service'	
END
GO
-- this breaks build since user is not part of project:
-- GRANT EXECUTE
--    ON OBJECT::[dbo].[GetEmails] TO [HrEmailService]
--    AS [dbo];

