﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 10/8/13
-- Description:	Get list of all active and future delegates of employee
-- =============================================
CREATE PROCEDURE [dbo].[GetApprovalDelegatesOfEmployee]
	@EmployeeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT  
		*
	FROM ApprovalDelegate
	where ApprovalDelegate.DelegatedToPersonId = @EmployeeId
	and Active = 1
	and ((Indefinite = 1) or (ApprovalDelegate.EndDate >= DATEADD(day, -1, getdate()))) --subtract day so that end date of current date is still active
	
	
	 

END