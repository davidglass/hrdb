﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-08
-- Description:	Load Abolished Positions (assumes current positions already loaded)
-- =============================================
CREATE PROCEDURE [dbo].[LoadAbolishedPositions] 
AS
BEGIN
	insert into Position (Id, OrgUnitId, CostCenter, JobId,
		JobTitle, LegacyClassCode, LegacyPositionNum, LegacyUnitId,
		LegacyLastUpdated, WorkingTitle, HrmsPositionId, SupervisorPositionId,
		IsAbolished)
	(
	--select distinct
	--cast(h.positionnum as int) Id,
	--null as OrgUnitId,
	--h.CostCenter,
	--null as JobId, -- ClassCode is legacy field; can correlate to HRMS JobId.
	--h.JobTitle,
	--h.PositionClassCode as LegacyClassCode,
	--h.positionnum as LegacyPositionNum,
	--h.UnitId as LegacyUnitId,
	----h.Supervisor as SupervisorPersonId,
	--h.LastUpdatedDate as LegacyLastUpdated,
	--null as WorkingTitle,
	--null as HrmsPositionId,
	--cast (e.PositionNum as int) as SupervisorPositionId,
	--cast(1 as bit) as IsAbolished
	--from stagelegacy.organization.employee_history h
	----left join stage_legacy.organization.Employee e
	--left join stagelegacy.organization.Employee_History e
	--on h.Supervisor = e.PersonId
	--and e.Id = (
	--	select max(Id)
	--	from stagelegacy.organization.employee_history h3
	--	where h3.PersonId = e.PersonId
	--)
	--where h.id = (select max(id)
	--	from stagelegacy.organization.employee_history h2
	--	where h2.positionnum = h.positionnum
	--)
	--and h.positionnum is not null
	--and h.positionnum <> ''
	--and left(h.positionnum, 2) <> 'UG'
	--and len(h.positionnum) = 4 -- exclude duplicates with/without leading zeros
	--and not exists (
	--	select 1 from StageLegacy.organization.Positions
	--	where PositionNum = h.positionnum
	--)
	--and not exists (
	--	select 1 from Position
	--	where Id = cast(h.positionnum as int)
	--)

-- 102 positions unloaded; get most recent supervisor
select cast(positionnum as int) as Id,
null as OrgUnitId,
h.CostCenter,
null as JobId, -- ClassCode is legacy field; can correlate to HRMS JobId.
h.JobTitle,
h.PositionClassCode as LegacyClassCode,
h.positionnum as LegacyPositionNum,
h.UnitId as LegacyUnitId,
--h.Supervisor as SupervisorPersonId,
h.LastUpdatedDate as LegacyLastUpdated,
null as WorkingTitle,
null as HrmsPositionId,

cast((select sv.PositionNum 
	from stagelegacy.organization.employee_history sv
	where Id = (
		select max(id)
		from stagelegacy.organization.employee_history
		where personid = h.Supervisor
	)
) as int) as SupervisorPositionId,
cast(1 as bit) as IsAbolished
from stagelegacy.organization.employee_history h
where
positionnum is not null
and positionnum <> ''
and left(positionnum, 2) <> 'UG'
and not exists (select 1 from position where id = cast(h.positionnum as int))
and id = (
	select max(id)
	from stagelegacy.organization.employee_history
	where
	cast(case when left(positionnum,2) = 'UG' then -1 else positionnum end as int) = cast(h.positionnum as int) -- normalize leading zeros
)
	--and not exists (
	--	select 1 from StageLegacy.organization.Positions
	--	where PositionNum = h.positionnum
	--)
	and not exists (
		select 1 from Position
		where Id = cast(h.positionnum as int)
	)
	)
END