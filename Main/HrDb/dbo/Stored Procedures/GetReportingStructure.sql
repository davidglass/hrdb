﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-11-20
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetReportingStructure]
	@RootPosId int = 5202
	,@LanId varchar(50) -- set root to user position if not HR/admin role
AS
BEGIN
-- 6722 = McVicker..., 6243: Huff, double-filling position #5353
--set statistics time on;
	--declare @RootPosId int = 5202; -- DIR
	--declare @LanId varchar(50) = 'dis\davidg'
--	declare @RootPosId int = 5353; -- CSD
	--declare @RootPosId int = 5548; -- ATS Mgr
--	declare @RootPosId int = 5853; -- SDC CTO

	select @RootPosId = dbo.GetRootPositionByRole(@LanId, @RootPosId);
	--declare @RootPosId int = 5101;
	--declare @RootPosId int = 6307;
--	declare @RootPosId int = 5548;
	select
		p.Id,
		datalength(ps.ancestry) as Depth,
		-- http://msdn.microsoft.com/en-us/library/ms187928.aspx
		-- prefix with ancestry+lastname of first double-filled ancestor position (if any):
		-- TODO: substitute (override) position ancestry portion of designated SupervisorEmployeeId if different from SupervisorPositionId.
		-- (this will allow displaying position structure to reflect temporary, acting supervisors)
		isnull(
				(select top 1 left(convert(varchar(50), ps2.Ancestry, 2), 2*(datalength(ps2.Ancestry)-1)) + e.LastName
		from PositionStructure ps2
		join Position p on ps2.Id = p.Id
		and ps2.ancestry = left(ps.ancestry, datalength(ps2.ancestry))
		join Employee e	on p.SupervisorEmpId = e.Id
		-- only apply to double-fill situations:
		and 1 < (
			select count(*) from Appointment where PositionId = @RootPosId
			and (enddate is null or enddate > getdate())
		)
		order by datalength(ps2.ancestry) desc
		), '')
		-- Ancestry string (could omit portion matching double-filled ancestor, but minimal impact to leaving, used only for sorting)
		+ convert(varchar(50), ps.ancestry, 2) +

		-- suffix ancestry with lastname if multi-filled:
		case when (select count(1) from appointment
			where positionid = p.Id
			and (
				enddate is null or
				enddate > getdate()
			)
		) > 1 then e.LastName else '' end
		as Anc,
		isnull(p.WorkingTitle, j.JobTitle) as T,
		isnull(a.EmployeeId,
			(
			select EmployeeId from Appointment where PositionId = p.Id
			and EndDate = (select max(EndDate) from Appointment where PositionId = p.Id)
			)
		) as EmpId,
		case when a.Id is null then
			(
			select e2.LastName from Appointment a2
			join Employee e2
			on a2.PositionId = p.Id
			and e2.Id = a2.EmployeeId
			and EndDate = (select max(EndDate) from Appointment where PositionId = p.Id)
			)

		else e.LastName + ', '
		+ case when e.PreferredName is null or e.PreferredName = ''
		then e.FirstName else e.PreferredName end
		end as SN, -- SurName
		p.SupervisorEmpId,
	cast(case when a.Id is null then 1 else 0 end as bit) as V, -- IsVacant
	case when a.EmployeeId = 6722 then 'Mainframe Services'
		 when p.Id = @RootPosId or (sp.OrgUnitId <> p.OrgUnitId) then isnull(ocu.OrgUnitName, '') else '' end OU,
	p.IsSupervisor as SV,
	case when (select count(*) from appointment
		where PositionId = p.Id
		and (EndDate is null or EndDate > getdate())
		) > 1 then cast(1 as bit) else cast(0 as bit) end as MF,
	case when p.OrgUnitId in (31008300, 31008301, 31008302, 31008325, 31008326, 31008332) then p.ShiftCode else 'N' end
		as ShiftCode,
	a.StartDate as ApptStartDate,
	a.EndDate as ApptEndDate
	from PositionStructure ps
	join PositionStructure bps
	on bps.Id = @RootPosId
	and left(ps.Ancestry, datalength(bps.Ancestry)) = bps.Ancestry
	join Position p on ps.Id = p.Id
	and p.IsAbolished = 0
	join OrgUnit ou on p.OrgUnitId = ou.Id
	left join OrgChartOrgUnit ocu on p.OrgUnitId = ocu.OrgUnitId
	-- exclude OU display for non-supervisors:
	and p.IsSupervisor = 1
	left join Appointment a
		on p.Id = a.PositionId
		and (EndDate is null or dateadd(d, 1, EndDate) > getdate())
	left join Employee e on a.EmployeeId = e.Id
	left join Position sp on p.SupervisorPositionId = sp.Id
	--left join hrms.Job j on j.Id = p.JobId
	left join JobClass j on j.Id = p.JobId
	order by Anc
	--order by case when a.Id is null then 2 else 1 end, Anc
END