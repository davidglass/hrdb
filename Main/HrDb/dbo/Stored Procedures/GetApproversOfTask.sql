﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 8/28/13
-- Description:	Get list of all employees that are assigned approval task, checks group if assigned to one
--  Modified to include delegates. 11/5/13
-- =============================================
CREATE PROCEDURE [dbo].[GetApproversOfTask]
	@TaskId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT  --top 1 included for dual fills
		Appointment.EmployeeId [Id]
	FROM [ApprovalTask]
	left outer join ApproveGroupMembers on ApproveGroupMembers.ApproveGroupId = [ApprovalTask].ApproveGroupId
	left outer join Position Pos on (ApproverPositionId = Pos.Id or ApproveGroupMembers.PositionId = Pos.Id)
	left outer join Appointment on Pos.Id = Appointment.PositionId
	where [ApprovalTask].[Id] = @TaskId
	and (Appointment.EndDate is null) or (Appointment.EndDate > getdate())
union
	SELECT  --top 1 included for dual fills
		ApprovalDelegate.DelegatedToPersonId [Id]
	FROM [ApprovalTask]
	left outer join ApproveGroupMembers on ApproveGroupMembers.ApproveGroupId = [ApprovalTask].ApproveGroupId
	left outer join ApprovalDelegate on (ApproverPositionId = ApprovalDelegate.DelegatedPositionId or ApproveGroupMembers.PositionId = ApprovalDelegate.DelegatedPositionId)
	where [ApprovalTask].[Id] = @TaskId
	and ApprovalDelegate.Active = 1
	and ((ApprovalDelegate.Indefinite = 1) or (ApprovalDelegate.EndDate >= DATEADD(day, -1, getdate()))) --subtract day so that end date of current date is still active

	
	 

END