﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-08-21
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[CreatePositionDescription] 
	@PositionId int,
	@LanId varchar(50)
AS
BEGIN
	declare @userId int;
	-- GetUserId is a proc instead of a function so it can insert missing users:
	exec @userId = GetUserId @UserName = @LanId;
	declare @Id int;
	select @Id = NEXT VALUE FOR [dbo].[PositionDescription_seq];

	insert into PositionDescription (
		Id,
		PositionId,
		xCurrentJobId,
		ProposedJobId,
		WorkingTitle,
		LastUpdatedByUserId,
		LegacyStatus,
		TopJobClassSupervised,
		TotalFTESupervised,
		DirectFteSupervised,
		PositionTypeCXW,
		IsSupervisor,
		xCurrentJvacEvalPts,
		xProposedJvacEvalPts,
		xCurrentBand,
		xProposedBand,
		CreatedByUserId
	)
	select
	@Id,
	p.Id as PositionId,
	jc.Id as xCurrentJobId, -- default to current job class
	jc.Id as ProposedJobId, -- default to current job class
	p.WorkingTitle,
	@userId,
	-- TODO: create Status column, independent of LegacyStatus (used only for import)
	-- or possibly modify ParStatus lookup to be shared among records (e.g. "RecordStatus")?
	'New' as LegacyStatus,
	dbo.GetTopSupervisedJobId(p.Id) as TopJobClassSupervised,
	(select cast(count(*) as numeric(5,2))
	from PositionStructure ps
	join PositionStructure subps
	on left(subps.Ancestry, datalength(ps.Ancestry)) = ps.Ancestry
	and datalength(subps.Ancestry) > datalength(ps.Ancestry)
	and ps.Id = p.Id) as TotalFteSupervised,
	-- this only counts direct reports, should count all (separate field?)
	(select cast(count(*) as numeric(4,2)) from position where SupervisorPositionId = p.Id)
		as DirectFteSupervised,
	case when p.PersonnelSubArea = '0003' then 'X' -- eXempt
	when p.PersonnelSubArea = '0002' then 'W' -- WMS
	else 'C' end -- Classified
	as PositionTypeCXW,
	p.IsSupervisor,
	p.PointValue,
	p.PointValue,
	p.PayGrade as xCurrentBand,
	p.PayGrade as xProposedBand,
	@userId
	from Position p
	join JobClass jc
	on p.JobId = jc.Id
	where p.Id = @PositionId;

	-- this populates PositionDescriptionRow entity (summary in list view), so not returning full record.
	select pd.Id,
	ProposedJobId,
	pd.WorkingTitle,
	j.LegacyCode as ProposedClassCode,
	j.JobTitle as ProposedClassTitle,
	pd.EffectiveDate as ApprovalDate, -- TODO: make sure this is importing right from approvals...
	pd.LastUpdatedAt,
	pd.LegacyStatus
	from PositionDescription pd
	join JobClass j
	on pd.ProposedJobId = j.Id
	where pd.Id = @Id
--	where PositionId = @PositionId
	order by pd.id desc;

	--select * from PositionDeCscription
	--where Id = @Id; -- retrieve just-inserted PD with any defaults
END