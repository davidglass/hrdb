﻿



CREATE PROCEDURE [dbo].[GetPayScaleLevel]
AS
BEGIN
	select Code as [Value], -- values must be char/string to use as SelectListItems 
	Code as [Text] -- TODO: get Description for PayScaleLevels, if possible...
	from PayScaleLevelCode
END