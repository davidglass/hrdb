﻿
-- =============================================
-- Author:		Mark Paul
-- Create date: 8/21/13
-- Description:	Get specific approval workflow
-- =============================================
create PROCEDURE [dbo].[GetApprovalWorkflow]
	-- Add the parameters for the stored procedure here
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT  
		[Id],
		StatusCodeId [StatusCode],
		ApprovalCodeId [ApprovalCode],
		AppId [App],
		PositionId,
		PersonId,
		CreatedDate
	FROM ApprovalWorkflow
	where [Id] = @Id
END