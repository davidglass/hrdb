﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-08-09
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetSupervisorSelection] 
AS
BEGIN
	-- double-filled supervisor positions will be a problem here...they select same position id.
	select
	isnull(e.lastname,
		'VACANT')
	+ ', ' +
	ou.ShortName
	+
	case when a.Id is null then
		' [' + cast(p.Id as varchar) + ']'
	else '' end
	as [Text],
	cast (p.Id as varchar) as Value
	from Position p
	join OrgUnit ou
	on p.OrgUnitId = ou.Id
	left join Appointment a
	on p.Id = a.PositionId
	and a.EndDate is null -- TODO: include future end dates too.
	left join Employee e
	on a.EmployeeId = e.Id
	where p.IsSupervisor = 1
	and isabolished = 0
	order by
	case when a.Id is null then 2 else 1 end,
	lastname
END