﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 12/2/13
-- Description:	create new email to send via service
-- =============================================
CREATE PROCEDURE [dbo].[CreateEmail] 
	-- Add the parameters for the stored procedure here

	@fromAddress varchar(100),
	@fromDisplayName varchar(100),
	@toAddress varchar(100),
	@toDisplayName varchar(100),
	@body varchar(max),
	@subject varchar(max),
	@isBodyHtml bit = 1,
	@source varchar(50) = null,
	@sourceId int = null
	
AS
BEGIN


		INSERT INTO Email
			   (fromAddress,
				fromDisplayName,
				toAddress,
				toDisplayName,
				body,
				[subject],
				isBodyHtml,
				[source],
				sourceId)
			   
		VALUES
			   (@fromAddress,
				@fromDisplayName,
				@toAddress,
				@toDisplayName,
				@body,
				@subject,
				@isBodyHtml,
				@source,
				@sourceId)
			   
			   
END