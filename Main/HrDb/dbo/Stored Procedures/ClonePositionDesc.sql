﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2014-05-30
-- Description:	clone PositionDescription to new deep copy,
-- including related PositionDuties.
-- =============================================
CREATE PROCEDURE [dbo].[ClonePositionDesc] 
	-- Add the parameters for the stored procedure here
	@PdId int, 
	@LanId varchar(50)
AS
BEGIN
	--declare @PdId int = 2232;
	--declare @LanId varchar(50) = 'DIS\DavidG';
	declare @DutyId int;
	SET NOCOUNT ON;
	begin try
		declare @UserId int;
		declare @Id int;
		declare @WfId int; -- workflow Id;
		declare @PositionId int;
		begin tran
			select @PositionId = pd.PositionId
			from PositionDescription pd
			where pd.Id = @PdId;

			exec @UserId = GetUserId @UserName = @LanId
			exec @WfId = CreatePdApprovalWorkflow @PositionId = @PositionId;

			-- get ID in advance for new PositionDescription record:
			select @Id = NEXT VALUE FOR [dbo].[PositionDescription_seq];

			-- INSERT Pdf clone FIRST, THEN Duties
			insert into PositionDescription (
				Id,
				PositionId,
				ProposedJobId,
				PositionObjective,
				WorkingTitle,
				LastUpdatedByUserId,
				LastUpdatedAt,
				LegacyStatus,
				IsLead,
				TopJobClassSupervised,
				TotalFteSupervised,
				AssignsWork,
				Instructs,
				ChecksWork,
				PlansWork,
				EvalsPerformance,
				Disciplines,
				Hires,
				Terminates,
				SupervisorLeadAddInfo,
				SupervisionLevel,
				SupervisionAddInfo,
				IsCoopCritical,
				CoopSupportFunctions,
				WorkSetting,
				WorkSchedule,
				TravelReqs,
				ToolsEquipment,
				CustomerInteraction,
				WorkingConditionsOther,
				QualificationsRequired,
				QualificationsPreferred,
				SpecialRequirements,
				InTrainingPlan,
				PositionTypeCXW,
				IsSupervisor,
				DirectFteSupervised,
				-- newly added:
				wxDecisionResponsibility,
				wxFinancialBudget,
				wxFinancialImpact,
				wxDecisionsUnauthorized,
				wxDecisionPolicyImpact,
				wAccountabilityScope,
				wDecisionExpertise,
				wDecisionsTacticalStrategic,
				wDecisionsRisk,
				xCurrentJobId,
				xMgmtCodePMC,
				--xProposedJvacEvalPts,
				wxProposedJvacCode,
				xExemptCitationHeading,
				xMarketSegment,
				--xCurrentJvacEvalPts,
				wxCurrentJvacCode,
				xCurrentBand,
				xProposedBand,
				ApprovalWorkflowId,
				CreatedByUserId
			)
			(select
				@Id,
				PositionId,
				ProposedJobId,
				PositionObjective,
				pd.WorkingTitle,
				@UserId as LastUpdatedByUserId,
				getdate() as LastUpdatedAt,
				'cloned from Description #' + cast(@PdId as varchar) as LegacyStatus,
				IsLead,
				TopJobClassSupervised,
				TotalFteSupervised,
				AssignsWork,
				Instructs,
				ChecksWork,
				PlansWork,
				EvalsPerformance,
				Disciplines,
				Hires,
				Terminates,
				SupervisorLeadAddInfo,
				SupervisionLevel,
				SupervisionAddInfo,
				IsCoopCritical,
				CoopSupportFunctions,
				WorkSetting,
				WorkSchedule,
				TravelReqs,
				ToolsEquipment,
				CustomerInteraction,
				WorkingConditionsOther,
				QualificationsRequired,
				QualificationsPreferred,
				SpecialRequirements,
				InTrainingPlan,
				PositionTypeCXW,
				pd.IsSupervisor,
				DirectFteSupervised,
				wxDecisionResponsibility,
				wxFinancialBudget,
				wxFinancialImpact,
				wxDecisionsUnauthorized,
				wxDecisionPolicyImpact,
				wAccountabilityScope,
				wDecisionExpertise,
				wDecisionsTacticalStrategic,
				wDecisionsRisk,
				-- use current Position job ID, if exists:
				isnull(p.JobId, xCurrentJobId),
				xMgmtCodePMC,
				wxProposedJvacCode,
--				xProposedJvacEvalPts,
				xExemptCitationHeading,
				xMarketSegment,
				--xCurrentJvacEvalPts,
				wxCurrentJvacCode,
				xCurrentBand,
				xProposedBand, 
				@WfId,
				@userId
			from PositionDescription pd
			left join Position p
			on pd.PositionId = p.Id
			where pd.Id = @PdId
			);

			if (select count(*) from PositionDuty
				where PositionDescriptionId = @PdId) = 0 begin
					-- Default Duty seed:
					insert into PositionDuty (PositionDescriptionId, Duty, TasksHtml, TimePercent)
					values (@Id, 'Default Duty', '<ul><li>Sample Task 1</li><li>Sample Task 2</li></ul>', 0);
			end
			else begin
			-- now clone duties:
			insert into PositionDuty (Id, PositionDescriptionId, Duty, TasksHtml, TimePercent)
			(	select
				NEXT VALUE FOR [dbo].[PositionDuty_seq],
				@Id,
				pd.Duty,
				pd.TasksHtml,
				pd.TimePercent
				from PositionDuty pd
				where pd.PositionDescriptionId = @PdId
			);
			end
		commit tran
		select pd.Id,
			ProposedJobId,
			jc.LegacyCode as ProposedClassCode,
			jc.JobTitle as ProposedClassTitle,
			LastUpdatedByuserId,
			WorkingTitle,
			LegacyStatus,
			LastUpdatedAt
			from PositionDescription pd
			-- DG changed to left join to allow cloning incomplete PDs
			left join JobClass jc on pd.ProposedJobId = jc.Id
			where pd.Id = @Id;
	end try
	begin catch
		rollback;
		declare @errmsg nvarchar(255);
		select @errmsg = ERROR_MESSAGE();
		raiserror('processing error: %s',11,1,@errmsg) with seterror;
		return;
	end catch
END