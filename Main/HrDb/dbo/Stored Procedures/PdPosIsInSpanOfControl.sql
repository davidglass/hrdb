﻿

-- =============================================
-- Author:		D. Glass
-- Create date: 2014-11-03
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[PdPosIsInSpanOfControl] 
	-- Add the parameters for the stored procedure here
	@PdId int, 
	@LanId varchar(50)
AS
BEGIN
	--declare @PdId int = 2112;
	--declare @LanId varchar(50) = 'DIS\JudyF'
	declare @PositionId int;
	declare @IsCreator bit;
	-- PD Creator can always view...
	select @PositionId = pd.PositionId,
	@IsCreator = case when u.LanId = @LanId
	then 1 else 0 end
	from PositionDescription pd
	left join AppUser u on pd.CreatedByUserId = u.Id
	where pd.Id = @PdId;

	select
	case when @IsCreator = 1 or exists (
		select 1 from AppUser u
		where u.LanId = @LanId
		and u.RoleId in (1,2) -- Admin, HR
	) then cast(1 as bit)
	else	
		case when (
			exists (
				select 1 from GetSpanOfControlWithDelegates(@LanId, 2) p -- 2: PDF app
				where p.Id = @PositionId
			)
			--or (select dbo.IsPosDelegated(
			--	2, -- PositionDesc
			--	@PositionId,
			--	@LanId,
			--	getdate() -- @AsOf
			--)) = 1
		)
		then cast(1 as bit)
		else cast(0 as bit)	
		end
	end as IsSub
END