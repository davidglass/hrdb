﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-04-22
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[AddPositionStructureSubnodes] 
	@SuperPosId int
AS
BEGIN
	declare @PosId int;
	insert into PositionStructure (Id, SupervisorPositionId, Ancestry)
	(
	select p.Id, @SuperPosId
	, psn.Ancestry + cast (row_number() over (
			order by
			-- non-vacants first
			case when exists (select 1 from appointment where PositionId = p.Id
				and (EndDate is null or EndDate > getdate())) then 1 else 2 end,
			-- list non-supervisors first
			--case when exists (select 1 from position where supervisorpositionid = p.Id)
			--	then 2 else 1 end,
			case when p.IsSupervisor = 1 then 2 else 1 end,
			isnull(ocu.OrgUnitName, ''),
			p.WorkingTitle
		) as binary(1)
	)
	from PositionStructure psn
	join Position p on psn.Id = p.SupervisorPositionId
	and psn.Id = @SuperPosId
	and p.IsAbolished = 0
	left join OrgChartOrgUnit ocu
	on p.OrgUnitId = ocu.OrgUnitId
	);

	-- now iterate over just-inserted subnodes:
	declare n_curs cursor local fast_forward read_only for
		select psn.Id
		from PositionStructure psn
		where SupervisorPositionId = @SuperPosId
		
	open n_curs;
	fetch next from n_curs into @PosId
	while @@fetch_status = 0 begin
		exec AddPositionStructureSubnodes @PosId;
		fetch next from n_curs into @PosId;
	end
	close n_curs;
	deallocate n_curs;
END