﻿-- =============================================
-- Author:		P. McKusick
-- Create date: 2014-10-09
-- Description:	truncate and reload leave data 
-- =============================================
CREATE PROCEDURE [dbo].[RefreshEmergencyContact] 
AS
BEGIN
	begin tran
		if (select count(*) from StageHRMS.dbo.EmergencyContact) > 0 begin
			truncate table hrms.EmergencyContact;
		
		insert into hrms.EmergencyContact
		
		select PersonId
		  ,EffectiveDate
		  ,EndDate
		  ,Member
		  ,LastName
		  ,FirstName
		  ,TelephoneNumber
		 from StageHRMS.dbo.EmergencyContact
		
		end
	commit;
END