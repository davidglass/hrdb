﻿
-- =============================================
-- Author:		D. Glass
-- Create date: 2013-05-01
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetChainOfCommandWithDirectReports] 
	-- Add the parameters for the stored procedure here
	@Id int -- PositionId
AS
BEGIN
-- TODO: move this to SSIS (clears legacy director "fake supervisor")
--update position 
--set supervisorpositionid = null
--where LegacyClassCode = 'EX011'; -- director
--declare @Id int = 5078; -- ppid = 5548, DIR pid = 5202
--declare @Id int = 5542; -- Anne K, good example with vacant sub and vacant supervisor
--declare @Id int = 5548;
-- declare @Id int = 5353; -- CSD AD (double-fill test)
--declare @Id int = 5430; -- HR Mgr pos, testing vacant MSD AD
with chain as (
	select Id, LegacyPositionNum, SupervisorPositionId,
		JobId, WorkingTitle, 1 as Level
	from Position
	where Id = @Id

	union all

	select sp.Id, sp.LegacyPositionNum, sp.SupervisorPositionId,
		sp.JobId, sp.WorkingTitle, p.Level + 1 as Level
	from position sp
	join chain p
	on p.SupervisorPositionId = sp.Id
)
select chain.Id,
chain.LegacyPositionNum, -- for union to direct reports
--	left(j.JobTitle, 40) as JobTitle,
-- this was to provide "short" title, but now including full, "pretentious" titles:
	--rtrim(case when chain.WorkingTitle is not null
	--	and len(chain.WorkingTitle) < len(j.JobTitle)
	--	then chain.WorkingTitle else left(j.JobTitle, 30) end) as JobTitle,
	isnull(chain.WorkingTitle, j.JobTitle) as JobTitle,
-- TODO: include date test here to exclude historical appointments
case when a.Id is null
	or (a.EndDate is not null and a.EndDate < getdate())
then cast(1 as bit) else cast(0 as bit) end as IsVacant,

	isnull(e.LastName, 
		(select fe.LastName -- Former Employee
		from Employee fe
		where fe.Id = (
			select EmployeeId
			from Appointment
			where PositionId = chain.Id
			and EndDate = (
				select max(EndDate)
				from Appointment
				where PositionId = chain.Id
			)
		))
	)
--isnull(e.LastName, 'VACANT')
+ case when (select count(*) from appointment
	where positionid = chain.Id and (enddate is null or enddate > getdate())
	) > 1 then
	' (PRIMARY)'
	else ''
	end 
as IncumbentLastName,
dbo.IsNullOrEmpty(e.PreferredName, e.FirstName) as IncumbentFirstName,
Level,
a.EmployeeId,
-- these nulls to allow union for direct reports
null as PdfId,
null as KeyCompId,
null as WmsId
from chain
--join hrms.Job j
join JobClass j
on chain.JobId = j.Id
left join Appointment a
on chain.Id = a.PositionId
and a.IsPrimary = 1
left join Employee e
on a.EmployeeId = e.Id
where (a.EndDate is null -- current appointments only
	or (
		a.EndDate = (
			select max(EndDate) from appointment
			where positionId = chain.Id
		)
		and not exists (
			select 1 from appointment
			where positionid = chain.id
			and enddate is null
		)
	)
)
-- add subordinates:
union
select p.Id,
p.LegacyPositionNum,
--	left(j.JobTitle, 40) as JobTitle,
	--rtrim(case when p.WorkingTitle is not null
	--	and len(p.WorkingTitle) < len(j.JobTitle)
	--	then p.WorkingTitle else left(j.JobTitle, 30) end) as JobTitle,
	isnull(p.WorkingTitle, j.JobTitle) as JobTitle,

--	rtrim(p.WorkingTitle) as JobTitle,
-- TODO: include date test here to exclude historical appointments
case when a.Id is null then cast(1 as bit) else cast(0 as bit) end as IsVacant,
	isnull(e.LastName, 
		(select fe.LastName
		from Employee fe
		where fe.Id = (
			select EmployeeId
			from Appointment
			where PositionId = p.Id
			and EndDate = (
				select max(EndDate)
				from Appointment
				where PositionId = p.Id
			)
		))
	)
	as IncumbentLastName,
	dbo.IsNullOrEmpty(e.PreferredName, e.FirstName) as IncumbentFirstName,
--e.LastName as IncumbentLastName,
0 as Level,
a.EmployeeId,
--pal.PdfId, -- replacing Legacy PdfId with new PdId:
pd.Id as PdfId,
isnull(pal.KeyCompId, 0) as KeyCompId,
pal.WmsId
from Position p
--join hrms.Job j
join JobClass j
on p.JobId = j.Id
left join Appointment a
on p.Id = a.PositionId
and a.IsPrimary = 1
and (a.EndDate is null or a.EndDate > getdate())
left join Employee e
on a.EmployeeId = e.Id
left join legacy.PositionAppLinkId pal
on p.Id = pal.PositionId
-- add in new PD-F:
left join PositionDescription pd
on pd.Id = (select max(pd.Id)
	from PositionDescription pd
	left join ApprovalWorkflow wf
	on pd.ApprovalWorkflowId = wf.Id
	-- include legacy PD-Fs for which there is no new Workflow:
	where pd.PositionId = p.Id
	and wf.Id is null or wf.StatusCodeId = 3 -- Approved
)
where p.SupervisorPositionId = @Id
and p.IsAbolished = 0
order by Level desc, IsVacant, JobTitle, IncumbentLastName

END