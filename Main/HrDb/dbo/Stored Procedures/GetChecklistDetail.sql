﻿-- =============================================
-- Author:		Pauline McKusick
-- Create date: 04/10/2014
-- Description:	Get Details for Checklist Display
-- =============================================
CREATE PROCEDURE [dbo].[GetChecklistDetail]
	
	@ParId int,
	@LanId varchar(50)
AS
BEGIN

		Select c.parid [ParId]
		,case when p.IsTransferIn = 1 then 'New Hire' 
		when p.IsNameChangeOnly = 1 then 'Name Change'
		when (p.IsTransferOut =  1 and p.ActionReasonId = 81) or p.ActionCode = 'U5' then 'Separation' 
		when p.actioncode = 'P1' then 'Transfer Position within Agency' 
		when (p.ActionCode = 'U5' and p.ActionReasonId = 16) then 'Did Not Start'  else 
		rtrim(ltrim(ac.Description))
		end 
		as [ParType]
		,case when p.actioncode = 'P1' then case when EmpA.EmployeeId is null then 'Vacant' else (isnull(ea.PreferredName, ea.FirstName) + ' ' + ea.LastName) end else (case when p.EmpPreferredName is null or p.EmpPreferredName = '' then p.EmpFirstName else p.EmpPreferredName end + ' ' + p.EmpLastName)  end as [Name]
		,p.EffectiveDate [EffectiveDate]
		,case when ap.EmployeeId is null then 'Vacant' else  (se.PreferredName + ' ' + se.LastName) end [Supervisor]
		,p.PosCostCenter [CostCenter1]
		,REPLACE(STR(p.PositionId, 4, 0), ' ', '0') [PositionNumber]
		,isnull(p.posworkingtitle, j.jobtitle) [Title]
		--,j.jobtitle [Title]
		,l.location [Location]
		,rtrim(ltrim(o.Description)) [Orgunit]
		,c.Cubicle
		,c.Comments
		,c.UserIdType
		,c.SimilarTo
		,case when c.SimilarTo > 0  then ausimilar.LanId else null end [SimilarToLanID]
		,c.ComputerEquip
		,c.StateDevice
		,c.VPNToken
		,c.ActiveSync
		,c.Broadband
		,c.Telecom
		,c.Scan
		,c.BuildingBadge
		,c.SDCBadge
		,c.Keys
		,c.CostCenter
		,c.NamePlate
		,c.BusinessCards
		,c.UpdateDt
		,case when au.EmployeeId is null then au.LanId else updatedinfo.PreferredName + ' ' + updatedinfo.lastname end [UpdateBy]
		,case when p.actioncode = 'P1' then c.CkEmpId else null end as [CkEmpId],
		c.UpdateADAccount,
		c.CubicleTo,
		dbo.GetParDisplayOuName(OrgUnitId) as OrgUnitShortName

		from Par p
		left join Employee e on e.Id = p.EmployeeId
		left join Checklist c on c.Parid = p.Id
		left join ActionCode ac on ac.Code = p.ActionCode
		left join ApptActionReason ar on ar.id = p.ActionReasonId
		left join Position po on po.Id = p.PosSupervisorPosId
		left join legacy.Location l on l.ID = p.PosLocationId
		left join orgunit o on o.Id = p.posorgunitid
		left join jobclass j on j.id = p.FillLevel
		left join Appointment ap on ap.PositionId = p.PosSupervisorPosId 
		--and ap.enddate is null
			and ((ap.enddate is null or ap.enddate > getdate()) and ap.StartDate <= getdate())
			left join Employee se ON ap.EmployeeId = se.Id -- get supervisor name from appointment supervisor position id
		left join appuser au on au.Id = c.UpdateBy
		left join employee updatedinfo on updatedinfo.id = au.employeeid
		left join Appointment EmpA on EmpA.PositionId = p.PositionId and ((EmpA.enddate is null or EmpA.enddate > getdate()) and EmpA.StartDate <= getdate() and p.EmployeeId = EmpA.EmployeeId)
				-- added the and p.EmployeeId = EmpA.EmployeeId to filter out other doublefills
		left join employee ea on ea.Id = EmpA.EmployeeId
		left join appuser ausimilar on ausimilar.EmployeeId = c.similarto
		
		--where p.id = 73

		where p.id = c.parid
		and c.Parid = @ParId

		order by p.id desc


END