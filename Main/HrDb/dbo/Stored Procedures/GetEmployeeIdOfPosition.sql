﻿-- =============================================
-- Author:		Mark Paul
-- Create date: 10/8/13
-- Description:	Get EmployeeId of Position. Checks Appointment table
-- =============================================
CREATE PROCEDURE [dbo].[GetEmployeeIdOfPosition]
	
	@positionId int
AS
BEGIN

	select top 1 EmployeeId [Id],
	Employee.PreferredName [PreferredName],
	Employee.LastName [LastName]
	
	 from appointment 
	 left outer join Employee on Employee.Id = EmployeeId
	 where PositionId = @positionId 
	and ((EndDate is null) or (EndDate > getdate()))
	
END