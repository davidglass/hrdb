﻿-- =============================================
-- Author:		D. Glass
-- Create date: 2013-04-24
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[GetHistoricalAppointments] 
AS
BEGIN
	select h.PersonId, cast(h.PositionNum as int) as PositionId,
	--h.FullName, h.FirstName, h.LastName,
	updated_date as StartDate,
	(select max(updated_date) from StageLegacy.organization.Employee_History
		where PersonId = h.PersonId
		and PositionNum = h.PositionNum
	) as EndDate
	from StageLegacy.organization.employee_history h
	where h.Id = (
		select min(Id) from StageLegacy.organization.employee_history h2
		where h2.PersonId = h.PersonId
		and h2.PositionNum = h.PositionNum
	)
	and left(h.PositionNum,2) <> 'UG'
	and h.LastName <> 'VACANT'
	and h.FirstName <> 'VACANT'
	and h.LastName <> ''
	and h.PositionNum <> '' -- see history Id 86198
	and not exists (
		select 1 from StageLegacy.organization.employee
		where PersonId = h.PersonId
		and PositionNum = h.PositionNum
	)
	order by startdate
END