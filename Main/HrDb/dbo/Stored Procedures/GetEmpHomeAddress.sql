﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetEmpHomeAddress] @EmpId int
AS
BEGIN
--declare @EmpId int = 5980;

select AddressType,
--case when AddressType = 1 then 'Permanent residence' 
--when AddressType = 5 then 'Mailing address' 
--when AddressType = 'US01' then 'Paycheck Location' 
--else '' end AddressType, 
StreetLine1, 
StreetLine2, 
City,
County as CountyId,
State, 
ZipCode,
HomePhone

--from StageHRMS.dbo.dishrms_Address a
from hrms.Addresses a
join Employee e on a.personid = e.HrmsPersonId
and e.Id = @EmpId
--and AddressType = 1
--left join StageLegacy.CLAO.WACounty cty
--on a.County = cty.CountyID;

END