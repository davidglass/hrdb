﻿

-- =============================================
-- Author:		D. Glass
-- Create date: 2013-06-04
-- Description:	
-- *NOTE*, this is identical to CreateNewHirePar
-- except for @ActionCode.  TODO: consolidate to CreatePar...?
-- =============================================
CREATE PROCEDURE [dbo].[CreateAppointmentChangePar]
	@LanId varchar(50),
	@PosId int
	--@EmpId int -- Employee is selected *after* creation for now...
AS
BEGIN
	declare @userId int;
	-- TODO: make GetUserId a scalar function instead of proc?
	exec @userId = GetUserId @UserName = @LanId;

	-- TODO: create sequences for emp and appt, use instead of identity...
declare @hrmsId int;
declare @jobId int;

select @hrmsId = HrmsPositionId, @jobId = JobId
from Position where id = @PosId;

insert into Par (
	PositionId,
	ActionCode,
	FillLevel,
	PosHrmsId,
	RequestedByUserId
	)
	values (
	@PosId,
	'U3',
	@jobId,
	@hrmsId,
	@userId
);
	select * from Par where Id = SCOPE_IDENTITY(); -- new ApptActionRequest with default values.
END