﻿
-- =============================================
-- Author:		Dave Glass
-- Create date: 2011-12-21
-- Description:	deletes NodeState record, interpreted as node collapse for user.
-- =============================================
CREATE PROCEDURE [dbo].[CollapseUserOrgUnitNode] 
	-- Add the parameters for the stored procedure here
	@NodeId int, 
	--@AppUserId int,
	@LanId varchar(50), 
	@IncludeSubNodes bit = 1 -- default to collapsing all subnodes until code can handle hidden expansions
AS
BEGIN
	declare @AppUserId int;
	exec @AppUserId = GetUserId @UserName = @LanId;

	delete from AppUser_ExpandedOrgNode
	where OrgUnitId = @NodeId and AppUserId = @AppUserId
	or (
		@IncludeSubNodes = 1
		and AppUserId = @AppUserId
		and OrgUnitId in (
		select sn.Id from OrgStructure n
			join OrgStructure sn
			on n.Id = @NodeId
			and left(sn.Ancestry, datalength(n.Ancestry)) = n.Ancestry
		)
	);
END