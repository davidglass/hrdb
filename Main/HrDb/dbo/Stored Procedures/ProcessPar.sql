﻿CREATE PROCEDURE [dbo].[ProcessPar] 
	@ParId int
AS
BEGIN

--declare @ParId int = 45;

declare @wfstatus varchar(50);
declare @parstatus int;
declare @actioncode char(2);
declare @positionid int;
declare @isNameChange bit;
declare @isnewappt bit;
declare @isxferin bit;
declare @isxferout bit;
declare @newappointmentid int;
declare @loa bit; -- leave of absence status
declare @currentappointmentid int;
declare @effectivedate datetime;
declare @DeactivateClassCode bit;

select
	@actioncode = ActionCode,
	@wfstatus = sc.Name,
	@parstatus = par.StatusId,
	@isnewappt = case when a.PositionId is null
		or par.PositionId <> a.PositionId
		or a.FillLevelJobId <> par.FillLevel
	then 1 else 0 end,
	@effectivedate = EffectiveDate,
	@currentappointmentid = a.Id,
	@positionid = par.PositionId,
	@loa = case when ActionCode in ('U8', 'U9') then 1
		when ActionCode = 'UA' then 0
		else isnull(a.IsOnLeaveOfAbsence, 0) end,
	@isxferin = IsTransferin,
	@isxferout = IsTransferOut,
	@DeactivateClassCode = DeactivateClassCode,
	@isNameChange = IsNameChangeOnly
from par
join ApprovalWorkflow wf
on par.ApprovalWorkflowId = wf.Id
join ApprovalStatusCode sc
on wf.StatusCodeId = sc.Id
-- this is to determine whether an Appointment Change
-- involves a new position
-- TODO: handle multi-fills
left join appointment a
on
--DG: trying to resolve Process failure when separating double-fill and wrong current appt is returned...
--(par.PositionId = a.PositionId
	--or par.EmployeeId = a.EmployeeId)
	-- TODO: test whether removing Position join above breaks anything???
	par.EmployeeId = a.EmployeeId
and (a.EndDate is null or a.EndDate > getdate())
where par.id = @parid;
-- TODO: ensure that no more than one Appointment with null EndDate exists per Employee.
--select @currentappointmentid

if (@wfstatus <> 'Approved') begin
	raiserror('PARs must be approved before processing',11,1) with seterror;
	return;
end;
if (@parstatus = 4) begin
	raiserror('PAR has already been processed',11,1) with seterror;
	return;
end;
else begin
	-- branch on partype...
	begin try
		begin tran
			if (@actioncode in ('U0', 'U6') or @isxferin=1 ) begin -- New Hire, Rehire
				insert into Employee (
					Id,
					HrmsPersonId,
					FirstName,
					PreferredName,
					MiddleName,
					LastName,
					Suffix,
					DOB,
					Gender,
					HireDate
				)
				select
					EmployeeId,
					EmpHrmsId, -- will be null for New Hires.
					EmpFirstName,
					EmpPreferredName,
					EmpMiddleName,
					EmpLastName,
					EmpSuffix,
					EmpDOB,
					EmpGender,
					EffectiveDate
				from Par
				where Id = @parid;
			end; -- end U0 New Hire
			if @actioncode in ('U0', 'U6') -- New Hire, Rehire
			-- TODO: add change in FillLevel to conditions for inserting new Appointment:
				or
				(@actioncode in ('U3', 'UJ') and @isnewappt =1)
				begin
				-- create new Appointment if change involves new Position or fill level:
				-- *also*, terminate existing appointment at StartDate of new appt.
				-- (else update existing Appointment):
				-- TODO: this part could be shared by all ActionTypes involving new appointments:
				insert into Appointment (
					PositionId,
					EmployeeId,
					StartDate,
					IsPrimary, -- TODO: default to IsPrimary only for currently vacant positions
					FillLevelJobId,
					ContractTypeCode,
					WorkScheduleCode,
					EmploymentPercent,
					ContractEndDate,
					TimeMgtStatus,
					PayScaleTypeArea,
					PayScaleReason,
					PayScaleEffectiveDate,
					StandbyPay,
					PayScaleGroup,
					PayScaleLevel,
					PayNextIncrease,
					StepMEligibleDate,
					PayCapUtilityLevel,
					PayAnnualSalary,
					PayHourlyRate,
					FillOtEligibilitySubgroup,
					ShiftDesignation,
					WorkHoursWk1,
					WorkHoursWk2
				)
				-- potential slight inefficiency using separate select for each insert...
				-- (could grab all in single select, cache in local vars)
				select PositionId,
					EmployeeId,
					EffectiveDate,
					case when exists (
						select 1 from appointment
						where positionid = par.PositionId
						and EndDate is null
					) then 0 else 1 end as IsPrimary, -- false if existing appointment
					FillLevel,
					ContractTypeCode,
					WorkScheduleCode,
					PercentFullTime,
					ContractEndDate,
					isnull(ApptTimeMgtStatus, '9'),
					ApptPayScaleTypeArea,
					ApptPayScaleReason,
					ApptPayScaleEffectiveDate,
					ApptStandbyPay,
					ApptPayScaleGroup,
					ApptPayScaleLevel,
					ApptPayNextIncrease,
					ApptStepMEligibleDate,
					ApptPayCapUtilityLevel,
					ApptPayAnnualSalary,
					ApptPayHourlyRate,
					ApptFillOtEligCode,
					ApptShiftDesignation,
					ApptWorkHoursWk1,
					ApptWorkHoursWk2
				from Par
				where Id = @parid;
				select @newappointmentid = SCOPE_IDENTITY();
			end; -- end U0 or U3+newappt
			-- terminate current appointment if existing employee is beginning new one:
			if (@actioncode in ('U3', 'UJ') and @isnewappt=1) begin
				update appointment
				set enddate = @effectivedate,
				-- TODO: cache Par properties into local vars at top, use here
				LastUpdatedDate = getdate()
				where id = @currentappointmentid;
			end
			-- Appt Change without new Appointment:
			-- *NOTE*, this does not update Position or Employee info.
			if (@actioncode in ('U3','UJ', 'U8', 'U9', 'UA') and @isnewappt=0) begin
				update a
				set
				a.ContractTypeCode = p.ContractTypeCode,
				a.WorkScheduleCode = p.WorkScheduleCode,
				a.EmploymentPercent = p.PercentFullTime,
				a.ContractEndDate = p.ContractEndDate,
				a.TimeMgtStatus = isnull(p.ApptTimeMgtStatus,'9'),
				a.PayScaleTypeArea = p.ApptPayScaleTypeArea,
				a.PayScaleReason = p.ApptPayScaleReason,
				a.PayScaleEffectiveDate = p.ApptPayScaleEffectiveDate,
				a.StandbyPay = p.ApptStandbyPay,
				a.PayScaleGroup = p.ApptPayScaleGroup,
				a.PayScaleLevel = p.ApptPayScaleLevel,
				a.PayNextIncrease = p.ApptPayNextIncrease,
				a.StepMEligibleDate = p.ApptStepMEligibleDate,
				a.PayCapUtilityLevel = p.ApptPayCapUtilityLevel,
				a.PayAnnualSalary =	p.ApptPayAnnualSalary,
				a.PayHourlyRate = p.ApptPayHourlyRate,
				a.ShiftDesignation = p.ApptShiftDesignation,
				a.WorkHoursWk1 = p.ApptWorkHoursWk1,
				a.WorkHoursWk2 = p.ApptWorkHoursWk2,
				a.LastUpdatedDate = getdate(),
				a.IsOnLeaveOfAbsence = @loa
				from Appointment a
				join Par p
				on p.Id = @ParId
				and a.id = @currentappointmentid
			end
			-- update employee name:
			if (@actioncode = 'U3' and @isNameChange = 1) begin
				update e
				set e.FirstName = p.EmpFirstName,
				e.LastName = p.EmpLastName,
				e.PreferredName = p.EmpPreferredName,
				e.MiddleName = p.EmpMiddleName,
				e.Suffix = p.EmpSuffix
				from Employee e join Par p
				on e.Id = p.EmployeeId
				and p.Id = @ParId
			end
			if (@actioncode = 'U5' or (@actioncode='U3' and @isxferout = 1)) begin
				update appointment
				set enddate = @effectivedate
				where id = @currentappointmentid
			end;

			if @actioncode = 'P0' begin -- New Position
				insert into Position (
					Id,
					OrgUnitId,
					CostCenter,
					JobId,
					WorkingTitle,
					SupervisorPositionId,
					CBA,
					ValidFromDate,
					IsInTraining,
					GeneralDescription,
					PersonnelSubArea,
					IsBackgroundCheckRequired,
					WorkersCompCode,
					IsSupervisor,
					IsManager,
					IsAppointingAuthority,
					EeGroup,
					EeSubGroup,
					IsSection4,
					PayGradeType,
					PayGradeArea,
					PayGrade,
					IsTandem,
					LocationId,
					DefaultWorkScheduleCode,
					PayGradeTypeArea,
					ShiftCode,
					--ShiftDesignation,
					Jvac,
					PointValue,
					IsRetireEligible
				)
				select
					PositionId,
					PosOrgUnitId,
					PosCostCenter,
					PosJobId,
					PosWorkingTitle,
					PosSupervisorPosId,
					case when PosPersonnelSubArea in ('00JZ', '00GW')
						then 1 else 0 end as CBA,
					--PosValidFrom, -- this would only be for updates...
					@effectivedate,
					PosIsInTraining,
					PosGeneralDescription,
					PosPersonnelSubArea,
					PosReqsBackgroundCheck,
					PosWorkersCompCode,
					PosIsSupervisor,
					PosIsManager,
					PosIsAppointingAuthority,
					PosEeGroup,
					PosEeSubgroup,
					PosIsSection4,
					pta.PsType,
					pta.PsArea,
					PosPayGrade,
					PosIsTandem,
					PosLocationId,
					PosWorkScheduleCode,
					PosPayGradeTypeArea,
					--PosShiftDesignation,
					PosShiftCode,
					PosJvac,
					PosPointValue,
					PosIsRetireEligible
				from Par
				join PayScaleTypeArea pta
				on par.PosPayGradeTypeArea = pta.Id
				where Par.Id = @ParId;
				-- need to run this to insert new pos in org view:
				-- TODO: run this only once nightly, after processing all inserts, etc.
				exec RebuildPosStructure;
			end
			else begin -- update position unless new
			-- TODO: check whether anything has changed before updating?
			-- this also updates Abolished positions for now...
			update p set 
				OrgUnitId = par.PosOrgUnitId,
				CostCenter = par.PosCostCenter,
				JobId = par.PosJobId,
				WorkingTitle = par.PosWorkingTitle,
				SupervisorPositionId = par.PosSupervisorPosId,
				CBA = case when par.PosPersonnelSubArea in ('00JZ', '00GW')
					then 1 else 0 end,
				ValidFromDate = Par.PosValidFrom,
				IsInTraining = par.PosIsInTraining,
				GeneralDescription = par.PosGeneralDescription,
				PersonnelSubArea = par.PosPersonnelSubArea,
				IsBackgroundCheckRequired = par.PosReqsBackgroundCheck,
				WorkersCompCode = par.PosWorkersCompCode,
				IsSupervisor = par.PosIsSupervisor,
				IsManager = par.PosIsManager,
				IsAppointingAuthority = par.PosIsAppointingAuthority,
				EeGroup = par.PosEeGroup,
				EeSubGroup = par.PosEeSubgroup,
				IsSection4 = par.PosIsSection4,
				PayGradeType = pta.PsType,
				PayGradeArea = pta.PsArea,
				PayGrade = par.PosPayGrade,
				IsTandem = par.PosIsTandem,
				LocationId = par.PosLocationId,
				DefaultWorkScheduleCode = isnull(par.PosWorkScheduleCode, 'FULL'),
				PayGradeTypeArea = par.PosPayGradeTypeArea,
				ShiftCode = par.PosShiftCode,
--				ShiftDesignation = par.PosShiftDesignation,
				Jvac = par.PosJvac,
				PointValue = par.PosPointValue,
				IsRetireEligible = par.PosIsRetireEligible
			from Position p
			join par
			on par.Id = @ParId
			and p.Id = par.PositionId
			join PayScaleTypeArea pta
			on par.PosPayGradeTypeArea = pta.Id
			end

			if (@actioncode = 'P2') begin -- Abolish Position
				update Position
				set
				-- TODO: allow future-scheduling abolishments.
				-- for now setting immediately on par process:
				IsAbolished = 1,
--				IsAbolished = case when @effectivedate <= getdate() then 1 else 0,
				ValidToDate = @effectivedate
				where Id = @positionid;
				if @DeactivateClassCode = 1 begin
					--update hrms.Job
					update JobClass
					set IsActive = 0
					where Id = (
						select JobId
						from Position
						where Id = @positionid
					)
				end
			end;

			update par
			set statusid = (select id from parstatus where description='Processed'),
			-- update appointmentid for appointment PARs (position PARs should stay null):
			appointmentid =
				case when @isnewappt = 1 then @newappointmentid
				else @currentappointmentid end,
			lastupdateddate = getdate(),
			processeddate = getdate()
			where id = @ParId;

		commit tran
		return;
	end try
	begin catch
		rollback;
		declare @errmsg nvarchar(255);
		select @errmsg = ERROR_MESSAGE();
		raiserror('processing error: %s',11,1,@errmsg) with seterror;
		return;
	end catch
end;

END