﻿CREATE TABLE [dbo].[Person] (
    [Id]            INT          NOT NULL,
    [EffectiveDate] DATE         NULL,
    [EndDate]       DATE         NULL,
    [LastName]      VARCHAR (40) NULL,
    [FirstName]     VARCHAR (40) NULL,
    [MiddleName]    VARCHAR (40) NULL,
    [Alias]         VARCHAR (40) NULL,
    [BirthDate]     DATE         NULL,
    [Gender]        CHAR (1)     NULL,
    [MaritalStatus] CHAR (1)     NULL,
    [SSN] CHAR(9) NULL, 
    CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HRMS Personnel Number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Person', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DESCRIPTION', @value = N'Holds all primary information of an employee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Person';

