﻿CREATE TABLE [dbo].[OrganizationalAssignment] (
    [PersonId]             INT       NOT NULL,
    [EffectiveDate]        DATE      NULL,
    [EndDate]              DATE      NULL,
    [PositionId]           INT       NOT NULL,
    [WorkContractTypeCode] CHAR (2)  NULL,
    [OrgKey]               CHAR (14) NULL,
    [JobId] INT NULL, 
    CONSTRAINT [PK_OrganizationalAssignment] PRIMARY KEY CLUSTERED ([PersonId] ASC, [PositionId] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cost Center', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'OrganizationalAssignment', @level2type = N'COLUMN', @level2name = N'OrgKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Integer value, LookUp table for value description in HRMS LkUpWorkContractType', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'OrganizationalAssignment', @level2type = N'COLUMN', @level2name = N'WorkContractTypeCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HRMS Personnel Number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'OrganizationalAssignment', @level2type = N'COLUMN', @level2name = N'PersonId';

