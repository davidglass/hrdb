﻿CREATE TABLE [dbo].[PersonEvent] (
    [PersonId]      INT      NOT NULL,
    [EffectiveDate] DATE     NULL,
    [EndDate]       DATE     NULL,
    [EventTypeCode] CHAR (2) NULL,
    [EventDate]     DATE     NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Number value of Type of Date, can have multiple types of dates, lookup table with values at HRMS_LkUpDateSpecification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PersonEvent', @level2type = N'COLUMN', @level2name = N'EventTypeCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HRMS Personnel Number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PersonEvent', @level2type = N'COLUMN', @level2name = N'PersonId';

