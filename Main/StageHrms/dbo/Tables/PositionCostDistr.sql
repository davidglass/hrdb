﻿CREATE TABLE [dbo].[PositionCostDistr] (
    [PositionId] INT            NULL,
    [CostCenter] CHAR (4)       NULL,
    [Percentage] DECIMAL (5, 2) CONSTRAINT [DF_HRMS_PositionCostDistr_Percentage] DEFAULT ((100.00)) NOT NULL
);

