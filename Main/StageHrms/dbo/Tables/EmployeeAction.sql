﻿CREATE TABLE [dbo].[EmployeeAction] (
    [PersonId]             INT      NOT NULL,
    [EffectiveDate]        DATE     NULL,
    [EndDate]              DATE     NULL,
    [ActionCode]           CHAR (2) NULL,
    [ReasonCode]           CHAR (2) NULL,
    [EmploymentStatusCode] CHAR (1) NULL,
    CONSTRAINT [ID] PRIMARY KEY CLUSTERED ([PersonId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HRMS Personnel Number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeAction', @level2type = N'COLUMN', @level2name = N'PersonId';

