﻿CREATE TABLE [dbo].[BasicPay] (
    [PersonId]                    INT             NOT NULL,
    [EffectiveDate]               DATE            NULL,
    [EndDate]                     DATE            NULL,
    [PayScaleReasonForChangeCode] CHAR (2)        NULL,
    [PayScaleType]                CHAR (2)        NULL,
    [PayScaleArea]                CHAR (2)        NULL,
    [PayScaleGroup]               VARCHAR (8)     NULL,
    [PayScaleLevel]               CHAR (2)        NULL,
    [AnnualSalary]                DECIMAL (12, 2) NULL,
    [PayCode]                     CHAR (4)        NULL,
    [SalaryRate]                  DECIMAL (7, 2)  NULL,
    CONSTRAINT [PK_BasicPay] PRIMARY KEY CLUSTERED ([PersonId] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HRMS Personnel Number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BasicPay', @level2type = N'COLUMN', @level2name = N'PersonId';

