﻿CREATE TABLE [dbo].[OrgUnit] (
    [Id]                   INT          NOT NULL,
    [PersonnelArea]        CHAR (4)     NULL,
    [OrgCodeLegacy]        VARCHAR (12) NULL,
    [OrgUnitDescription]   VARCHAR (40) NULL,
    [ShortName]            VARCHAR (15) NULL,
    [Name]                 VARCHAR (50) NULL,
    [RbsCode]              VARCHAR (25) NULL,
    [HrmsOrgUnitId]        INT          NULL,
    [WhenCreated]          DATETIME     NULL,
    [SupervisorPositionId] INT          NULL,
    CONSTRAINT [PK_HRMS_OrgUnit] PRIMARY KEY CLUSTERED ([Id] ASC)
);

