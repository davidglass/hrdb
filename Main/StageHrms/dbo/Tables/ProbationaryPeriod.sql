﻿CREATE TABLE [dbo].[ProbationaryPeriod] (
    [PersonId]               INT      NOT NULL,
    [EffectiveDate]          DATE     NULL,
    [EndDate]                DATE     NULL,
    [ContractTypePermStatus] CHAR (2) NULL,
    [Number]                 CHAR (4) NULL,
    [Unit]                   CHAR (3) NULL,
    CONSTRAINT [PK_HRMS_ProbationaryPeriod] PRIMARY KEY CLUSTERED ([PersonId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HRMS Personnel Number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProbationaryPeriod', @level2type = N'COLUMN', @level2name = N'PersonId';

