﻿CREATE TABLE [dbo].[EmergencyContact] (
    [PersonId]        INT       NULL,
    [EffectiveDate]   DATE      NULL,
    [EndDate]         DATE      NULL,
    [Member]          CHAR (4)  NULL,
    [LastName]        CHAR (40) NULL,
    [FirstName]       CHAR (40) NULL,
    [TelephoneNumber] CHAR (14) NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HRMS Personnel Number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmergencyContact', @level2type = N'COLUMN', @level2name = N'PersonId';

