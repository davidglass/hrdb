﻿CREATE TABLE [dbo].[EmployeeWorkSchedule] (
    [PersonId]             INT            NOT NULL,
    [EffectiveDate]        DATE           NULL,
    [EndDate]              DATE           NULL,
    [WorkSchedule]         VARCHAR (8)    NULL,
    [TimeManagementStatus] INT            NULL,
    [WorkingWeek]          CHAR (2)       NULL,
    [FullTimePercent]      DECIMAL (5, 2) NULL,
    [HoursWorkedDaily]     DECIMAL (5, 2) NULL,
    [HoursWorkedWeekly]    DECIMAL (5, 2) NULL,
    [HoursWorkedMonthly]   DECIMAL (5, 2) NULL,
    [HoursWorkedYearly]    DECIMAL (7, 2) NULL,
    [DaysWorkedWeekly]     DECIMAL (4, 2) NULL,
    CONSTRAINT [PK_EmployeeWorkSchedule] PRIMARY KEY CLUSTERED ([PersonId] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HRMS Personnel Number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeWorkSchedule', @level2type = N'COLUMN', @level2name = N'PersonId';

