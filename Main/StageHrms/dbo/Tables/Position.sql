﻿CREATE TABLE [dbo].[Position] (
    [Id]                   INT          NOT NULL,
    [AgencyCode]           CHAR (4)     NULL,
    [PositionTitleLeg]     CHAR (12)    NULL,
    [PositionTitle]        VARCHAR (40) NULL,
    [EffectiveDate]        DATE         NULL,
    [EndDate]              DATE         NULL,
    [JobId]                INT          NULL,
    [OrgUnitId]            INT          NULL,
    [SupervisorPositionId] INT          NULL,
    [EmployeeGroupCode]    CHAR (1)     NULL,
    [EmployeeSubGroupCode] CHAR (2)     NULL,
    [JobTitle]             VARCHAR (40) NULL,
    [JobTitleLegacy]       VARCHAR (12) NULL,
    [PayGradeType]         CHAR (2)     NULL,
    [PayGradeArea]         CHAR (2)     NULL,
    [PayGrade]             VARCHAR(8)     NULL,
    [PayGradeLevelFrom]    CHAR (2)     NULL,
    [PayGradeLevelTo]      CHAR (2)     NULL,
    [MinSalary]            NCHAR (14)   NULL,
    [MaxSalary]            NCHAR (14)   NULL,
    [PersonnelSubArea]     CHAR (4)     NULL,
    CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED ([Id] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The positions supervisor position number, chain of command, reporting relationship', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Position', @level2type = N'COLUMN', @level2name = N'SupervisorPositionId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Position Title Description, associated to class code, JOBID unique number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Position', @level2type = N'COLUMN', @level2name = N'PositionTitle';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Agency specific position number, this is created by agency not HRMS and associated to HRMS position number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Position', @level2type = N'COLUMN', @level2name = N'PositionTitleLeg';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Agency Number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Position', @level2type = N'COLUMN', @level2name = N'AgencyCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HRMS generated position number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Position', @level2type = N'COLUMN', @level2name = N'Id';

