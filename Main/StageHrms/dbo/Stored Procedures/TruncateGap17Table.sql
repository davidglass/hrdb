﻿
CREATE PROCEDURE [dbo].[TruncateGap17Table] 
AS
BEGIN
	declare @errmsg nvarchar(127);
	begin try
		begin tran
			truncate table OrgUnit;
		commit;
	end try
	begin catch
		select @errmsg = ERROR_MESSAGE();
		rollback tran;
		raiserror('truncate operation failed: %s',11,1,@errmsg) with seterror;
	end catch
END