﻿CREATE PROCEDURE [dbo].[TruncateGap10Tables] 
AS
BEGIN
	declare @errmsg nvarchar(127);
	begin try
		begin tran
			truncate table Position;
			truncate table PositionCostDistr
		commit;
	end try
	begin catch
		select @errmsg = ERROR_MESSAGE();
		rollback tran;
		raiserror('truncate operation failed: %s',11,1,@errmsg) with seterror;
	end catch
END