﻿CREATE PROCEDURE [dbo].[TruncateGap9Tables] 
AS
BEGIN
	declare @errmsg nvarchar(255);
	-- *NOTE*, single-user mode breaks SSIS,
	-- which needs metadata from a second channel.
	--alter database Organization set single_user with rollback immediate;
	begin try
		begin tran
			truncate table OrganizationalAssignment;
			truncate table EmployeeAction;
			truncate table [Address];
			truncate table EmployeeWorkSchedule;
			truncate table EmergencyContact;
			truncate table BasicPay;
			truncate table ProbationaryPeriod;
			truncate table PersonEvent;
			truncate table Person;
			truncate table Leave; -- GAP 8 data for Leave
		commit;
	end try
	begin catch
		select @errmsg = ERROR_MESSAGE();
		rollback tran;
		raiserror('truncate operation failed: %s',11,1,@errmsg) with seterror;
	end catch
END